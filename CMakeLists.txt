tdaq_package()

add_definitions(-DTDAQ_RELEASE_NAME=\"${TDAQ_PROJECT_NAME}\")

tdaq_generate_dal(databases/mda/schema/mda.schema.xml
  NAMESPACE daq::mda_dal
  INCLUDE mda_dal
  INCLUDE_DIRECTORIES coca dal
  CPP_OUTPUT dal_cpp_srcs
  JAVA_OUTPUT dal_java_srcs)

tdaq_add_schema(databases/mda/schema/mda.schema.xml)

tdaq_add_jar(mdadal DAL
  ${dal_java_srcs}
  INCLUDE_JARS coca/cocadal.jar DFConfiguration/DFdal.jar dal/dal.jar config/config.jar Jers/ers.jar TDAQExtJars/external.jar)

# FIXME: install schema, examples

tdaq_add_library(mda_dal DAL
  ${dal_cpp_srcs}
  LINK_LIBRARIES coca_dal tdaq-common::ers)

tdaq_add_library(mda_common
  src/common/*.cxx
  LINK_LIBRARIES CORAL::CoralCommon tdaq-common::ers)

tdaq_add_library(mda_archive
  src/archive/*.cxx
  LINK_LIBRARIES TTCInfo_ISINFO coca_client oh ohroot rc_ItemCtrl rc_OnlSvc mda_common mda_dal Boost::thread rt)

tdaq_root_generate_dictionary(mda_read_back
  mda/read_back/FileRead.h
  LINKDEF mda/read_back/LinkDef.h)

tdaq_add_pcm(mda_read_back)

tdaq_add_library(mda_read_back
  src/read_back/*.cxx
  mda_read_back.cpp
  LINK_LIBRARIES mda_common coca_client ROOT::RIO ROOT::Thread ROOT::Core)

tdaq_add_executable(mda
  src/apps/mda.cxx
  LINK_LIBRARIES mda_archive mda_common mda_dal cmdline)

tdaq_add_executable(mda_eor
  src/apps/mda_eor.cxx
  LINK_LIBRARIES mda_archive mda_common mda_dal pmgsync cmdline)

tdaq_add_executable(mda_get_info
  src/apps/mda_get_info.cxx
  LINK_LIBRARIES mda_read_back mda_common coca_common cmdline rt)

tdaq_add_executable(mda_stat_publish
  src/apps/stat_publish.cxx
  LINK_LIBRARIES mda_read_back mda_common coca_client coca_common coca_dal cmdline oh ohroot)

tdaq_add_executable(mda_store_versions
  src/apps/mda_store_versions.cxx
  LINK_LIBRARIES mda_read_back mda_common cmdline rt)

tdaq_add_executable(mda_set_archive
  src/apps/mda_set_archive.cxx
  LINK_LIBRARIES coca_client mda_common cmdline)

tdaq_add_executable(mda_is_playback
  src/apps/mda_is_playback.cxx
  LINK_LIBRARIES oh ohroot ROOT::RIO ROOT::Thread ROOT::Core Boost::program_options)

tdaq_add_executable(mda_test_store NOINSTALL
  src/test/mda_test_store.cxx
  LINK_LIBRARIES mda_archive mda_common mda_dal cmdline rt)

tdaq_add_executable(mda_test_flagnotify NOINSTALL
  src/test/mda_test_flagnotify.cxx
  LINK_LIBRARIES mda_archive mda_common mda_dal Boost::unit_test_framework)
add_test(NAME mda_test_flagnotify COMMAND mda_test_flagnotify)

tdaq_add_executable(mda_test_predicate NOINSTALL
  src/test/mda_test_predicate.cxx
  LINK_LIBRARIES mda_archive mda_common mda_dal Boost::unit_test_framework)
add_test(NAME mda_test_predicate COMMAND mda_test_predicate)

tdaq_add_executable(mda_test_normalize_name NOINSTALL
  src/test/mda_test_normalize_name.cxx
  LINK_LIBRARIES mda_archive mda_common mda_dal Boost::unit_test_framework)
add_test(NAME mda_test_normalize_name COMMAND mda_test_normalize_name)

tdaq_add_executable(mda_test_dbschema NOINSTALL
  src/test/mda_test_dbschema.cxx
  LINK_LIBRARIES mda_common Boost::unit_test_framework Boost::filesystem)
add_test(NAME mda_test_dbschema COMMAND mda_test_dbschema)

tdaq_add_executable(mda_test_fileread NOINSTALL
  src/test/mda_test_fileread.cxx
  LINK_LIBRARIES mda_read_back mda_common cmdline)

tdaq_add_executable(mda_run_params NOINSTALL
  src/test/run_params.cxx
  LINK_LIBRARIES mda_archive mda_common mda_dal cmdline is ipc)

tdaq_add_library(pymda MODULE
  src/python/pymda.cxx
  LINK_LIBRARIES mda_read_back mda_archive mda_common mda_dal Boost::python Python::Development)

tdaq_add_python_package(mda)

tdaq_add_python_files( python/mda/*.pyi python/mda/py.typed DESTINATION mda/ )

tdaq_add_scripts(scripts/mda_root_ls scripts/mda_register scripts/mda_dump_db
  scripts/mda_annotations scripts/mda_schema scripts/mda-cli)

tdaq_add_test(
  NAME mypy
  COMMAND tdaq_python -m mypy
  POST_INSTALL
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
