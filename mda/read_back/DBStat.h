#ifndef MDA_DBSTAT_H
#define MDA_DBSTAT_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/DBData.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/read_back/StatRun.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_read_back

/**
 *  @ingroup mda_read_back
 *
 *  @brief Class providing API for gathering statistics information from MDA database.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DBStat : private DBData {
public:

    /**
     *  @brief Constructor from a connection string.
     *   
     *  @param[in] connStr  Standard CORAL connection string.
     *  @param[in] prefix   Table name prefix, like "MDA3".
     */
    explicit DBStat (const std::string& connStr=DBData::defConnStr(), const std::string& prefix=DBData::defPrefix());

    // Destructor
    ~DBStat () throw ();

    /**
     *  @brief Retrieve the number of saved histograms per run for last several runs.
     *  
     *  @param[out]  stat    Returned statistics, key is run number.
     *  @param[in]  nRuns    Number of runs to include into statistics, only last nRuns. 
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void nHistoPerRun(std::map<unsigned, StatRun>& stat, int nRuns = 100);
  
    /**
     *  @brief Retrieve the number of saved histograms per OHS server.
     *  
     *  @param[out]  stat    Returned statistics, key is server name.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void nHistoPerServer(std::map<std::string, StatRun>& stat);
  
    /**
     *  @brief Retrieve the number of saved histograms per OHS provider.
     *  
     *  @param[out]  stat    Returned statistics, key is provider name.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void nHistoPerProvider(std::map<std::string, StatRun>& stat);
  
    /**
     *  @brief Retrieve the number of saved histograms per dataset.
     *  
     *  @param[out]  stat    Returned statistics, key is dataset name.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void nHistoPerDataset(std::map<std::string, StatRun>& stat);
  
protected:

private:

};

} // namespace mda
} // namespace daq

#endif // MDA_DBSTAT_H
