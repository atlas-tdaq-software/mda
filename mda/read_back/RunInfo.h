#ifndef MDA_RUNINFO_H
#define MDA_RUNINFO_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <time.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_read_back

/**
 *  @ingroup mda_read_back
 *
 *  @brief Class representing run-related information.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class RunInfo {
public:

    /**
     *  @brief Standard constructor
     *  
     *  @param[in] run     Run number
     *  @param[in] runType Run type
     *  @param[in] smk     Super Master Key for this run
     *  @param[in] time    Time when information was stored (not when run started or ended)
     */
    RunInfo (unsigned run, const std::string& runType, unsigned smk, time_t time) ;

    /// default constructor
    RunInfo() ;
    
    // Destructor
    ~RunInfo () throw() ;

    /// Returns run number
    unsigned run () const {return m_run;}
    
    /// Returns run type
    const std::string& runType () const {return m_runType;}
    
    /// Returns Super Master Key
    unsigned smk () const {return m_smk;}

    /// Returns time when information about run was stored
    time_t time () const {return m_time;}

    /// Compare to other object, used for sorting.
    bool operator< (const RunInfo& rhs) const;

protected:

private:

    // Data members
    unsigned m_run;        ///< Run number
    std::string m_runType; ///< Run type (string)
    unsigned m_smk;        ///< Super Master Key
    time_t m_time;         ///< Approximate time when this info was stored

};

} // namespace mda
} // namespace daq

#endif // MDA_RUNINFO_H
