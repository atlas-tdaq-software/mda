#ifndef MDA_STATRUN_H
#define MDA_STATRUN_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_read_back

/**
 *  @ingroup mda_read_back
 *
 *  @brief Class containing statistics collected for single run
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class StatRun  {
public:

    // Constructor
    StatRun (uint32_t nHist, uint32_t nHistVersions) 
        : m_nHist(nHist), m_nHistVersions(nHistVersions) {}
    
    /// Return number of histograms store for this run number
    uint32_t nHist() const { return m_nHist; }
    
    /// Return number of histogram versions stored for this run
    uint32_t nHistVersions() const { return m_nHistVersions; }
    
protected:

private:

    uint32_t m_nHist; 
    uint32_t m_nHistVersions;
  
};

/// Standard stream insertion operator
inline
std::ostream&
operator<<(std::ostream& str, const StatRun& stat)
{
  return str << "StatRun(nHist=" << stat.nHist()
      << ", versions=" << stat.nHistVersions() << ")" ;
}

} // namespace mda
} // namespace daq

#endif // MDA_STATRUN_H
