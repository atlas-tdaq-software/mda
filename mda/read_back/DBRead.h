#ifndef MDA_DBREAD_H
#define MDA_DBREAD_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <set>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/DBData.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/read_back/HistoFile.h"
#include "mda/read_back/RunInfo.h"
#include "mda/read_back/RunLB.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_read_back

/**
 *  @ingroup mda_read_back
 *
 *  @brief Class representing client API for MDA database.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class DBRead : private DBData {
public:

    /**
     *  @brief Constructor from a connection string.
     *   
     *  @param[in] connStr  Standard CORAL connection string.
     *  @param[in] prefix   Table name prefix, like "MDA3".
     */
    explicit DBRead (const std::string& connStr=DBData::defConnStr(), const std::string& prefix=DBData::defPrefix());

    // Destructor
    virtual ~DBRead () throw ();

    /**
     *  @brief  Get the list of partitions in the database.
     *  
     *  @param[out] partitions  Returned set of partition names
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void partitions (std::set<std::string>& partitions);

    /**
     *  @brief  Get the list of runs for a partition.
     *  
     *  Only the number of the last runs is returned, not a complete set of runs.
     *  
     *  @param[out] runs      Set of RunInfo objects returned
     *  @param[in]  partition Partition name
     *  @param[in]  nRuns     Number of runs to return
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void runs (std::set<RunInfo>& runs,
               const std::string& partition,
               unsigned int nRuns = 30);

    /**
     *  @brief Get the list of OH servers for a given partition/run
     *  
     *  @param[out] ohsServers  Returned set of OH server names.
     *  @param[in]  partition   Partition name.
     *  @param[in]  run         Run number.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void ohsServers (std::set<std::string>& ohsServers,
                     const std::string& partition,
                     unsigned run);

    /**
     *  @brief Get the list of providers for a given run/server
     *  
     *  @param[out] ohsProviders Returned set of OH provider names.
     *  @param[in]  partition    Partition name.
     *  @param[in]  run          Run number.
     *  @param[in]  ohsServer    OH server name.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void ohsProviders (std::set<std::string>& ohsProviders,
                       const std::string& partition,
                       unsigned run,
                       const std::string& ohsServer);

    /**
     *  @brief Get the list of histograms names for a given run/server/provider.
     * 
     *  All histograms below the given folder will be returned (recursive
     *  behavior), and the returned names do not include leading folder name.
     * 
     *  @param[out] histograms   Returned set of histogram names.
     *  @param[in]  partition    Partition name.
     *  @param[in]  run          Run number.
     *  @param[in]  ohsServer    OH server name.
     *  @param[in]  ohsProvider  OH provider name.
     *  @param[in]  folder       Folder name.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void histoNames (std::set<std::string>& histograms,
                     const std::string& partition,
                     unsigned run,
                     const std::string& ohsServer,
                     const std::string& ohsProvider,
                     const std::string& folder = "/");

    /**
     *  @brief Get the list of the histogram names and folders in the
     *  the given folder (one level only).
     *  
     *  @param[out] histograms   Returned set of histogram names.
     *  @param[out] folders      Returned set of sub-folder names.
     *  @param[in]  partition    Partition name.
     *  @param[in]  run          Run number.
     *  @param[in]  ohsServer    OH server name.
     *  @param[in]  ohsProvider  OH provider name.
     *  @param[in]  folder       Folder name.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void listdir (std::set<std::string>& folders,
                  std::set<std::string>& histograms,
                  const std::string& partition,
                  unsigned run,
                  const std::string& ohsServer,
                  const std::string& ohsProvider,
                  const std::string& folder = "/");

    /**
     *  @brief Get the list of the histograms versions (lumi blocks) and their
     *  corresponding locations files for a given histogram name.
     *  
     *  @param[out] histograms   Returned set of HistoFile objects.
     *  @param[in]  partition    Partition name.
     *  @param[in]  run          Run number.
     *  @param[in]  ohsServer    OH server name.
     *  @param[in]  ohsProvider  OH provider name.
     *  @param[in]  histogram    Histogram name (full path).
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void histogram (std::set<HistoFile>& histograms,
                    const std::string& partition,
                    unsigned run,
                    const std::string& ohsServer,
                    const std::string& ohsProvider,
                    const std::string& histogram);

    /**
     *  @brief Get the list of the histograms versions (lumi blocks) and their
     *  corresponding locations for a given folder.
     *
     *  @param[out] histograms   Returned set of HistoFile objects.
     *  @param[in]  partition    Partition name.
     *  @param[in]  run          Run number.
     *  @param[in]  ohsServer    OH server name.
     *  @param[in]  ohsProvider  OH provider name.
     *  @param[in]  folder       Folder name.
     *
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void histograms(std::set<HistoFile>& histograms,
                    const std::string& partition,
                    unsigned run,
                    const std::string& ohsServer,
                    const std::string& ohsProvider,
                    const std::string& folder);

    /**
     *  @brief Get all combinations of runs and lumi blocks for a given histogram.
     *  
     *  This method returns the list (or set) of all run/lb combinations
     *  (encoded into RunLB object) existing between 'since' and 'until' 
     *  runs inclusively.
     *  
     *  @param[out] runLBs       Returned set of RunLB objects.
     *  @param[in]  partition    Partition name.
     *  @param[in]  since        Starting run number.
     *  @param[in]  until        Ending run number.
     *  @param[in]  ohsServer    OH server name.
     *  @param[in]  ohsProvider  OH provider name.
     *  @param[in]  histogram    Histogram name (full path).
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void available (std::set<RunLB>& runLBs,
                    const std::string& partition,
                    unsigned since,
                    unsigned until,
                    const std::string& ohsServer,
                    const std::string& ohsProvider,
                    const std::string& histogram);

private:

    // Override session method to do one-time initialization
    coral::ISessionProxy& session() override;

    int m_schemaVersion = -1;
};

} // namespace mda
} // namespace daq

#endif // MDA_DBREAD_H
