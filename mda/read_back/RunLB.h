#ifndef MDA_RUNLB_H
#define MDA_RUNLB_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <time.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_read_back

/**
 *  @ingroup mda_read_back
 *
 *  @brief Class representing combination of run and lumi block numbers.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class RunLB  {
public:

    /**
     *  @brief Standard constructor
     *  
     *  @param[in] run   Run number
     *  @param[in] lb    Lumi block number
     *  @param[in] time  Approximate time when run info was created
     */
    RunLB (unsigned run, unsigned lb, time_t time)
        : m_run(run), m_lb(lb), m_time(time) {}

    /// Default constructor
    RunLB () : m_run(0), m_lb(0), m_time(0) {}

    // Destructor
    ~RunLB () throw() {}

    /// Returns run number
    unsigned run () const {return m_run;}
    
    /// Returns lumi block number
    unsigned lb () const {return m_lb;}
    
    /// Returns time when the run information was created
    time_t time () const {return m_time;}
    
    /// Compare two objects, may be used for ordering
    bool operator< (const RunLB& rhs) const ;

protected:

private:

    // Data members
    unsigned m_run;   ///< Run number
    unsigned m_lb;    ///< Lumi block number
    time_t m_time;    ///< Approximate time when this info was stored

};

} // namespace mda
} // namespace daq

#endif // MDA_RUNLB_H
