#ifndef MDA_HISTOFILE_H
#define MDA_HISTOFILE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_read_back

/**
 *  @ingroup mda_read_back
 *
 *  @brief Class representing one histogram with its corresponding file.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class HistoFile  {
public:

    /**
     *  @brief Standard constructor
     *  
     *  @param[in] histoName    Histogram name not including directories
     *  @param[in] lb           Lumi block number
     *  @param[in] dataset      Dataset name
     *  @param[in] archive      archive name
     *  @param[in] file         ROOT file name
     *  @param[in] histoPath    Full histogram path in ROOT file
     */
    HistoFile(const std::string& histoName,
              unsigned lb,
              const std::string& dataset,
              const std::string& archive,
              const std::string& file,
              const std::string& histoPath) ;
    
    /// Default constructor
    HistoFile();

    // Destructor
    ~HistoFile () throw() ;

    /// Returns histogram name
    const std::string& histoName () const {return m_histoName;}
    
    /// Returns lumi block number
    unsigned lb() const { return m_lb; }
    
    /// Returns dataset name
    const std::string& dataset () const {return m_dataset;}
    
    /// Returns archive name (may be the same as file())
    const std::string& archive () const {return m_archive;}
    
    /// Returns ROOT file name
    const std::string& file () const {return m_file;}
    
    /// Returns full histogram path name inside ROOT file
    const std::string& histoPath () const {return m_histoPath;}
    
    /// Compare two objects, may be used for sorting
    bool operator< (const HistoFile& rhs) const;
    
protected:

private:

    // Data members
    std::string m_histoName;    ///< Full histogram name
    unsigned m_lb;              ///< Lumi block number
    std::string m_dataset;      ///< Dataset name
    std::string m_archive;      ///< COCA archive name
    std::string m_file;         ///< ROOT file name
    std::string m_histoPath;    ///< complete path in a ROOT file

};

} // namespace mda
} // namespace daq

#endif // MDA_HISTOFILE_H
