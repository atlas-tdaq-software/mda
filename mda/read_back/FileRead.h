#ifndef MDA_FILEREAD_H
#define MDA_FILEREAD_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TFile.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace coca {
class DBClient;
}
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_read_back

/**
 *  @ingroup mda_read_back
 *
 *  @brief Class which provides access to ROOT files stored in CoCa by MDA.
 *  
 *  This class provides a user-friendly interface for CoCa which simplifies 
 *  the task of opening ROOT files stored by CoCa. This is a part of mda 
 *  package and not coca because coca does not have dependency on ROOT
 *  and I wanted to avoid bringing new dependency (ROOT especially) to that 
 *  package. MDA on the other hand produces ROOT files and this is a logical 
 *  pace to put code which reads back the same data.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class FileRead {
public:

    enum FindMode {
        CacheFirst,  ///< Tries to open file in CoCa cache first, or archived if not in cache
        ArchiveFirst  ///< Tries to open file in archive first, or in CoCa cache if not archived
    };
    
    /**
     *  @brief Make an instance which uses default CoCa connection string
     */
    FileRead () ;

    /**
     *  @brief Constructor takes CORAL connection string for CoCa database.
     *   
     *  @param[in] cocaConnStr CORAL connection string for CoCa database.
     */
    explicit FileRead (const std::string& cocaConnStr) ;

    // Destructor
    ~FileRead () throw () ;

    /**
     *  @brief Locate ROOT file and open it for reading.
     *  
     *  Tries to locate file at one of the possible location, either in CoCa 
     *  cache or archived. If file is found then it is opened and pointer is
     *  returned to client. If file is not found then zero pointer is returned.
     *  
     *  @param[in] fileName  Name of the ROOT file as stored in MDA database
     *  @param[in] dataset   CoCa dataset in which ROOT file is stored
     *  @param[in] mode      Location search mode, default is ArchiveFirst
     *
     *  @throw Issues::CoralException Thrown for databases-related conditions.
     */
    TFile* openFile(const std::string& fileName,
                    const std::string& dataset = "",
                    FindMode mode=ArchiveFirst);
    
protected:

private:

    // this could be std::unique_ptr, but ROOT5 has problems with that
    coca::DBClient* m_cocaClient; ///< pointer to CoCa client instance, owned by this instance

    // disable copy
    FileRead(const FileRead&);
    FileRead& operator=(const FileRead&);
};

} // namespace mda
} // namespace daq

#endif // MDA_FILEREAD_H
