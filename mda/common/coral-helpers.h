#ifndef MDA_CORAL_HELPERS_H
#define MDA_CORAL_HELPERS_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <RelationalAccess/IQuery.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 * 
 *  @brief Few helper functions to simplify CORAL coding.
 *  
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */
namespace coral_helpers {

    /**
     *  @brief Add an attribute to the CORAL attribute list and set its value.
     *  
     *  @param data   CORAL attribute list.
     *  @param name   Attribute name.
     *  @param value  Attribute value.
     */
    template <typename T>
    void
    addAttr(coral::AttributeList& data, const std::string& name, const T& value) {
        const size_t idx = data.size() ;
        data.extend<T>(name);
        data[idx].data<T>() = value;
    }

    /**
     *  @brief Set a value of the attribute in a CORAL attribute list.
     *  
     *  @param data   CORAL attribute list.
     *  @param name   Attribute name.
     *  @param value  Attribute value.
     */
    template <typename T>
    void
    setAttr(coral::AttributeList& data, const std::string& name, const T& value) {
        data[name].data<T>() = value;
    }

    /**
     *  @brief Define output variable for query.
     *  
     *  @param query  CORAL query object.
     *  @param name   Attribute name.
     */
    template <typename T>
    void
    defineOutput(coral::IQuery& query, const std::string& name) {
        query.addToOutputList(name) ;
        query.defineOutputType(name, coral::AttributeSpecification::typeNameForType<T>()) ;
    }

    /**
     *  @brief Define output variable for query.
     *  
     *  @param query  CORAL query object.
     *  @param expr   SQL expression.
     *  @param name   Alias for expression.
     */
    template <typename T>
    void
    defineOutput(coral::IQuery& query, const std::string& expr, const std::string& name) {
        query.addToOutputList(expr, name) ;
        query.defineOutputType(name, coral::AttributeSpecification::typeNameForType<T>()) ;
    }

} // namespace coral_helpers
} // namespace mda
} // namespace daq

#endif // MDA_CORAL_HELPERS_H
