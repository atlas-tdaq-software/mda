#ifndef MDA_ISSUES_H
#define MDA_ISSUES_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "ers/ers.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 */

    /**
     *  @namespace daq::mda::Issues
     *  @brief Collection of issue classes for mda package.
     *  
     *  The classes defined in this namespace would generally be useful for all mda
     *  sub-packages. For reader- or archive-specific issues define corresponding 
     *  classes in their sub-packages.
     *  
     *  @version $Id$
     *
     *  @author Andy Salnikov
     */

    /**
     *  @class Issues::Exception
     *  @brief Generic exception class, base for many exception classes in mda package.
     */
    ERS_DECLARE_ISSUE (Issues, Exception, , )

    /**
     *  @class Issues::CoralException
     *  @brief Generic CORAL exception, generated for CORAL-related issues.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, CoralException, Exception,
            "Generic CORAL exception", , )

    /**
     *  @class Issues::DBConnectionFailure
     *  @brief Exception generated when database connection fails.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, DBConnectionFailure, CoralException,
            "Database connection failed", , )

    /**
     *  @class Issues::TransactionException
     *  @brief Generic transaction-related error.
     */
    ERS_DECLARE_ISSUE_BASE(Issues, TransactionException, Exception, , , )

    /**
     *  @class Issues::TransactionActive
     *  @brief Exception generated when trying to start transaction when 
     *         transaction is already active.
     */
    ERS_DECLARE_ISSUE_BASE(Issues, TransactionActive, TransactionException,
            "cannot start transaction, another transaction is active", , )

    /**
     *  @class Issues::TransactionInactive
     *  @brief Exception generated when trying to stop transaction when 
     *         transaction is not active.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, TransactionInactive, TransactionException,
            "cannot close transaction, transaction already closed", , )

    /**
     *  @class Issues::RecordExists
     *  @brief Exception generated when trying to insert a new record when
     *         primary key already exists.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, RecordExists, Exception, , , )

    /**
     *  @class Issues::RecordExistsInt
     *  @brief Exception generated when trying to insert a new record when
     *         primary key already exists (PK type is integer).
     */
    ERS_DECLARE_ISSUE_BASE (Issues, RecordExistsInt, RecordExists,
            "record already exists with PK = " << PK, , ((long) PK))

    /**
     *  @class Issues::RecordExistsString
     *  @brief Exception generated when trying to insert a new record when
     *         primary key already exists (PK type is string).
     */
    ERS_DECLARE_ISSUE_BASE(Issues, RecordExistsString, RecordExists,
            "record already exists with PK = " << PK, , ((std::string) PK))

    /**
     *  @class Issues::NoSuchRecord
     *  @brief Exception generated for the missing record in the database when 
     *  the code expects it to be there.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, NoSuchRecord, Exception,
      "no such record: " << PK, , ((std::string) PK))

    /**
     *  @class Issues::SchemaException
     *  @brief Base class for schema-related exceptions.
     */
    ERS_DECLARE_ISSUE_BASE(Issues, SchemaException, Exception, , ,)

    /**
     *  @class Issues::SchemaTablesExistError
     *  @brief Exception generated when trying to make a schema when database
     *         already hase table with thesame prefix.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, SchemaTablesExistError, SchemaException,
            "Cannot initialize schema, there are already tables with prefix " << prefix,
            , ((std::string) prefix))

    /**
     *  @class Issues::MetaTableExistError
     *  @brief Exception generated when trying to make a META table
     *         when database already has one.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, MetaTableExistError, SchemaException,
            "Cannot make META table, table exists: " << table,
            , ((std::string) table))

    /**
     *  @class Issues::SchemaVersionRecordError
     *  @brief Exception generated when META table exists but VERSION record is invalid.
     */
    ERS_DECLARE_ISSUE_BASE (Issues, SchemaVersionRecordError, SchemaException,
            message, , ((std::string) message))

    /**
     *  @class Issues::Message
     *  @brief Generic message class, base for many message classes in mda package.
     */
    ERS_DECLARE_ISSUE (Issues, Message, , )


} // namespace mda
} // namespace daq

#endif // MDA_ISSUES_H
