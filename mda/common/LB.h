#ifndef MDA_LB_H
#define MDA_LB_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <iosfwd>
#include <stdint.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *
 *  @brief Class for Lumi Block numbers.
 *
 *  This class describes a Lumi Block which is an integer number with one
 *  special value (in the context of MDA) for the last lumi block.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class LB  {
public:

    /// constant used to signify last lumi block
    static const uint32_t LastLB = 0xFFFFFFFF;

    /**
     *  Format Lumi Block, will print it as a number with leading
     *  zeros or 'EoR' if LastLB.
     */
    static void print(std::ostream& out, uint32_t lb, int width = 0);

    /**
     *  Return the string formatted in same way as in previous method.
     */
    static std::string lb2string(uint32_t lb, int width = 0);
};

} // namespace mda
} // namespace daq

#endif // MDA_LB_H
