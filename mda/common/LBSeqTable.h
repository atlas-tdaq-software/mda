#ifndef MDA_LBSEQTABLE_H
#define MDA_LBSEQTABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <string>
#include <boost/utility.hpp>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/TableBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/NextIdGen.h"
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

class LBRangeTable;

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class representing LBSEQ table which is a description of 
 *  lumi block sequences.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see LBRangeTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class LBSeqTable : public TableBase {
public:

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     */
    LBSeqTable (const std::string& prefix, const std::string& tableName) ;
    
    // Destructor
    ~LBSeqTable () throw() {}

    /**
     * @brief Find a record in a table for given LB list or create a new record.
     * 
     * @param[in] lbList        Sequence of lumi blocks
     * @param[in] rangeTable    Table for LB ranges
     * @param[in] tran          Transaction context
     * @return              ID of the existing or new record
     * 
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t findOrInsert (const std::vector<unsigned>& lbList,
                           LBRangeTable& rangeTable,
                           Transaction& tran) ;

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

private:

    // Data members
    NextIdGen m_nextIdGen;           ///< ID generator for this table

};

} // namespace mda
} // namespace daq

#endif // MDA_LBSEQTABLE_H
