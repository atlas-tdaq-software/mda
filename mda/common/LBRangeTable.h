#ifndef MDA_LBRANGETABLE_H
#define MDA_LBRANGETABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>
#include <boost/utility.hpp>
#include <RelationalAccess/ISessionProxy.h>
#include <stdint.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/TableBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class representing LBRANGE table which is a description of 
 *  lumi block ranges.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see LBSeqTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class LBRangeTable : public TableBase {
public:

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     */
    LBRangeTable (const std::string& prefix,
                  const std::string& tableName) ;
    
    // Destructor
    ~LBRangeTable () throw() {}

    /**
     *  @brief Checks if the given list matches existing one.
     * 
     *  @param[in] lbSeqId    ID number from LBSEQ table
     *  @param[in] lbList     List of the lumi block numbers
     *  @param[in] tran       Transaction context.
     *  @return             True if the list is defined already.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    bool checkList (uint32_t lbSeqId, const std::vector<unsigned>& lbList, Transaction& tran);

    /**
     *  @brief Creates new LB list in the ranges table.
     * 
     *  @param[in] lbSeqId    ID number from LBSEQ table
     *  @param[in] lbList     List of the lumi block numbers
     *  @param[in] tran       Transaction context.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    void addList (uint32_t lbSeqId, const std::vector<unsigned>& lbList, Transaction& tran);

    /**
     *  @brief Extracts LB list from the ranges table.
     *  
     *  @param[in] lbSeqId    ID number from LBSEQ table
     *  @param[out] lbList    List of the lumi block numbers
     *  @param[in] tran       Transaction context.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    void getList (uint32_t lbSeqId, std::vector<unsigned>& lbList, Transaction& tran);

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

private:

};

} // namespace mda
} // namespace daq

#endif // MDA_LBRANGETABLE_H
