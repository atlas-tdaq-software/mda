#ifndef MDA_HGROUPHISTOTABLE_H
#define MDA_HGROUPHISTOTABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <boost/utility.hpp>
#include <RelationalAccess/ISessionProxy.h>
#include <stdint.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/TableBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class representing HGROUP_HISTO table which is a mapping of the 
 *  histo group ID to the list of histo IDs.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see HgroupTable
 *  @see HistoTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class HgroupHistoTable : public TableBase {
public:

    /// Mapping from histogram ID to lumi block sequence ID.
    typedef std::map<uint32_t, uint32_t> HistoIdLBListId ;

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     */
    HgroupHistoTable (const std::string& prefix, const std::string& tableName) ;
    
    // Destructor
    ~HgroupHistoTable () throw() {}

    /**
     *  @brief Add new mapping to a table.
     * 
     *  @param[in] hgroup_id     ID of the record in HGROUP table.
     *  @param[in] histo_ids     Set of the histogram IDS and their LB lists.
     *  @param[in] tran          Transaction context.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    void insert (uint32_t hgroup_id,
                 const HistoIdLBListId& histo_ids,
                 Transaction& tran) ;

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

private:

};

} // namespace mda
} // namespace daq

#endif // MDA_HGROUPHISTOTABLE_H
