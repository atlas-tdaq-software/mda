#ifndef MDA_METATABLE_H
#define MDA_METATABLE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <map>
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *
 *  @brief Class representing meta-information table.
 *
 *  META table is a table that keeps records describing MDA database
 *  structure, in particular its schema version. META records are simple
 *  key/value pairs, each key and value are arbitrary strings but key
 *  is unique. The key for schema version is "VERSION", the value is
 *  version number cionverted to string. META table was introduced with
 *  schema version 2, in version 1 the table did not exist (this is how
 *  schema version 1 can be identified).
 */

class MetaTable {
public:

    /**
     *  @brief Standard constructor.
     *
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     */
    MetaTable(const std::string& prefix, const std::string& tableName);

    /// Returns the name of the table
    const std::string& tableName() const { return  m_tableName; }

    /**
     *  Create database table.
     *
     *  The table must not exist prior to this call. This method also adds
     *  VERSION record to the table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

    /**
     *  Check META table existence.
     */
    bool tableExists(Transaction& tran);

    /**
     *  Check existence of a key in the META table.
     *
     *  If table does not exist then false is returned.
     */
    bool hasKey(const std::string& key, Transaction& tran);

    /**
     *  Return value corresponding to given key.
     *
     *  If table or key does not exist then defVal is returned.
     */
    std::string get(const std::string& key, Transaction& tran,
                    const std::string& defVal=std::string());

    /**
     *  Return all records from META table as mapping from key to value.
     *
     *  If table or key does not exist then empty map is returned.
     */
    std::map<std::string, std::string> getAll(Transaction& tran);

    /**
     *  Store new record in META table.
     *
     *  Table must exist prior to this call. If given key already exists in
     *  the table then its value will be replaced.
     */
    void put(const std::string& key, const std::string& value, Transaction& tran);

    /**
     *  Return recorded schema version. -1 is returned if table does not
     *  exist. Exception is raised when table exists but VERSION key is
     *  missing from table.
     *
     *  @throw Issues::SchemaVersionRecordError Thrown when META table exists
     *         but has no VERSION record.
     */
    int dbSchemaVersion(Transaction& tran);

protected:

private:

    const std::string m_tableName;         ///< Full table name
    const std::string m_columnName;
};

} // namespace mda
} // namespace daq

#endif // MDA_METATABLE_H
