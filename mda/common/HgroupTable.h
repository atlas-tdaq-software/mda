#ifndef MDA_HGROUPTABLE_H
#define MDA_HGROUPTABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/UniqueIdTable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class representing HGROUP table which is a description of 
 *  histogram groups.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see UniqueIdTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class HgroupTable : public UniqueIdTable {
public:

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     */
    HgroupTable(const std::string& prefix,
                const std::string& tableName) ;
    
    // Destructor
    ~HgroupTable () throw() {}

    /**
     *  @brief Finds existing record in the table or creates new record 
     *  with new ID.
     *  
     *  @param[in] hgroup_id    ID number from HGRPNAME table
     *  @param[in] file_id      ID number from FILE table
     *  @param[in] hserver_id   ID number from HSERVER table
     *  @param[in] provider_id  ID number from PROVIDER table
     *  @param[in] hcount       Number of histograms to be stored, for statistics only
     *  @param[in] vcount       Number of histogram versions to be stored, for statistics only
     *  @param[in] tr           Transaction context.
     *  @return             Record id, positive number.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t findOrInsert (uint32_t hgroup_id,
                           uint32_t file_id,
                           uint32_t hserver_id,
                           uint32_t provider_id,
                           uint32_t hcount,
                           uint32_t vcount,
                           Transaction& tr) ;

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

private:
    
    bool m_hasStatColumns;

};

} // namespace mda
} // namespace daq

#endif // MDA_HGROUPTABLE_H
