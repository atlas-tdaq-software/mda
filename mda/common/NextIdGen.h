#ifndef MDA_NEXTIDGEN_H
#define MDA_NEXTIDGEN_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <RelationalAccess/ISessionProxy.h>
#include <boost/utility.hpp>
#include <stdint.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class responsible for generating unique numeric values used as primary
 *  keys in MDA tables.
 *  
 *  The instance of this class generates sequences of unique IDs (positive numbers).
 *  There can be any number of sequences defined, each sequence is identified by 
 *  sequence name. The sequences must be created in advance during database setup.
 *  For example in ORACLE the command to create a sequence with the name HISTOID 
 *  and prefix MDA3 would be 
 *  "CREATE SEQUENCE MDA3_SEQ_HISTOID START WITH 1 [CACHE 100]".
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see UniqueIdTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class NextIdGen : boost::noncopyable {
public:

    /** 
     *  @brief Standard constructor.
     *  
     *  @param prefix      Table name prefix like "MDA3"
     *  @param name        Sequence name, e.g. "RUNLB"
     */
    NextIdGen(const std::string& prefix, const std::string& name) ;

    // Destructor
    ~NextIdGen () throw() {}

    /**
     *  @brief Return next ID. 
     *  
     *  The code musts executed in a context of a transaction so it takes 
     *  transaction context as an argument.
     *  
     *  @param[in] trans   Transaction context.
     *  @return   Unique ID, positive number.
     * 
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t next(Transaction& trans) ;

protected:

private:

    // Data members
    std::string m_seqName ;           ///< Specific sequence name.
    std::string m_queryStr ;          ///< Query string to get next ID.

};

} // namespace mda
} // namespace daq

#endif // MDA_NEXTIDGEN_H
