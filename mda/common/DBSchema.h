#ifndef MDA_DBSCHEMA_H
#define MDA_DBSCHEMA_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/DBData.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------


namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 * @ingroup mda_common
 *
 * @brief Class Implementing MDA database schema operations.
 *
 * For now it supports creating completely new schema (possibly with
 * current or older version) and querying schema properties. Schema
 * migration from one version to another is not supported by this
 * class as CORAL does not have portable tools for that so it needs
 * to be done in a backend-specific way.
 *
 * Note that MDA implementation relies on Oracle sequences for generation
 * of ID numbers (and thus cannot be used with any other database). CORAL
 * des not provide a way to create sequences, so thay need to be created
 * separately using Oracle tools. DBSchema class ahs a method top generate
 * SQL syntax for defining all sequences needed by MDA.
 */

class DBSchema : private DBData {
public:

    /**
     *  @brief Constructor from a connection string.
     *
     *  @param connStr  Standard CORAL connection string.
     *  @param prefix   Table name prefix, like "MDA3".
     *  @param update   If true opens connection in update mode.
     */
    explicit DBSchema(const std::string& connStr=DBData::defConnStr(),
                      const std::string& prefix=DBData::defPrefix(),
                      bool update=true);

    /**
     *  @brief Create completely new schema.
     *
     *  If schema already contains any tables with the same prefix then exception is raised.
     *
     *  @param version Version of the schema to create, -1 means use latest version.
     *  @param makeMetaTable If true then create metadata table ("MDA3_META").
     *
     *  @throw Issues::SchemaTablesExistError Thrown when database already has schema tables.
     */
    void makeSchema(int version=-1, bool makeMetaTable=true);

    /**
     *  @brief Create schema metadata table.
     *
     *  If table already exists then exception is generated.
     *
     *  @param version Version to write to meta table, -1 means use latest version.
     *
     *  @throw Issues::MetaTableExistError Thrown when database already has version table.
     */
    void makeMetaTable(int version=-1);

    /**
     *  @brief Generate SQL for creating Oracle sequences.
     *
     *  @param version Version of MDA database schema.
     */
    std::vector<std::string> makeSequncesSQL(int version=-1);

    /**
     *  Return table name for metadata, this is a fixed name but includes
     *  common prefix (e.g. "MDA3_").
     */
    std::string metaTableName() const;

    /**
     *  Return table names which are defined in a specified schema version.
     *
     *  @param version Version of MDA database schema, -1 means latest schema.
     */
    std::vector<std::string> tables(int version=-1);

    /**
     *  Return table names in MDA database which have matching prefix.
     */
    std::vector<std::string> dbTables();

    /**
     *  Check existence of schema metadata table.
     */
    bool hasMetaTable();

    /**
     *  Return all records from META table as mapping from key to value.
     *
     *  If table or key does not exist then empty map is returned.
     */
    std::map<std::string, std::string> getMeta();

    /**
     *  Add/update record in metadata table.
     */
    void setMeta(std::string const& key, std::string const& value);

    /**
     *  Return recorded schema version. -1 is returned is schema version table
     *  is missing.
     *
     *  @throw Issues::SchemaVersionRecordError Thrown when version table is empty
     *         or has more than one record.
     */
    int dbSchemaVersion();

    /**
     *  Return list of supported schema versions. This is what is implemented in
     *  software presently, but database schem could be different from any of
     *  these versions.
     */
    static std::vector<int> supportedSchemaVersions();

private:

    std::string m_prefix;

    static int s_latestVersion;
    static std::vector<int> s_supportedVersions;
};

} // namespace mda
} // namespace daq

#endif // MDA_DBSCHEMA_H
