#ifndef MDA_NAMEIDTABLE_H
#define MDA_NAMEIDTABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/UniqueIdTable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"
#include "mda/common/CacheMode.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class that represents a table providing one-to-one mapping between 
 *  strings and numeric IDs.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see UniqueIdTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class NameIdTable : public UniqueIdTable {
public:
    
    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     *  @param[in] idColName Name of the ID column.
     *  @param[in] nameColName Name of the name column.
     *  @param[in] cache    Data caching mode (see CacheMode::Mode).
     *  @param[in] cacheSize Max number of cached rows, negative means cache everything
     */
    NameIdTable(const std::string& prefix,
                const std::string& tableName,
                const std::string& idColName,
                const std::string& nameColName,
                CacheMode::Mode cache = CacheMode::NoCache,
                int cacheSize=-1) ;
    
    // Destructor
    ~NameIdTable () throw() {}

    /**
     *  @brief Finds existing value in the table or creates new record in a table
     *  with new primary key.
     *  
     *  @param[in] name     Name string
     *  @param[in] tran     Transaction context
     *  @param[out] newId   If non-zero then pointed variable will be set to true
     *                      when new record is created
     *  @return             Record id
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t findOrInsert (const std::string& name, Transaction& tran, bool* newId=0) ;

    /**
     *  @brief Finds existing record in the table.
     *
     *  @param[in] name     name string
     *  @param[in] tran     transaction context
     *  @return             0 on unsuccessful find, positive number otherwise
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t find (const std::string& name, Transaction& tran) ;

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

    // if cache mode is is OnStart then fill cache with data from database
    void initCache(Transaction& tr);

private:

    /// Typedef for cache map
    typedef std::map<std::string,uint32_t> CacheMap ;

    // Data members
    std::string m_idCol;          ///< Name of the ID column 
    std::string m_nameCol;        ///< Name of the name column
    mutable CacheMap m_cache;     ///< Cashed mappings
    CacheMode::Mode m_cacheMode;  ///< Caching mode
    int m_cacheSize;              ///< Max cache size
};

} // namespace mda
} // namespace daq

#endif // MDA_NAMEIDTABLE_H
