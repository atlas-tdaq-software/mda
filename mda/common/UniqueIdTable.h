#ifndef MDA_UNIQUEIDTABLE_H
#define MDA_UNIQUEIDTABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <CoralBase/AttributeList.h>
#include <RelationalAccess/ISessionProxy.h>
#include <RelationalAccess/ITable.h>
#include <boost/utility.hpp>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/TableBase.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/NextIdGen.h"
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Table with the numeric unique ID column (typically primary key).
 *  
 *  Class that represents table providing one-to-one mapping between
 *  primary key and the rest of the table columns. Every primary key
 *  maps to a unique set of the column values.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class UniqueIdTable : public TableBase {
public:

    /// Returns the name of the primary key column.
    const std::string& idColName() const { return  m_idColName; }

protected:
    
    /**
     *  @brief Standard constructor.
     *
     *  @param[in] prefix       table name prefix like "MDA3"
     *  @param[in] tableName    table name without prefix
     *  @param[in] idColName    name of the ID (PK) column
     */
    UniqueIdTable(const std::string& prefix,
                  const std::string& tableName,
                  const std::string& idColName) ;

    // Destructor
    ~UniqueIdTable () throw() {}

    /**
     *  @brief Finds existing values in the table or creates new record in a table
     *  with new primary key.
     *  
     *  @param[in] uniqueData   set of attributes with values for data columns
     *  @param[in] tran         transaction context
     *  @param[in] newId        if non-zero then pointed variable will be set to true
     *                          when new record is created
     *  @param[in] acond        if non-empty then used instead of standard WHERE clause
     *  @return  Primary key value, positive number.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t findOrInsert (const coral::AttributeList& uniqueData,
                           Transaction& tran,
                           bool* newId=0,
                           const std::string& acond=std::string()) ;

    /**
     *  @brief Finds existing values in the table.
     *
     *  @param uniqueData   set of attributes with values for data columns
     *  @param tran         transaction context
     *  @param acond        if non-empty then used instead of default WHERE clause
     *  @return             0 on unsuccessful find, positive number otherwise
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t find (const coral::AttributeList& uniqueData,
                   Transaction& tran,
                   const std::string& acond=std::string()) ;

    /**
     *  Returns CORAL table handle.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    coral::ITable& tableHandle(Transaction&);

private:

    // Data members
    const std::string m_idColName;   ///< Name of the ID (PK) column
    NextIdGen m_nextIdGen;           ///< Generator for unique IDs for this table.

};

} // namespace mda
} // namespace daq

#endif // MDA_UNIQUEIDTABLE_H
