#ifndef MDA_FILETABLE_H
#define MDA_FILETABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/UniqueIdTable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class representing FILE table which is a description of a file
 *  with the histograms.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see UniqueIdTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class FileTable : public UniqueIdTable {
public:

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     */
    FileTable(const std::string& prefix, const std::string& tableName) ;
    
    // Destructor
    ~FileTable () throw() {}
    
    /**
     *  @brief Finds existing record in the table or creates new record 
     *  with new ID.
     *  
     *  @param[in] runlb_id ID number from RUNLB table.
     *  @param[in] file     File name.
     *  @param[in] ds_id    ID number from dataset table.
     *  @param[in] arch_id  ID number from archive table.
     *  @param[in] tran     Transaction context.
     *  @return         Record id, positive number.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t findOrInsert (uint32_t runlb_id,
                           const std::string& file,
                           uint32_t ds_id,
                           uint32_t arch_id,
                           Transaction& tran) ;

    /**
     *  @brief Updates archive for matching file records.
     *
     *  @param[in] file     File name.
     *  @param[in] ds_id    ID number from dataset table.
     *  @param[in] arch_id  New ID number from archive table.
     *  @param[in] tran     Transaction context.
     *  @return         Number of changed rows.
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    long updateArchive(const std::string& file,
                       uint32_t ds_id,
                       uint32_t arch_id,
                       Transaction& tr);

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

private:

};

} // namespace mda
} // namespace daq

#endif // MDA_FILETABLE_H
