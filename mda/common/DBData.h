#ifndef MDA_DBDATA_H
#define MDA_DBDATA_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <boost/utility.hpp>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/FileTable.h"
#include "mda/common/HgroupTable.h"
#include "mda/common/HgroupHistoTable.h"
#include "mda/common/HistoTable.h"
#include "mda/common/LBRangeTable.h"
#include "mda/common/LBSeqTable.h"
#include "mda/common/NameIdTable.h"
#include "mda/common/RunLBTable.h"
#include "mda/common/MetaTable.h"
#include "mda/common/CacheMode.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *
 *  @brief Class providing access to data table in MDA database.
 *
 *  This class is a collection of the table classes for each table in 
 *  MDA database and provides a common implementation of the database 
 *  view. It is designed to be used through (private) inheritance, all
 *  useful methods and members are protected and can only be access via 
 *  subclassing.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class DBData : boost::noncopyable {
public:

    /**
     *  @brief Returns default prefix for MDA tables.
     *
     *  Value returned from this method can be used as a parameter to
     *  DBData constructor.
     */
    static const char* defPrefix();

    /**
     *  Returns default CORAL connection string for MDA database.
     */
    static std::string defConnStr();

    /// Constant used in place of archive name for files not registered with CoCa.
    static const std::string FileNotRegistered;

protected:

    /**
     *  @brief Standard constructor creates read-only instances.
     *   
     *  @param[in] session  CORAL session object.
     *  @param[in] prefix   Table name prefix, like "MDA3". See defPrefix().
     *  @param[in] largeTableCacheMode Caching mode for large tables.
     */
    DBData (const std::string& connStr,
            const std::string& prefix, 
            CacheMode::Mode largeTableCacheMode = CacheMode::OnStart);

    /**
     *  @brief Constructor which enables read-write access.
     *
     *  @param[in] session  CORAL session object.
     *  @param[in] prefix   Table name prefix, like "MDA3". See defPrefix().
     *  @param[in] update   If true then create modifiable instance.
     *  @param[in] largeTableCacheMode Caching mode for large tables.
     */
    DBData (const std::string& connStr,
            const std::string& prefix,
            bool update,
            CacheMode::Mode largeTableCacheMode = CacheMode::OnStart);

    // Destructor
    virtual ~DBData () throw ();
  
    /**
     *  @brief Create session object.
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    virtual coral::ISessionProxy& session();

    // Data members
    const std::string m_connStr;        ///< CORAL connection string
    bool m_update;                      ///< True for update session
    coral::ISessionProxy* m_session;    ///< CORAL session object
    NameIdTable m_partitionTbl;         ///< PART table object
    NameIdTable m_runtypeTbl;           ///< RUNTYPE table object
    NameIdTable m_datasetTbl;           ///< DATASET table object
    NameIdTable m_archiveTbl;           ///< ARCHIVE table object
    NameIdTable m_providerTbl;          ///< PROVIDER table object
    NameIdTable m_hserverTbl;           ///< HSERVER table object
    NameIdTable m_hgrpnameTbl;          ///< HGRPNAME table object
    NameIdTable m_hnameTbl;             ///< HNAME table object
    RunLBTable m_runlbTbl;              ///< RUNLB table object
    FileTable m_fileTbl;                ///< FILE table object
    NameIdTable m_folderTbl;            ///< FOLDER table object
    HistoTable m_histoTbl;              ///< HISTO table object
    HgroupTable m_hgroupTbl;            ///< HGROUP table object
    LBSeqTable m_lbSeqTbl;              ///< LBSEQ table object
    LBRangeTable m_lbRangeTbl;          ///< LBRANGE table object
    HgroupHistoTable m_hgroup2histoTbl; ///< HGRP2HIST table object
    MetaTable m_metaTbl;                ///< VERSION table

};

} // namespace mda
} // namespace daq

#endif // MDA_DBDATA_H
