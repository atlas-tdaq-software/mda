#ifndef MDA_TABLEBASE_H
#define MDA_TABLEBASE_H

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *
 *  @brief Base class for all table classes.
 *
 *  Encapsulates naming conventions for tables and indices.
 */

class TableBase {
public:

    TableBase(const std::string& prefix, const std::string& tableName)
        : m_prefix(prefix), m_tableName(prefix + "_" + tableName)
    {}

    /// Returns the name of the table, which includes prefix name.
    const std::string& tableName() const { return  m_tableName; }

protected:

    // Return full/prefixed name for a given table name.
    std::string PFX(const std::string& table) const {
        return m_prefix + "_" + table;
    }

    // Return full index name prefixing it with table name
    std::string TBL(const std::string& index) const {
        return m_tableName + "_" + index;
    }

private:

    const std::string m_prefix;      ///< Common table name prefix
    const std::string m_tableName;   ///< Full table name
};

} // namespace mda
} // namespace daq

#endif // MDA_TABLEBASE_H
