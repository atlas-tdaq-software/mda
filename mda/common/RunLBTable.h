#ifndef MDA_RUNLBTABLE_H
#define MDA_RUNLBTABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/UniqueIdTable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class representing RUNLB table which is a description of a run plus 
 *  lumi block.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see UniqueIdTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class RunLBTable : public UniqueIdTable {
public:

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     */
    RunLBTable(const std::string& prefix, const std::string& tableName) ;

    // Destructor
    ~RunLBTable () throw() {}

    /**
     *  @brief Finds existing record in the table or creates new record 
     *  with new ID.
     *  
     *  @param[in] run        Run number
     *  @param[in] lb         Lumi block number
     *  @param[in] smk        Super master key number
     *  @param[in] runtype_id ID number from runtype table
     *  @param[in] part_id    ID number from partition table
     *  @param[in] tran       Transaction context
     *  @return           Record id, positive number
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t findOrInsert (unsigned run,
                           unsigned lb,
                           unsigned smk,
                           uint32_t runtype_id,
                           uint32_t part_id,
                           Transaction& tran) ;

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

private:

};

} // namespace mda
} // namespace daq

#endif // MDA_RUNLBTABLE_H
