#ifndef MDA_CACHEMODE_H
#define MDA_CACHEMODE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------


//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *
 *  @brief Definition for values which control caching of the data from database.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class CacheMode  {
public:

    /// Enum values for different caching modes.
    enum Mode {
        NoCache,     ///< Do not cache any data
        Cache,       ///< Cache data that has been requested
        OnStart      ///< Read all data from database and cache them in advance
    };

};

} // namespace mda
} // namespace daq

#endif // MDA_CACHEMODE_H
