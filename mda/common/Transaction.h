#ifndef MDA_TRANSACTION_H
#define MDA_TRANSACTION_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/utility.hpp>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 * @ingroup mda_common
 * 
 * @brief Class representing a transaction context.
 * 
 *  Class that signifies open transaction. Code that must be executed
 *  inside transaction may require presence of the transaction as one of
 *  the arguments to the methods.
 *  
 *  This class also supports RAII idiom for managing transaction state.
 *  Transaction can be activated in constructor and closed in destructor
 *  if you pass corresponding options to the constructor. 
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Transaction : boost::noncopyable {
public:

    /**
     *  Transaction mode for creating new transaction.
     */
    enum Mode { 
        Read,    ///<  Transaction will be open in read mode.
        Update,  ///<  Transaction will be open in update mode.
        NoOpen   ///<  Transaction mode will not change.
    };
    
    /**
     *  Options for implicit close of the transaction in the destructor.
     */
    enum CloseMode { 
        Abort,    ///< Transaction will be aborted. 
        Commit,   ///< Transaction will be committed.
        NoClose   ///< Transaction mode will not be changed.
    };
    
    /**
     *  @brief Standard constructor
     *  
     *  @param session CORAL session object
     *  @param mode    Mode for opening transaction in constructor 
     *  @param cmode   Mode for closing transaction in destructor
     *  
     *  @throw Issues::TransactionActive  Thrown if the transaction is already active and mode is not NoOpen.
     *  @throw coral::Exception Thrown for other databases-related conditions.
     */
    explicit Transaction (coral::ISessionProxy& session, Mode mode=Read, CloseMode cmode=Commit);
    
    /**
     *  @brief Destructor.
     *  
     *  If transaction has not been closed already then depending on m_cmode value 
     *  it may close transaction and either commit or roll back the changes happened 
     *  during transaction.
     */
    ~Transaction () throw ();
    
    /**
     *  @brief Close the transaction.
     *  
     *  Commits all the changes which happened since transaction started.
     *  
     *  @throw Issues::TransactionInactive  Thrown if transaction is not currently active.
     *  @throw coral::Exception Thrown for other databases-related conditions.
     */
    void commit();
    
    /**
     *  @brief Close the transaction.
     *  
     *  Discards all the changes which happened since transaction started.
     *  
     *  @throw Issues::TransactionInactive  Thrown if transaction is not currently active.
     *  @throw coral::Exception Thrown for other databases-related conditions.
     */
    void rollback();
    
    /**
     *  @brief Get current transaction mode.
     *  
     *  @throw coral::Exception Thrown for other databases-related conditions.
     */
    Mode mode();

    /**
     *  @brief Get CORAL session object.
     */
    coral::ISessionProxy& session() const { return m_session; }

protected:

private:

    // Data members
    coral::ISessionProxy& m_session;  ///< CORAL session object.
    CloseMode m_cmode;             ///< What to do when transaction closed implicitly.

};

} // namespace mda
} // namespace daq

#endif // MDA_TRANSACTION_H
