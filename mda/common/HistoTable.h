#ifndef MDA_HISTOTABLE_H
#define MDA_HISTOTABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/UniqueIdTable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Transaction.h"
#include "mda/common/CacheMode.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_common

/**
 *  @ingroup mda_common
 *  
 *  @brief Class representing HISTO table which is a description of 
 *  the single histogram.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @see UniqueIdTable
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class HistoTable : public UniqueIdTable {
public:

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] prefix   Prefix for the table names, something like "MDA3".
     *  @param[in] tableName Name of the corresponding database table without prefix.
     *  @param[in] cache    Data caching mode (see CacheMode::Mode).
     *  @param[in] cacheSize Max number of cached rows, negative means cache everything
     */
    HistoTable (const std::string& prefix,
                const std::string& tableName,
                CacheMode::Mode cache = CacheMode::NoCache,
                int cacheSize=-1) ;
    
    // Destructor
    ~HistoTable () throw() {}

    /**
     *  @brief Finds existing record in the table or creates new record with new ID.
     *  
     *  @param hnam_id      ID number from HNAME table
     *  @param folder_id    ID number from FOLDER table
     *  @param tr           transaction context
     *  @param newId        if non-zero then pointed variable will be set to true
     *                      when new record is created
     *  @return             record id
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t findOrInsert (uint32_t hnam_id,
                           uint32_t folder_id,
                           Transaction& tr,
                           bool* newId=0) ;

    /**
     *  @brief Finds existing record in the table.
     *
     *  @param hnam_id      ID number from HNAME table
     *  @param folder_id    ID number from FOLDER table
     *  @param tr           transaction context
     *  @return             0 on unsuccessful find, positive number otherwise
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    uint32_t find (uint32_t hnam_id, 
                   uint32_t folder_id,
                   Transaction& tr) ;

    /**
     *  Create database table.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

protected:

    /**
     *  @brief if cache mode is is OnStart then fill cache with data from database
     *  
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    void initCache(Transaction& tr);

private:

    typedef std::map<uint64_t,uint32_t> CacheMap ;

    // Data members
    mutable CacheMap m_cache;      ///< Cached results
    CacheMode::Mode m_cacheMode;   ///< Caching mode
    int m_cacheSize;               ///< Max number of records to cache
};

} // namespace mda
} // namespace daq

#endif // MDA_HISTOTABLE_H
