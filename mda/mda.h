// $Id$

#ifndef MDA_MDA_H
#define MDA_MDA_H

#include "mda/read_back/DBRead.h"
#include "mda/read_back/FileRead.h"


/**
 *  @defgroup mda Package mda
 *
 *  @{
 */ 

/**
 *  @defgroup mda_common Package mda_common
 *
 *  @brief Code which is shared by mda_archive and mda_read_back packages.
 *  
 *  This package contains set of classes and namespaces which 
 *  is common to \ref mda_archive and \ref mda_read_back. It contains  
 *  database access classes and set of common data type definitions.
 */

/**
 *  @defgroup mda_archive Package mda_archive
 *
 *  @brief Code which is used to build MDA archiving applications.
 *  
 *  This package contains set of classes and namespaces which 
 *  are used to build MDA archiving applications (mda and mda_eor).
 */

/**
 *  @defgroup mda_read_back Package mda_read_back
 *
 *  @brief Client-side library for extracting MDA data.
 *  
 *  This package contains set of classes and namespaces which 
 *  together constitute MDA client library. There are two distinct 
 *  pieces in the package:
 *   - database access library which reads information about 
 *     archived histograms from MDA database (class DBRead)
 *   - data file access library which is a wrapper for CoCa 
 *     client library (class FileRead)
 */

/**
 *  @}
 */ 

#endif // MDA_MDA_H
