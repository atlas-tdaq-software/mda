#ifndef MDA_ARCHIVER_H
#define MDA_ARCHIVER_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/coca.h"
#include "mda/archive/ArchiveFile.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace mda {
class RunTypeSMK;
class Predicate;
}
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class that reads histograms from all OH servers and archives them.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class Archiver  {
public:

    /// Type for the collection of the ArchiveFile objects
    typedef std::vector<ArchiveFile> FileList;

    /**
     *  @brief Constructor
     *  
     *  @param[in] files        Set of associated files
     *  @param[in] topDir       Directory where filess will be located
     *  @param[in] cocaServer   Name of the CoCa server
     *  @param[in] cocaPartition Partition where CoCa server runs
     * 
     *  @throw ArchiveIssues::CannotMakeDir
     *  @throw ArchiveIssues::FilePrefixNotUnique
     *  @throw ArchiveIssues::CocaInitError
     */
    Archiver (const FileList& files,
              const std::string& topDir,
              const std::string& cocaServer,
              const std::string& cocaPartition = "initial");
    
    // Destructor
    ~Archiver () throw ();

    /**
     *  @brief Start reading histograms from OH servers.
     *  
     *  @param[in] cacheHistos  If true then reading will be done asynchronously
     *                          and histograms will be stored in local memory
     *  @param[in] runNumber    Current run number
     *  @param[in] lb           Current lumi block number
     */
    void readHistos (bool cacheHistos, unsigned runNumber, unsigned lb);

    /**
     *  @brief Store all histograms in files, register them with CoCa and MDA database
     *  
     *  @param[in] runType      Run type
     *  @param[in] runNumber    Current run number
     *  @param[in] lb           Current lumi block number
     *  @param[in] stopRequested Predicate for checking whether the stop was requested
     */
    void archive (const RunTypeSMK& runType,
                  unsigned runNumber,
                  unsigned lb,
                  const Predicate& stopRequested);

protected:

private:

    // Data members
    FileList m_mdaFiles;        ///< Set of files
    std::string m_topDir;       ///< Directory name where to store ROOT files
    std::string m_serverName;   ///< CoCa server name
    coca::Register m_cocaServer;///< CoCa server object
    
};

} // namespace mda
} // namespace daq

#endif // MDA_ARCHIVER_H
