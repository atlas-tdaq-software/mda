#ifndef MDA_PREDICATE_H
#define MDA_PREDICATE_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <boost/utility.hpp>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class that defines interface for all predicate classes.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class Predicate : boost::noncopyable {
public:
    
    /// Destructor
    virtual ~Predicate() throw()  {}
    
    /**
     *  @brief Predicate is a functor returning boolean value.
     *  
     *  This is the main and only extension point for this class, any
     *  subclass should implement it. 
     */
    virtual bool operator() () const = 0;
    
    /**
     *  @brief Wait indefinitely until predicate evaluates to true.
     *  
     *  @param[in] pollTime  Polling interval, default is half second.
     */
    void wait (const std::chrono::milliseconds& pollTime = std::chrono::milliseconds(500)) const;
    
    /**
     *  @brief Wait until predicate evaluates to true or timeout expires.
     *  
     *  @param[in] timeout   Timeout, max time to wait.
     *  @param[in] pollTime  Polling interval, default is half second.
     *  
     *  @return False if timeout expires, true otherwise.
     */
    bool timedWait (const std::chrono::milliseconds& timeout,
            const std::chrono::milliseconds& pollTime = std::chrono::milliseconds(500)) const;

};

} // namespace mda
} // namespace daq

#endif // MDA_PREDICATE_H
