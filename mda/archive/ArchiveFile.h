#ifndef MDA_ARCHIVEFILE_H
#define MDA_ARCHIVEFILE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>
#include <memory>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/coca.h"
#include "mda/archive/ArchiveHistoGroup.h"
#include "mda/archive/DBArchive.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace mda {
class Predicate;
class RunTypeSMK;
}
}

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class that archives histograms from a single file.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class ArchiveFile  {
public:

    /// Type for the list of histogram groups
    typedef std::vector<ArchiveHistoGroup> HistoGroupList;

    /**
     *  @brief Standard constructor with lots of arguments.
     *  
     *  @param[in] name         File name (an ID in OKS config)
     *  @param[in] histoGroups  List of histogram groups in a file.
     *  @param[in] filePrefix   Run-independent part of the file name.
     *  @param[in] frequencyLB  Interval between saving (in lumi blocks).
     *  @param[in] compression  Compression level for ROOT files.
     *  @param[in] dataset      CoCa dataset name.
     *  @param[in] prio         Priority number for CoCa.
     *  @param[in] dtc          Desired Time in Cache for CoCa.
     *  @param[in] deleteAfterSaving Delete data from OH server after receiving.
     *  @param[in] saveAnnotations If true then IS annotations will be stored in ROOT file.
     *  @param[in] dbArchive    Database access object.
     *  @param[in] partition    Partition name.
     */
    ArchiveFile (const std::string &name,
                 const HistoGroupList& histoGroups,
                 const std::string& filePrefix,
                 unsigned frequencyLB,
                 int compression,
                 const std::string& dataset,
                 int prio,
                 unsigned dtc,
                 bool deleteAfterSaving,
                 bool saveAnnotations,
                 const std::shared_ptr<DBArchive>& dbArchive,
                 const std::string& partition) ;
    
    // Destructor
    ~ArchiveFile () throw() ;

    /**
     *  @brief read all histograms if the time is right.
     *  
     *  Checks if there enough time passed since the last save operation
     *  (based on run/LB numbers and m_frequencyLB) and asks all its histogram
     *  groups to read their corresponding histograms from OH servers.
     *  
     *  @param[in] cacheHistos If true start immediate reading in a separate 
     *    thread to load all histos in memory.
     *  @param[in] runNumber Current run number
     *  @param[in] lb Current lumi block number
     *  @return True if there is anything to read, false otherwise
     */
    bool readHistos (bool cacheHistos, unsigned runNumber, unsigned lb);

    /**
     *  @brief Store all histograms to a file, register it to CoCa and 
     *         store info in database.
     *         
     *  @param[in] cocaServer   CoCa registration API.
     *  @param[in] topDir       Directory for ROOT files.
     *  @param[in] runType      Run type and Super Master Key in one object.
     *  @param[in] runNumber    Run number.
     *  @param[in] lb           Lumi block number.
     *  @param[in] stopRequested Predicate for checking whether the stop was requested
     *  @return True if there is anything to archive, false otherwise
     */
    bool archive (coca::Register& cocaServer,
                  const std::string& topDir,
                  const RunTypeSMK& runType,
                  unsigned runNumber,
                  unsigned lb,
                  const Predicate& stopRequested);

    /// Return run-independent part of the file name 
    const std::string& filePrefix () const { return m_filePrefix; }

    /// Return name of the file (ID from OKS config).
    const std::string& name () const { return m_name; }

protected:

private:

    // Data members
    std::string m_name;         ///< File name (an ID in OKS config)
    HistoGroupList m_histoGroups;       ///< List of histogram groups in a file
    std::string m_filePrefix;   ///< Run-independent part of the file name
    unsigned int m_frequencyLB; ///< Interval between saving (in lumi blocks)
    unsigned int m_lastTimeRun; ///< Run number when last saving was performed
    unsigned int m_lastTimeLB;  ///< LB number when last saving was performed
    bool m_ready2archive;       ///< If true then archiving can be performed
    int m_compression;          ///< Compression level for ROOT files 
    std::string m_dataset;      ///< CoCa dataset name
    int m_prio;                 ///< Priority number for CoCa
    unsigned int m_dtc;         ///< Desired Time in Cache for CoCa
    bool m_deleteAfterSaving;   ///< Delete data from OH server after receiving
    bool m_saveAnnotations;     ///< If true then IS annotations will be stored in ROOT
    std::shared_ptr<DBArchive> m_dbArchive; ///< Database access object
    std::string m_partition;    ///< Partition name

};

} // namespace mda
} // namespace daq

#endif // MDA_ARCHIVEFILE_H
