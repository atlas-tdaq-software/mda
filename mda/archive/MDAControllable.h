#ifndef MDA_MDACONTROLLABLE_H
#define MDA_MDACONTROLLABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <chrono>

//----------------------
// Base Class Headers --
//----------------------
#include "RunControl/Common/Controllable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/archive/ArchiveThread.h"
#include "mda/archive/Archiver.h"
#include "mda/common/LB.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace mda {

    /**
     *  @class MDAApplication::InitializationTimeout
     *  @brief Issue generated when spawned mda_eor process takes too long to respond.
     */
    ERS_DECLARE_ISSUE (MDAApplication, InitializationTimeout,
    "MDAApplication '" << name << "' did not yet finish reading histograms. "
    "This may cause loss of some histograms if partition will be immediately shutdown.",
    ((std::string) name))

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class that implements Controllable interface for MDA applications.
 *  
 *  Interaction between Run Control and MDA applications is done via
 *  the methods of this class. Specific methods get called for specific
 *  Run control transitions. MDAControllable implements the methods for 
 *  transitions which are of interes for MDA applications. At the en of 
 *  the run it checks the run duration and if it was long enough then it 
 *  spawns mda_eor process to do archiving. After spawning it waits
 *  until mda_eor reads all histograms from IS server (mda_oer signals
 *  this via special ync file) and reports back to run control that 
 *  life can continue.
 *  
 *  Additionally this class controls the thread which performs archiving 
 *  of histograms during the run. The thread (class ArchiveThread)
 *  is started in PrepareForRun transition and stopped in StopArchiving.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */
class MDAControllable : public daq::rc::Controllable {
public:

    /**
     *  @brief Creates instance of this class
     *  
     *  @param[in] name  Instance name
     */
    explicit MDAControllable (const std::string& name);
    
    // destructor
    virtual ~MDAControllable() throw ();
    
    /// Method called by Run Control on Configure
    virtual void configure(const daq::rc::TransitionCmd& cmd) override;

    /// Method called by Run Control on PrepareForRun
    virtual void prepareForRun(const daq::rc::TransitionCmd& cmd) override;

    /// Method called by Run Control on StopROIB
    virtual void stopROIB(const daq::rc::TransitionCmd& cmd) override;

    /// Method called by Run Control on StopArchiving
    virtual void stopArchiving(const daq::rc::TransitionCmd& cmd) override;

    /// Method called by Run Control on Unconfigure
    virtual void unconfigure(const daq::rc::TransitionCmd& cmd) override;

private:

    /**
     *  @brief Start mda_eor process.
     *
     *  @param[in] runType  Run type and Super Master Key
     *  @param[in] runNumber Run number
     *  @param[in] lb       LumiBlock number
     */
    void launchMdaEoR(const RunTypeSMK &runType, unsigned runNumber, uint32_t lb = LB::LastLB);

    /// Returns true if we need to start mda_eor
    bool doEoRArchiving() const;
    
    /// Waits until mda_eor finishes collecting data and starts saving them
    bool waitForArchiver();

    // data members
    std::string m_name;                 ///< Process/config name
    std::chrono::seconds m_minRunDuration;        ///< Minimum run duration to archive
    std::chrono::seconds m_archiverInitTimeout;   ///< Longest wait time for waitForArchiver()
    std::chrono::system_clock::time_point m_startTime;     ///< Run start time
    std::chrono::system_clock::time_point m_stopTime;     ///< Run stop time
    std::string m_synchFile;                      ///< Name of the synck file for mda_eor wait
    std::shared_ptr<Archiver> m_archiver;         ///< Archiver object
    std::unique_ptr<ArchiveThread> m_runThread;   ///< Thread that archives data during run
    unsigned m_runNumber;               ///< Current run number
    std::string m_runType;              ///< Current run type

};

} // namespace mda
} // namespace daq

#endif // MDA_MDACONTROLLABLE_H
