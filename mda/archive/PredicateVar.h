#ifndef MDA_PREDICATEVAR_H
#define MDA_PREDICATEVAR_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "mda/archive/Predicate.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class that defines predicates based on external variable.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

template <class Type>
class PredicateVar : public Predicate {
public:

  /**
   *  @brief  Constructor takes reference to a variable.
   *  
   *  The object stores the reference to a variable and not a copy
   *  of the variable. Lifetime of the variable must be at least as 
   *  long as lifetime of predicate object. 
   */ 
  explicit PredicateVar (const Type& var) : m_var(var) {}

  // Destructor
  virtual ~PredicateVar () throw() {}

  /// Feturns the value of the variable converted to bool.
  bool operator() () const { return bool(m_var); }

protected:

private:

  // Data members
  const Type &m_var;

};

} // namespace mda
} // namespace daq

#endif // MDA_PREDICATEVAR_H
