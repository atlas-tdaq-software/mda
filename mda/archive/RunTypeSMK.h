#ifndef MDA_RUNTYPESMK_H
#define MDA_RUNTYPESMK_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class representing a combination of run type and super master key.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class RunTypeSMK  {
public:

  // Default constructor
  explicit RunTypeSMK (const std::string& runType, unsigned smk = 0) ;

  // Destructor
  ~RunTypeSMK () throw ();

  /// returns run type string
  const std::string& runtype () const {return m_runType;}

  /// Returns Super Master Key value
  unsigned smk () const {return m_smk;}

protected:

private:

  // Data members
  std::string m_runType;    ///< Run type string
  unsigned m_smk;           ///< Super Master Key

};

} // namespace mda
} // namespace daq

#endif // MDA_RUNTYPESMK_H
