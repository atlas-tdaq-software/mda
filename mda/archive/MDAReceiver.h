#ifndef MDA_MDARECEIVER_H
#define MDA_MDARECEIVER_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "oh/OHRootReceiver.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/archive/RootFile.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class that receives histograms from OH and stores to ROOT file.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */
class MDAReceiver : public OHRootReceiver {
public:

    /**
     *  @brief Standard constructor.
     *
     *  @param[in] filename   Name for ROOT file
     *  @param[in] compression Compression level
     *  @param[in] deleteAfterSaving If true then delete histograms after storing them
     *  @param[in] runNumber  Current run number
     *  @param[in] storeAnnotations  If true then IS annotations will be stored in ROOT
     */
    MDAReceiver (const std::string & filename,
                 int compression,
                 bool deleteAfterSaving,
                 int runNumber,
                 bool storeAnnotations);

    // Destructor
    ~MDAReceiver () throw ();

    /// Defines top directory for the next arriving histograms
    void setTopDirectory (const std::string &OHSServer,
                          const std::string &Provider);

    /// Sets number of the versions to store in a file
    void setVersions(int versions) { this->m_versions = versions; }

    /// Returns latest tag (version) that has been stored
    int getLastTag() const { return m_lastTag; }

    /// Tells receiver there will be no more histograms
    void finish();

    /// Throws an exceptions if there was an error happened before.
    void throwIfError () const;

private:

    // typedef for annotations attached to histograms
    typedef std::vector<std::pair<std::string, std::string>> Annotations;

    void receive (OHRootHistogram &histo) override;

    void receive (std::vector<OHRootHistogram *> &histos) override;

    void receive(OHRootEfficiency & efficiency) override;

    void receive(std::vector<OHRootEfficiency*> &efficiencies) override;

    void receive (OHRootGraph &graph) override;

    void receive (std::vector<OHRootGraph *> &graphs) override;

    void receive (OHRootGraph2D &graph) override;

    void receive (std::vector<OHRootGraph2D *> &graphs) override;

    virtual bool removePolicy ();

    /// Archives the given object using its supplied tag (version).
    void archive (::TNamed *obj, int tag, const Annotations& annotations);

    // Data members
    RootFile m_file;            ///< ROOT file to save everything
    std::unique_ptr<RootFileIssues::Exception> m_error;   ///< Last ROOT error
    std::string m_topDir;       ///< Directory to store everything
    std::string m_runPrefix;    ///<
    bool m_deleteAfterSaving;   ///< If true then delete histograms after saving
    int m_versions;             ///< Number of versions to save
    std::string m_lastName;     ///< name of the last stored object
    int m_lastTag;              ///< Latest tag (version) saved
    bool m_storeAnnotations;    ///< If true then IS annotations will be stored in ROOT
    std::map<std::string, Annotations> m_annotations;  ///< per-histogram annotations

};

inline void
MDAReceiver::throwIfError () const
{
    if (m_error.get()) {
        ERS_DEBUG (0, *m_error);
        throw *m_error;
    }
}

} // namespace mda
} // namespace daq

#endif // MDA_MDARECEIVER_H
