#ifndef MDA_OHSPROVIDER_H
#define MDA_OHSPROVIDER_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class representing single OHS provider.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class OHSProvider  {
public:

    /**
     *  @brief Standard constructor.
     *  
     *  @param[in] server   Name of the OH server
     *  @param[in] provider Name of the histogramming provider
     */
    OHSProvider (const std::string& server, const std::string& provider) ;
    
    // Destructor
    ~OHSProvider () throw ();

    /// Returns server name
    const std::string& server () const { return m_server; }
    
    /// Returns provider name
    const std::string& provider () const { return m_provider; }
    
    /**
     *  @brief Compare two providers.
     *  
     *  @param[in] other reference to other provider object.
     */
    bool operator< (const OHSProvider& other) const ;
    
private:
    std::string m_server;
    std::string m_provider;

};

/// Standard stream insertion operator
std::ostream&
operator<<(std::ostream& out, const OHSProvider& provider);

} // namespace mda
} // namespace daq

#endif // MDA_OHSPROVIDER_H
