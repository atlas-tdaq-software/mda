#ifndef MDA_ARCHIVETHREAD_H
#define MDA_ARCHIVETHREAD_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <boost/thread.hpp>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace mda {
class Archiver;
class RunTypeSMK;
}
}

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace mda {

namespace detail {

/*
 *  Synchronized flag which allows thread to wait until flag value changes.
 *
 *  This class is only used internally by ArchiveThread class, it is exposed
 *  here only for testing purposes, there should not be other clients of this
 *  code.
 */
class FlagNotify {
public:
    FlagNotify(bool initVal=false) : m_flag(initVal) {}

    FlagNotify(const FlagNotify&) = delete;
    FlagNotify& operator=(const FlagNotify&) = delete;

    // return current flag value
    operator bool() const;

    // set flag value, returns old value
    bool set(bool value);

    /**
     *  If flag value is the same as value parameter return immediately,
     *  otherwise wait until its value changes.
     *
     *  Returns flag value, independent of whether timeout was reached or not.
     */
    bool wait_for(bool value, int millisec);

private:
    mutable boost::mutex m_mutex;
    boost::condition_variable m_cv;
    bool m_flag;
};

} // namespace detail


/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class implementing a thread which archives histograms during run.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class ArchiveThread {
public:

    /**
     *  @brief Standard constructor
     *
     *  All parameters are passed by reference and references to objects
     *  are stored in the thread body object. Lifetime of the arguments
     *  to the constructor must be longer than lifetime of this thread.
     *
     *  @param[in] archiver   Archiver object
     *  @param[in] runTypeSMK Run type and Super Master Key wrapped into one
     *  @param[in] runNumber  Current run number
     *
     *  @throw ArchiveIssues::ThreadStartException
     */
    ArchiveThread (const std::shared_ptr<Archiver>& archiver,
                   const RunTypeSMK& runTypeSMK,
                   unsigned runNumber) ;

    // Destructor
    ~ArchiveThread () throw();

private:

    /// Internal class defined in implementation file
    class ArchiveThreadBody;

    detail::FlagNotify m_stopFlag;  ///< must be declared prior to m_thread
    boost::thread m_thread; ///< thread object which runs the code
};

} // namespace mda
} // namespace daq

#endif // MDA_ARCHIVETHREAD_H
