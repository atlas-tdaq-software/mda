#ifndef MDA_DBARCHIVE_H
#define MDA_DBARCHIVE_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------
#include "mda/common/DBData.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace mda {
    class OHSProvider;
    class RunTypeSMK;
}
}

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Database API for archiving part of MDA.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class DBArchive : private DBData {
public:

    /// Type which maps histogram name to the list of LB numbers
    typedef std::map<std::string, std::vector<unsigned> > HistoLBList;
    
    /// Type which maps histo ID (in database) to the list of LB numbers
    typedef std::map<uint32_t, std::vector<unsigned> > HistoIdLBList ;

    /**
     *  @brief Constructor from connection string
     *  
     *  @param[in] connStr  CORAL connection string
     *  @param[in] prefix   Table name prefix, like "MDA3".
     */
    explicit DBArchive (const std::string& connStr, const std::string& prefix=DBData::defPrefix());

    // destructor
    virtual ~DBArchive () throw ();

    /**
     *  Return version number for database schema.
     */
    int dbSchemaVersion();

    /**
     *  @brief Store histograms from the list and return their IDs.
     *  
     *  @param[in] histoLBList List of histogram names and their corresponding LBs.
     *  @param[out] histoIdLBList  Returned list of historam IDs and their corresponding LBs.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void insertHistoList (const HistoLBList& histoLBList, HistoIdLBList& histoIdLBList);

    /**
     *  @brief Store histograms for a given partition/run/LB/etc.
     *  
     *  @param[in] partition    Partition name.
     *  @param[in] run          Run number.
     *  @param[in] lb           Lumi block number.
     *  @param[in] histoGroup   Histogram group name.
     *  @param[in] ohsProvider  Server and provider wrapped into single object.
     *  @param[in] runType      Run type and Super Master Key wrapped into single object.
     *  @param[in] histoIdLBList List of historam IDs and their corresponding LBs.
     *  @param[in] dataset      Dataset name.
     *  @param[in] file         File name.
     *  @param[in] archive      Archive name.
     *  
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     */
    void insertInterval (const std::string& partition,
                         unsigned run,
                         unsigned lb,
                         const std::string& histoGroup,
                         const OHSProvider& ohsProvider,
                         const RunTypeSMK& runType,
                         const HistoIdLBList& histoIdLBList,
                         const std::string& dataset,
                         const std::string& file,
                         const std::string& archive);

    /**
     *  @brief Update archive name for a given file name.
     *
     *  This method is useful when CoCa registration fails for some file and file is
     *  registered in MDA database with some non-name for archive. Using this method
     *  it is possible to change corresponding archive name at a later time after
     *  registering file in CoCa. In CoCa file name and dataset name must be unique,
     *  so this method takes dataset name and file name and a new archive name. If
     *  for some reason there is more than one matching record then all of them will
     *  be updated.
     *
     *  @param[in] dataset      Dataset name.
     *  @param[in] file         File name.
     *  @param[in] archive      New archive name.
     *
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     *  @throw Issues::NoSuchRecord Thrown if file name is not in the database.
     */
    void updateArchive(const std::string& dataset,
                       const std::string& file,
                       const std::string& archive);

private:

    // Override session method to do one-time initialization
    coral::ISessionProxy& session() override;

    int m_schemaVersion = -1;
};

} // namespace mda
} // namespace daq

#endif // MDA_DBARCHIVE_H
