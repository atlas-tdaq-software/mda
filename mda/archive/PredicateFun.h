#ifndef MDA_PREDICATEFUN_H
#define MDA_PREDICATEFUN_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "mda/archive/Predicate.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 *
 *  @brief Class that defines predicates based on external functions.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

template <class RetType>
class PredicateFun : public Predicate {
public:

    /// Function type
    typedef RetType (*FunType) ();

    /// Constructor takes pointer to function
    explicit PredicateFun (FunType fun) : m_fun(fun) {}
    
    // Destructor
    virtual ~PredicateFun () throw() {}

    /// Call the function and convert result to bool
    bool operator()() const { return bool((*m_fun)()); }
    
protected:

private:

    // Data members  
    FunType m_fun;

};

} // namespace mda
} // namespace daq

#endif // MDA_PREDICATEFUN_H
