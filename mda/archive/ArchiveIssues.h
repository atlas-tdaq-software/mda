#ifndef MDA_ARCHIVEISSUES_H
#define MDA_ARCHIVEISSUES_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda/common/Issues.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace mda {

/// @addtogroup mda_archive

/**
 *  @ingroup mda_archive
 */

    /**
     *  @namespace daq::mda::ArchiveIssues
     *  @brief Collection of issue classes for mda_archive package.
     *  
     *  @version $Id$
     *
     *  @author Andy Salnikov
     */



    /**
     *  @class ArchiveIssues::CannotMakeDir
     *  @brief Issue generated when archive fails to create directory
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, CannotMakeDir, Issues::Exception,
            "cannot create directory: " << dir, , ((std::string) dir))

    /**
     *  @class ArchiveIssues::RenameFailed
     *  @brief Issue generated when file rename operation fails
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, RenameFailed, Issues::Exception,
            "failed to rename file " << path1 << " to " << path2, ,
            ((std::string) path1)((std::string) path2))

    /**
     *  @class ArchiveIssues::FilePrefixNotUnique
     *  @brief Issue generated when configuration specifies non-unique file name
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, FilePrefixNotUnique, Issues::Exception,
     "mda_file `" << mda_file << "': file prefix already used: " << file_prefix,
     , ((std::string) mda_file) ((std::string) file_prefix))

    /**
     *  @class ArchiveIssues::CocaInitError
     *  @brief Issue generated when CoCa initialization fails
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, CocaInitError, Issues::Exception,
     "CoCa initialization failed for server `" << server << "in partition " << partition,
     , ((std::string) server) ((std::string) partition))

    /**
     *  @class ArchiveIssues::InterruptRequested
     *  @brief Base class for messages generated when archiving is interrupted
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, InterruptRequested, Issues::Message, , , )

    /**
     *  @class ArchiveIssues::IncompleteFile
     *  @brief Message generated when archiving is interrupted and file is incomplete.
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, IncompleteFile, InterruptRequested,
            "MDA was requested to interrupt archiving histograms;"
            " this file may be incomplete or corrupted: " << filename,
            , ((std::string) filename))

    /**
     *  @class ArchiveIssues::MissingFile
     *  @brief Message generated when archiving is interrupted and no file is created.
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, MissingFile, InterruptRequested,
            "MDA was requested to interrupt archiving histograms;"
            " this file will not be created: " << filename,
            , ((std::string) filename))

    /**
     *  @class ArchiveIssues::RegistrationFailed
     *  @brief Message generated when CoCa registration fails.
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, RegistrationFailed, Issues::Message,
            "cannot register file " << filename<< " to CoCa server",
            , ((std::string) filename))

    /**
     *  @class ArchiveIssues::HistoGroupArchive
     *  @brief Message generated when database registration fails for histogram group.
     */
    ERS_DECLARE_ISSUE_BASE(ArchiveIssues, HistoGroupArchive, Issues::Message,
            "database registration failed for histo group " << histo_group, , ((std::string) histo_group))
            
    /**
     *  @class ArchiveIssues::HistoFileArchive
     *  @brief Message generated when database registration fails.
     */
    ERS_DECLARE_ISSUE_BASE(ArchiveIssues, HistoFileArchive, Issues::Message,
            "database registration failed for file " << histo_file, , ((std::string) histo_file))

    /**
     *  @class ArchiveIssues::ThreadException
     *  @brief Exception thrown for thread library errors.
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, ThreadException, Issues::Exception, , , )

    /**
     *  @class ArchiveIssues::ThreadStartException
     *  @brief Exception thrown when new thread fails to start.
     */
    ERS_DECLARE_ISSUE_BASE (ArchiveIssues, ThreadStartException, ThreadException,
            "Failed to start archiving thread", , )

            
} // namespace mda
} // namespace daq

#endif // MDA_ARCHIVEISSUES_H
