#  Script for generating SQL to update runlb table with the correct run
#  timestamp info

from __future__ import print_function

import sys
import time
import calendar
from optparse import OptionParser


_pfx = 'MDA3'


# make table name
def T(name):
    return _pfx + '_' + name


def main(argv):

    global _pfx

    parser = OptionParser(usage='%prog [options]')
    parser.set_defaults(prefix=_pfx)
    parser.add_option('-p', dest='prefix', metavar='PREFIX',
                      help="specifies table name prefix, def: " + _pfx)

    (options, args) = parser.parse_args()

    # read stdin
    for line in sys.stdin.readlines():

        x = line.split('|')
        if len(x) < 4 or not x[2].strip().isdigit():
            continue

        run = int(x[2].strip())
        start_str = x[3].strip()
        dur_str = x[4].strip()

        # parse begin time
        t = time.strptime(start_str, "%Y-%b-%d %H:%M:%S")
        start_utc = calendar.timegm(t)
        end_utc = start_utc

        # parse duration
        if dur_str:
            h, m, s = map(int, dur_str.split(':'))
            end_utc += h * 3600 + m * 60 + s

        # format end time
        t = time.gmtime(end_utc)
        end_str = time.strftime("%Y-%m-%d %H:%M:%S", t)
        #print "run:%d start_str:%s dur_str:%s end_str:%s" % (run, start_str, dur_str, end_str)
        print("UPDATE %s_RUNLB SET TS_UTC=(TIMESTAMP '%s') WHERE RUN_NUMBER=%d;" %
              (options.prefix, end_str, run))


if __name__ == "__main__":
    sys.exit(main(sys.argv))
