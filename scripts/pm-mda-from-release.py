

import pm
import pm.project
from config.dal import module as dal_module

dal = dal_module('dal', 'daq/schema/core.schema.xml')
COCAdal = dal_module('COCAdal', 'daq/schema/coca.schema.xml', [dal])
MDAdal = dal_module('MDAdal', 'daq/schema/mda.schema.xml', [dal])

setup = pm.project.Project("daq/segments/setup.data.xml")
setup_initial = pm.project.Project("daq/segments/setup-initial.data.xml")
common_env = pm.project.Project("daq/segments/common-environment.data.xml")
daqrepo = pm.project.Project("daq/sw/repository.data.xml")

# ================== setup repository ============================
tag = dal.Tag('x86_64-slc6-gcc48-dbg')
# cocabin = dal.Binary('coca_server_test', BinaryName='coca_server')
# mdabin = dal.Binary('mda_test', BinaryName='mda')
# repo = dal.SW_Repository('Test', Name="Test",
#                          InstallationPath='/afs/cern.ch/user/s/salnikov/testarea/tdaq-05-03-00/installed',
#                          SW_Objects=[cocabin, mdabin],
#                          Tags=[tag],
#                          Uses=[daqrepo.getObject("SW_Repository", "Online")])
# cocabin.BelongsTo = repo
# mdabin.BelongsTo = repo
cocabin = daqrepo.getObject("Binary", "coca_server")
mdabin = daqrepo.getObject("Binary", "mda")

template = pm.project.Project('daq/sw/common-templates.data.xml')

# tnsadmin = dal.Variable('MY_TNS_ADMIN', Name='TNS_ADMIN',
#                         Value='/afs/cern.ch/user/s/salnikov/testarea/tdaq-nightly')

# lddebug = dal.Variable('LD_DEBUG', Name='LD_DEBUG', Value='all')

# ============ CoCa =======================
unixp = MDAdal.UnixProperties("COCATestUnixP", ProcessUmask=0x2)
ds = COCAdal.CoCaDataset('TestDataset', MinArchiveSize_MB=20, MaxArchiveSize_MB=2000)
dsq = COCAdal.CoCaDatasetQuota('TestDatasetQuota', Dataset=ds)
coca = COCAdal.CoCaServer('coca',
                          Database="oracle://INTR/ATLAS_COCA",
                          CacheDir="/var/tmp/COCA/cache",
                          CDRDir="/var/tmp/COCA/cdr",
                          CDRHost="localhost",
                          CDRDir_on_CDRHost="/var/tmp/COCA/cdr",
                          CASTOR_dir='/castor/cern.ch/atlas/P1commisioning/coca_test',
                          Program=cocabin,
                          Datasets=[dsq],
                          UnixProperties=unixp)

# RC app for segment
rcapp = dal.RunControlApplication('coca_ctrl',
                                  Program=daqrepo.getObject("Binary", "rc_controller"))

cocadebug = dal.Variable('COCA_CORAL_DEBUG', Name='COCA_CORAL_DEBUG', Value='1')
tnsadmin = dal.Variable('TNS_ADMIN', Name='TNS_ADMIN',
                        Value='/afs/cern.ch/atlas/offline/external/oracle/latest/admin')


# make CoCa segment
authpath = dal.Variable('MY_CORAL_AUTH_PATH',
                        Name='CORAL_AUTH_PATH',
                        Value='/afs/cern.ch/user/s/salnikov/testarea/tdaq-02-00-03')
coca_seg = dal.Segment('COCA_Segment',
                       IsControlledBy=rcapp,
                       Applications=[coca],
                       ProcessEnvironment=[authpath, cocadebug, tnsadmin])

# ====================== MDA =======================


# OHS provider
ohpbin = daqrepo.getObject("Binary", "oh_test_provider")
ohp_parm = ("-p env(TDAQ_PARTITION) -n env(TDAQ_APPLICATION_NAME) -s MDA-Histogramming"
            " -h /EXPERT/env(TDAQ_APPLICATION_NAME)/Test/Histogram")
ohp = dal.ResourceApplication('MDA-HistogramProvider',
                              Parameters=ohp_parm,
                              RestartParameters=ohp_parm,
                              Program=ohpbin,
                              InitTimeout=0)

# IS server
iss = dal.InfrastructureApplication('MDA-Histogramming',
                                    Parameters='-s ',
                                    RestartParameters='-s ',
                                    Program=daqrepo.getObject("Binary", "is_server"),
                                    RestartableDuringRun=1)


# histo group
hgroup = MDAdal.MDAHistoGroup('MDATestHistoGroup',
                              OHSServer='MDA-Histogramming',
                              Provider='.*',
                              Histograms='.*',
                              VersionsNumber=100)

# MDA file
mdafile = MDAdal.MDAFile('MDATestFile',
                         Frequency_LB=10,
                         CoCaPRIO=10,
                         CoCaDTC=86400,
                         CompressionLevel=1,
                         DeleteAfterSaving=0,
                         CoCaDataset=ds,
                         HistoGroups=[hgroup])

mdadebug = dal.Variable('MDA_CORAL_DEBUG', Name='MDA_CORAL_DEBUG', Value='1')

ersvars = [dal.Variable('DEF_WARNING_STREAM', Name='TDAQ_ERS_WARNING', Value='lstderr,mts'),
           dal.Variable('DEF_ERROR_STREAM', Name='TDAQ_ERS_ERROR', Value='lstderr,mts')]


mdaapp = MDAdal.MDAApplication('MDATestApp',
                               Database='oracle://INTR/ATLAS_MDA',
                               TopDir='/tmp/MDA',
                               CoCaPartition='part_mda_andy',
                               MinRunDuration_s=1,
                               ArchiverInitTimeout_s=20,
                               Program=mdabin,
                               UnixProperties=unixp,
                               Files=[mdafile],
                               CoCaServer=coca,
                               RestartableDuringRun=1)

# RC app for segment
rcapp = dal.RunControlApplication('mda_ctrl',
                                  Program=daqrepo.getObject("Binary", "rc_controller"),
                                  ControlsTTCPartitions=1)

mda_seg = dal.Segment('MDA_Segment',
                      IsControlledBy=rcapp,
                      Resources=[mdaapp, ohp],
                      Infrastructure=[iss],
                      ProcessEnvironment=[authpath, mdadebug, tnsadmin] + ersvars)

# Computer
host = 'pcslac02.cern.ch'
comp = dal.Computer(host, HW_Tag='x86_64-slc6')

# make partition object
part = dal.Partition('part_mda_andy',
                     OnlineInfrastructure=setup.getObject("OnlineSegment", "setup"),
                     Segments=[coca_seg, mda_seg],
                     DefaultTags=[tag],
                     Parameters=[common_env.getObject('VariableSet', 'CommonParameters')])

includes = ['daq/schema/core.schema.xml',
            'daq/schema/coca.schema.xml',
            'daq/schema/mda.schema.xml',
            "daq/sw/common-templates.data.xml",
            "daq/segments/setup.data.xml",
            "daq/sw/tags.data.xml",
            "daq/sw/repository.data.xml"
            ]
save_db = pm.project.Project(part.id + '.data.xml', includes)
save_db.addObjects([part, comp])
