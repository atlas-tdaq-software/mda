#!/usr/bin/env tdaq_python

#
#  PartitionMaker script to generate test partition for MDA
#

import argparse
from configparser import ConfigParser
import os
import sys

import pm
import pm.project
from config.dal import module as dal_module


# default values for some parameters

# host name where to run partition
_host_name = os.environ.get('HOSTNAME', '')

# Tag name
_tag_name = os.environ['CMTCONFIG']

DAL = dal_module('dal', 'daq/schema/core.schema.xml')


def main():
    """run PM to create partition for MIG
    """

    parser = argparse.ArgumentParser(description='Generate small test partition for MDA')
    parser.add_argument('-c', '--config', metavar='PATH', type=argparse.FileType(),
                        required=True, help='Configuration file')
    parser.add_argument('output_file', help='The name of the output XML file')
    args = parser.parse_args()

    output_file = args.output_file

    # parse config file
    config = ConfigParser(dict(Tag=_tag_name, Host=_host_name))
    config.read_file(args.config)

    includes = ['daq/schema/core.schema.xml']

    tag = config.get("DEFAULT", "Tag")
    try:
        tags = pm.project.Project("daq/sw/tags.data.xml")
        tag_dal = tags.getObject("Tag", tag)
        includes += ["daq/sw/tags.data.xml"]
    except RuntimeError:
        tag_dal = DAL.Tag(tag)

    # Computer
    comp = make_comp(config, includes)

    # make local SW_Repo
    local_repo = make_local_repo(config, tag_dal, includes)

    # IS server for histograms
    is_server = make_is_server(config, local_repo, includes)

    # CoCa app
    coca_app, coca_ds = make_coca_app(config, local_repo, includes)

    # MDA app
    mda_app = make_mda_app(config, local_repo, coca_app, coca_ds, includes)

    # OH provider
    oh_provider = make_oh_provider(config, local_repo, includes)

    # segment for all of them
    mda_seg = make_segment(
        config,
        infrastructure=[is_server],
        resources=[mda_app, oh_provider],
        apps=[coca_app],
        includes=includes
    )

    part = make_partition(config, [mda_seg], tag_dal, comp, includes)

    includes.sort()
    save_db = pm.project.Project(output_file, includes)
    save_db.addObjects([part])


def make_comp(config, includes):
    # Computer
    comp = None
    host = config.get("DEFAULT", "Host")
    if host:
        try:
            hosts = pm.project.Project("daq/hw/hosts.data.xml")
            host = hosts.getObject("Computer", host)
            comp = host
            includes += ["daq/hw/hosts.data.xml"]
        except RuntimeError:
            tag = config.get("DEFAULT", "Tag")
            hw_tag = '-'.join(tag.split('-')[:2])
            comp = DAL.Computer(host, HW_Tag=hw_tag)
    return comp


def make_local_repo(config, tag_dal, includes):
    """Make SW_Repository instance for local repo
    """

    repo_path = config.get("SW_Repository", "InstallationPath")
    if repo_path:
        repo = pm.project.Project('daq/sw/repository.data.xml')
        tag = tag_dal.HW_Tag + "-" + tag_dal.SW_Tag
        pp1 = DAL.SW_PackageVariable('PYTHONPATH-local', Name='PYTHONPATH', Suffix='share/lib/python')
        pp2 = DAL.SW_PackageVariable('PYTHONPATH-Local-SOLIB', Name='PYTHONPATH', Suffix=tag + '/lib')
        localrepo = DAL.SW_Repository(
            'Local-Repository',
            Name="Local software repository",
            InstallationPath=repo_path,
            SW_Objects=[],
            Tags=[tag_dal],
            Uses=[repo.getObject("SW_Repository", "Online")],
            ProcessEnvironment=[],
            AddProcessEnvironment=[pp1, pp2]
        )
        return localrepo


def make_env(env_string):
    env = []
    for varstr in env_string.strip().split():
        varname, sep, value = varstr.partition("=")
        env += [DAL.Variable("MDA-ENV-" + varname, Name=varname, Value=value)]
    return env


def get_binary(binary_name, local_repo, includes):
    """Get binary either from local repo of common repo,
    """
    if local_repo:
        local_binary_name = binary_name + "_local"
        binaries = [b for b in local_repo.SW_Objects if b.id == local_binary_name]
        if binaries:
            binary = binaries[0]
        else:
            binary = DAL.Binary(
                local_binary_name,
                BinaryName=binary_name,
                BelongsTo=local_repo
            )
            local_repo.SW_Objects.append(binary)
    else:
        repo_inc = 'daq/sw/repository.data.xml'
        includes += [repo_inc]
        repo = pm.project.Project(repo_inc)
        binary = repo.getObject("Binary", binary_name)
    return binary


def make_is_server(config, local_repo, includes):

    is_repo = local_repo if config.getboolean("ISServer", "UseLocalRepo") else None
    binary = get_binary("is_server", is_repo, includes)

    isparam = "-s -p ${TDAQ_PARTITION} -n env(TDAQ_APPLICATION_NAME) " \
              "-b ${TDAQ_BACKUP_PATH}/env(TDAQ_APPLICATION_NAME).backup"
    iss = DAL.InfrastructureApplication(
        config.get("ISServer", "Name"),
        Parameters=isparam,
        RestartParameters=isparam,
        Program=binary,
        RestartableDuringRun=1
    )
    return iss


def make_coca_app(config, local_repo, includes):

    MDAdal = dal_module('MDAdal', 'daq/schema/mda.schema.xml', [DAL])
    COCAdal = dal_module('COCAdal', 'daq/schema/coca.schema.xml', [DAL])
    includes += ['daq/schema/mda.schema.xml', 'daq/schema/coca.schema.xml']

    coca_repo = local_repo if config.getboolean("CoCaApp", "UseLocalRepo") else None
    cocabin = get_binary("coca_server", coca_repo, includes)

    # ============ CoCa =======================
    unixp = MDAdal.UnixProperties("COCATestUnixP", ProcessUmask=0x2)
    ds = COCAdal.CoCaDataset('TestDataset', MinArchiveSize_MB=20, MaxArchiveSize_MB=2000)
    dsq = COCAdal.CoCaDatasetQuota('TestDatasetQuota', Dataset=ds)
    coca = COCAdal.CoCaServer(
        config.get("CoCaApp", "Name"),
        Database=config.get("CoCaApp", "Database"),
        CacheDir=config.get("CoCaApp", "CacheDir"),
        CDRDir=config.get("CoCaApp", "CDRDir"),
        CDRHost=config.get("CoCaApp", "CDRHost"),
        CDRDir_on_CDRHost=config.get("CoCaApp", "CDRDir_on_CDRHost"),
        CASTOR_dir=config.get("CoCaApp", "CASTOR_dir"),
        Program=cocabin,
        Datasets=[dsq],
        UnixProperties=unixp
    )

    return coca, ds


def make_mda_app(config, local_repo, coca, coca_ds, includes):

    MDAdal = dal_module('MDAdal', 'daq/schema/mda.schema.xml', [DAL])
    includes += ['daq/schema/mda.schema.xml', 'daq/schema/coca.schema.xml']

    mda_repo = local_repo if config.getboolean("MDAApp", "UseLocalRepo") else None
    mdabin = get_binary("mda", mda_repo, includes)

    # histo group
    hgroup = MDAdal.MDAHistoGroup(
        'MDATestHistoGroup',
        OHSServer=config.get("ISServer", "Name"),
        Provider='.*',
        Histograms='.*',
        VersionsNumber=100
    )

    # MDA file
    mdafile = MDAdal.MDAFile(
        'MDATestFile',
        Frequency_LB=10,
        CoCaPRIO=10,
        CoCaDTC=86400,
        CompressionLevel=1,
        DeleteAfterSaving=0,
        CoCaDataset=coca_ds,
        HistoGroups=[hgroup]
    )

    env = make_env(config.get("MDAApp", "Environment"))
    unixp = MDAdal.UnixProperties("MDATestUnixP", ProcessUmask=0x2)
    mdaapp = MDAdal.MDAApplication(
        config.get("MDAApp", "Name"),
        InitTimeout=5,
        InterfaceName="rc/commander",
        Database=config.get("MDAApp", "Database"),
        TopDir=config.get("MDAApp", "TopDir"),
        CoCaPartition=config.get("Partition", "Name"),
        MinRunDuration_s=1,
        ArchiverInitTimeout_s=20,
        Program=mdabin,
        UnixProperties=unixp,
        Files=[mdafile],
        CoCaServer=coca,
        RestartableDuringRun=1,
        ProcessEnvironment=env
    )

    return mdaapp


def make_oh_provider(config, local_repo, includes):

    oh_repo = local_repo if config.getboolean("OHProviderApp", "UseLocalRepo") else None
    ohpbin = get_binary("oh_test_provider", oh_repo, includes)

    hname = config.get("OHProviderApp", "HistoName")
    is_server = config.get("ISServer", "Name")
    ohp_parm = "-p env(TDAQ_PARTITION) -n env(TDAQ_APPLICATION_NAME) -s {is_server} -h {hname}".format(
        **locals())

    ohp = DAL.ResourceApplication(
        config.get("OHProviderApp", "Name"),
        Parameters=ohp_parm,
        RestartParameters=ohp_parm,
        Program=ohpbin,
        InitTimeout=0
    )
    return ohp


def make_segment(config, resources=[], infrastructure=[], apps=[], includes=[]):

    repo = pm.project.Project('daq/sw/repository.data.xml')

    segment_name = config.get("MDASegment", "Name")
    rcapp = DAL.RunControlApplication(
        segment_name + "RCApp",
        InterfaceName="rc/commander",
        InitTimeout=30,
        Program=repo.getObject("Binary", "rc_controller")
    )
    env = make_env(config.get("MDASegment", "Environment"))
    segment = DAL.Segment(
        segment_name,
        IsControlledBy=rcapp,
        Applications=apps,
        Infrastructure=infrastructure,
        Resources=resources,
        ProcessEnvironment=env,
        Hosts=[]
    )
    return segment


def make_partition(config, segments, tag_dal, comp, includes):

    common_env = pm.project.Project("daq/segments/common-environment.data.xml")
    setup = pm.project.Project("daq/segments/setup.data.xml")
    includes += ["daq/segments/common-environment.data.xml",
                 "daq/segments/setup.data.xml"]

    # make partition object
    part = DAL.Partition(
        config.get("Partition", "Name"),
        OnlineInfrastructure=setup.getObject("OnlineSegment", "setup"),
        Segments=segments,
        DefaultHost=comp,
        DefaultTags=[tag_dal],
        LogRoot=config.get("Partition", "LogRoot"),
        WorkingDirectory=config.get("Partition", "WorkingDirectory"),
        ProcessEnvironment=[common_env.getObject('VariableSet', 'CommonEnvironment')],
        Parameters=[common_env.getObject('VariableSet', 'CommonParameters')]
    )

    return part


if __name__ == '__main__':
    sys.exit(main())
