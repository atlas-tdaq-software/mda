#!/bin/env tdaq_python
#
#  Script for copying the content of MDA2 database into MDA3
#

from __future__ import print_function

import sys
from optparse import OptionParser
import time
import calendar

from mda.import_coral import coral


_pfx = 'MDA3'


# make table name
def T(name):
    return _pfx + '_' + name


class HListTable:

    def __init__(self, schema, id2str):

        self.schema = schema
        self.id2str = id2str
        self.hlmap = {}

    def get_hl(self, hl_main, hl_add, hl_sub):

        hlist = self._get_hl(hl_main)
        if hl_add:
            hlist_add = self._get_hl(hl_add)
            hlist.extend(hlist_add)
        if hl_sub:
            for h in self._get_hl(hl_sub):
                try:
                    hlist.remove(h)
                except Exception:
                    print("warning: hl_sub=%d is not in hl_main=%d (%s)" % (hl_sub, hl_main, h))

        # check for duplicates
        hlistset = set(hlist)
        if len(hlistset) != len(hlist):
            print('warning: Histogram list has duplicates: main=%d add=%d sub=%d' %
                  (hl_main, hl_add, hl_sub))
            # it will fail later

        return hlist

    def _get_hl(self, list_id):

        hlist = self.hlmap.get(list_id, None)
        if hlist is not None:
            return hlist[:]

        self.read_lists(list_id, list_id)

        return self.hlmap.setdefault(list_id, [])[:]

    def read_lists(self, list_id_min, list_id_max):

        print("  Reading HISTO_LISTS with ids %d-%d" % (list_id_min, list_id_max))

        t_hl = self.schema.tableHandle('MDA2_HISTO_LISTS')
        q = t_hl.newQuery()
        conddata = coral.AttributeList()
        q.addToOutputList('PATH_COMP_0')
        q.addToOutputList('PATH_COMP_1')
        q.addToOutputList('PATH_COMP_2')
        q.addToOutputList('PATH_COMP_3')
        q.addToOutputList('PATH_COMP_4')
        q.addToOutputList('PATH_COMP_5')
        q.addToOutputList('PATH_COMP_6')
        q.addToOutputList('PATH_COMP_7')
        q.addToOutputList('PATH_LAST')
        q.addToOutputList('LIST_ID')
        conddata = coral.AttributeList()
        conddata.extend("list_id_min", "int")
        conddata.extend("list_id_max", "int")
        conddata[0].setData(list_id_min)
        conddata[1].setData(list_id_max)
        cond = "LIST_ID >= :list_id_min AND LIST_ID <= :list_id_max"
        q.setCondition(cond, conddata)
        cursor = q.execute()
        while cursor.next():

            row = cursor.currentRow()

            comp = [row[i].data() for i in range(0, 9)]
            list_id = row[9].data()
            # bug fix
            if comp[6] == 0 and comp[7] != 0:
                comp[7] = 0
            comp = [''] + [self.id2str[c] for c in comp[:8] if c] + [comp[8]]
            path = '/'.join(comp)

            self.hlmap.setdefault(list_id, []).append(path)


class NextIdGen:

    def __init__(self, schema, name):
        self.schema = schema
        self.name = name
        self.seqname = T('SEQ_' + name)

    def next(self, spec=0):

        q = self.schema.newQuery()
        q.addToTableList('DUAL')
        q.addToOutputList(self.seqname + '.NEXTVAL')

        cursor = q.execute()
        if cursor.next():
            nextid = int(cursor.currentRow()[0].data())
        else:
            nextid = None

        return nextid


_namemaps = {}


class NameIdTable:

    def __init__(self, schema, name, idcol, namecol):

        self.name = name
        self.tbl = schema.tableHandle(T(name))
        self.nextid = NextIdGen(schema, name)
        self.idcol = idcol
        self.namecol = namecol

        self.conddata = coral.AttributeList()
        self.conddata.extend("name", "string")
        self.cond = self.namecol + " = :name"

        self.newdata = coral.AttributeList()
        self.newdata.extend(self.namecol, "string")
        self.newdata.extend(self.idcol, "int")

        self.namemap = _namemaps.setdefault(name, {})

    def get_id(self, name):

        id = self.namemap.get(name, None)
        if id is not None:
            return id

        q = self.tbl.newQuery()
        q.addToOutputList(self.idcol)
        self.conddata[0].setData(name)
        q.setCondition(self.cond, self.conddata)
        cursor = q.execute()

        if cursor.next():

            id = cursor.currentRow()[0].data()

        else:

            id = self.nextid.next()
            print("    Creating new %s record, id=%d" % (self.name, id))
            self.newdata[0].setData(name)
            self.newdata[1].setData(id)
            self.tbl.dataEditor().insertRow(self.newdata)

        self.namemap[name] = id
        return id


class RunLBTable:

    def __init__(self, schema, name):

        self.name = name
        self.tbl = schema.tableHandle(T(name))
        self.nextid = NextIdGen(schema, name)

        self.cache = _namemaps.setdefault(name, {})

    def register(self, part_id, run, lb, runtype_id, smkey, runTime):

        t = (part_id, run, lb, runtype_id, smkey)
        id = self.cache.get(t, None)
        if id is not None:
            print("  Cached %s record, id=%d" % (self.name, id))
            return id

        q = self.tbl.newQuery()
        q.addToOutputList('RUNLB_ID')
        data = coral.AttributeList()
        data.extend('RUN_NUMBER', 'int')
        data.extend('SM_KEY', 'int')
        data.extend('RUNTYPE_ID', 'int')
        data.extend('PART_ID', 'int')
        data[0].setData(run)
        data[1].setData(smkey)
        data[2].setData(runtype_id)
        data[3].setData(part_id)
        cond = "RUN_NUMBER=:RUN_NUMBER AND SM_KEY=:SM_KEY AND RUNTYPE_ID=:RUNTYPE_ID AND PART_ID=:PART_ID"
        if lb is not None:
            data.extend('LB_NUMBER', 'int')
            data[4].setData(lb)
            cond += " AND LB_NUMBER=:LB_NUMBER"
        else:
            cond += " AND LB_NUMBER IS NULL"
        q.setCondition(cond, data)
        cursor = q.execute()
        if cursor.next():

            id = cursor.currentRow()[0].data()
            print("  Reusing %s record, id=%d" % (self.name, id))

        else:

            id = self.nextid.next()
            data.extend('RUNLB_ID', 'int')
            data['RUNLB_ID'].setData(id)
            if runTime:
                data.extend('TS_UTC', 'time stamp')
                data['TS_UTC'].setData(runTime)
            print("  Creating new %s record, id=%d" % (self.name, id))
            self.tbl.dataEditor().insertRow(data)

        self.cache[t] = id
        return id


class FileTable:

    def __init__(self, schema, name):

        self.name = name
        self.tbl = schema.tableHandle(T(name))
        self.nextid = NextIdGen(schema, name)

        self.cache = _namemaps.setdefault(name, {})

    def register(self, runlb_id, relpath, ds_id, arch_id):

        t = (runlb_id, relpath, ds_id, arch_id)
        id = self.cache.get(t, None)
        if id is not None:
            print("  Cached %s record, id=%d" % (self.name, id))
            return id

        q = self.tbl.newQuery()
        q.addToOutputList('FILE_ID')
        data = coral.AttributeList()
        data.extend('RUNLB_ID', 'int')
        data.extend('FILE_NAME', 'string')
        data.extend('DS_ID', 'int')
        data.extend('ARCH_ID', 'int')
        data[0].setData(runlb_id)
        data[1].setData(relpath)
        data[2].setData(ds_id)
        data[3].setData(arch_id)
        cond = "RUNLB_ID=:RUNLB_ID AND FILE_NAME=:FILE_NAME AND DS_ID=:DS_ID AND ARCH_ID=:ARCH_ID"
        q.setCondition(cond, data)
        cursor = q.execute()
        if cursor.next():

            id = cursor.currentRow()[0].data()
            print("  Reusing %s record, id=%d" % (self.name, id))

        else:

            id = self.nextid.next()
            data.extend('FILE_ID', 'int')
            data[4].setData(id)
            print("  Creating new %s record, id=%d" % (self.name, id))
            self.tbl.dataEditor().insertRow(data)

        self.cache[t] = id
        return id


class HGroupTable:

    def __init__(self, schema, name):

        self.name = name
        self.tbl = schema.tableHandle(T(name))
        self.nextid = NextIdGen(schema, name)

    def register(self, hgrpname_id, file_id, hsrv_id, prov_id):

        q = self.tbl.newQuery()
        q.addToOutputList('HGRP_ID')
        data = coral.AttributeList()
        data.extend('HGRPNAME_ID', 'int')
        data.extend('FILE_ID', 'int')
        data.extend('HSRV_ID', 'int')
        data.extend('PROV_ID', 'int')
        data[0].setData(hgrpname_id)
        data[1].setData(file_id)
        data[2].setData(hsrv_id)
        data[3].setData(prov_id)
        cond = "HGRPNAME_ID=:HGRPNAME_ID AND FILE_ID=:FILE_ID AND HSRV_ID=:HSRV_ID AND PROV_ID=:PROV_ID"
        q.setCondition(cond, data)
        cursor = q.execute()
        if cursor.next():

            id = cursor.currentRow()[0].data()
            print("  Reusing %s record, id=%d" % (self.name, id))

        else:

            id = self.nextid.next()
            data.extend('HGRP_ID', 'int')
            data[4].setData(id)
            print("  Creating new %s record, id=%d" % (self.name, id))
            self.tbl.dataEditor().insertRow(data)

        return id


class HistoTable:

    def __init__(self, schema, name):

        self.name = name
        self.tbl = schema.tableHandle(T(name))
        self.nextid = NextIdGen(schema, name)

        self.namemap = _namemaps.setdefault(name, {})

    def register(self, hnam_id, folder_id):

        id = self.namemap.get((hnam_id, folder_id), None)
        if id is not None:
            return id

        q = self.tbl.newQuery()
        q.addToOutputList('HIST_ID')
        data = coral.AttributeList()
        data.extend('HNAM_ID', 'int')
        data.extend('FLDR_ID', 'int')
        data[0].setData(hnam_id)
        data[1].setData(folder_id)
        cond = "HNAM_ID=:HNAM_ID AND FLDR_ID=:FLDR_ID"
        q.setCondition(cond, data)
        cursor = q.execute()
        if cursor.next():

            id = cursor.currentRow()[0].data()

        else:

            id = self.nextid.next()
            data.extend('HIST_ID', 'int')
            data[2].setData(id)
            #print("  Creating new %s record, id=%d, name=%s" % (self.name, id, name))
            self.tbl.dataEditor().insertRow(data)

        self.namemap[(hnam_id, folder_id)] = id
        return id


class HGroupHistoTable:

    def __init__(self, schema, name):

        self.name = name
        self.tbl = schema.tableHandle(T(name))

        self.data = coral.AttributeList()
        self.data.extend('HGRP_ID', 'int')
        self.data.extend('HIST_ID', 'int')

    def register(self, hgrp_id, histids):

        #print('HGroupHistoTable: %s -> %s' % (hgrp_id, histids))

        bulkInserter = self.tbl.dataEditor().bulkInsert(self.data, 10000)

        for hid in histids:
            self.data[0].setData(hgrp_id)
            self.data[1].setData(hid)
            bulkInserter.processNextIteration()

        bulkInserter.flush()

        print("    Stored %d records in %s" % (len(histids), self.name))


class MDA3Writer:

    def __init__(self, schema, id2str):

        self.id2str = id2str

        self.t_partition = NameIdTable(schema, 'PARTITION', 'PART_ID', 'PART_NAME')
        self.t_runtype = NameIdTable(schema, 'RUNTYPE', 'RUNTYPE_ID', 'RUNTYPE_NAME')
        self.t_runlb = RunLBTable(schema, 'RUNLB')
        self.t_dataset = NameIdTable(schema, 'DATASET', 'DS_ID', 'DS_NAME')
        self.t_archive = NameIdTable(schema, 'ARCHIVE', 'ARCH_ID', 'ARCH_NAME')
        self.t_file = FileTable(schema, 'FILE')
        self.t_provider = NameIdTable(schema, 'PROVIDER', 'PROV_ID', 'PROV_NAME')
        self.t_hserver = NameIdTable(schema, 'HSERVER', 'HSRV_ID', 'HSRV_NAME')
        self.t_hgrpname = NameIdTable(schema, 'HGRPNAME', 'HGRPNAME_ID', 'HGRPNAME_NAME')
        self.t_hgroup = HGroupTable(schema, 'HGROUP')
        self.t_folder = NameIdTable(schema, 'FOLDER', 'FLDR_ID', 'FLDR_NAME')
        self.t_histo = HistoTable(schema, 'HISTO')
        self.t_hname = NameIdTable(schema, 'HNAME', 'HNAM_ID', 'HNAM_NAME')
        self.t_hgroup_histo = HGroupHistoTable(schema, 'HGROUP_HISTO')

    def register(self, part, run, lb, hgroup, ohserver, provider,
                 runtype, smkey, hlist, dataset, relpath, archive, runTime):

        lb = int(lb)
        if lb == -1:
            lb = None
        print('  Register part:', part, 'run:', run, 'lb:', lb, 'hgroup:', hgroup,
              'ohserver:', ohserver, 'provider:', provider)

        part_id = self.t_partition.get_id(part)
        hgrpname_id = self.t_hgrpname.get_id(hgroup)
        hsrv_id = self.t_hserver.get_id(ohserver)
        prov_id = self.t_provider.get_id(provider)
        runtype_id = self.t_runtype.get_id(runtype)
        ds_id = self.t_dataset.get_id(dataset)
        arch_id = self.t_archive.get_id(archive)

        runlb_id = self.t_runlb.register(part_id, run, lb, runtype_id, smkey, runTime)
        file_id = self.t_file.register(runlb_id, relpath, ds_id, arch_id)
        hgrp_id = self.t_hgroup.register(hgrpname_id, file_id, hsrv_id, prov_id)

        histids = []
        for histo in hlist:
            splitpath = [''] + [c for c in histo.split('/') if c]
            folder = '/'.join(splitpath[:-1])
            if not folder:
                folder = '/'
            fldr_id = self.t_folder.get_id(folder)
            hnam_id = self.t_hname.get_id(splitpath[-1])
            hid = self.t_histo.register(hnam_id, fldr_id)
            #print("      histo:", histo, 'parents:', parents)
            histids.append(hid)
        self.t_hgroup_histo.register(hgrp_id, histids)


def main(argv):

    global _pfx

    parser = OptionParser(usage='%prog [options] min_run max_run')
    parser.set_defaults(conn_string="oracle://INTR/ATLAS_MDA",
                        prefix=_pfx,
                        run_numbers=None)
    parser.add_option('-c', dest='conn_string', metavar='CONN_STRING',
                      help="specifies CORAL connection string for MDA database")
    parser.add_option('-r', dest='run_numbers', metavar='PATH',
                      help="file name with the run numbers and times (output of rn_ls)")
    parser.add_option('-p', dest='prefix', metavar='PREFIX',
                      help="specifies table name prefix, def: " + _pfx)
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")
    min_run = int(args[0])
    max_run = int(args[1])

    _pfx = options.prefix

    # read run times from a file
    runs = _readRunTimes(options.run_numbers)

    # start session
    conn_svc = coral.ConnectionService()
    try:
        print("Opening connection to database:", options.conn_string)
        session = conn_svc.connect(options.conn_string, coral.access_Update)
    except coral.Exception as e:
        print(str(e))
        return 1
    schema = session.nominalSchema()

    # open transaction
    trans = session.transaction()
    trans.start()

    id2str = _read_str2id(schema)

    hl_tab = HListTable(schema, id2str)
    #min_list_id, max_list_id = _get_list_ids(schema, min_run, max_run)
    #hl_tab.read_lists(min_list_id, max_list_id)

    # do copy
    for run in range(min_run, max_run + 1):
        trans.start()
        _copyOne(schema, run, id2str, hl_tab, runs)
        trans.commit()


def _copyOne(schema, run, id2str, hl_tab, runs):

    print("Processing RUN_NUMBER=%d" % run)
    t0 = time.time()

    t_int = schema.tableHandle('MDA2_INTERVALS')

    writer = MDA3Writer(schema, id2str)

    # query the intervals table and get the list of intervals
    q = t_int.newQuery()
    q.addToOutputList('DAQ_PARTITION')
    q.addToOutputList('RUN_NUMBER')
    q.addToOutputList('CASE LUMI_BLOCK WHEN 4294967295 THEN -1 ELSE LUMI_BLOCK END')
    q.addToOutputList('HISTO_GROUP')
    q.addToOutputList('OH_SERVER')
    q.addToOutputList('OH_PROVIDER')
    q.addToOutputList('RUN_TYPE')
    q.addToOutputList('SM_KEY')
    q.addToOutputList('HISTO_LIST_MAIN')
    q.addToOutputList('HISTO_LIST_ADD')
    q.addToOutputList('HISTO_LIST_SUB')
    q.addToOutputList('DATASET')
    q.addToOutputList('REL_PATH')
    q.addToOutputList('ARCHIVE')
    conddata = coral.AttributeList()
    conddata.extend("run", "int")
    conddata[0].setData(run)
    cond = "RUN_NUMBER = :run"
    q.setCondition(cond, conddata)
    cursor = q.execute()
    count = 0
    while cursor.next():
        row = cursor.currentRow()

        part = id2str[row[0].data()]
        run = row[1].data()
        lb = row[2].data()
        hgroup = id2str[row[3].data()]
        ohserver = id2str[row[4].data()]
        provider = id2str[row[5].data()]
        runtype = id2str[row[6].data()]
        smkey = row[7].data()
        hl_main = row[8].data()
        hl_add = row[9].data()
        hl_sub = row[10].data()
        dataset = id2str[row[11].data()]
        relpath = row[12].data()
        archive = row[13].data()

        hlist = hl_tab.get_hl(hl_main, hl_add, hl_sub)

        runTime = runs.get(run)
        writer.register(part, run, lb, hgroup, ohserver, provider,
                        runtype, smkey, hlist, dataset, relpath, archive, runTime)

        count += 1
        #pprint(row)
        #pprint(hlist)

    t1 = time.time()
    print("Processed %d records for RUN_NUMBER=%d in %d sec" % (count, run, int(t1 - t0)))


def _read_str2id(schema):

    id2str = {}

    t_str2id = schema.tableHandle('MDA2_STR2ID')
    # read STR2ID map
    print("Reading STR2ID table")
    q = t_str2id.newQuery()
    q.addToOutputList('STR')
    q.addToOutputList('ID')
    cursor = q.execute()
    while cursor.next():
        row = cursor.currentRow()
        st = row[0].data()
        id = row[1].data()
        id2str[id] = st
        del row
    del cursor

    return id2str


def _get_list_ids(schema, min_run, max_run):

    t_int = schema.tableHandle('MDA2_INTERVALS')

    print("Reading min/max list IDs")

    # query the intervals table and get the list of intervals
    q = t_int.newQuery()
    q.addToOutputList('MIN(HISTO_LIST_MAIN)')
    q.addToOutputList('MAX(HISTO_LIST_MAIN)')
    conddata = coral.AttributeList()
    conddata.extend("min_run", "int")
    conddata.extend("max_run", "int")
    conddata[0].setData(min_run)
    conddata[1].setData(max_run)
    cond = "RUN_NUMBER >= :min_run AND RUN_NUMBER <= :max_run"
    q.setCondition(cond, conddata)
    cursor = q.execute()
    if cursor.next():
        row = cursor.currentRow()

        id1 = int(row[0].data())
        id2 = int(row[1].data())
        print("min-max list ID = %d-%d" % (id1, id2))
        return (id1, id2)

    else:

        print("Cannot find min/max list ID")
        return (0, 0)


def _readRunTimes(path):

    if not path:
        return {}

    print("Reading run times from", path)

    res = {}
    for line in open(path).readlines():
        x = line.split('|')
        if len(x) < 4 or not x[2].strip().isdigit():
            continue

        run = int(x[2].strip())
        start_str = x[3].strip()
        dur_str = x[4].strip()

        # parse begin time
        t = time.strptime(start_str, "%Y-%b-%d %H:%M:%S")
        start_utc = calendar.timegm(t)
        end_utc = start_utc

        # parse duration
        if dur_str:
            h, m, s = map(int, dur_str.split(':'))
            end_utc += h * 3600 + m * 60 + s

        # format end time
        t = time.gmtime(end_utc)
        res[run] = coral.TimeStamp(*t[:6])

    print("Read %d records" % len(res))

    return res


if __name__ == "__main__":

    main(sys.argv)
