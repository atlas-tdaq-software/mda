#!/bin/env tdaq_python
#
#  Script for (re-)creating schema for he new version of MDA database.
#

from __future__ import print_function

import sys
from optparse import OptionParser

from mda.import_coral import coral


# make table name
_pfx = 'MDA3'


def T(name):
    return _pfx + '_' + name

# method to create a table with all indices


def _createTable(schema, tdict):

    print('Creating table', tdict['name'])

    td = coral.TableDescription()
    td.setName(tdict['name'])

    for c in tdict['columns']:
        td.insertColumn(*(c[0:2]))
        td.setNotNullConstraint(c[0], c[2])

    if 'pk' in tdict:
        td.setPrimaryKey(tdict['pk'])

    for fk in tdict.get('fk', []):
        print('Creating FK', fk[0])
        td.createForeignKey(*fk)

    for idx in tdict.get('index', []):
        print('Creating index', idx[0])
        td.createIndex(*idx)

    table = schema.createTable(td)
    return table

# method to create a table with all indices


def _createSeq(name, cache=None):
    q = "CREATE SEQUENCE " + T("SEQ_" + name) + " START WITH 1"
    if cache:
        q += " CACHE " + str(cache)
    print(' ' + q + ';')


def main(argv):

    global _pfx

    parser = OptionParser(usage='%prog [options]')
    parser.set_defaults(conn_string="oracle://INTR/ATLAS_MDA",
                        prefix=_pfx,
                        delete=False,
                        no_create=False)
    parser.add_option('-c', dest='conn_string', metavar='CONN_STRING',
                      help="specifies CORAL connection string for MDA database")
    parser.add_option('-p', dest='prefix', metavar='PREFIX',
                      help="specifies table name prefix, def: " + _pfx)
    parser.add_option('-d', action='store_true', dest='delete',
                      help="if specified then the existing schema will be deleted")
    parser.add_option('-n', action='store_true', dest='no_create',
                      help="do not create tables (use to delete old schema)")

    (options, args) = parser.parse_args()

    conn_svc = coral.ConnectionService()
    try:
        print("Opening connection to database:", options.conn_string)
        session = conn_svc.connect(options.conn_string, coral.access_Update)
    except coral.Exception as e:
        print(str(e))
        return 1

    trans = session.transaction()
    trans.start()

    # get the nominal schema
    schema = session.nominalSchema()

    _pfx = options.prefix

    # list of all table names
    alltables = [
        'HGROUP_HISTO', 'HGROUP_FOLDER', 'HISTO',
        'HGROUP', 'FILE', 'RUNLB', 'PARTITION',
        'NEXT_ID', 'ARCHIVE', 'DATASET',
        'PROVIDER', 'HSERVER', 'RUNTYPE',
        'HGRPNAME', 'FOLDER', 'HNAME',
        'LBRANGE', 'LBSEQ',
    ]

    # list of sequence names together with their cache sizes
    # (cache size is oracle-specific thing).
    sequences = [('ARCHIVE', 10),
                 ('DATASET', None),
                 ('FILE', 100),
                 ('FOLDER', 100),
                 ('HGROUP', 1000),
                 ('HGRPNAME', None),
                 ('HISTO', 1000),
                 ('HNAME', 100),
                 ('HSERVER', None),
                 ('LBSEQ', 10),
                 ('PARTITION', None),
                 ('PROVIDER', 10),
                 ('RUNLB', 10),
                 ('RUNTYPE', None)]

    # drop tables
    if options.delete:
        for t in alltables:
            print('Dropping table', T(t))
            schema.dropIfExistsTable(T(t))
        print("\nSequence generators need to be deleted manually.\n"
              "Open sqlplus session on the new database and execute these commands:\n\n+++++++++")
        for seq in sequences:
            q = "DROP SEQUENCE " + T("SEQ_" + seq[0])
            print(' ' + q + ';')
        print("---------\n")
    else:
        print("\n - Will NOT delete existing schema, if you need to delete it\n"
              "   first then specify -d option.\n")

    # may need to stop here
    if options.no_create:
        return 0

    Unique = True
    Null = False

    table = dict(name=T('PARTITION'),
                 columns=[('PART_ID', 'int', not Null),
                          ('PART_NAME', 'string', not Null), ],
                 pk='PART_ID',
                 index=[(T('PARTITION_PART_NAME_IDX'), 'PART_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('RUNTYPE'),
                 columns=[('RUNTYPE_ID', 'int', not Null),
                          ('RUNTYPE_NAME', 'string', not Null), ],
                 pk='RUNTYPE_ID',
                 index=[(T('RUNTYPE_RUNTYPE_NAME_IDX'), 'RUNTYPE_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('RUNLB'),
                 columns=[('RUNLB_ID', 'int', not Null),
                          ('RUN_NUMBER', 'int', not Null),
                          ('LB_NUMBER', 'int', Null),
                          ('SM_KEY', 'int', not Null),
                          ('RUNTYPE_ID', 'int', not Null),
                          ('PART_ID', 'int', not Null),
                          ('TS_UTC', 'time stamp', not Null), ],
                 pk='RUNLB_ID',
                 index=[(T('RUNLB_RUN_LB_UNI'), ('PART_ID', 'RUN_NUMBER', 'LB_NUMBER'), Unique),
                        (T('RUNLB_PART_ID_IDX'), 'PART_ID', not Unique),
                        (T('RUNLB_TS_UTC_IDX'), 'TS_UTC', not Unique), ],
                 fk=[(T('RUNLB_PART_ID_FK'), 'PART_ID', T('PARTITION'), 'PART_ID'),
                     (T('RUNLB_RUNTYPE_ID_FK'), 'RUNTYPE_ID', T('RUNTYPE'), 'RUNTYPE_ID')]
                 )
    _createTable(schema, table)

    table = dict(name=T('DATASET'),
                 columns=[('DS_ID', 'int', not Null),
                          ('DS_NAME', 'string', not Null), ],
                 pk='DS_ID',
                 index=[(T('DATASET_DS_NAME_IDX'), 'DS_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('ARCHIVE'),
                 columns=[('ARCH_ID', 'int', not Null),
                          ('ARCH_NAME', 'string', not Null), ],
                 pk='ARCH_ID',
                 index=[(T('ARCHIVE_ARCH_NAME_IDX'), 'ARCH_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('FILE'),
                 columns=[('FILE_ID', 'int', not Null),
                          ('RUNLB_ID', 'int', not Null),
                          ('FILE_NAME', 'string', not Null),
                          ('DS_ID', 'int', not Null),
                          ('ARCH_ID', 'int', not Null), ],
                 pk='FILE_ID',
                 index=[(T('FILE_RUNLB_ID_IDX'), 'RUNLB_ID', not Unique),
                        (T('FILE_DS_ID_IDX'), 'DS_ID', not Unique),
                        (T('FILE_ARCH_ID_IDX'), 'ARCH_ID', not Unique), ],
                 fk=[(T('FILE_RUNLB_ID_FK'), ('RUNLB_ID',), T('RUNLB'), ('RUNLB_ID',)),
                     (T('FILE_DS_ID_FK'), ('DS_ID',), T('DATASET'), ('DS_ID',)),
                     (T('FILE_ARCH_ID_FK'), ('ARCH_ID',), T('ARCHIVE'), ('ARCH_ID',)), ]
                 )
    _createTable(schema, table)

    table = dict(name=T('PROVIDER'),
                 columns=[('PROV_ID', 'int', not Null),
                          ('PROV_NAME', 'string', not Null), ],
                 pk='PROV_ID',
                 index=[(T('PROVIDER_PROV_NAME_IDX'), 'PROV_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('HSERVER'),
                 columns=[('HSRV_ID', 'int', not Null),
                          ('HSRV_NAME', 'string', not Null), ],
                 pk='HSRV_ID',
                 index=[(T('HSERVER_HSRV_NAME_IDX'), 'HSRV_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('HGRPNAME'),
                 columns=[('HGRPNAME_ID', 'int', not Null),
                          ('HGRPNAME_NAME', 'string', not Null), ],
                 pk='HGRPNAME_ID',
                 index=[(T('HGRPNAME_NAME_IDX'), 'HGRPNAME_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('HGROUP'),
                 columns=[('HGRP_ID', 'int', not Null),
                          ('HGRPNAME_ID', 'int', not Null),
                          ('FILE_ID', 'int', not Null),
                          ('HSRV_ID', 'int', not Null),
                          ('PROV_ID', 'int', not Null), ],
                 pk='HGRP_ID',
                 index=[(T('HGROUP_FILE_ID_IDX'), 'FILE_ID', not Unique),
                        (T('HGROUP_HSRV_ID_IDX'), 'HSRV_ID', not Unique),
                        (T('HGROUP_PROV_ID_IDX'), 'PROV_ID', not Unique),
                        (T('HGROUP_UNI'), ('HGRPNAME_ID', 'FILE_ID', 'HSRV_ID', 'PROV_ID'), Unique),
                        ],
                 fk=[(T('HGROUP_HGRPNAME_ID_FK'), 'HGRPNAME_ID', T('HGRPNAME'), 'HGRPNAME_ID'),
                     (T('HGROUP_FILE_ID_FK'), 'FILE_ID', T('FILE'), 'FILE_ID'),
                     (T('HGROUP_HSRV_ID_FK'), 'HSRV_ID', T('HSERVER'), 'HSRV_ID'),
                     (T('HGROUP_PROV_ID_FK'), 'PROV_ID', T('PROVIDER'), 'PROV_ID'), ]
                 )
    _createTable(schema, table)

    table = dict(name=T('FOLDER'),
                 columns=[('FLDR_ID', 'int', not Null),
                          ('FLDR_NAME', 'string', not Null), ],
                 pk='FLDR_ID',
                 index=[(T('FOLDER_FLDR_NAME_IDX'), 'FLDR_NAME', Unique)],
                 )
    _createTable(schema, table)

    table = dict(name=T('HNAME'),
                 columns=[('HNAM_ID', 'int', not Null),
                          ('HNAM_NAME', 'string', not Null), ],
                 pk='HNAM_ID',
                 index=[(T('HNAME_HNAM_NAME_IDX'), 'HNAM_NAME', Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('HISTO'),
                 columns=[('HIST_ID', 'int', not Null),
                          ('HNAM_ID', 'int', not Null),
                          ('FLDR_ID', 'int', not Null), ],
                 pk='HIST_ID',
                 index=[(T('HISTO_HNAM_ID_IDX'), 'HNAM_ID', not Unique),
                        (T('HISTO_FLDR_ID_IDX'), 'FLDR_ID', not Unique), ],
                 fk=[(T('HISTO_FLDR_ID_FK'), 'FLDR_ID', T('FOLDER'), 'FLDR_ID'),
                     (T('HISTO_HNAM_ID_FK'), 'HNAM_ID', T('HNAME'), 'HNAM_ID'), ]
                 )
    _createTable(schema, table)

    table = dict(name=T('LBSEQ'),
                 columns=[('LBSQ_ID', 'int', not Null),
                          ('LBSQ_SIZE', 'int', not Null),
                          ('LBSQ_MIN_LB', 'int', not Null),
                          ('LBSQ_MAX_LB', 'int', not Null), ],
                 pk='LBSQ_ID',
                 index=[(T('LBSEQ_LBSQ_IDX'), ('LBSQ_SIZE', 'LBSQ_MIN_LB', 'LBSQ_MAX_LB',), not Unique), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('LBRANGE'),
                 columns=[('LBSQ_ID', 'int', not Null),
                          ('LBRNG_MIN', 'int', not Null),
                          ('LBRNG_MAX', 'int', not Null), ],
                 pk=('LBSQ_ID', 'LBRNG_MIN', 'LBRNG_MAX'),
                 fk=[(T('LBRANGE_LBSQ_ID_FK'), 'LBSQ_ID', T('LBSEQ'), 'LBSQ_ID'), ],
                 )
    _createTable(schema, table)

    table = dict(name=T('HGROUP_HISTO'),
                 columns=[('HGRP_ID', 'int', not Null),
                          ('HIST_ID', 'int', not Null),
                          ('LBSQ_ID', 'int', Null)],
                 pk=('HGRP_ID', 'HIST_ID'),
                 index=[(T('HGROUP_HISTO_HIST_ID_IDX'), ('HIST_ID',), not Unique), ],
                 fk=[(T('HGROUP_HISTO_HGRP_ID_FK'), 'HGRP_ID', T('HGROUP'), 'HGRP_ID'),
                     (T('HGROUP_HISTO_HIST_ID_FK'), 'HIST_ID', T('HISTO'), 'HIST_ID'),
                     (T('HGROUP_HISTO_LBSQ_ID_FK'), 'LBSQ_ID', T('LBSEQ'), 'LBSQ_ID'), ],
                 )
    _createTable(schema, table)
    print("\nIndices for some tables need to be compressed.\n"
          "Open sqlplus session on the new database and execute these commands:\n\n+++++++++"
          "\n ALTER INDEX " + T('LBRANGE_PK') + " REBUILD COMPRESS 1 PCTFREE 5;\n"
          "---------\n")

    print("\nSequence generators need to be created manually.\n"
          "Open sqlplus session on the new database and execute these commands:\n\n+++++++++")
    for seq in sequences:
        _createSeq(*seq)
    print("---------\n")

    print("\nSome columns need to have default values set up.\n"
          "Open sqlplus session on the new database and execute these commands:\n\n+++++++++"
          "\n ALTER TABLE " + T('RUNLB') + " MODIFY (TS_UTC DEFAULT SYS_EXTRACT_UTC(SYSTIMESTAMP) );\n"
          "---------\n")

    trans.commit()


if __name__ == "__main__":

    sys.exit(main(sys.argv))
