# Helper module to import PyROOT when its module is not in PYTHONPATH.
# First try to import it as usual and if it fails add $ROOTSYS/lib
# to PYTHONPATH and retry.
# Typical use: from mda.import_ROOT import ROOT

from __future__ import print_function

import sys
import os

try:
    import ROOT  # noqa: F401
except ImportError:
    try:
        rootsys = os.getenv('ROOTSYS')
        if not rootsys:
            raise
        sys.path.append(os.path.join(rootsys, 'lib'))
        import ROOT  # noqa: F401
    except ImportError:
        print("Failed to import PyROOT, check your PYTHONPATH", file=sys.stderr)
        raise
