from __future__ import annotations

__all__ = ["main"]

import argparse
import logging
import sys

from .. import scripts

_logfmt = "%(asctime)s %(levelname)-8s %(message)s"


def _logLevels(lvl: int) -> int:
    if lvl == 0:
        return logging.WARNING
    if lvl == 1:
        return logging.INFO
    return logging.DEBUG


def _setLogging(lvl: int) -> None:
    rootlog = logging.getLogger()
    hdlr = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(_logfmt)
    hdlr.setFormatter(formatter)
    rootlog.addHandler(hdlr)
    rootlog.setLevel(_logLevels(lvl))


def main() -> int | None:
    parser = argparse.ArgumentParser(description="MDA command line interface")
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="increase verbosity, can be used multiple times",
    )
    subparsers = parser.add_subparsers(title="available subcommands", required=True)
    _schema_subcommand(subparsers)
    _annotations_subcommand(subparsers)

    parsed_args = parser.parse_args()

    kwargs = vars(parsed_args)
    # Strip keywords not understood by scripts.
    verbose = kwargs.pop("verbose")
    _setLogging(verbose)
    method = kwargs.pop("method")
    return method(**kwargs)


def _annotations_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        "annotations", help="Dump contents of MDA annotations."
    )
    parser.add_argument(
        "-d",
        dest="dataset",
        default="",
        help="CoCa dataset name, default is to use any dataset.",
    )
    parser.add_argument(
        "file_name", help="Name of ROOT file on local disk or stored in CoCa."
    )
    parser.add_argument(
        "regexp",
        default="",
        nargs="?",
        help="Regular expression for histogram names, by default all histogram annotations are printed.",
    )
    parser.set_defaults(method=scripts.annotations)


def _schema_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser("schema", help="Operations on MDA database schema.")
    parser.add_argument(
        "-c",
        dest="conn_str",
        default="MDA",
        metavar="CONN_STR",
        help="Connection string for CORAL database.",
    )
    parser.add_argument(
        "-p",
        dest="prefix",
        metavar="PREFIX",
        default="MDA3",
        help="Table name prefix, def: %(default)s",
    )

    # define sub-commands
    subparsers = parser.add_subparsers(title="available subcommands", required=True)
    _schema_create_subcommand(subparsers)
    _schema_make_version_table_subcommand(subparsers)
    _schema_show_tables_subcommand(subparsers)
    _schema_show_db_tables_subcommand(subparsers)
    _schema_show_version_subcommand(subparsers)
    _schema_show_versions_subcommand(subparsers)
    _schema_show_meta_subcommand(subparsers)
    _schema_set_meta_subcommand(subparsers)


def _schema_create_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser("create", description="Create new schema.")
    parser.add_argument(
        "-v",
        dest="version",
        default=-1,
        type=int,
        metavar="NUMBER",
        help="Schema version, default is to use latest version.",
    )
    parser.add_argument(
        "--no-version-table",
        dest="make_vtable",
        default=True,
        action="store_false",
        help="Do not create schema version table.",
    )
    parser.set_defaults(method=scripts.create_schema)


def _schema_make_version_table_subcommand(
    subparsers: argparse._SubParsersAction,
) -> None:
    parser = subparsers.add_parser(
        "make-version-table", description="Create VERSION table."
    )
    parser.add_argument(
        "-v",
        dest="version",
        default=-1,
        type=int,
        metavar="NUMBER",
        help="Schema version, default is to use latest version.",
    )
    parser.set_defaults(method=scripts.make_version_table)


def _schema_show_tables_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        "tables", description="Show table names in schema."
    )
    parser.add_argument(
        "-v",
        dest="version",
        default=-1,
        type=int,
        metavar="NUMBER",
        help="Schema version, default is to use latest version.",
    )
    parser.set_defaults(method=scripts.show_tables)


def _schema_show_db_tables_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        "db-tables", description="Show table names in database schema."
    )
    parser.set_defaults(method=scripts.show_db_tables)


def _schema_show_version_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        "db-version", description="Show schema version of MDA database."
    )
    parser.set_defaults(method=scripts.show_db_version)


def _schema_show_versions_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        "versions", description="Show schema versions supported by MDA."
    )
    parser.set_defaults(method=scripts.show_supported_versions)


def _schema_show_meta_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        "show-meta", description="Show schema metadata values."
    )
    parser.set_defaults(method=scripts.show_meta)


def _schema_set_meta_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser(
        "set-meta", description="Update schema metadata values."
    )
    parser.add_argument("key", metavar="STRING", help="Metadata key, string.")
    parser.add_argument("value", metavar="STRING", help="Metadata value, string.")
    parser.set_defaults(method=scripts.set_meta)
