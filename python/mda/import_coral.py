#  Script which contains workaround for tdaq releases which do not
#  include coral directories in Python path and cannot import coral module.
#  Typical use: from mda.import_coral import coral

from __future__ import print_function

import sys
import os

# We need PyCoral
try:
    import coral  # noqa: F401
except ImportError:

    # try to find a directory to import, this is the directory where liblcg_PyCoral.so stays
    for p in os.environ['LD_LIBRARY_PATH'].split(':'):
        so = os.path.join(p, "liblcg_PyCoral.so")
        if os.path.exists(so):
            so = os.path.realpath(so)
            libdir = os.path.dirname(so)
            sys.path.append(libdir)
            sys.path.append(os.path.join(os.path.dirname(libdir), "python"))
            break

    try:
        import coral  # noqa: F401
    except ImportError:
        print("\nThis script needs PyCoral module to be accessible and it was not found.\n"
              "Please add PyCoral location to PYTHONPATH envvar.\n"
              "Typical locations that need to be in PYTHONPATH are:\n"
              "  /afs/cern.ch/sw/lcg/app/releases/CORAL/CORAL_x/arch/python\n"
              "  /afs/cern.ch/sw/lcg/app/releases/CORAL/CORAL_x/arch/lib\n\n"
              "(You need to change CORAL version and architecture name)\n",
              file=sys.stderr)
        raise
