
from __future__ import annotations

__all__ = ["FileRead"]

import os
from typing import TYPE_CHECKING

import coca

if TYPE_CHECKING:
    import ROOT


# method which guesses whether this binary runs at Point1.
# Thanks Sergei for idea
def _runningAtP1() -> bool:

    instpath = os.getenv("TDAQ_INST_PATH")
    if not instpath:
        return False

    # file that should exist only at P1
    filename = os.path.join(instpath, "com/ipc_reference_location")
    return os.path.exists(filename)


_atP1 = _runningAtP1()


class FileRead:
    """Class which provides access to ROOT files stored in CoCa by MDA.

    Parameters
    ----------
    cocaConnStr : `str`, optional
        Connection string for CoCa database
    """

    CacheFirst = 0
    ArchiveFirst = 1

    def __init__(self, cocaConnStr: str | None = None):
        """ Constructor takes a single argument which is CORAL
        connection string for CoCa database. """

        if not cocaConnStr:
            cocaConnStr = coca.defConnStr
        self.cocaClient = coca.DBClient(cocaConnStr)

        from mda.import_ROOT import ROOT
        self.root = ROOT

    def openFile(self, fileName: str, dataset: str = "", mode: int | None = None) -> ROOT.TFile | None:
        """
        @brief Locate ROOT file and open it for reading.

        Tries to locate file at one of the possible location, either in CoCa
        cache or archive. If file is found then it is opened and
        returned to client. If file is not found then None is returned.

        @param[in] fileName  Name of the ROOT file as stored in MDA database
        @param[in] dataset   Dataset name in which ROOT file is stored, may be empty
        @param[in] mode      Location search mode, default is ArchiveFirst
         """

        # set default for mode
        if mode is None:
            mode = FileRead.ArchiveFirst

        # this should return  a list consisting of location strings
        archiveLocations = self.cocaClient.fileLocations(fileName, dataset, self.cocaClient.LocArchive)
        cacheLocations = []
        if _atP1:
            cacheLocations = self.cocaClient.fileLocations(fileName, dataset, self.cocaClient.LocCache)
        if not archiveLocations and not cacheLocations:
            return None

        # If requested try to open file in CoCa cache first
        f = None
        if mode == FileRead.CacheFirst and cacheLocations and _atP1:
            f = self.root.TFile.Open(cacheLocations[0])

        # next try archive location, for which we need to query coca again
        if f is None and archiveLocations:
            f = self.root.TFile.Open(archiveLocations[0])

        # try cache location if cache should come after archive
        if f is None and mode != FileRead.CacheFirst and cacheLocations and _atP1:
            f = self.root.TFile.Open(cacheLocations[0])

        return f
