"""Wrapper module for libpymda, see help(libpymda) for details"""

import sys
if sys.platform == 'linux2':
    import DLFCN
    sys.setdlopenflags( DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL )
    from libpymda import *  # noqa: F401, F403
    import libpymda
    __doc__ = libpymda.__doc__
    del libpymda
    del DLFCN
elif sys.platform == 'linux':
    # python 3, DLFCN replaced by os
    import os
    sys.setdlopenflags( os.RTLD_NOW | os.RTLD_GLOBAL )
    from libpymda import *  # noqa: F401, F403
    import libpymda
    __doc__ = libpymda.__doc__
    del libpymda
    del os
else:
    from libpymda import *  # noqa: F401, F403
    import libpymda
    __doc__ = libpymda.__doc__
    del libpymda
del sys

from ._file_read import *
