__all__ = [
    "create_schema",
    "make_version_table",
    "show_tables",
    "show_db_tables",
    "show_db_version",
    "show_supported_versions",
    "show_meta",
    "set_meta",
]

import mda


def create_schema(conn_str: str, prefix: str, version: int, make_vtable: bool) -> None:
    mda_schema = mda.DBSchema(conn_str, prefix)
    mda_schema.makeSchema(version, make_vtable)
    seq_sql = mda_schema.makeSequncesSQL(version)
    print("""\

MDA schema was successfully created, for Oracle you will need to create
sequences which cannot be created with this tool, here are SQL statement
to create them:

""")
    for stmt in seq_sql:
        print("  ", stmt)


def make_version_table(conn_str: str, prefix: str, version: int) -> None:
    mda_schema = mda.DBSchema(conn_str, prefix)
    mda_schema.makeMetaTable(version)


def show_tables(conn_str: str, prefix: str, version: int) -> None:
    mda_schema = mda.DBSchema(conn_str, prefix, False)
    tables = sorted(mda_schema.tables(version))
    for table in tables:
        print(table)


def show_db_tables(conn_str: str, prefix: str) -> None:
    mda_schema = mda.DBSchema(conn_str, prefix, False)
    tables = sorted(mda_schema.dbTables())
    for table in tables:
        print(table)


def show_db_version(conn_str: str, prefix: str) -> None:
    mda_schema = mda.DBSchema(conn_str, prefix, False)
    print(mda_schema.dbSchemaVersion())


def show_supported_versions(conn_str: str, prefix: str) -> None:
    versions = mda.DBSchema.supportedSchemaVersions()
    for version in versions:
        print(version)


def show_meta(conn_str: str, prefix: str) -> None:
    mda_schema = mda.DBSchema(conn_str, prefix, False)
    meta = mda_schema.getMeta()
    for key in sorted(meta.keys()):
        print("'{}': '{}'".format(key, meta[key]))


def set_meta(conn_str: str, prefix: str, key: str, value: str) -> None:
    mda_schema = mda.DBSchema(conn_str, prefix)
    mda_schema.setMeta(key, value)
