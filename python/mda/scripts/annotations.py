"""Dump annotations from MDA files."""

import re
import sys

import mda

# hack to force ROOT to ignore command line
# _sys_argv = sys.argv[1:]
# sys.argv[1:] = []
from mda.import_ROOT import ROOT  # noqa: E402
ROOT.gROOT.SetBatch(True)

# Recognized types (one for now) of annotation structures
TYPE_TMAP_OARRAY = 'TMap of TObjArray<TObjString> [interleaved key/value]'

# regular expression to extract run and LB number from histogram name
runlb_re = re.compile(r'^/run_(\d+)/+lb_(\d+)/(.*)$')


def _cmp_hist_key(path: str) -> str:
    """Compare histogram paths ordering by numeric LB value."""
    m1 = runlb_re.match(str(path))
    if m1:
        return "{}-{:06d}-{}".format(m1.group(1), int(m1.group(2)), m1.group(3))
    return path


def annotations(dataset: str, file_name: str, regexp: str) -> int:
    """Dump annotations from MDA file."""

    # compile histogram regexp
    hist_re = None
    if regexp:
        try:
            hist_re = re.compile(regexp)
        except Exception as exc:
            print('Invalid regular expression "{}": {}'.format(regexp, exc), file=sys.stderr)
            return 1

    # try to open a file, local file first then in CoCa
    try:
        ROOT.gErrorIgnoreLevel = ROOT.kError + 1
        tfile = ROOT.TFile.Open(file_name, 'READ')
    except Exception:
        tfile = None

    if not tfile:
        try:
            fread = mda.FileRead()
        except Exception as exc:
            print('ERROR: CoCa database is not accessible:', exc, file=sys.stderr)
            return 1

        # open file
        try:
            tfile = fread.openFile(file_name, dataset)
        except Exception as exc:
            print('ERROR: failed to find file in CoCa:', exc, file=sys.stderr)
            return 1
        if not tfile:
            print('ERROR: cannot locate file on disk or in CoCa:', file_name, file=sys.stderr)
            return 1

    # find ROOT annotations objects
    ann_type = tfile.Get(".meta/annotations_type")
    ann_obj = tfile.Get(".meta/annotations")
    if not ann_obj or not ann_type:
        print('ERROR: Cannot locate annotations in file', file_name, file=sys.stderr)
        return 1

    if ann_type != TYPE_TMAP_OARRAY:
        print('ERROR: Unsupported annotation type:' + ann_type, file=sys.stderr)
        return 1

    # get and optionally filter all hist names
    keys = list(ann_obj)
    if hist_re:
        keys = [key for key in keys if hist_re.match(str(key))]
    keys = sorted(keys, key=_cmp_hist_key)

    for key in keys:
        ann = ann_obj(key)
        n_ann = len(ann) // 2
        ann = '; '.join(str(ann[2 * i]) + ': ' + str(ann[2 * i + 1]) for i in range(n_ann))
        print('{}: [{}]'.format(key, ann))

    return 0
