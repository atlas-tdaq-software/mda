
# mda

## tdaq-12-00-00

- tag `mda-07-20-00`
- Add `mda-cli` script replacing `mda_annotations` and `mda_schema`.
- tag `mda-07-19-01`
- Add type annotations for Python modules.

## tdaq-11-02-00

- tag `mda-07-19-00`
- Allow MDA app to read histograms from other partitions

## tdaq-10-00-00

- tag `mda-07-18-04`
- Update `CMakeFileList.txt`

## tdaq-09-04-00

- tag `mda-07-18-03`
- Small fix in mda_register script, updates for Python3.

## tdaq-09-01-00

Implemented support for future upgrades of MDA database schema.
Original intent was to enable storing histograms in ROOT file in their
original form without replacing colons with underscores but that did not
work out (see ADHI-4177 for details). Schema version support ay still be
useful for future updates so I keep it.
