//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/read_back/RunLB.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

/// Compare two objects, may be used for ordering
bool 
RunLB::operator< (const RunLB& rhs) const 
{
    if ( this->m_time < rhs.m_time ) return true;
    if ( this->m_time > rhs.m_time ) return false;
    if ( this->m_run < rhs.m_run ) return true;
    if ( this->m_run > rhs.m_run ) return false;
    if ( this->m_lb < rhs.m_lb ) return true;
    return false;
}

} // namespace mda
} // namespace daq
