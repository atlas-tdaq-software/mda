//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/read_back/HistoFile.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
HistoFile::HistoFile (const std::string& histoName,
                      unsigned lb,
                      const std::string& dataset,
                      const std::string& archive,
                      const std::string& file,
                      const std::string& histoPath)
    : m_histoName (histoName)
    , m_lb(lb)
    , m_dataset (dataset)
    , m_archive (archive)
    , m_file (file)
    , m_histoPath (histoPath)
{
}

// Default constructor
HistoFile::HistoFile()
    : m_histoName()
    , m_lb(0)
    , m_dataset()
    , m_archive()
    , m_file()
    , m_histoPath()
{
}

//--------------
// Destructor --
//--------------
HistoFile::~HistoFile () throw()
{
}

bool
HistoFile::operator< (const HistoFile& rhs) const
{
    if ( this->m_histoName < rhs.m_histoName ) return true;
    if ( this->m_histoName > rhs.m_histoName ) return false;
    if ( this->m_lb < rhs.m_lb ) return true;
    if ( this->m_lb > rhs.m_lb ) return false;
    if ( this->m_dataset < rhs.m_dataset ) return true;
    if ( this->m_dataset > rhs.m_dataset ) return false;
    if ( this->m_archive < rhs.m_archive ) return true;
    if ( this->m_archive > rhs.m_archive ) return false;
    if ( this->m_file < rhs.m_file ) return true;
    if ( this->m_file > rhs.m_file ) return false;
    if ( this->m_histoPath < rhs.m_histoPath ) return true;
    return false;
}

} // namespace mda
} // namespace daq
