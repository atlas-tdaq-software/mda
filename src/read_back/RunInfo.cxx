//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/read_back/RunInfo.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
RunInfo::RunInfo (unsigned run, const std::string& runType, unsigned smk, time_t time)
    : m_run(run)
    , m_runType(runType)
    , m_smk(smk)
    , m_time(time)
{
}

/// default constructor
RunInfo::RunInfo()
    : m_run(0)
    , m_runType()
    , m_smk(0)
    , m_time(0)
{
}

//--------------
// Destructor --
//--------------
RunInfo::~RunInfo () throw()
{
}

// Compare to other object, used for sorting.
bool 
RunInfo::operator< (const RunInfo& rhs) const
{
    if ( this->m_time < rhs.m_time ) return true;
    if ( this->m_time > rhs.m_time ) return false;
    if ( this->m_run < rhs.m_run ) return true;
    if ( this->m_run > rhs.m_run ) return false;
    if ( this->m_runType < rhs.m_runType ) return true;
    if ( this->m_runType > rhs.m_runType ) return false;
    if ( this->m_smk < rhs.m_smk ) return true;
    return false;
}


} // namespace mda
} // namespace daq
