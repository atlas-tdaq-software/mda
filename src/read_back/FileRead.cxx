//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/read_back/FileRead.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/algorithm/string/predicate.hpp>
#include <stdlib.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/DBClient.h"
#include "ers/ers.h"
#include "TEnv.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    // method which guesses whether this binary runs at Point1.
    // Thanks Sergei for idea
    bool runningAtP1()
    {
        const char * instpath = getenv("TDAQ_INST_PATH");
        if (not instpath) {
            return false;
        }

        // file that should exist only at P1
        std::string filename = instpath;
        filename += "/com/ipc_reference_location";
        return ::access(filename.c_str(), R_OK) == 0;
    }


}


//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
FileRead::FileRead ()
    : m_cocaClient(new coca::DBClient(daq::coca::DBBase::defConnStr()))
{
}

FileRead::FileRead (const std::string& cocaConnStr)
    : m_cocaClient(new coca::DBClient(cocaConnStr))
{
}

//--------------
// Destructor --
//--------------
FileRead::~FileRead () throw ()
{
    delete m_cocaClient;
}

TFile*
FileRead::openFile(const std::string& fileName,
                   const std::string& dataset,
                   FindMode mode)
{
    static bool atP1 = ::runningAtP1();

    // get file information from coca
    std::vector<std::string> archiveLocations = m_cocaClient->fileLocations(fileName, dataset, coca::DBClient::LocArchive);
    std::vector<std::string> cacheLocations;
    if (atP1) cacheLocations = m_cocaClient->fileLocations(fileName, dataset, coca::DBClient::LocCache);
    if (archiveLocations.empty() and cacheLocations.empty()) {
        // file name or dataset is wrong
        ERS_DEBUG (2, "cannot find locations dataset=" << dataset << "name=" << fileName);
        return 0;
    }

    TFile* f = 0;

    ERS_DEBUG (0, "FileRead::openFile: running " << (atP1 ? "at Point1" : "outside Point1"));

    // If requested try to open file in CoCa cache first
    if (mode == CacheFirst and not cacheLocations.empty() and atP1) {
        ERS_DEBUG (2, "cache location=" << cacheLocations.front());
        f = TFile::Open(cacheLocations.front().c_str());
    }

    // next try archive location
    if (not f and not archiveLocations.empty()) {
        ERS_DEBUG (2, "archive location=" << archiveLocations.front());
        f = TFile::Open(archiveLocations.front().c_str());
    }

    // try cache location if cache comes after archive
    if (not f and mode != CacheFirst and not cacheLocations.empty() and atP1) {
        ERS_DEBUG (2, "cache location=" << cacheLocations.front());
        f = TFile::Open(cacheLocations.front().c_str());
    }

    return f;
}

} // namespace mda
} // namespace daq
