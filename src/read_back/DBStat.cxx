//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/read_back/DBStat.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ConnectionServiceException.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/Transaction.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
DBStat::DBStat (const std::string& connStr, const std::string& prefix)
    : DBData(connStr, prefix, CacheMode::Cache)
{
}

//--------------
// Destructor --
//--------------
DBStat::~DBStat() throw()
{
}

// Retrieve the number of saved histograms per run for last several runs.
void 
DBStat::nHistoPerRun(std::map<unsigned, StatRun>& stat, int nRuns)
try {
 
    // set a database session
    coral::ISessionProxy& session = DBData::session();

    // start read-only transaction
    Transaction tran(session);

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    // what
    q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
    q->addToTableList(m_fileTbl.tableName(), "FT");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");

    coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.RUN_NUMBER", "RUN");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_HCOUNT)", "CNT");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_VCOUNT)", "VCNT");

    if (nRuns > 0) {
        // construct a table with subquery returning last nRuns run numbers
        coral::IQueryDefinition& sq = q->defineSubQuery("RUNS");
        sq.addToTableList(m_runlbTbl.tableName(), "RUNLB1");
        sq.addToOutputList("RUNLB1.RUN_NUMBER", "RUN") ;
        sq.groupBy("RUNLB1.RUN_NUMBER");
        sq.addToOrderList("MIN(RUNLB1.TS_UTC) DESC");
        sq.limitReturnedRows(nRuns);
        q->addToTableList("RUNS", "RUNS");
    }
    
    // where
    coral::AttributeList data;
    std::string cond;
    if (nRuns > 0) {
        cond = "RUNS.RUN = RUNLB.RUN_NUMBER AND ";
    }
    cond += "RUNLB.RUNLB_ID = FT.RUNLB_ID"
        " AND FT.FILE_ID = HGRP.FILE_ID";
    q->setCondition ( cond, data ) ;

    // group
    q->groupBy("RUNLB.RUN_NUMBER");

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();

        unsigned run = row[0].data<uint32_t>();
        uint32_t count = row[1].data<uint32_t>();
        uint32_t versions = row[2].data<uint32_t>();
        stat.insert (std::make_pair(run, StatRun(count, versions)));
    }
    
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

// Retrieve the number of saved histograms per OHS server.
void 
DBStat::nHistoPerServer(std::map<std::string, StatRun>& stat)
try {
 
    // set a database session
    coral::ISessionProxy& session = DBData::session();

    // start read-only transaction
    Transaction tran(session);

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    q->addToTableList(m_hserverTbl.tableName(), "HSRV");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");

    coral_helpers::defineOutput<std::string>(*q, "HSRV.HSRV_NAME", "HSRV_NAME");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_HCOUNT)", "CNT");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_VCOUNT)", "VCNT");

    // where
    coral::AttributeList data;
    std::string cond = "HSRV.HSRV_ID = HGRP.HSRV_ID";
    q->setCondition ( cond, data ) ;

    // group
    q->groupBy("HSRV.HSRV_NAME");

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();

        const std::string& hsrv = row[0].data<std::string>();
        uint32_t count = row[1].data<uint32_t>();
        uint32_t versions = row[2].data<uint32_t>();
        stat.insert (std::make_pair(hsrv, StatRun(count, versions)));
    }
    
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

// Retrieve the number of saved histograms per OHS provider.
void 
DBStat::nHistoPerProvider(std::map<std::string, StatRun>& stat)
try {
 
    // set a database session
    coral::ISessionProxy& session = DBData::session();

    // start read-only transaction
    Transaction tran(session);

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    q->addToTableList(m_providerTbl.tableName(), "PROV");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");

    coral_helpers::defineOutput<std::string>(*q, "PROV.PROV_NAME", "PROV_NAME");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_HCOUNT)", "CNT");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_VCOUNT)", "VCNT");

    // where
    coral::AttributeList data;
    std::string cond = "PROV.PROV_ID = HGRP.PROV_ID";
    q->setCondition ( cond, data ) ;

    // group
    q->groupBy("PROV.PROV_NAME");

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();

        const std::string& hsrv = row[0].data<std::string>();
        uint32_t count = row[1].data<uint32_t>();
        uint32_t versions = row[2].data<uint32_t>();
        stat.insert (std::make_pair(hsrv, StatRun(count, versions)));
    }
    
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

// Retrieve the number of saved histograms per dataset.
void 
DBStat::nHistoPerDataset(std::map<std::string, StatRun>& stat)
try {
 
    // set a database session
    coral::ISessionProxy& session = DBData::session();

    // start read-only transaction
    Transaction tran(session);

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    q->addToTableList(m_datasetTbl.tableName(), "DS");
    q->addToTableList(m_fileTbl.tableName(), "FT");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");

    coral_helpers::defineOutput<std::string>(*q, "DS.DS_NAME", "DS_NAME");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_HCOUNT)", "CNT");
    coral_helpers::defineOutput<uint32_t>(*q, "SUM(HGRP.STAT_VCOUNT)", "VCNT");

    // where
    coral::AttributeList data;
    std::string cond = "DS.DS_ID = FT.DS_ID AND FT.FILE_ID = HGRP.FILE_ID";
    q->setCondition ( cond, data ) ;

    // group
    q->groupBy("DS.DS_NAME");

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();

        const std::string& hsrv = row[0].data<std::string>();
        uint32_t count = row[1].data<uint32_t>();
        uint32_t versions = row[2].data<uint32_t>();
        stat.insert (std::make_pair(hsrv, StatRun(count, versions)));
    }
    
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

} // namespace mda
} // namespace daq
