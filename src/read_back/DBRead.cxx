// $Id$
// $Name$

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/read_back/DBRead.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <stdio.h>
#include <CoralBase/TimeStamp.h>
#include <RelationalAccess/ConnectionServiceException.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/Transaction.h"
#include "mda/common/coral-helpers.h"
#include "mda/common/LB.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

/*
 * DBRead
 */

DBRead::DBRead (const std::string& connStr, const std::string& prefix)
    : DBData(connStr, prefix, CacheMode::Cache)
{
}

DBRead::~DBRead () throw ()
{
}

coral::ISessionProxy& DBRead::session()
{
    coral::ISessionProxy& session = DBData::session();

    if (m_schemaVersion < 0) {
        // read schema version from database
        Transaction tran(session);
        m_schemaVersion = m_metaTbl.dbSchemaVersion(tran);
        ERS_DEBUG(0, "MDA schema version from database: " << m_schemaVersion);
        if (m_schemaVersion < 0) {
            // means there is no META table, it did not exist in V1
            m_schemaVersion = 1;
            ERS_DEBUG(0, "assume MDA schema version: " << m_schemaVersion);
        }
    }

    return session;
}

/*
 * GetPartitions
 */

void
DBRead::partitions (std::set<std::string>& partitions)
try {

    std::set<std::string> buf;

    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);
    
    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    // what 
    q->addToTableList(m_partitionTbl.tableName(), "PART");
    q->addToTableList(m_runlbTbl.tableName(), "RUNLB");

    coral_helpers::defineOutput<std::string>(*q, "PART.PART_NAME", "PNAME");
    
    // where
    coral::AttributeList data;
    std::string cond = "PART.PART_ID = RUNLB.PART_ID";
    q->setCondition ( cond, data ) ;

    // remove duplicates
    q->setDistinct();

    // adjust cache params for performance
    q->setRowCacheSize(1000);
    q->setMemoryCacheSize(1);

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();
        buf.insert( row[0].data<std::string>() );
    }

    std::swap (buf, partitions);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

/*
 * GetLastRuns
 */

void
DBRead::runs (std::set<RunInfo> &lastRuns,
             const std::string &partition,
             unsigned int nRuns)
try {
    
    std::set<RunInfo> buf;

    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);
    
    // get object IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;

    // new query
    std::unique_ptr<coral::IQuery> q(tran.session().nominalSchema().newQuery() ) ;

    // what
    q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
    q->addToTableList(m_runtypeTbl.tableName(), "RUNTYPE");
    
    coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.RUN_NUMBER", "RUN");
    coral_helpers::defineOutput<std::string>(*q, "RUNTYPE.RUNTYPE_NAME", "RUNTYPE");
    coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.SM_KEY", "SMK");
    coral_helpers::defineOutput<coral::TimeStamp>(*q, "MAX(RUNLB.TS_UTC)", "MAX_TS_UTC");

    // where
    coral::AttributeList data;
    coral_helpers::addAttr(data, "PART_ID", partID);
    std::string cond = "RUNLB.PART_ID = :PART_ID AND RUNTYPE.RUNTYPE_ID = RUNLB.RUNTYPE_ID";
    q->setCondition ( cond, data ) ;

    // how
    q->groupBy("RUNLB.RUN_NUMBER, RUNTYPE.RUNTYPE_NAME, RUNLB.SM_KEY");
    q->addToOrderList("MAX_TS_UTC DESC");
    q->addToOrderList("RUN DESC");

    q->limitReturnedRows(nRuns);

    // adjust cache params for performance
    q->setRowCacheSize(nRuns);
    q->setMemoryCacheSize(1);

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();

        unsigned run = row[0].data<uint32_t>();
        const std::string& runtype = row[1].data<std::string>();
        unsigned smk = row[2].data<uint32_t>();
        const coral::TimeStamp& ts = row[3].data<coral::TimeStamp>();
        time_t time = ts.total_nanoseconds()/1000000000;

        buf.insert (RunInfo(run, runtype, smk, time));
    }

    std::swap (buf, lastRuns);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

void
DBRead::ohsServers (std::set<std::string>& ohsServers,
                   const std::string& partition,
                   unsigned run)
try {

    std::set<std::string> buf;

    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);

    // get object IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    // what
    q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
    q->addToTableList(m_fileTbl.tableName(), "FT");
    q->addToTableList(m_hserverTbl.tableName(), "HSRV");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");

    coral_helpers::defineOutput<std::string>(*q, "HSRV.HSRV_NAME", "HSRVNAME");

    // where
    std::string cond = 
        "RUNLB.PART_ID = :PART_ID"
        " AND RUNLB.RUN_NUMBER = :RUN"
        " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
        " AND FT.FILE_ID = HGRP.FILE_ID"
        " AND HGRP.HSRV_ID = HSRV.HSRV_ID" ;
    coral::AttributeList data;
    coral_helpers::addAttr(data, "PART_ID", partID);
    coral_helpers::addAttr(data, "RUN", run);
    q->setCondition ( cond, data ) ;

    // remove duplicates
    q->setDistinct();

    // adjust cache params for performance
    q->setRowCacheSize(1000);
    q->setMemoryCacheSize(1);

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();
        buf.insert (row[0].data<std::string>());
    }

    std::swap (buf, ohsServers);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

void
DBRead::ohsProviders (std::set<std::string> &ohsProviders,
                      const std::string &partition,
                      unsigned run,
                      const std::string &ohsServer)
try {

    std::set<std::string> buf;
    
    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);

    // get object IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;
    uint32_t ohserverID = m_hserverTbl.find(ohsServer, tran);
    if ( ohserverID == 0 ) return ;

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    // what
    q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
    q->addToTableList(m_fileTbl.tableName(), "FT");
    q->addToTableList(m_providerTbl.tableName(), "PROV");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");

    coral_helpers::defineOutput<std::string>(*q, "PROV.PROV_NAME", "PROVNAME");

    // where
    std::string cond = 
        "RUNLB.PART_ID = :PART_ID"
        " AND RUNLB.RUN_NUMBER = :RUN"
        " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
        " AND FT.FILE_ID = HGRP.FILE_ID"
        " AND HGRP.HSRV_ID = :HSRV_ID"
        " AND HGRP.PROV_ID = PROV.PROV_ID" ;
    coral::AttributeList data;
    coral_helpers::addAttr(data, "PART_ID", partID);
    coral_helpers::addAttr(data, "RUN", run);
    coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
    q->setCondition ( cond, data ) ;

    // remove duplicates
    q->setDistinct();

    // adjust cache params for performance
    q->setRowCacheSize(1000);
    q->setMemoryCacheSize(1);

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();
        buf.insert (row[0].data<std::string>());
    }

    std::swap (buf, ohsProviders);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}


/**
 * Get the list of histograms names for a given run/server/provider.
 * All histograms below the given folder will be returned (recursive
 * behavior), and the returned names do not include leading folder name.
 */
void
DBRead::histoNames (std::set<std::string>& histograms,
                    const std::string& partition,
                    unsigned run,
                    const std::string& ohsServer,
                    const std::string& ohsProvider,
                    const std::string& folder)
try {

    ERS_DEBUG(3, "DBRead::histoNames: with partition=" << partition << " run=" << run
          << " ohsServer=" << ohsServer << " ohsProvider=" << ohsProvider << " folder=" << folder);

    // Fix folder name, add slash at front, strip trailing slash.
    // Empty should remain empty, '/' will become empty too.
    std::string hfolder = folder ;
    if (not hfolder.empty()) {
        if (hfolder[0] != '/') hfolder.insert(0,1,'/');
        if (hfolder[hfolder.size()-1] == '/') hfolder.erase(hfolder.size()-1) ;
    }

    ERS_DEBUG(3, "DBRead::histoNames: hfolder=" << hfolder);

    std::set<std::string> buf;
    
    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);

    // get object IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;
    uint32_t ohserverID = m_hserverTbl.find(ohsServer, tran);
    if ( ohserverID == 0 ) return ;
    uint32_t providerID = m_providerTbl.find(ohsProvider, tran);
    if ( providerID == 0 ) return ;

    ERS_DEBUG(3, "DBRead::histoNames: partID=" << partID << " ohserverID=" << ohserverID << " providerID=" << providerID);

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    // what
    q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
    q->addToTableList(m_fileTbl.tableName(), "FT");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
    q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");
    q->addToTableList(m_histoTbl.tableName(), "HIST");
    q->addToTableList(m_hnameTbl.tableName(), "HNAM");
    q->addToTableList(m_folderTbl.tableName(), "FLDR");

    coral_helpers::defineOutput<std::string>(*q, "HNAM.HNAM_NAME", "HNAME");
    coral_helpers::defineOutput<std::string>(*q, "FLDR.FLDR_NAME", "FNAME");

    // where
    std::string cond = 
        "RUNLB.PART_ID = :PART_ID"
        " AND RUNLB.RUN_NUMBER = :RUN"
        " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
        " AND FT.FILE_ID = HGRP.FILE_ID"
        " AND HGRP.HSRV_ID = :HSRV_ID"
        " AND HGRP.PROV_ID = :PROV_ID"
        " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
        " AND HG2HI.HIST_ID = HIST.HIST_ID"
        " AND HIST.HNAM_ID = HNAM.HNAM_ID"
        " AND HIST.FLDR_ID = FLDR.FLDR_ID" ;
    coral::AttributeList data;
    coral_helpers::addAttr(data, "PART_ID", partID);
    coral_helpers::addAttr(data, "RUN", run);
    coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
    coral_helpers::addAttr(data, "PROV_ID", providerID);
    if ( not hfolder.empty() ) {
        cond += " AND SUBSTR(FLDR.FLDR_NAME, 1, :FOLDER_LEN) = :FOLDER";
        coral_helpers::addAttr(data, "FOLDER_LEN", hfolder.size());
        coral_helpers::addAttr(data, "FOLDER", hfolder);
    }
    q->setCondition ( cond, data ) ;

    // remove duplicates
    q->setDistinct();

    // adjust cache params for performance
    q->setRowCacheSize(100000);
    q->setMemoryCacheSize(1);

    // execute
    ERS_DEBUG(3, "DBRead::histoNames: start query");
    coral::ICursor& cursor = q->execute() ;
    ERS_DEBUG(3, "DBRead::histoNames: after execute");
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();

        std::string hpath = row[1].data<std::string>();

        // Filter out folders which are not sub-folders of the hfolder.
        // E.g. hfolder='/a/b/c' can match '/a/b/c/d' and '/a/b/cde/f',
        // we need to keep only former and filter-out latter.
        if ( hpath.size() > hfolder.size() and hpath[hfolder.size()] != '/' ) continue ;

        if (hpath != "/") hpath += '/' ;
        hpath += row[0].data<std::string>();
        hpath.erase(0,hfolder.size()+1);
        buf.insert (hpath);
    }
    ERS_DEBUG(3, "DBRead::histoNames: finish query");

    std::swap (buf, histograms);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

/*
 * ListFolder
 */

void
DBRead::listdir (std::set<std::string>& folders,
                 std::set<std::string>& histograms,
                 const std::string& partition,
                 unsigned run,
                 const std::string& ohsServer,
                 const std::string& ohsProvider,
                 const std::string& folder)
{

    // Fix folder name, add slash at front, strip trailing slash.
    // Empty should remain empty, '/' will become empty too.
    std::string hfolder = folder ;
    if (not hfolder.empty()) {
        if (hfolder[0] != '/') hfolder.insert(0,1,'/');
        if (hfolder[hfolder.size()-1] == '/') hfolder.erase(hfolder.size()-1) ;
    }

    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);

    // get object IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;
    uint32_t ohserverID = m_hserverTbl.find(ohsServer, tran);
    if ( ohserverID == 0 ) return ;
    uint32_t providerID = m_providerTbl.find(ohsProvider, tran);
    if ( providerID == 0 ) return ;

    // Get histograms
    std::set<std::string> histoBuf;
    try {

        // new query
        std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

        // what
        q->addToTableList(m_hnameTbl.tableName(), "HNAM");
        q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
        q->addToTableList(m_fileTbl.tableName(), "FT");
        q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
        q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");
        q->addToTableList(m_histoTbl.tableName(), "HIST");
        q->addToTableList(m_folderTbl.tableName(), "FLDR");

        coral_helpers::defineOutput<std::string>(*q, "HNAM.HNAM_NAME", "HNAME");

        // where
        std::string cond = 
            "RUNLB.PART_ID = :PART_ID"
            " AND RUNLB.RUN_NUMBER = :RUN"
            " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
            " AND FT.FILE_ID = HGRP.FILE_ID"
            " AND HGRP.HSRV_ID = :HSRV_ID"
            " AND HGRP.PROV_ID = :PROV_ID"
            " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
            " AND HG2HI.HIST_ID = HIST.HIST_ID"
            " AND HIST.HNAM_ID = HNAM.HNAM_ID"
            " AND HIST.FLDR_ID = FLDR.FLDR_ID"
            " AND FLDR.FLDR_NAME = :FOLDER";
        coral::AttributeList data;
        coral_helpers::addAttr(data, "PART_ID", partID);
        coral_helpers::addAttr(data, "RUN", run);
        coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
        coral_helpers::addAttr(data, "PROV_ID", providerID);
        coral_helpers::addAttr(data, "FOLDER", hfolder.empty() ? std::string("/") : hfolder);
        q->setCondition ( cond, data ) ;

        // remove duplicates
        q->setDistinct();

        // adjust cache params for performance
        q->setRowCacheSize(10000);
        q->setMemoryCacheSize(1);

        // execute
        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();

            histoBuf.insert (row[0].data<std::string>());
        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex);
    }


    // Get folders
    std::set<std::string> foldersBuf;
    try {

        // new query
        std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

        // what
        q->addToTableList(m_folderTbl.tableName(), "FLDR");
        q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
        q->addToTableList(m_fileTbl.tableName(), "FT");
        q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
        q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");
        q->addToTableList(m_histoTbl.tableName(), "HIST");

        coral_helpers::defineOutput<std::string>(*q, "FLDR.FLDR_NAME", "FLDRNAME");

        // where
        std::string cond = 
            "RUNLB.PART_ID = :PART_ID"
            " AND RUNLB.RUN_NUMBER = :RUN"
            " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
            " AND FT.FILE_ID = HGRP.FILE_ID"
            " AND HGRP.HSRV_ID = :HSRV_ID"
            " AND HGRP.PROV_ID = :PROV_ID"
            " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
            " AND HG2HI.HIST_ID = HIST.HIST_ID"
            " AND HIST.FLDR_ID = FLDR.FLDR_ID";
        coral::AttributeList data;
        coral_helpers::addAttr(data, "PART_ID", partID);
        coral_helpers::addAttr(data, "RUN", run);
        coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
        coral_helpers::addAttr(data, "PROV_ID", providerID);
        if ( hfolder == "/" ) hfolder.clear() ;
        if ( not hfolder.empty() ) {
            cond += " AND SUBSTR(FLDR.FLDR_NAME, 1, :FOLDER_LEN) = :FOLDER";
            coral_helpers::addAttr(data, "FOLDER_LEN", hfolder.size());
            coral_helpers::addAttr(data, "FOLDER", hfolder);
        }
        q->setCondition ( cond, data ) ;

        // remove duplicates
        q->setDistinct();

        // adjust cache params for performance
        q->setRowCacheSize(10000);
        q->setMemoryCacheSize(1);

        // execute
        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();

            std::string hpath = row[0].data<std::string>() ;

            if ( hpath.size() > hfolder.size() ) {

                // Filter out folders which are not sub-folders of the hfolder.
                // E.g. hfolder='/a/b/c' can match '/a/b/c/d' and '/a/b/cde/f',
                // we need to keep only former and filter-out latter.
                if ( hpath[hfolder.size()] != '/' ) continue ;

                hpath.erase(0,hfolder.size()+1);
                std::string::size_type p = hpath.find('/');
                if ( p != std::string::npos ) hpath.erase(p);
                if (not hpath.empty()) foldersBuf.insert (hpath);

            }

        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex);
    }

    std::swap (histoBuf, histograms);
    std::swap (foldersBuf, folders);
}

/**
 *  Get the list of the histogram versions (lumi blocks) and their
 *  corresponding locations files for a given histogram name.
 */
void
DBRead::histogram (std::set<HistoFile>& histograms,
                   const std::string& partition,
                   unsigned run,
                   const std::string& ohsServer,
                   const std::string& ohsProvider,
                   const std::string& histogram)
{

    // fix histo name, add slash at front
    std::string histo = histogram ;
    if ( histo.empty() or histo[0] != '/' ) histo.insert(0,1,'/');

    // split histo name
    std::string::size_type p = histo.rfind('/');
    std::string folder (histo, 0, p);
    if (folder.empty()) folder = '/';
    std::string hnam (histo, p+1);

    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);

    // find histo ID
    uint32_t hnamID = m_hnameTbl.find(hnam, tran);
    if ( hnamID == 0 ) return ;
    uint32_t fldrID = m_folderTbl.find(folder, tran);
    if ( fldrID == 0 ) return ;
    uint32_t histoId = m_histoTbl.find(hnamID, fldrID, tran);
    if ( histoId == 0 ) return ;

    // get other IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;
    uint32_t ohserverID = m_hserverTbl.find(ohsServer, tran);
    if ( ohserverID == 0 ) return ;
    uint32_t providerID = m_providerTbl.find(ohsProvider, tran);
    if ( providerID == 0 ) return ;


    std::set<HistoFile> buf;

    /*
     * First get all the lumi blocks when the data was stored
     */
    try {

        // new query
        std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

        // what
        q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
        q->addToTableList(m_datasetTbl.tableName(), "DS");
        q->addToTableList(m_fileTbl.tableName(), "FT");
        q->addToTableList(m_archiveTbl.tableName(), "ARCH");
        q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
        q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");

        coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.LB_NUMBER", "LB");
        coral_helpers::defineOutput<std::string>(*q, "DS.DS_NAME", "DSNAME");
        coral_helpers::defineOutput<std::string>(*q, "FT.FILE_NAME", "FILENAME");
        coral_helpers::defineOutput<std::string>(*q, "ARCH.ARCH_NAME", "ARCHNAME");

        // where
        std::string cond = 
            "RUNLB.PART_ID = :PART_ID"
            " AND RUNLB.RUN_NUMBER = :RUN"
            " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
            " AND FT.ARCH_ID = ARCH.ARCH_ID"
            " AND FT.DS_ID = DS.DS_ID"
            " AND FT.FILE_ID = HGRP.FILE_ID"
            " AND HGRP.HSRV_ID = :HSRV_ID"
            " AND HGRP.PROV_ID = :PROV_ID"
            " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
            " AND HG2HI.HIST_ID = :HIST_ID";
        coral::AttributeList data;
        coral_helpers::addAttr(data, "PART_ID", partID);
        coral_helpers::addAttr(data, "RUN", run);
        coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
        coral_helpers::addAttr(data, "PROV_ID", providerID);
        coral_helpers::addAttr(data, "HIST_ID", histoId);
        q->setCondition ( cond, data ) ;

        // remove duplicates
        q->setDistinct();

        // adjust cache params for performance
        q->setRowCacheSize(10000);
        q->setMemoryCacheSize(1);

        // execute
        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();

            std::string path("/");
            path += ohsServer;
            path += '/';
            path += ohsProvider;
            path += histo;

            // we replace colons with underscores when write histograms to ROOT file,
            // but database may still contain colons in the names, replace them here
            std::replace (path.begin(), path.end(), ':', '_' );

            unsigned lb = LB::LastLB;
            if (not row[0].isNull()) lb = row[0].data<uint32_t>();
            const std::string& ds = row[1].data<std::string>();
            const std::string& fname = row[2].data<std::string>();
            const std::string& archive = row[3].data<std::string>();

            HistoFile hfile(histo, lb, ds, archive, fname, path);
            buf.insert (hfile);

        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex);
    }


    /*
     *  get also the lumi block from the history tables, for those
     *  the histograms are stored under /run_12345/lb_123/ directories
     */
    try {

        // new query
        std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

        // what
        q->addToTableList(m_lbRangeTbl.tableName(), "LBRNG");
        q->addToTableList(m_lbSeqTbl.tableName(), "LBSEQ");
        q->addToTableList(m_datasetTbl.tableName(), "DS");
        q->addToTableList(m_fileTbl.tableName(), "FT");
        q->addToTableList(m_archiveTbl.tableName(), "ARCH");
        q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
        q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
        q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");

        coral_helpers::defineOutput<uint32_t>(*q, "LBRNG.LBRNG_MIN", "LBMIN");
        coral_helpers::defineOutput<uint32_t>(*q, "LBRNG.LBRNG_MAX", "LBMAX");
        coral_helpers::defineOutput<std::string>(*q, "DS.DS_NAME", "DSNAME");
        coral_helpers::defineOutput<std::string>(*q, "FT.FILE_NAME", "FILENAME");
        coral_helpers::defineOutput<std::string>(*q, "ARCH.ARCH_NAME", "ARCHNAME");
        coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.LB_NUMBER", "LB_NUMBER");

        // where
        std::string cond = 
            "RUNLB.PART_ID = :PART_ID"
            " AND RUNLB.RUN_NUMBER = :RUN"
            " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
            " AND FT.ARCH_ID = ARCH.ARCH_ID"
            " AND FT.DS_ID = DS.DS_ID"
            " AND FT.FILE_ID = HGRP.FILE_ID"
            " AND HGRP.HSRV_ID = :HSRV_ID"
            " AND HGRP.PROV_ID = :PROV_ID"
            " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
            " AND HG2HI.HIST_ID = :HIST_ID"
            " AND HG2HI.LBSQ_ID = LBSEQ.LBSQ_ID"
            " AND LBRNG.LBSQ_ID = LBSEQ.LBSQ_ID" ;
        coral::AttributeList data;
        coral_helpers::addAttr(data, "PART_ID", partID);
        coral_helpers::addAttr(data, "RUN", run);
        coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
        coral_helpers::addAttr(data, "PROV_ID", providerID);
        coral_helpers::addAttr(data, "HIST_ID", histoId);
        q->setCondition ( cond, data ) ;

        // remove duplicates
        q->setDistinct();

        // adjust cache params for performance
        q->setRowCacheSize(10000);
        q->setMemoryCacheSize(1);

        // map lb number (when histogram was saved) to list of histograms
        std::map<uint32_t, std::set<HistoFile> > lb2histos;

        // execute
        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();

            uint32_t lbmin = row[0].data<uint32_t>() ;
            uint32_t lbmax = row[1].data<uint32_t>() ;

            std::string path(ohsServer);
            path += '/';
            path += ohsProvider;
            path += histo;

            // we replace colons with underscores when write histograms to ROOT file,
            // but database may still contain colons in the names, replace them here
            std::replace (path.begin(), path.end(), ':', '_' );

            const std::string& ds = row[2].data<std::string>();
            const std::string& fname = row[3].data<std::string>();
            const std::string& archive = row[4].data<std::string>();
            unsigned lbSaved = LB::LastLB;
            if (not row[5].isNull()) lbSaved = row[5].data<uint32_t>() ;

            for ( uint32_t lb = lbmin ; lb <= lbmax ; ++ lb ) {
                char runplbfx[64];
                snprintf( runplbfx, sizeof runplbfx, "/run_%d/lb_%d/", run, lb);
                HistoFile hfile(histo, lb, ds, archive, fname, runplbfx+path);
                lb2histos[lbSaved].insert(hfile);
            }

        }

        // post-processing: some providers save/update histograms with tag=0,
        // we want to filter some of those. Here I remove histogram with tag=0
        // only if it was the only tag saved for that histogram at that lumi block
        for (std::map<uint32_t, std::set<HistoFile> >::iterator it = lb2histos.begin(); it != lb2histos.end(); ++ it) {
            std::set<HistoFile>& histos = it->second;
            if (histos.size() != 1 or histos.begin()->lb() != 0) {
                // copy that to destination
                buf.insert(histos.begin(), histos.end());
            }
        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex);
    }

    std::swap (buf, histograms);
}


/**
 *  Get the list of the histogram versions (lumi blocks) and their
 *  corresponding locations for a given fodler name.
 */
void
DBRead::histograms(std::set<HistoFile>& histograms,
                   const std::string& partition,
                   unsigned run,
                   const std::string& ohsServer,
                   const std::string& ohsProvider,
                   const std::string& folder)
{

    // fix folder name, add slash at front
    std::string fldr = folder;
    if (fldr.empty()) fldr = '/';

    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);

    // find histo ID
    uint32_t fldrID = m_folderTbl.find(folder, tran);
    if ( fldrID == 0 ) return ;

    // get other IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;
    uint32_t ohserverID = m_hserverTbl.find(ohsServer, tran);
    if ( ohserverID == 0 ) return ;
    uint32_t providerID = m_providerTbl.find(ohsProvider, tran);
    if ( providerID == 0 ) return ;


    std::set<HistoFile> buf;

    /*
     * First get all the lumi blocks when the data was stored
     */
    try {

        // new query
        std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

        // what
        q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
        q->addToTableList(m_datasetTbl.tableName(), "DS");
        q->addToTableList(m_fileTbl.tableName(), "FT");
        q->addToTableList(m_archiveTbl.tableName(), "ARCH");
        q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
        q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");
        q->addToTableList(m_histoTbl.tableName(), "HIST");
        q->addToTableList(m_hnameTbl.tableName(), "HNAM");

        coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.LB_NUMBER", "LB");
        coral_helpers::defineOutput<std::string>(*q, "DS.DS_NAME", "DSNAME");
        coral_helpers::defineOutput<std::string>(*q, "FT.FILE_NAME", "FILENAME");
        coral_helpers::defineOutput<std::string>(*q, "ARCH.ARCH_NAME", "ARCHNAME");
        coral_helpers::defineOutput<std::string>(*q, "HNAM.HNAM_NAME", "HISTNAME");

        // where
        std::string cond =
            "RUNLB.PART_ID = :PART_ID"
            " AND RUNLB.RUN_NUMBER = :RUN"
            " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
            " AND FT.ARCH_ID = ARCH.ARCH_ID"
            " AND FT.DS_ID = DS.DS_ID"
            " AND FT.FILE_ID = HGRP.FILE_ID"
            " AND HGRP.HSRV_ID = :HSRV_ID"
            " AND HGRP.PROV_ID = :PROV_ID"
            " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
            " AND HG2HI.HIST_ID = HIST.HIST_ID"
            " AND HIST.HNAM_ID = HNAM.HNAM_ID"
            " AND HIST.FLDR_ID = :FLDR_ID";
        coral::AttributeList data;
        coral_helpers::addAttr(data, "PART_ID", partID);
        coral_helpers::addAttr(data, "RUN", run);
        coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
        coral_helpers::addAttr(data, "PROV_ID", providerID);
        coral_helpers::addAttr(data, "FLDR_ID", fldrID);
        q->setCondition ( cond, data ) ;

        // remove duplicates
        q->setDistinct();

        // adjust cache params for performance
        q->setRowCacheSize(10000);
        q->setMemoryCacheSize(1);

        // execute
        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();

            unsigned lb = LB::LastLB;
            if (not row[0].isNull()) lb = row[0].data<uint32_t>();
            const std::string& ds = row[1].data<std::string>();
            const std::string& fname = row[2].data<std::string>();
            const std::string& archive = row[3].data<std::string>();
            const std::string& histo = fldr + "/" + row[4].data<std::string>();

            std::string path("/");
            path += ohsServer;
            path += '/';
            path += ohsProvider;
            path += histo;

            // we replace colons with underscores when write histograms to ROOT file,
            // but database may still contain colons in the names, replace them here
            std::replace (path.begin(), path.end(), ':', '_' );

            HistoFile hfile(histo, lb, ds, archive, fname, path);
            buf.insert (hfile);

        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex);
    }


    /*
     *  get also the lumi block from the history tables, for those
     *  the histograms are stored under /run_12345/lb_123/ directories
     */
    if (not buf.empty()) {
        // if previous query did not find any histograms then this one is guaranteed to return nothing too
        try {

            // new query
            std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

            // what
            q->addToTableList(m_lbRangeTbl.tableName(), "LBRNG");
            q->addToTableList(m_lbSeqTbl.tableName(), "LBSEQ");
            q->addToTableList(m_datasetTbl.tableName(), "DS");
            q->addToTableList(m_fileTbl.tableName(), "FT");
            q->addToTableList(m_archiveTbl.tableName(), "ARCH");
            q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
            q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
            q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");
            q->addToTableList(m_histoTbl.tableName(), "HIST");
            q->addToTableList(m_hnameTbl.tableName(), "HNAM");

            coral_helpers::defineOutput<uint32_t>(*q, "LBRNG.LBRNG_MIN", "LBMIN");
            coral_helpers::defineOutput<uint32_t>(*q, "LBRNG.LBRNG_MAX", "LBMAX");
            coral_helpers::defineOutput<std::string>(*q, "DS.DS_NAME", "DSNAME");
            coral_helpers::defineOutput<std::string>(*q, "FT.FILE_NAME", "FILENAME");
            coral_helpers::defineOutput<std::string>(*q, "ARCH.ARCH_NAME", "ARCHNAME");
            coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.LB_NUMBER", "LB_NUMBER");
            coral_helpers::defineOutput<std::string>(*q, "HNAM.HNAM_NAME", "HISTNAME");

            // where
            std::string cond =
                "RUNLB.PART_ID = :PART_ID"
                " AND RUNLB.RUN_NUMBER = :RUN"
                " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
                " AND FT.ARCH_ID = ARCH.ARCH_ID"
                " AND FT.DS_ID = DS.DS_ID"
                " AND FT.FILE_ID = HGRP.FILE_ID"
                " AND HGRP.HSRV_ID = :HSRV_ID"
                " AND HGRP.PROV_ID = :PROV_ID"
                " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
                " AND HG2HI.HIST_ID = HIST.HIST_ID"
                " AND HIST.HNAM_ID = HNAM.HNAM_ID"
                " AND HIST.FLDR_ID = :FLDR_ID"
                " AND HG2HI.LBSQ_ID = LBSEQ.LBSQ_ID"
                " AND LBRNG.LBSQ_ID = LBSEQ.LBSQ_ID";
            coral::AttributeList data;
            coral_helpers::addAttr(data, "PART_ID", partID);
            coral_helpers::addAttr(data, "RUN", run);
            coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
            coral_helpers::addAttr(data, "PROV_ID", providerID);
            coral_helpers::addAttr(data, "FLDR_ID", fldrID);
            q->setCondition ( cond, data ) ;

            // remove duplicates
            q->setDistinct();

            // adjust cache params for performance
            q->setRowCacheSize(10000);
            q->setMemoryCacheSize(1);

            // map lb number (when histogram was saved) to list of histograms
            std::map<uint32_t, std::set<HistoFile> > lb2histos;

            // execute
            coral::ICursor& cursor = q->execute() ;
            while ( cursor.next() ) {
                const coral::AttributeList& row = cursor.currentRow();

                uint32_t lbmin = row[0].data<uint32_t>() ;
                uint32_t lbmax = row[1].data<uint32_t>() ;

                const std::string& ds = row[2].data<std::string>();
                const std::string& fname = row[3].data<std::string>();
                const std::string& archive = row[4].data<std::string>();
                unsigned lbSaved = LB::LastLB;
                if (not row[5].isNull()) lbSaved = row[5].data<uint32_t>() ;
                const std::string& histo = fldr + "/" + row[6].data<std::string>();

                std::string path(ohsServer);
                path += '/';
                path += ohsProvider;
                path += histo;

                // we replace colons with underscores when write histograms to ROOT file,
                // but database may still contain colons in the names, replace them here
                std::replace (path.begin(), path.end(), ':', '_' );

                for ( uint32_t lb = lbmin ; lb <= lbmax ; ++ lb ) {
                    char runplbfx[64];
                    snprintf( runplbfx, sizeof runplbfx, "/run_%d/lb_%d/", run, lb);
                    HistoFile hfile(histo, lb, ds, archive, fname, runplbfx+path);
                    lb2histos[lbSaved].insert(hfile);
                }

            }

            // post-processing: some providers save/update histograms with tag=0,
            // we want to filter some of those. Here I remove histogram with tag=0
            // only if it was the only tag saved for that histogram at that lumi block
            for (std::map<uint32_t, std::set<HistoFile> >::iterator it = lb2histos.begin(); it != lb2histos.end(); ++ it) {
                std::set<HistoFile>& histos = it->second;
                if (histos.size() != 1 or histos.begin()->lb() != 0) {
                    // copy that to destination
                    buf.insert(histos.begin(), histos.end());
                }
            }

        } catch (const coral::ConnectionNotAvailableException& ex) {
            throw Issues::DBConnectionFailure(ERS_HERE, ex);
        } catch (const coral::Exception& ex) {
            throw Issues::CoralException(ERS_HERE, ex);
        }
    }

    std::swap (buf, histograms);
}


/*
 * WhenAvailable
 */

void
DBRead::available (std::set<RunLB>& timestamps,
                   const std::string& partition,
                   unsigned since,
                   unsigned until,
                   const std::string& ohsServer,
                   const std::string& ohsProvider,
                   const std::string& histogram)
{
    // fix histo name, add slash at front
    std::string histo = histogram ;
    if ( histo.empty() or histo[0] != '/' ) histo.insert(0,1,'/');

    // split histo name
    std::string::size_type p = histo.rfind('/');
    std::string folder (histo, 0, p == 0 ? 1 : p);
    std::string hnam (histo, p+1);

    // set a database session
    coral::ISessionProxy& session = this->session();

    // start read-only transaction
    Transaction tran(session);

    // find histo ID
    uint32_t hnamID = m_hnameTbl.find(hnam, tran);
    if ( hnamID == 0 ) return ;
    uint32_t fldrID = m_folderTbl.find(folder, tran);
    if ( fldrID == 0 ) return ;
    uint32_t histoId = m_histoTbl.find(hnamID, fldrID, tran);
    if ( histoId == 0 ) return ;

    // get other IDs
    uint32_t partID = m_partitionTbl.find(partition, tran);
    if ( partID == 0 ) return ;
    uint32_t ohserverID = m_hserverTbl.find(ohsServer, tran);
    if ( ohserverID == 0 ) return ;
    uint32_t providerID = m_providerTbl.find(ohsProvider, tran);
    if ( providerID == 0 ) return ;

    std::set<RunLB> buf;

    /*
     * First get the lumi blocks when the data was stored
     */
    try {

        // new query
        std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

        // what
        q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
        q->addToTableList(m_fileTbl.tableName(), "FT");
        q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
        q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");

        coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.RUN_NUMBER", "RUN");
        coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.LB_NUMBER", "LB");
        coral_helpers::defineOutput<coral::TimeStamp>(*q, "RUNLB.TS_UTC", "TS_UTC");

        // where
        std::string cond = 
            "RUNLB.PART_ID = :PART_ID"
            " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
            " AND FT.FILE_ID = HGRP.FILE_ID"
            " AND HGRP.HSRV_ID = :HSRV_ID"
            " AND HGRP.PROV_ID = :PROV_ID"
            " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
            " AND HG2HI.HIST_ID = :HIST_ID";
        coral::AttributeList data;
        coral_helpers::addAttr(data, "PART_ID", partID);
        coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
        coral_helpers::addAttr(data, "PROV_ID", providerID);
        coral_helpers::addAttr(data, "HIST_ID", histoId);
        if ( since > 0 ) {
            cond += " AND RUNLB.RUN_NUMBER >= :RUN_SINCE";
            coral_helpers::addAttr(data, "RUN_SINCE", since);
        }
        if ( until < 0xFFFFFFFF ) {
            cond += " AND RUNLB.RUN_NUMBER <= :RUN_UNTIL";
            coral_helpers::addAttr(data, "RUN_UNTIL", until);
        }
        q->setCondition ( cond, data ) ;

        // adjust cache params for performance
        q->setRowCacheSize(50000);
        q->setMemoryCacheSize(1);

        // execute
        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();

            unsigned run = row[0].data<uint32_t>() ;
            unsigned lb = LB::LastLB;
            if (not row[1].isNull()) lb = row[1].data<uint32_t>() ;
            const coral::TimeStamp& ts = row[2].data<coral::TimeStamp>();
            time_t time = ts.total_nanoseconds()/1000000000;
            RunLB tmp (run, lb, time);
            buf.insert (tmp);
        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex);
    }


    /*
     * Extend it with the history lumi blocks (LBSEQ/LBRANGE tables)
     */
    try {

        // new query
        std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

        // what
        q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
        q->addToTableList(m_lbRangeTbl.tableName(), "LBRNG");
        q->addToTableList(m_lbSeqTbl.tableName(), "LBSEQ");
        q->addToTableList(m_fileTbl.tableName(), "FT");
        q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
        q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");

        coral_helpers::defineOutput<uint32_t>(*q, "RUNLB.RUN_NUMBER", "RUN");
        coral_helpers::defineOutput<uint32_t>(*q, "LBRNG.LBRNG_MIN", "LBMIN");
        coral_helpers::defineOutput<uint32_t>(*q, "LBRNG.LBRNG_MAX", "LBMAX");
        coral_helpers::defineOutput<coral::TimeStamp>(*q, "RUNLB.TS_UTC", "TS_UTC");

        // where
        std::string cond = 
            "RUNLB.PART_ID = :PART_ID"
            " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
            " AND FT.FILE_ID = HGRP.FILE_ID"
            " AND HGRP.HSRV_ID = :HSRV_ID"
            " AND HGRP.PROV_ID = :PROV_ID"
            " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
            " AND HG2HI.HIST_ID = :HIST_ID"
            " AND HG2HI.LBSQ_ID = LBSEQ.LBSQ_ID"
            " AND LBRNG.LBSQ_ID = LBSEQ.LBSQ_ID" ;
        coral::AttributeList data;
        coral_helpers::addAttr(data, "PART_ID", partID);
        coral_helpers::addAttr(data, "HSRV_ID", ohserverID);
        coral_helpers::addAttr(data, "PROV_ID", providerID);
        coral_helpers::addAttr(data, "HIST_ID", histoId);
        if ( since > 0 ) {
            cond += " AND RUNLB.RUN_NUMBER >= :RUN_SINCE";
            coral_helpers::addAttr(data, "RUN_SINCE", since);
        }
        if ( until < 0xFFFFFFFF ) {
            cond += " AND RUNLB.RUN_NUMBER <= :RUN_UNTIL";
            coral_helpers::addAttr(data, "RUN_UNTIL", until);
        }
        q->setCondition ( cond, data ) ;

        // adjust cache params for performance
        q->setRowCacheSize(50000);
        q->setMemoryCacheSize(1);

        // execute
        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();

            unsigned run = row[0].data<uint32_t>() ;
            uint32_t lbmin = row[1].data<uint32_t>() ;
            uint32_t lbmax = row[2].data<uint32_t>() ;
            const coral::TimeStamp& ts = row[3].data<coral::TimeStamp>();
            time_t time = ts.total_nanoseconds()/1000000000;
            for ( uint32_t lb = lbmin ; lb <= lbmax ; ++ lb ) {
                buf.insert (RunLB(run, lb, time));
            }
        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex);
    }

    std::swap (buf, timestamps);
}

