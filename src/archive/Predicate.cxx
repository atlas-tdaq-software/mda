//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/Predicate.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <time.h>
#include <errno.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

void
Predicate::wait (const std::chrono::milliseconds& pollTime) const
{
    // make big timeout
    std::chrono::hours week(7*24);
    
    // loop forever (until it returns true)
    while (not timedWait(week, pollTime)) {
        ;
    }
}

bool
Predicate::timedWait (const std::chrono::milliseconds& timeout,
        const std::chrono::milliseconds& pollTime) const
{
    for (std::chrono::milliseconds passed = std::chrono::milliseconds::zero(); 
        passed < timeout; passed += pollTime) {
        
        if ( (*this)() ) {
            return true;
        }

        // sleep for pollTime
        struct timespec tsrem = {pollTime.count()/1000, (pollTime.count()%1000)*1000000};
        while (true) {
            int rc = nanosleep(&tsrem, &tsrem);
            // if sleep was interrupted by signal restart it 
            if (not (rc == -1 and errno == EINTR)) break;
        }
        
    }
    
    // timeout expired
    return false;
}

} // namespace mda
} // namespace daq
