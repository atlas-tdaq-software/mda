//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/MDAReceiver.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cstdio>
#include <TMap.h>
#include <TObjArray.h>
#include <TObjString.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "oh/OHUtil.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// join paths to make full path name, insert slashes where needed
std::string pathJoin(const std::string& dir, const std::string& name)
{
    std::string path;
    if (not dir.empty() and dir[0] != '/') {
        path += '/';
    }
    path += dir;

    if (path.empty() or path[path.size()-1] != '/') {
        if (not name.empty() and name[0] != '/') {
            path += '/';
        }
    }
    path += name;

    return path;
}

}

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

MDAReceiver::MDAReceiver ( const std::string & filename,
                           int compression,
                           bool deleteAfterSaving,
                           int runNumber,
                           bool storeAnnotations )
    : m_file (filename, "NEW", filename, compression)
    , m_error()
    , m_topDir()
    , m_runPrefix()
    , m_deleteAfterSaving (deleteAfterSaving)
    , m_lastName()
    , m_lastTag(-1)
    , m_storeAnnotations(storeAnnotations)
    , m_annotations()
{
    m_runPrefix = "run_" + std::to_string(runNumber);
}

MDAReceiver::~MDAReceiver () throw ()
{
}

void
MDAReceiver::setTopDirectory (const std::string &OHSServer,
                              const std::string &Provider)
{
    m_topDir = OHSServer + "/" + Provider;
}

void
MDAReceiver::archive (TNamed *obj, int tag, const Annotations& annotations)
{
    ERS_DEBUG (3, "original name: " << obj->GetName() << " tag: " << tag );
    std::string fullname = obj->GetName();
    // strip server/provider names from the full name
    std::string obj_name = ::oh::util::get_object_name (fullname);
    // set stripped name as actual object name, this will include directories
    obj->SetName (obj_name.c_str());

    // directory name for the object, will be prefixed with /run_NNN/lb/_MMM/
    // for the versioned histograms
    std::string dir;
    if ( tag > -1 and m_versions > 1 ) {
        dir = m_runPrefix + "/lb_" + std::to_string(tag) + "/" + m_topDir;
    } else {
        dir = m_topDir;
    }

    ERS_DEBUG (3, "archiving " << obj->GetName() << " under " << dir);
    try {
        // store in a file, this will change object name
        m_file.write (*obj, dir);
        if (m_storeAnnotations and not annotations.empty()) {
            std::string path = ::pathJoin(dir, obj_name);
            ERS_DEBUG (2, "archiving annotations for " << path);
            m_annotations.insert(std::make_pair(path, annotations));
        }
    } catch (const RootFileIssues::Exception &ex) {
        m_error.reset(new RootFileIssues::Exception(ex));
        ERS_LOG(*m_error);
    }

    // For the versioned histogram we want to save last version of the
    // histogram in an "EoR" folder which does not have /run_NNN/lb/_MMM/
    // prefix. Histogram versions come from IS server in the reverse time
    // order (newer objects come first) so we take very first version
    // and store it in a m_topDir.

    if (tag > -1 and m_versions > 1 and fullname != m_lastName) {

        // check that histogram was not saved yet
        if (not m_file.exists(m_topDir + "/" + obj_name)) {
            // reset a name, as the name has changed in last write()
            obj->SetName(obj_name.c_str());
            ERS_DEBUG (3, "archiving " << obj->GetName() << " under " << m_topDir);
            try {
                // and save it
                m_file.write (*obj, m_topDir);
                if (m_storeAnnotations and not annotations.empty()) {
                    std::string path = ::pathJoin(m_topDir, obj_name);
                    ERS_DEBUG (2, "archiving annotations for " << path);
                    m_annotations.insert(std::make_pair(path, annotations));
                }
            } catch (const RootFileIssues::Exception &ex) {
                m_error.reset(new RootFileIssues::Exception(ex));
                ERS_LOG(*m_error);
            }
        }

        m_lastName = fullname;
    }

    delete obj;
    m_lastTag = tag;
}

void
MDAReceiver::finish()
{
    if (m_storeAnnotations and not m_annotations.empty()) {
        // store annotations, convert map to ROOT TMap object
        TMap tmap(m_annotations.size());
        tmap.SetOwnerKeyValue();

        for (const auto& pair: m_annotations) {
            const auto& histoPath = pair.first;
            const auto& annotations = pair.second;

            // store key and value as interleaved strings
            TObjArray* tann = new TObjArray(2*annotations.size());
            tann->SetOwner();
            for (unsigned i = 0; i != annotations.size(); ++ i) {
                (*tann)[2*i] = new TObjString(annotations[i].first.c_str());
                (*tann)[2*i+1] = new TObjString(annotations[i].second.c_str());
            }

            tmap.Add(new TObjString(histoPath.c_str()), tann);
        }

        m_file.writeTObject(tmap, "annotations", ".meta", TObject::kSingleKey);

        // Store also the description of the object type used to store annotations.
        // This string is frozen forever, clients are supposed to check it and
        // access the data in the map based on this string.
        TObjString annType("TMap of TObjArray<TObjString> [interleaved key/value]");
        m_file.writeTObject(annType, "annotations_type", ".meta");
    }
}

void
MDAReceiver::receive (OHRootHistogram &histo)
{
    archive (histo.histogram.release(), histo.tag, histo.annotations);
}

void
MDAReceiver::receive (std::vector<OHRootHistogram *> &histos)
{
    for (size_t k = 0; k < histos.size(); ++ k) {
        receive (*histos[k]);
    }
}

void
MDAReceiver::receive(OHRootEfficiency & efficiency)
{
    archive (efficiency.efficiency.release(), efficiency.tag, efficiency.annotations);
}

void
MDAReceiver::receive(std::vector<OHRootEfficiency*> &efficiencies)
{
    for (auto eff: efficiencies) {
        receive (*eff);
    }
}

void
MDAReceiver::receive (OHRootGraph &graph)
{
    archive (graph.graph.release(), graph.tag, graph.annotations);
}

void
MDAReceiver::receive (std::vector<OHRootGraph *> &graphs)
{
    for (size_t k = 0; k < graphs.size(); ++ k) {
        receive (*graphs[k]);
    }
}

void
MDAReceiver::receive (OHRootGraph2D &graph)
{
    archive (graph.graph.release(), graph.tag, graph.annotations);
}

void
MDAReceiver::receive (std::vector<OHRootGraph2D *> &graphs)
{
    for (size_t k = 0; k < graphs.size(); ++ k) {
        receive (*graphs[k]);
    }
}

bool
MDAReceiver::removePolicy ()
{
    ERS_DEBUG (3, "remove policy: " << (m_deleteAfterSaving ? "REMOVE" : "KEEP"));
    return m_deleteAfterSaving;
}

} // namespace mda
} // namespace daq
