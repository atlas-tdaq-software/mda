//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/OHSProvider.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
OHSProvider::OHSProvider (const std::string& server, const std::string& provider)
    : m_server(server)
    , m_provider(provider)
{
}

//--------------
// Destructor --
//--------------
OHSProvider::~OHSProvider () throw()
{
}

bool 
OHSProvider::operator< (const OHSProvider& other) const
{
    if ( this->m_server < other.m_server ) return true ;
    if ( this->m_server > other.m_server ) return false ;
    if ( this->m_provider < other.m_provider ) return true ;
    return false ;
}

// Standard stream insertion operator
std::ostream&
operator<<(std::ostream& out, const OHSProvider& provider)
{
    return out << provider.server() << '.' << provider.provider();
}

} // namespace mda
} // namespace daq
