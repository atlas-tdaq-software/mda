//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/RunTypeSMK.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
RunTypeSMK::RunTypeSMK (const std::string& runType, unsigned smk)
    : m_runType(runType)
    , m_smk(smk)
{
}

//--------------
// Destructor --
//--------------
RunTypeSMK::~RunTypeSMK () throw ()
{
}

} // namespace mda
} // namespace daq
