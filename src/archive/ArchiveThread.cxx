//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/ArchiveThread.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <time.h>
#include <boost/date_time.hpp>
#include <boost/thread/exceptions.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/archive/ArchiveIssues.h"
#include "mda/archive/Archiver.h"
#include "mda/archive/PredicateVar.h"
#include "mda/archive/RunParameters.h"
#include "mda/archive/RunTypeSMK.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    ERS_DECLARE_ISSUE(errors, UnhandledException,
            "ArchiveThreads: Unhandled '" << name << "' exception was thrown",
            ((const char *)name) )

}

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

detail::FlagNotify::operator bool() const {
    boost::unique_lock<boost::mutex> lock(m_mutex);
    return m_flag;
}

// set flag value, returns old value
bool
detail::FlagNotify::set(bool value) {
    boost::unique_lock<boost::mutex> lock(m_mutex);
    bool tmp = m_flag;
    m_flag = value;
    m_cv.notify_all();
    return tmp;
}

bool
detail::FlagNotify::wait_for(bool value, int millisec) {
    boost::unique_lock<boost::mutex> lock(m_mutex);
    if (m_flag != value) {
        m_cv.wait_for(lock, boost::chrono::milliseconds(millisec),
                [this, value](){return m_flag == value;});
    }
    return m_flag;
}

/**
 *  @brief internal class implementing thread body
 */

class ArchiveThread::ArchiveThreadBody {
public:
    
    ArchiveThreadBody (const std::shared_ptr<Archiver>& archiver,
                       const RunTypeSMK& runTypeSMK,
                       unsigned runNumber,
                       detail::FlagNotify& stopFlag);
    
    ~ArchiveThreadBody () throw ();

    void operator()();

private:
    std::shared_ptr<Archiver> m_archiver;
    RunTypeSMK m_runTypeSMK;
    unsigned m_runNumber;
    detail::FlagNotify& m_stopFlag;
};


/*
 * RunThread
 */

ArchiveThread::ArchiveThread(const std::shared_ptr<Archiver>& archiver,
                             const RunTypeSMK& runTypeSMK,
                             unsigned runNumber)
try
    : m_thread(ArchiveThreadBody(archiver, runTypeSMK, runNumber, m_stopFlag))
{
} catch (const boost::thread_resource_error& ex) {
    throw ArchiveIssues::ThreadStartException(ERS_HERE, ex);
}

ArchiveThread::~ArchiveThread()  throw()
{
    // join()  may throw if the _current_ thread is interrupted, so to 
    // be safe we'll disable interrupts here until the end of destructor
    boost::this_thread::disable_interruption dis;

    // signal it to stop
    ERS_LOG("Requesting stop of archiving thread");
    m_stopFlag.set(true);
    
    // wait until it actually finishes
    m_thread.join();
}


/*
 * ArchiveThreadBody
 */

ArchiveThread::ArchiveThreadBody::ArchiveThreadBody(const std::shared_ptr<Archiver>& archiver,
                                                    const RunTypeSMK& runTypeSMK,
                                                    const unsigned runNumber,
                                                    detail::FlagNotify& stopFlag)
    : m_archiver(archiver)
    , m_runTypeSMK(runTypeSMK)
    , m_runNumber(runNumber)
    , m_stopFlag(stopFlag)
{
    ERS_LOG("Starting periodic archiving thread");
}

ArchiveThread::ArchiveThreadBody::~ArchiveThreadBody () throw ()
{
}

void
ArchiveThread::ArchiveThreadBody::operator()()
try {
    
    // predicate which tells when this thread was scheduled to stop
    PredicateVar<detail::FlagNotify> stop_requested(m_stopFlag);
    
    time_t last_complaint = 0;
    unsigned const complaint_interval = 120;  // complain every 2 minutes
    while (true) {
        
        // LB number
        unsigned lb(0);
        try {
            
            RunParameters rp;
            lb = rp.getLBNumber();
            
        } catch (const RunParametersIssues::Exception &ex) {

            // complain but not too frequently
            time_t now = ::time(nullptr);
            if (now > last_complaint + complaint_interval) {
                last_complaint = now;
                ers::warning(ex);
            }
            
        }
        
        // archive if necessary
        m_archiver->readHistos (false, m_runNumber, lb);
        m_archiver->archive (m_runTypeSMK, m_runNumber, lb, stop_requested);

        // wat for some time or until stop flag is set
        if (m_stopFlag.wait_for(true, 10000)) {
            ERS_LOG("Archiving thread finishing due to stop signal");
            break;
        }

    }

} catch( ers::Issue & ex ) {
    ers::fatal(::errors::UnhandledException(ERS_HERE, ex.get_class_name(), ex));
} catch (const std::exception &ex) {
    ers::fatal(::errors::UnhandledException(ERS_HERE, "standard", ex));
} catch (...) {
    ers::fatal(::errors::UnhandledException(ERS_HERE, "unknown"));
}



} // namespace mda
} // namespace daq
