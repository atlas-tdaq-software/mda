//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/Archiver.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"
#include "mda/archive/ArchiveIssues.h"
#include "mda/archive/Predicate.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace std::chrono;
namespace fs = boost::filesystem;

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

Archiver::Archiver (const std::vector<ArchiveFile>& files,
                    const std::string& topDir,
                    const std::string& cocaServer,
                    const std::string& cocaPartition)
try
    : m_mdaFiles (files)
    , m_topDir (topDir)
    , m_serverName (cocaServer)
    , m_cocaServer (cocaServer, cocaPartition)
{
    
    // create directory for ROOT files
    try {
        coca::FileSystem::create_dirs(m_topDir);
    } catch (const fs::filesystem_error &ex) {
        throw ArchiveIssues::CannotMakeDir(ERS_HERE, m_topDir, ex);
    }

    // do some cleanup of empty directories
    boost::system::error_code ec;
    for (auto itr = fs::directory_iterator(m_topDir, ec), end = fs::directory_iterator(); itr != end; ++ itr) {
        const auto& entry = *itr;
        if (boost::starts_with(entry.path().filename().string(), "mda-unique-")) {
            if (fs::is_directory(entry.status()) and fs::is_empty(entry.path())) {
                ERS_LOG("removing empty directory: " << entry.path());
                fs::remove(entry.path(), ec);
            }
        }
    }

    // check file names
    std::set<std::string> file_prefixes;
    for (FileList::iterator it = m_mdaFiles.begin(); it != m_mdaFiles.end(); ++ it) {
        const std::string& file_prefix = it->filePrefix();
        if (file_prefixes.find (file_prefix) != file_prefixes.end()) {
            throw ArchiveIssues::FilePrefixNotUnique (ERS_HERE, it->name(), file_prefix);
        }
        file_prefixes.insert (file_prefix);
    }

} catch (const coca::RegisterIssues::Exception& ex) {
    // CoCa init failed
    throw ArchiveIssues::CocaInitError(ERS_HERE, cocaServer, cocaPartition, ex);
}

Archiver::~Archiver ()
    throw ()
{
}

void
Archiver::readHistos (bool cacheHistos, unsigned runNumber, unsigned lb)
{
    auto t0 = system_clock::now();
    bool anyhtingToRead = false;
    for (FileList::iterator it = m_mdaFiles.begin(); it != m_mdaFiles.end(); ++ it) {
        if (it->readHistos (cacheHistos, runNumber, lb)) {
            anyhtingToRead = true;
        }
    }
    if (anyhtingToRead) {
        auto elapsed = system_clock::now() - t0;
        ERS_LOG ("Archiver::readHistos, time elapsed: " << duration_cast<seconds>(elapsed).count() << " sec");
    }
}

void
Archiver::archive (const RunTypeSMK& runType,
                   unsigned runNumber,
                   unsigned lb,
                   const Predicate& stopRequested)
{
    auto t0 = system_clock::now();
    bool anyhtingToArchive = false;
    for (FileList::iterator it = m_mdaFiles.begin(); it != m_mdaFiles.end();++ it) {
        if (it->archive (m_cocaServer, m_topDir, runType, runNumber, lb, stopRequested)) {
            anyhtingToArchive = true;
        }
    }
    if (anyhtingToArchive) {
        auto elapsed = system_clock::now() - t0;
        ERS_LOG ("Archiver::archive, time elapsed: " << duration_cast<seconds>(elapsed).count() << " sec");
    }
}

} // namespace mda
} // namespace daq
