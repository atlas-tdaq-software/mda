//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/DBArchive.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <cassert>
#include <time.h>
#include <RelationalAccess/ConnectionServiceException.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda/archive/OHSProvider.h"
#include "mda/archive/RunTypeSMK.h"
#include "mda/common/Issues.h"
#include "mda/common/Transaction.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

  inline
  struct timespec now()
  {
    struct timespec ts ;
    clock_gettime( CLOCK_REALTIME, &ts );
    return ts;
  }

  inline
  double delta(struct timespec ts0, struct timespec ts1) {
    return (ts1.tv_sec-ts0.tv_sec) + (ts1.tv_nsec-ts0.tv_nsec)/1e9;
  }

}

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

DBArchive::DBArchive (const std::string& connStr, const std::string& prefix)
    : DBData (connStr, prefix, true)
{
}

DBArchive::~DBArchive () throw ()
{
}

coral::ISessionProxy& DBArchive::session()
{
    coral::ISessionProxy& session = DBData::session();

    if (m_schemaVersion < 0) {
        // read schema version from database
        Transaction tran(session);
        m_schemaVersion = m_metaTbl.dbSchemaVersion(tran);
        ERS_DEBUG(0, "MDA schema version from database: " << m_schemaVersion);
        if (m_schemaVersion < 0) {
            // means there is no META table, it did not exist in V1
            m_schemaVersion = 1;
            ERS_DEBUG(0, "assume MDA schema version: " << m_schemaVersion);
        }
    }

    return session;
}

int
DBArchive::dbSchemaVersion()
try {
    if (m_schemaVersion < 0) {
        // make sure that session() is called to initialize m_schemaVersion
        this->session();
    }
    return m_schemaVersion;
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

void
DBArchive::insertHistoList (const HistoLBList& histoLBList,
                            HistoIdLBList& histoIdLBList)
try {
    
    ERS_DEBUG(2, "DBArchive::insertHistoList begin");

    if (histoLBList.empty()) {
        // nothing to do
        return ;
    }

    // these will collect timing statistics
    double t0=0;
    double t1=0;
    double t2=0;

    // iterate over all histograms
    HistoLBList::const_iterator it = histoLBList.begin();
    while ( it != histoLBList.end() ) {

        HistoIdLBList histos ;

        // set a database session
        coral::ISessionProxy& session = this->session();

        ERS_DEBUG(2, "DBArchive::insertHistoList start transaction");
        Transaction tran (session, Transaction::Update, Transaction::Abort);

        // want to commit transaction after small number of inserts
        for ( int inserts = 0 ; inserts < 1000 and it != histoLBList.end() ; ++ it ) {

            std::string path = it->first ;
            if (path.empty() or path[0] != '/') {
                // some histogram names are missing leading slash but we need it here
                path.insert(0, "/");
            }

            // split path into folder and basename
            std::string::size_type p = path.rfind('/');
            assert( p != std::string::npos );
            std::string folder ( path, 0, p );
            if ( folder.empty() ) folder = "/";
            std::string hname ( path, p+1 );

            bool newId ;            
            // find or store folder name
            struct timespec ts0 = ::now();
            uint32_t fid = m_folderTbl.findOrInsert( folder, tran, &newId ) ;
            if ( newId ) ++ inserts ;
            
            // find or store base name
            struct timespec ts1 = ::now();
            uint32_t hnamid = m_hnameTbl.findOrInsert( hname, tran, &newId ) ;
            if ( newId ) ++ inserts ;
            
            // find or store path (combination of folder id and hname id)
            struct timespec ts2 = ::now();
            uint32_t hid = m_histoTbl.findOrInsert( hnamid, fid, tran, &newId ) ;
            if ( newId ) ++ inserts ;
            struct timespec ts3 = ::now();

            t0 += ::delta(ts0, ts1);
            t1 += ::delta(ts1, ts2);
            t2 += ::delta(ts2, ts3);

            // 
            histos.insert ( HistoIdLBList::value_type(hid, it->second) ) ;

        }

        ERS_DEBUG(2, "DBArchive::insertHistoList end transaction");
        tran.commit();
        
        // after commit we can copy the list of stored histograms        
        histoIdLBList.insert(histos.begin(), histos.end());
        
    }

    // dump some statistics
    ERS_DEBUG(2, "DBArchive::insertHistoList times: count=" << histoLBList.size()
        << " folder=" << t0 << " hname=" << t1 << " histo=" << t2);
    ERS_DEBUG(2, "DBArchive::insertHistoList end");

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

void
DBArchive::insertInterval (const std::string& partition,
                unsigned run,
                unsigned lb,
                const std::string& histoGroup,
                const OHSProvider& ohsProvider,
                const RunTypeSMK& runType,
                const HistoIdLBList& histoIdLBList,
                const std::string& dataset,
                const std::string& file,
                const std::string& archive)
try {
    
    ERS_DEBUG(2, "DBArchive::insertInterval begin");

    // set a database session
    coral::ISessionProxy& session = this->session();

    Transaction tran (session, Transaction::Update, Transaction::Abort);

    // find/insert partition name
    uint32_t part_id = m_partitionTbl.findOrInsert(partition, tran) ;

    // Hgroup name
    uint32_t hgrpname_id = m_hgrpnameTbl.findOrInsert(histoGroup, tran);

    // server/provider names
    uint32_t hsrv_id = m_hserverTbl.findOrInsert(ohsProvider.server(), tran);
    uint32_t prov_id = m_providerTbl.findOrInsert(ohsProvider.provider(), tran);

    // runtype
    uint32_t runtype_id = m_runtypeTbl.findOrInsert(runType.runtype(), tran);

    // dataset
    uint32_t ds_id = m_datasetTbl.findOrInsert(dataset, tran);
    uint32_t arch_id = m_archiveTbl.findOrInsert(archive, tran);

    // runlb
    uint32_t runlb_id = m_runlbTbl.findOrInsert(run, lb, runType.smk(),
            runtype_id, part_id, tran);

    // file
    uint32_t file_id = m_fileTbl.findOrInsert( runlb_id, file, ds_id, arch_id, tran);

    // count number of histograms and versions
    uint32_t hcount = histoIdLBList.size();
    uint32_t vcount = 0;
    for (HistoIdLBList::const_iterator it = histoIdLBList.begin() ; it != histoIdLBList.end() ; ++ it ) {
        vcount += std::max(uint32_t(it->second.size()), uint32_t(1));
    }
    ERS_DEBUG(2, "DBArchive::insertInterval hcount=" << hcount << " vcount=" << vcount);

    // hgroup
    uint32_t hgrp_id = m_hgroupTbl.findOrInsert(hgrpname_id, file_id, hsrv_id, prov_id, hcount, vcount, tran);

    // LBLists
    typedef HgroupHistoTable::HistoIdLBListId HistoIdLBListId;
    HistoIdLBListId histoIdLBListId ;
    for (HistoIdLBList::const_iterator it = histoIdLBList.begin() ; it != histoIdLBList.end() ; ++ it ) {
        uint32_t lbListId = m_lbSeqTbl.findOrInsert( it->second, m_lbRangeTbl, tran );
        histoIdLBListId.insert( HistoIdLBListId::value_type(it->first, lbListId) );
    }

    // hgroup-histo map
    m_hgroup2histoTbl.insert(hgrp_id, histoIdLBListId, tran) ;

    tran.commit();
    ERS_DEBUG(2, "DBArchive::insertInterval end");

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

// Update archive name for a given file name.
void
DBArchive::updateArchive(const std::string& dataset,
        const std::string& file,
        const std::string& archive)
try {

    ERS_DEBUG(2, "DBArchive::updateArchive begin");

    // set a database session
    coral::ISessionProxy& session = this->session();

    Transaction tran(session, Transaction::Update, Transaction::Abort);

    // Find ID for dataset name
    uint32_t ds_id = m_datasetTbl.find(dataset, tran);
    if (ds_id == 0) {
        throw Issues::NoSuchRecord(ERS_HERE, "dataset name: "+dataset);
    }

    // ID for archive name
    uint32_t arch_id = m_archiveTbl.findOrInsert(archive, tran);

    // Update all matching records
    long count = m_fileTbl.updateArchive(file, ds_id, arch_id, tran);
    ERS_DEBUG(2, "DBArchive::updateArchive updated " << count << " records");
    if (count == 0) {
        throw Issues::NoSuchRecord(ERS_HERE, "file: "+file+" dataset: "+dataset);
    }

    tran.commit();
    ERS_DEBUG(2, "DBArchive::updateArchive end");

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

} // namespace mda
} // namespace daq
