//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/ArchiveHistoGroup.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "mda/archive/ArchiveIssues.h"
#include "mda/archive/Predicate.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace std::chrono;

namespace {

    ERS_DECLARE_ISSUE(errors, OHStreamFailure,
            "ArchiveHistoGroup: failed to create OHStream, partition="
            << partition << " ohsServer=" << ohsServer,
            ((std::string) partition) ((std::string) ohsServer))
    ERS_DECLARE_ISSUE(errors, MissingHistogramWarning,
            "MDAHistoGroup: no histograms received, if this warning repeats it could be due to "
            "mis-configuration of MDAHistoGroup.\n"
            "    Please check MDA configuration:\n"
            "    Group='" << group << "' server='" << server << "' providerRe='" << provider
            << "' histoRe='" << hist << "'",
            ((std::string) group) ((std::string) server) ((std::string) provider) ((std::string) hist))
    ERS_DECLARE_ISSUE(errors, EmptyHistoName,
        "ArchiveHistoGroup: histogram name is empty, ignoring histogram. server="
        << server << " provider=" << provider,
        ((std::string) server) ((std::string) provider)
    )
    ERS_DECLARE_ISSUE(errors, TrailingSlash,
        "ArchiveHistoGroup: histogram name has trailing slash, ignoring histogram: "
        << name << " server=" << server << " provider=" << provider,
        ((std::string) server) ((std::string) provider) ((std::string) name)
    )

}

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
ArchiveHistoGroup::ArchiveHistoGroup(const std::string& name,
                                     const std::string& partition,
                                     const std::string& ohsServer,
                                     const std::string& provider,
                                     const std::string& histograms,
                                     int versionsNumber,
                                     const std::shared_ptr<DBArchive>& dbArchive)
    : m_name (name)
    , m_partition (partition)
    , m_ohsServer (ohsServer)
    , m_provider (provider)
    , m_histograms (histograms)
    , m_versions (versionsNumber)
    , m_ohStream (0)
    , m_dbArchive (dbArchive)
{
}

//--------------
// Destructor --
//--------------
ArchiveHistoGroup::~ArchiveHistoGroup () throw()
{
}

void
ArchiveHistoGroup::reset ()
{
    if (m_ohStream) {
        ERS_LOG ("deleting OHStream: " << (void *) m_ohStream);
        delete m_ohStream;
        m_ohStream = 0;
    }
}

void
ArchiveHistoGroup::readHistos (bool cacheHistos)
{
    // remove accumulated stuff
    reset();

    // instantiate OHStream with the needed parameters
    try {
        auto t0 = system_clock::now();
        m_ohStream = new OHStream (m_partition, m_ohsServer, m_provider, m_histograms,
                                  cacheHistos, m_versions);
        auto elapsed = system_clock::now() - t0;
        ERS_LOG ("HistoGroup " << m_name << ": created OH stream, time elapsed: "
                 << duration_cast<seconds>(elapsed).count() << " sec");
    } catch (const oh::RepositoryNotFound &ex) {
        ers::warning(::errors::OHStreamFailure(ERS_HERE, m_partition, m_ohsServer, ex));
    } catch (const oh::Exception &ex) {
        ers::error(::errors::OHStreamFailure(ERS_HERE, m_partition, m_ohsServer, ex));
    }
}

unsigned
ArchiveHistoGroup::archive (MDAReceiver& receiver,
                            Provider2HistoList& histoLists,
                            const Predicate& stopRequested)
{
    if (! m_ohStream) {
        // nothing to do here if OH stream is not defined,
        // which may happen if OH server has troubles
        ERS_LOG ("HistoGroup " << m_name
                 << ": stream not initialized, skipping archive: server=" << m_ohsServer);
        return 0;
    }

    typedef std::map<OHSProvider, DBArchive::HistoLBList> HistoListMap;
    HistoListMap histoListMap;

    // write histograms to file and fill histoListMap
    try {

        ERS_LOG ("HistoGroup " << m_name
                 << ": retrieving histograms from server: " << m_ohsServer);

        while (! m_ohStream->eof()) {

            std::string provider = m_ohStream->provider();
            std::string name = m_ohStream->name();

            if (name.empty()) {
                ers::warning(::errors::EmptyHistoName(ERS_HERE, m_ohsServer, provider));
                m_ohStream->skip();
                continue;
            }
            if (name.back() == '/') {
                ers::warning(::errors::TrailingSlash(ERS_HERE, m_ohsServer, provider, name));
                m_ohStream->skip();
                continue;
            }

            // make sure name formatting is good.
            name = normalize_histo_name(name);

            ERS_DEBUG (3, "Retrieving histogram " << name << " created by " << provider);

            // write histogram to file
            receiver.setVersions(m_versions);
            receiver.setTopDirectory (m_ohsServer, provider);
            m_ohStream->retrieve (receiver);
            receiver.throwIfError();

            // add to histoListMap
            OHSProvider ohprovider (m_ohsServer, provider);
            DBArchive::HistoLBList& histoLbList = histoListMap[ohprovider];
            std::vector<unsigned>& lbList = histoLbList[name];

            // get LB for this stored histogram
            int tag = receiver.getLastTag() ;
            if ( tag > -1 && m_versions > 1 ) {
                // preallocate some space
                if ( lbList.empty() ) lbList.reserve (m_versions) ;
                // add this tag as LB
                lbList.push_back (tag);
            }

            if (stopRequested()) {
                ERS_LOG("HistoGroup " << m_name << ": STOP requested while getting histograms");
                break;
            }
        }
    } catch (const daq::is::Exception &ex) {
        ers::error(ex);
    } catch (const daq::oh::Exception &ex) {
        ers::error(ex);
    } catch (const std::exception &ex) {
        reset();
        throw;
    }
    reset();

    // write lists to database and cache (histoListMap will be invalidated)
    unsigned count = 0;
    for (HistoListMap::iterator it = histoListMap.begin(); it != histoListMap.end(); ++ it) {

        if (it->second.empty()) continue;

        count += it->second.size();

        try {
            DBArchive::HistoIdLBList hlist;
            m_dbArchive->insertHistoList(it->second, hlist) ;
            histoLists.insert (Provider2HistoList::value_type(it->first, hlist));
        } catch (const Issues::CoralException& ex) {
            // complain but continue
            ers::error(ArchiveIssues::HistoGroupArchive(ERS_HERE, m_name, ex));
        }
    }

    if (count == 0) {
        // No histograms in this group, this could be due to mis-configuration,
        // produce warning so that experts could look at it
        ::errors::MissingHistogramWarning issue(ERS_HERE, m_name, m_ohsServer, m_provider, m_histograms);
        ers::warning(issue);
    }

    ERS_LOG("Collected " << count << " histograms per group");

    return count;
}

std::string
ArchiveHistoGroup::normalize_histo_name(const std::string& name)
{
    // Empty name should not happen here, but just to be paranoid
    if (name.empty()) {
        return name;
    }

    // some histogram names are missing leading slash but we need it here
    std::string fixed_name = "/";

    // compact multiple slashes into one
    char last_char = '/';
    for (char c: name) {
        if (last_char == '/' and c == '/') {
            continue;
        }

        last_char = c;
        fixed_name.push_back(c);
    }

    return fixed_name;
}

} // namespace mda
} // namespace daq
