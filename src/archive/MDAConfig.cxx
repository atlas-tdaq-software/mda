//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/MDAConfig.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "RunControl/Common/OnlineServices.h"
#include "dal/util.h"
#include "mda/archive/ArchiveFile.h"
#include "mda/archive/ArchiveHistoGroup.h"
#include "mda_dal/MDAHistoGroup.h"
#include "mda_dal/MDAHistoGroupSet.h"
#include "mda_dal/MDAFile.h"
#include "mda_dal/MDAFileSet.h"
#include "mda_dal/MDAApplication.h"
#include "coca/common/System.h"
#include "coca_dal/CoCaDataset.h"
#include "coca_dal/CoCaServer.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    /*
     * FillRecursively
     */

    template <class Type, class TypeBase, class TypeSet>
    void
    FillRecursively (const std::vector<const TypeBase *> &TopLevelList,
                              std::vector<const Type *> &CompleteList,
                              Configuration &ConfDB)
    {
        typedef typename std::vector<const TypeBase *>::const_iterator it_t;
        for (it_t it = TopLevelList.begin(); it != TopLevelList.end(); ++ it) {
            //ERS_INFO ("checking: " << (*it)->UID() << " (" << *(*it) << ")");
            if (*it == NULL) {
                continue;
            }
            const Type *tmp = ConfDB.cast<Type> (*it);
            if (tmp) {
                CompleteList.push_back (tmp);
                ERS_DEBUG (3, "added: " << tmp->UID());
            } else {
                const TypeSet *tmp_set = ConfDB.cast<TypeSet> (*it);
                ERS_ASSERT (tmp_set);
                FillRecursively <Type, TypeBase, TypeSet> (tmp_set->get_branches(),
                                                           CompleteList, ConfDB);
            }
        }
    }

}

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

MDAConfig::MDAConfig(const std::string& appName)
try
    : m_minRunDuration(0)
    , m_initTimeout(0)
    , m_archiver()
    , m_db()
{
    auto& os = daq::rc::OnlineServices::instance();

    // partition name from environment
    const auto partition = os.getIPCPartition().name();

    // substitute variables; needs partition object
    Configuration& confdb = os.getConfiguration();

    auto& app_base = os.getApplication();
    const mda_dal::MDAApplication *conf = confdb.cast<mda_dal::MDAApplication>(&app_base);
    if (! conf) {
        throw MDAConfigIssues::ConfigurationNotFound (ERS_HERE, appName);
    }

    const std::string &connection_string = conf->get_Database();
    m_db.reset (new DBArchive(connection_string));

    m_minRunDuration = conf->get_MinRunDuration_s();

    m_initTimeout = conf->get_ArchiverInitTimeout_s();

    // several releases can be active in P1
    std::string top_dir  = conf->get_TopDir() + "/" + TDAQ_RELEASE_NAME "/" + appName;

    // merge many slashes into one
    char last = '\0';
    std::string::iterator out = top_dir.begin();
    for ( std::string::const_iterator it = top_dir.begin(); it != top_dir.end(); ++ it ) {
      if ( *it == last and last == '/' ) continue;
      *(out++) = last = *it;
    }
    top_dir.erase(out, top_dir.end());


    std::string coca_partition = conf->get_CoCaPartition();
    std::string coca_server = conf->get_CoCaServer()->UID();

    // Try to set unix properties, if it does not succeed (e.g. unix
    // group name is wrong) it means most likely configuration error.
    // In case of error just complain about it so that configuration
    // get fixed eventually, but continue. We still should be able to
    // produce output files in this case, though they may have
    // incorrect permissions, etc.
    try {
        coca::System::initialize(*(conf->get_UnixProperties()));
    } catch (const coca::SystemIssues::GroupNotFound &ex) {
        ers::error (ex);
    }

    std::string pre_prefix = partition + "_" + appName + "_";

    std::vector<ArchiveFile> mda_files;
    std::vector<const mda_dal::MDAFile *> files;
    typedef std::vector<const mda_dal::MDAFile *>::const_iterator
        file_it_t;
    ::FillRecursively <mda_dal::MDAFile, mda_dal::MDAFileBase,
        mda_dal::MDAFileSet> (conf->get_Files(), files, confdb);

    for (file_it_t file_it = files.begin(); file_it != files.end(); ++ file_it) {
        std::vector<ArchiveHistoGroup> histo_groups;

        std::vector<const mda_dal::MDAHistoGroup *> hgs;
        typedef std::vector<const mda_dal::MDAHistoGroup *>::const_iterator
            hg_it_t;
        ::FillRecursively <mda_dal::MDAHistoGroup,
            mda_dal::MDAHistoGroupBase, mda_dal::MDAHistoGroupSet>
            ((*file_it)->get_HistoGroups(), hgs, confdb);


        for (hg_it_t hg_it = hgs.begin(); hg_it != hgs.end(); ++ hg_it) {
            auto source_partition_name = (*hg_it)->get_Partition();
            if (source_partition_name.empty()) {
                source_partition_name = partition;
            }
            ArchiveHistoGroup hg ((*hg_it)->UID(), source_partition_name,
                                    (*hg_it)->get_OHSServer(),
                                    (*hg_it)->get_Provider(),
                                    (*hg_it)->get_Histograms(),
                                    (*hg_it)->get_VersionsNumber(),
                                    m_db);
            histo_groups.push_back (hg);
        }

        std::string file_prefix = pre_prefix + (*file_it)->UID();
        ArchiveFile file ((*file_it)->UID(),
                            histo_groups,
                            file_prefix,
                            (*file_it)->get_Frequency_LB(),
                            (*file_it)->get_CompressionLevel(),
                            (*file_it)->get_CoCaDataset()->UID(),
                            (*file_it)->get_CoCaPRIO(),
                            (*file_it)->get_CoCaDTC(),
                            (*file_it)->get_DeleteAfterSaving(),
                            (*file_it)->get_SaveAnnotations(),
                            m_db,
                            partition);
        mda_files.push_back (file);
    }

    try {

        m_archiver = std::make_shared<Archiver>(mda_files, top_dir, coca_server, coca_partition);

    } catch( ers::Issue & ex ) {
    	throw MDAConfigIssues::CantArchive( ERS_HERE, ex );
    }

} catch (const daq::config::Exception& ex) {
    throw MDAConfigIssues::ConfigError(ERS_HERE, ex);
}


MDAConfig::~MDAConfig() throw()
{
}

} // namespace mda
} // namespace daq
