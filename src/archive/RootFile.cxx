//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/RootFile.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/filesystem.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
namespace fs = boost::filesystem;

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

RootFile::RootFile (const std::string& name,
                    const std::string& option,
                    const std::string& title,
                    int compLevel)
    : m_file (0)
{
    if (option == "NEW") {

        // We do not want to overwrite existing files so check that file
        // with the same name does not exist yet, throw exception if
        // it exists

        bool exists = false;
        try {
            exists = fs::exists(name);
        } catch (const fs::filesystem_error& ex) {
            RootFileIssues::CannotOpenFile issue (ERS_HERE, errno, name, 
                    option, compLevel, ex);
            ERS_DEBUG (0, issue);
            throw issue;
        }
        if (exists) {
            RootFileIssues::FileExists issue (ERS_HERE, name);
            ERS_DEBUG (0, issue);
            throw issue;
        }
            
    }
    
    // Open ROOT file
    m_file = new TFile(name.c_str(), option.c_str(), title.c_str(), compLevel);
    
    // check that opened correctly
    if (m_file->IsZombie()) {
        RootFileIssues::CannotOpenFile issue (ERS_HERE, m_file->GetErrno(), 
                name, option, compLevel);
        ERS_DEBUG (0, issue);
        throw issue;
    }
    ERS_ASSERT (m_file->IsOpen());
}

RootFile::~RootFile ()
    throw ()
{
    if (m_file->IsOpen()) {
        try {
            close();
        } catch (const RootFileIssues::ErrorOnFileClose &exc) {
            ers::warning (exc);
        }
    }
    
    delete m_file;
}

void
RootFile::close ()
{
    ERS_PRECONDITION (m_file->IsOpen());
    
    // Write everything that's still in memory
    m_file->ResetErrno();
    m_file->Write();
    int err1 = m_file->GetErrno();

    // close the file
    m_file->ResetErrno();
    m_file->Close();
    int err2 = m_file->GetErrno();

    if (err1 and err1 != 12) {
        // we know that sometimes ENOMEM is generated for no good reason
        RootFileIssues::ErrorOnFileClose issue (ERS_HERE, err1, m_file->GetName());
        ERS_LOG("Error in TFile::Write: " << issue);
        throw issue;
    }
    if (err2) {
        RootFileIssues::ErrorOnFileClose issue (ERS_HERE, err2, m_file->GetName());
        ERS_LOG("Error in TFile::Close: " << issue);
        throw issue;
    }

    ERS_ASSERT (! m_file->IsOpen());
}

void
RootFile::mkdir_cd (const std::string& dir)
{
    TDirectory *dir_obj = gDirectory->GetDirectory (dir.c_str());
    if (! dir_obj) {
        dir_obj = gDirectory->mkdir (dir.c_str(), dir.c_str());
        if (! dir_obj) {
            RootFileIssues::CannotMakeDir issue (ERS_HERE, m_file->GetErrno(), 
                    dir, gDirectory->GetPath());
            ERS_DEBUG (0, issue);
            throw issue;
        }
    }
    bool cd_ok = dir_obj->cd();
    ERS_ASSERT(cd_ok);
}

void
RootFile::write (TNamed& object, const std::string& dir)
{
    // adjust object name and directory name
    std::string dirName = dir;
    std::string basename = object.GetName();
    std::string::size_type pos = basename.rfind('/');
    if (pos != std::string::npos) {
        dirName += "/";
        dirName += basename.substr(0, pos);
        basename.erase(0, pos+1);
        object.SetName (basename.c_str());
    }

    // forward to TObject save method
    writeTObject(object, object.GetName(), dirName);
}

void
RootFile::writeTObject(TObject& object, const std::string& name, const std::string& dir, int option)
{
    ERS_PRECONDITION (m_file->IsOpen());

    // join directory name and object name
    std::string path = dir + '/' + name;

    // normalize, replace bad characters with underscore
    std::replace (path.begin(), path.end(), ':', '_' );

    // split into individual components
    std::vector<std::string> components;
    boost::split (components, path, boost::is_any_of("/"), boost::token_compress_on);
    size_t basename_pos = components.size() - 1;

    // file
    bool cd_ok = m_file->cd();
    ERS_ASSERT(cd_ok);

    // directories
    for (size_t k = 0; k < basename_pos; ++ k) {
        // account for slash at the beginning of directory or multiple slashes
        if (not components[k].empty()) mkdir_cd (components[k]);
    }

    // object
    int code = object.Write(components.back().c_str(), option);
    if (code == 0) {
        RootFileIssues::CannotWrite2File issue (ERS_HERE, m_file->GetErrno(),
                                                path, m_file->GetName());
        ERS_DEBUG (0, issue);
        throw issue;
    }
}

// Check if the object with the given name already exists.
bool
RootFile::exists(const std::string& path)
{
    // split into individual components
    std::vector<std::string> components;
    boost::split (components, path, boost::is_any_of("/"), boost::token_compress_on);
    size_t basename_pos = components.size() - 1;

    // try to find directory
    TDirectory* dir = m_file;
    for (size_t i = 0; i != basename_pos; ++ i) {
        // normalize, replace bad characters with underscore
        std::replace (components[i].begin(), components[i].end(), ':', '_' );
        dir = dir->GetDirectory(components[i].c_str());
        if (not dir) return false;
    }

    // object (or its key) in a directory
    std::string basename = components[basename_pos];
    // normalize, replace bad characters with underscore
    std::replace(basename.begin(), basename.end(), ':', '_' );
    bool exists = dir->FindKey(basename.c_str()) != 0;
    if (! exists) {
        // ADHI-4170:
        // there was bug in archiving code in 2015-16 which resulted in
        // incomplete substitution of colons with underscores, try to see if
        // there may be a matching histogram in a file which has colons in a
        // name. Only the "basename" was affected by that bug.
        exists = dir->FindKey(components[basename_pos].c_str()) != 0;
    }

    return exists;
}

} // namespace mda
} // namespace daq
