//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/RunParameters.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "TTCInfo/LumiBlock.h"
#include "rc/RunParams.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    std::string PartitionFromEnvironment()
    {
        static const char* envvar = "TDAQ_PARTITION";
        if ( const char* val = getenv(envvar) ) {
            return std::string(val);
        } else {
            daq::mda::RunParametersIssues::EnvVarNotDefined err (ERS_HERE, envvar);
            ERS_DEBUG (0, err);
            throw err;
        }
    }
}

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

RunParameters::RunParameters()
  : m_dict(IPCPartition(PartitionFromEnvironment()))
{
}

RunParameters::~RunParameters() throw ()
{
}

/*
 * RunNumber
 */

unsigned
RunParameters::getRunNumber ()
try {
    
    RunParams run_params;
    m_dict.getValue ("RunParams.RunParams", run_params);
    return run_params.run_number;
    
} catch (const daq::is::Exception &ex) {
    throw RunParametersIssues::ISexception(ERS_HERE, ex);
}

void
RunParameters::setRunNumber (unsigned runNumber)
try {
    RunParams run_params;
    try {
        m_dict.getValue ("RunParams.RunParams", run_params);
    } catch (const daq::is::Exception &ex) {
        ERS_DEBUG (0, ex);
    }
    run_params.run_number = runNumber;
    m_dict.checkin ("RunParams.RunParams", run_params);
} catch (const daq::is::Exception &ex) {
    throw RunParametersIssues::ISexception(ERS_HERE, ex);
}

/*
 * RunType
 */

std::string
RunParameters::getRunType ()
try {
    RunParams run_params;
    m_dict.getValue ("RunParams.RunParams", run_params);
    return run_params.run_type;
} catch (const daq::is::Exception &ex) {
    throw RunParametersIssues::ISexception(ERS_HERE, ex);
}

void
RunParameters::setRunType (const std::string &runType)
try {
    RunParams run_params;
    try {
        m_dict.getValue ("RunParams.RunParams", run_params);
    }
    catch (const daq::is::Exception &ex) {
        ERS_DEBUG (0, ex);
    }
    run_params.run_type = runType;
    m_dict.checkin ("RunParams.RunParams", run_params);
} catch (const daq::is::Exception &ex) {
    throw RunParametersIssues::ISexception(ERS_HERE, ex);
}

/*
 * LBNumber
 */

unsigned
RunParameters::getLBNumber ()
try {
    LumiBlock luminosity_info;
    m_dict.getValue ("RunParams.LumiBlock", luminosity_info);
    return luminosity_info.LumiBlockNumber;
} catch (const daq::is::Exception &ex) {
    throw RunParametersIssues::ISexception(ERS_HERE, ex);
}

void
RunParameters::setLBNumber (unsigned lb)
try {
    LumiBlock luminosity_info;
    try {
        m_dict.getValue ("RunParams.LumiBlock", luminosity_info);
    }
    catch (const daq::is::Exception &ex) {
        ERS_DEBUG (0, ex);
    }
    luminosity_info.LumiBlockNumber = lb;
    m_dict.checkin ("RunParams.LumiBlock", luminosity_info);
} catch (const daq::is::Exception &ex) {
    throw RunParametersIssues::ISexception(ERS_HERE, ex);
}

} // namespace mda
} // namespace daq
