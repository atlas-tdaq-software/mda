//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/MDAControllable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <fstream>
#include <ctime>
#include <boost/lexical_cast.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/coca.h"
#include "mda/archive/MDAConfig.h"
#include "mda/archive/RunParameters.h"
#include "mda/archive/RunTypeSMK.h"
#include "mda_dal/MDAApplication.h"
#include "mda_dal/MDAFile.h"
#include "mda_dal/MDAFileSet.h"
#include "mda_dal/MDAHistoGroup.h"
#include "mda_dal/MDAHistoGroupSet.h"
#include "pmg/pmg_syncMacros.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace std::chrono;

namespace {

    ERS_DECLARE_ISSUE (errors, SysCallError, 
     "system call `" << syscall << "' failed with error: " << ::strerror (errno), 
     ((std::string) syscall)) 

    ERS_DECLARE_ISSUE (errors, ForkError, "cannot create new process", )

    ERS_DECLARE_ISSUE (errors, ConfigError, "MDA configuration failed, aborting", )

    ERS_DECLARE_ISSUE (errors, RunNumberError,
            "Failed to determine run number from IS, will use current time for run number = " << run_num, ((unsigned) run_num))
    ERS_DECLARE_ISSUE (errors, LBNumberError,
            "Failed to determine LB number from IS, will use current time for LB number = " << lb, ((unsigned) lb))

}

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace mda {

MDAControllable::MDAControllable (const std::string& name)
    : Controllable ()
    , m_name (name)
    , m_minRunDuration(0)
    , m_archiverInitTimeout(0)
    , m_startTime()
    , m_stopTime()
    , m_synchFile()
    , m_archiver()
    , m_runThread()
    , m_runNumber (0)
    , m_runType()
{
    if ( char * my_m_synchFile = ::getenv( PMG_SYNC_ENV_VAR_NAME ) ) {
        m_synchFile = my_m_synchFile;
    } else {
        m_synchFile = PMG_SYNC_FILE_DIR + name + ".sync";
    }

    // ignore SIGCHLD, we start mda_eor processes but not watching them
    signal(SIGCHLD, SIG_IGN);
}

MDAControllable::~MDAControllable () throw ()
{
    // MDA can be stopped (killed) at any moment, we try to preserve data that has been
    // collected so far. We start regular mda_eor here but we pass it special option to
    // produce output file with LB number in it so taht in case MDA is restarted and tries
    // to save data later at true EoR the file names do not collide.

    if (m_runThread) {
        // it runThread is still there means run has started but stopArchiving() has not been called,
        // so we have some work left for us to do

        ERS_LOG("MDA is being stopped during the run, will try to save results but it may fail");

        // stop archiving thread
        m_runThread.reset();

        // spawn mda_eor
        if (doEoRArchiving()) {

            // find out current LB number or invent one
            uint32_t lb = LB::LastLB;
            try {
                RunParameters rp;
                lb = rp.getLBNumber();
            } catch (const RunParametersIssues::ISexception& ex) {
                // Ignore, we don't want to stop for that, use current time for LB instead
                if (lb == LB::LastLB) {
                    lb = static_cast<uint32_t>(std::time(nullptr));
                    ers::warning (::errors::LBNumberError(ERS_HERE, m_runNumber, ex));
                }
            }

            ERS_INFO("MDA is terminated unexpectedly, will try to archive histograms for run "
                    << m_runNumber << " and LB " << lb << " (" << m_runType << ")");

            // start mda_eor
            RunTypeSMK run_type_smk(m_runType);
            launchMdaEoR(run_type_smk, m_runNumber, lb);
        }
    }
}

void
MDAControllable::configure(const daq::rc::TransitionCmd& /*cmd*/)
{
    ERS_DEBUG (1, "function starting: " << __FUNCTION__);
    ERS_ASSERT (! m_runThread.get());

    try {
        // get configuration
        MDAConfig config(m_name);
        m_minRunDuration = seconds(config.minRunDuration());
        m_archiverInitTimeout = seconds(config.initTimeout());
        m_archiver = config.archiver();
    } catch (const MDAConfigIssues::Exception& ex) {
        // can't do anything here, just re-throw
        throw ::errors::ConfigError(ERS_HERE, ex);
    }
}

void
MDAControllable::prepareForRun (const daq::rc::TransitionCmd& /*cmd*/)
{
    ERS_DEBUG (1, "function starting: " << __FUNCTION__);

    // remember run start time
    m_startTime = system_clock::now();
    m_stopTime = system_clock::time_point();

    // get run parameters
    m_runNumber = 0;
    m_runType.clear();
    try {
        RunParameters rp;
        m_runNumber = rp.getRunNumber();
        m_runType = rp.getRunType();
    } catch (const RunParametersIssues::Exception &ex) {
        // Ignore, we don't want to stop for that, use time instead of run number
        if (m_runNumber == 0) {
            m_runNumber = static_cast<unsigned>(std::time(nullptr));
            ers::warning(::errors::RunNumberError(ERS_HERE, m_runNumber, ex));
        }
    }
    if (m_runType.empty()) {
        m_runType = "UNKNOWN";
    }
    ERS_LOG ("starting run: " << m_runNumber << " (" << m_runType << ")");

    ERS_ASSERT (m_archiver.get());

    // start in-run archiver thread
    ERS_ASSERT (! m_runThread.get());
    RunTypeSMK run_type_smk (m_runType);
    m_runThread.reset (new ArchiveThread(m_archiver, run_type_smk, m_runNumber));
}

void
MDAControllable::stopROIB (const daq::rc::TransitionCmd& /*cmd*/)
{
    ERS_DEBUG (1, "function starting: " << __FUNCTION__);

    // Remember stop time. Potentially we could use current time in stopArchiving()
    // or destructor, but there could be delays between stopROIB() and stopArchiving()
    // so to do better estimate of the run duration we use time of stopROIB. This may
    // be important for checking the limit on the sort run duration.
    m_stopTime = system_clock::now();
}

void
MDAControllable::stopArchiving (const daq::rc::TransitionCmd& /*cmd*/)
{
    ERS_DEBUG (1, "function starting: " << __FUNCTION__);
    ERS_LOG ("function starting: " << __FUNCTION__);

    if (! m_archiver.get()) {
        // nothing to do
        ERS_ASSERT (! m_runThread.get());
        return;
    }
    
    // stop archiving thread
    m_runThread.reset();
    
    // spawn mda_eor
    if (doEoRArchiving()) {
        ERS_INFO ("going to archive EoR histograms for run " << m_runNumber << " (" << m_runType << ")");
        RunTypeSMK run_type_smk (m_runType);
        launchMdaEoR (run_type_smk, m_runNumber);
    }
}

void
MDAControllable::unconfigure(const daq::rc::TransitionCmd& /*cmd*/)
{
    ERS_DEBUG (1, "function starting: " << __FUNCTION__);

    m_archiver.reset();
}

void
MDAControllable::launchMdaEoR (const RunTypeSMK &runType, unsigned runNumber, uint32_t lb)
{
    const std::string& runtypestr = runType.runtype();
    const std::string& smkstr = boost::lexical_cast<std::string>(runType.smk());
    const std::string& runstr = boost::lexical_cast<std::string>(runNumber);

    const char* cmd_line[] = {
        "mda_eor",
        "-n", m_name.c_str(),
        "-t", runtypestr.c_str(),
        "-s", smkstr.c_str(),
        "-r", runstr.c_str(),
        0,  0,                    // for LB
        0 };

    std::string lbstr;
    if (lb != LB::LastLB) {
        lbstr = boost::lexical_cast<std::string>(lb);
        cmd_line[9] = "-l";
        cmd_line[10] = lbstr.c_str();
    }

    ::setenv( PMG_SYNC_ENV_VAR_NAME, m_synchFile.c_str(), 1 );
    ::unlink( m_synchFile.c_str() );

    ERS_DEBUG (3, "Going to exec:" << cmd_line[0]);
    pid_t child = fork ();
    if (child == 0) {
        // this is the child

        close (0);
        execvp (cmd_line[0], (char**)cmd_line);

        // Got here? Error!!
        std::string fatal ("echo \"pid[$$]: FATAL: cannot execute: ");
        fatal += cmd_line[0];
        fatal += "; error: ";
        fatal += strerror (errno);
        fatal += "\"; false";
        execl ("/bin/sh", "sh", "-c", fatal.c_str(), "1>&2", NULL);

        // what? still here??
        abort();

    } else if (child == -1) {

        ers::error(::errors::ForkError(ERS_HERE, ::errors::SysCallError (ERS_HERE, "fork")));

    }
    // this is the server; the child was created successfully
    ERS_DEBUG (2, "pid [" << child << "]: " << cmd_line[0]);

    if ( !waitForArchiver () )
    {
        ers::warning( MDAApplication::InitializationTimeout(ERS_HERE, m_name) );
    }
}

// Returns true if we need to start mda_eor
bool
MDAControllable::doEoRArchiving() const
{
    if (! m_archiver.get()) {
        // no archiver means we are not even configured
        ERS_ASSERT (! m_runThread.get());
        return false;
    }

    if (m_startTime == system_clock::time_point()) {
        // run has not started
        return false;
    }

    // get stop time or use current time if we have not stopped yet
    const system_clock::time_point stop = (m_stopTime == system_clock::time_point()) ? system_clock::now() : m_stopTime;

    seconds elapsedTime = duration_cast<seconds>(stop - m_startTime);
    if (elapsedTime < m_minRunDuration) {
        ERS_INFO ("run too short: histograms not archived");
        return false;
    }

    return true;
}

bool
MDAControllable::waitForArchiver ()
{
    ERS_DEBUG( 0, "Wait for the '" << m_synchFile
            << "' appearence, timeout is " << m_archiverInitTimeout.count() << " seconds" );
    for (seconds elapsed(0); elapsed < m_archiverInitTimeout; elapsed += seconds(1)) {
        struct stat file_status;
        if (::stat(m_synchFile.c_str(), &file_status) == 0 ) {
            ERS_DEBUG( 0, "Synchronisation file '" << m_synchFile
                    << "' created after " << elapsed.count() << " seconds" );
            return true;
        }
        ::sleep( 1 );
    }
    ERS_DEBUG( 0, "Synchronisation file '" << m_synchFile
            << "' was NOT created after " << m_archiverInitTimeout.count() << " seconds" );
    return false;
}

} // namespace mda
} // namespace daq
