//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/archive/ArchiveFile.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/filesystem.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"
#include "mda/archive/ArchiveIssues.h"
#include "mda/archive/MDAReceiver.h"
#include "mda/archive/Predicate.h"
#include "mda/common/LB.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

//----------------
// Constructors --
//----------------
ArchiveFile::ArchiveFile(const std::string& name,
                         const HistoGroupList& histoGroups,
                         const std::string& filePrefix,
                         unsigned frequencyLB,
                         int compression,
                         const std::string& dataset,
                         int prio,
                         unsigned dtc,
                         bool deleteAfterSaving,
                         bool saveAnnotations,
                         const std::shared_ptr<DBArchive>& dbArchive,
                         const std::string& partition)
    : m_name (name)
    , m_histoGroups (histoGroups)
    , m_filePrefix (filePrefix)
    , m_frequencyLB (frequencyLB)
    , m_lastTimeRun (0)
    , m_lastTimeLB (0)
    , m_ready2archive (false)
    , m_compression (compression)
    , m_dataset (dataset)
    , m_prio (prio)
    , m_dtc (dtc)
    , m_deleteAfterSaving (deleteAfterSaving)
    , m_saveAnnotations(saveAnnotations)
    , m_dbArchive (dbArchive)
    , m_partition (partition)
{
}

//--------------
// Destructor --
//--------------
ArchiveFile::~ArchiveFile () throw()
{
}

bool
ArchiveFile::readHistos (bool cacheHistos, unsigned runNumber, unsigned lb)
{
    // time to get histos?
    if (runNumber != m_lastTimeRun) {
        m_lastTimeRun = runNumber;
        m_lastTimeLB = 0;
    }

    if ((m_frequencyLB == 0 || lb < m_lastTimeLB + m_frequencyLB) && lb != LB::LastLB) {
        ERS_DEBUG (2, "file " << m_filePrefix << " not saved for LB " << lb);
        m_ready2archive = false;
        return false;
    }
    // record we are getting histos now
    m_lastTimeLB = lb;
    m_ready2archive = true;

    // call readHistos for each histo group object
    for (auto& hgroup: m_histoGroups) {
        hgroup.readHistos (cacheHistos);
    }
    
    return true;
}

bool
ArchiveFile::archive(coca::Register& cocaServer,
                     const std::string &topDir,
                     const RunTypeSMK &runType,
                     unsigned runNumber,
                     unsigned lb,
                     const Predicate& stopRequested)
{
    // time to archive?
    if (!m_ready2archive) {
        ERS_DEBUG(2, "file " << m_filePrefix << " not saved for LB " << lb);
        return false;
    }

    // create directory for ROOT files in case it disappeared while we were idle
    fs::path topDirPath(topDir);
    if (m_frequencyLB != 0) {
        // write to a unique directory to avoid collisions
        topDirPath /= fs::unique_path("mda-unique-%%%%-%%%%-%%%%-%%%%");
    }
    try {
        coca::FileSystem::create_dirs(topDirPath.string());
    } catch (const fs::filesystem_error &ex) {
        throw ArchiveIssues::CannotMakeDir(ERS_HERE, topDirPath.string(), ex);
    }

    // construct file name
    std::ostringstream buf;
    buf << 'r' << std::setw(10) << std::setfill('0') << runNumber
        << "_l" << LB::lb2string(lb, 4) << '_' << m_filePrefix;
    const auto basename = buf.str();

    // build full path name
    const fs::path tmpPath = topDirPath / fs::unique_path(basename+".%%%%-%%%%-%%%%-%%%%.root");
    const fs::path finalPath = topDirPath / fs::path(basename + ".root");

    // see if somebody wants us terminated
    if (stopRequested() && lb == LB::LastLB) {
        ers::error(ArchiveIssues::MissingFile(ERS_HERE, tmpPath.string()));
        return false;
    }

    // typename for mapping from histo group name to histogram lists
    std::map<std::string, ArchiveHistoGroup::Provider2HistoList> hgname2hlists;
    unsigned count = 0;
    try {
        ERS_LOG("going to write file: " << tmpPath);

        MDAReceiver mda(tmpPath.string(), m_compression, m_deleteAfterSaving, runNumber, m_saveAnnotations);
        for (auto& hgroup: m_histoGroups) {
            ArchiveHistoGroup::Provider2HistoList infos;
            count += hgroup.archive(mda, infos, stopRequested);
            if (!infos.empty()) {
                hgname2hlists[hgroup.name()].swap(infos);
            }
            if (stopRequested()) {
                ArchiveIssues::IncompleteFile err(ERS_HERE, tmpPath.string());
                if (lb == LB::LastLB) {
                    // For EoR archiving this is unexpected, complain
                    ers::error(err);
                } else {
                    // for in-run archiving this could happen if EoR happens
                    // while archiving is in progress, no big deal, but still
                    // try to save what has been collected so far.
                    ERS_LOG(err);
                }
                break;
            }
        }
        // tell it there is no more histograms
        mda.finish();

    } catch (const RootFileIssues::Exception &ex) {
        // something bad happened, file is probably corrupted or missing
        ers::error(ex);
        return false;
    }

    // register with CoCa
    std::string archive = DBData::FileNotRegistered;
    if (count == 0) {
        ERS_LOG("empty file not registered and will be deleted: " << tmpPath);
        // delete file so that temporary directory can be removed
        try {
            fs::remove(tmpPath);
        } catch(const fs::filesystem_error& exc) {
            // just complain to the log, this is non-fatal
            ERS_LOG("failed to delete ROOT file: " << exc.what());
        }
        return false;
    } else {
        ERS_LOG("Collected " << count << " histograms per file");
        try {
            // first rename it to final form
            ERS_LOG("renaming " << tmpPath << " to " << finalPath);
            fs::rename(tmpPath, finalPath);
        } catch (const fs::filesystem_error &ex) {
            ers::error(ArchiveIssues::RenameFailed(ERS_HERE, tmpPath.string(), finalPath.string(), ex));
            return false;
        }
        try {
            archive = cocaServer.registerFile(m_dataset, finalPath.string(), m_prio, m_dtc, topDirPath.string());
            ERS_INFO("file successfully registered to CoCa: " << finalPath << "::" << archive);
        } catch (coca::RegisterIssues::RegistrationError &ex) {
            if (lb != LB::LastLB and ex.get_message() == "file already registered") {
                // This could happen after restart of archiving application when previous
                // instance managed to save the data for the same LBN, just produce non-error message
                ers::log(ArchiveIssues::RegistrationFailed(ERS_HERE, tmpPath.string(), ex));
                try {
                    ERS_LOG("removing file: " << finalPath);
                    fs::remove(finalPath);
                } catch(const fs::filesystem_error& exc) {
                    // just complain to the log, this is non-fatal
                    ERS_LOG("failed to delete ROOT file: " << exc.what());
                }
            } else {
                ers::error(ArchiveIssues::RegistrationFailed(ERS_HERE, tmpPath.string(), ex));
            }
            return false;
        } catch (const coca::RegisterIssues::Exception &ex) {
            ers::error(ArchiveIssues::RegistrationFailed(ERS_HERE, tmpPath.string(), ex));
            return false;
        }
    }

    // add to database 
    for (auto& name2list: hgname2hlists) {
        for (auto& provider2list: name2list.second) {
            try {
                m_dbArchive->insertInterval(m_partition, runNumber, lb, name2list.first,
                                            provider2list.first, runType, provider2list.second,
                                            m_dataset, finalPath.filename().string(), archive);
            } catch (const Issues::Exception& ex) {
                ers::error(ArchiveIssues::HistoFileArchive(ERS_HERE, finalPath.filename().string(), ex));
            }
        }
    }
    
    return true;
}

} // namespace mda
} // namespace daq
