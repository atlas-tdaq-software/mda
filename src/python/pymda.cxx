//
// @file $Id$
//
// @author Andy Salnikov
//
// @brief Boost.Python interface to the mda library class
//

// C++ includes
#include <sstream>
#include <set>
#include <string>
#include <time.h>
#include <boost/python.hpp>

// Related classes
#include "mda/mda.h"
#include "mda/read_back/DBStat.h"
#include "mda/archive/DBArchive.h"
#include "mda/archive/OHSProvider.h"
#include "mda/archive/RunTypeSMK.h"
#include "mda/common/DBSchema.h"
#include "mda/common/LB.h"
#include "ers/Issue.h"

using namespace daq::mda;
using namespace boost::python;
namespace boopy = boost::python;

namespace {

// helper function to convert collections to Python list
template <typename Coll> boopy::list coll2list(const Coll &coll) {
    boopy::list list;
    for (auto &item : coll) {
        list.append(item);
    }
    return list;
}
template <typename K, typename V>
boopy::dict map2dict(const std::map<K, V>& map) {
    boopy::dict result;
    for (auto& pair: map) {
        result[pair.first] = pair.second;
    }
    return result;
}

// Make a copy of the LB::LastLB.
const uint32_t LastLB = LB::LastLB;

//
// Boost.Python-friendly wrapper for DBRead class.
//
class PyDBRead {
public:
    // constructor
    PyDBRead(const std::string &connStr = DBData::defConnStr(),
             const std::string &pfx = DBData::defPrefix());

    /// Get the list of partitions in the database, returns a list of strings.
    boopy::list partitions();

    /// Get the list of run in the database, returns a list of RunInfo objects.
    boopy::list runs(const std::string &partition, unsigned int nRuns);
    boopy::list runs_def(const std::string &partition) { return runs(partition, 30); }

    /// Get the list of OH servers for a given partitions/run, returns a list of strings.
    boopy::list ohsServers(const std::string &partition, unsigned run);

    ///  Get the list of providers for a given run/server, returns a list of strings.
    boopy::list ohsProviders(const std::string &partition, unsigned run,
                             const std::string &ohsServer);

    /**
     * Get the list of histograms names for a given run/server/provider.
     * All histograms below the given folder will be returned (recursive
     * behavior), and the returned names do not include leading folder name.
     * Returns a list of strings.
     */
    boopy::list histoNames(const std::string &partition, unsigned run,
                           const std::string &ohsServer, const std::string &ohsProvider,
                           const std::string &folder);

    /**
     * Get the list of the histogram names and folders in the
     * the given folder (one level only). Returns 2-element tuple,
     * first element is a list of sub-directories, second is the list of
     * histogram names.
     */
    boopy::tuple listdir(const std::string &partition, unsigned run,
                         const std::string &ohsServer, const std::string &ohsProvider,
                         const std::string &folder);

    /**
     *  Get the list of the histograms versions (lumi blocks) and their
     *  corresponding locations files for a given histogram name. Returns
     *  a list of HistoFile objects.
     */
    boopy::list histogram(const std::string &partition, unsigned run,
                          const std::string &ohsServer, const std::string &ohsProvider,
                          const std::string &histogram);

    /**
     * Get all combinations of runs and lumi blocks for a given histogram,
     * returns a list of RunLB objects.
     */
    boopy::list available(const std::string &partition, unsigned since, unsigned until,
                          const std::string &ohsServer, const std::string &ohsProvider,
                          const std::string &histogram);

private:
    DBRead m_reader;
};

// constructor
PyDBRead::PyDBRead(const std::string &connStr, const std::string &pfx)
    : m_reader(connStr, pfx) {}

/// Get the list of partitions in the database
boopy::list PyDBRead::partitions() {
    std::set<std::string> partitions;
    m_reader.partitions(partitions);
    boopy::list res;
    for (std::set<std::string>::const_iterator i = partitions.begin();
         i != partitions.end(); ++i) {
        res.append(boopy::str(i->c_str()));
    }
    return res;
}

/// Get the list of run in the database
boopy::list PyDBRead::runs(const std::string &partition, unsigned int nRuns) {
    std::set<RunInfo> runs;
    m_reader.runs(runs, partition, nRuns);
    boopy::list res;
    for (std::set<RunInfo>::const_iterator i = runs.begin(); i != runs.end(); ++i) {
        res.append(*i);
    }
    return res;
}

/// Get the list of OH servers for a given partitions/run
boopy::list PyDBRead::ohsServers(const std::string &partition, unsigned run) {
    std::set<std::string> servers;
    m_reader.ohsServers(servers, partition, run);
    boopy::list res;
    for (std::set<std::string>::const_iterator i = servers.begin(); i != servers.end();
         ++i) {
        res.append(*i);
    }
    return res;
}

///  Get the list of providers for a given run/server
boopy::list PyDBRead::ohsProviders(const std::string &partition, unsigned run,
                                   const std::string &ohsServer) {
    std::set<std::string> providers;
    m_reader.ohsProviders(providers, partition, run, ohsServer);
    boopy::list res;
    for (std::set<std::string>::const_iterator i = providers.begin();
         i != providers.end(); ++i) {
        res.append(*i);
    }
    return res;
}

/**
 * Get the list of histograms names for a given run/server/provider.
 * All histograms below the given folder will be returned (recursive
 * behavior), and the returned names do not include leading folder name.
 */
boopy::list PyDBRead::histoNames(const std::string &partition, unsigned run,
                                 const std::string &ohsServer,
                                 const std::string &ohsProvider,
                                 const std::string &folder) {
    std::set<std::string> names;
    m_reader.histoNames(names, partition, run, ohsServer, ohsProvider, folder);
    boopy::list res;
    for (std::set<std::string>::const_iterator i = names.begin(); i != names.end(); ++i) {
        res.append(boopy::str(i->c_str()));
    }
    return res;
}

/**
 * Get the list of the histogram names and folders in the
 * the given folder (one level only). Returns 2-element tuple,
 * first element is a list of sub-directories, second is the list of
 * histogram names.
 */
boopy::tuple PyDBRead::listdir(const std::string &partition, unsigned run,
                               const std::string &ohsServer,
                               const std::string &ohsProvider,
                               const std::string &folder) {
    std::set<std::string> folders;
    std::set<std::string> histos;
    m_reader.listdir(folders, histos, partition, run, ohsServer, ohsProvider, folder);

    boopy::list flist;
    for (std::set<std::string>::const_iterator i = folders.begin(); i != folders.end();
         ++i) {
        flist.append(boopy::str(i->c_str()));
    }
    boopy::list hlist;
    for (std::set<std::string>::const_iterator i = histos.begin(); i != histos.end();
         ++i) {
        hlist.append(boopy::str(i->c_str()));
    }

    return boopy::make_tuple(flist, hlist);
}

/**
 *  Get the list of the histograms versions (lumi blocks) and their
 *  corresponding locations files for a given histogram name. Returns
 *  a list of HistoFile objects.
 */
boopy::list PyDBRead::histogram(const std::string &partition, unsigned run,
                                const std::string &ohsServer,
                                const std::string &ohsProvider,
                                const std::string &histogram) {
    std::set<HistoFile> histos;
    m_reader.histogram(histos, partition, run, ohsServer, ohsProvider, histogram);

    boopy::list hlist;
    for (std::set<HistoFile>::const_iterator i = histos.begin(); i != histos.end(); ++i) {
        hlist.append(*i);
    }

    return hlist;
}

/**
 * Get all combinations of runs and lumi blocks for a given histogram,
 * returns a list of RunLB objects.
 */
boopy::list PyDBRead::available(const std::string &partition, unsigned since,
                                unsigned until, const std::string &ohsServer,
                                const std::string &ohsProvider,
                                const std::string &histogram) {
    std::set<RunLB> runLBs;
    m_reader.available(runLBs, partition, since, until, ohsServer, ohsProvider,
                       histogram);

    boopy::list tlist;
    for (std::set<RunLB>::const_iterator i = runLBs.begin(); i != runLBs.end(); ++i) {
        tlist.append(*i);
    }

    return tlist;
}

void exception_translator(const std::exception &exc) {
    PyErr_SetString(PyExc_RuntimeError, exc.what());
}

void ers_issue_translator(const ers::Issue &exc) {
    std::string msg = exc.message();
    for (auto cause = exc.cause(); cause != nullptr; cause = cause->cause()) {
        msg += "\n    caused by: ";
        msg += cause->message();
    }
    PyErr_SetString(PyExc_RuntimeError, msg.c_str());
}

void formatTime(char *buf, size_t sizeofbuf, time_t timeSec) {
    struct tm timeTm;
    localtime_r(&timeSec, &timeTm);
    strftime(buf, sizeofbuf, "%Y%m%dT%H%M%S%z", &timeTm);
}

std::string formatRunInfo(const RunInfo &run) {
    char buf[64];
    formatTime(buf, sizeof buf, run.time());
    std::ostringstream out;
    out << "<RunInfo(run=" << run.run() << ", runtype=" << run.runType()
        << ", SMK=" << run.smk() << ", time=" << buf << ")>";
    return out.str();
}

std::string formatRunLB(const RunLB &ts) {
    char buf[64];
    formatTime(buf, sizeof buf, ts.time());
    std::ostringstream out;
    out << "<RunLB(run=" << ts.run() << ", lb=" << ts.lb() << ", time=" << buf << ")>";
    return out.str();
}

//
// Boost.Python-friendly wrapper for DBArchive class.
//
class PyDBArchive {
public:
    // constructor
    PyDBArchive(const std::string &connStr, const std::string &pfx = DBData::defPrefix());

    /**
     *  @brief Store histogram list and return their IDs.
     *
     *  @param[in] histoLBList Map of histogram names and their corresponding LBs,
     *                         keys must be strings, values can be None or list of LBs.
     *  @return Map of historam IDs and their corresponding LBs.
     *
     *  @throw Issues::CoralException Thrown for databases-related conditions.
     */
    boopy::dict insertHistoList(const boopy::dict &histoLBList);

    /**
     *  @brief Store histograms for a given partition/run/LB/etc.
     *
     *  @param[in] partition    Partition name.
     *  @param[in] run          Run number.
     *  @param[in] lb           Lumi block number.
     *  @param[in] histoGroup   Histogram group name.
     *  @param[in] ohsServer    OH server name.
     *  @param[in] ohsProvider  OH provider name.
     *  @param[in] runType      Run type name.
     *  @param[in] smk          Super Master Key.
     *  @param[in] histoIdLBList List of historam IDs and their corresponding LBs.
     *  @param[in] dataset      Dataset name.
     *  @param[in] file         File name.
     *  @param[in] archive      Archive name.
     *
     *  @throw Issues::CoralException Thrown for databases-related conditions.
     */
    void insertInterval(const std::string &partition, unsigned run, unsigned lb,
                        const std::string &histoGroup, const std::string &ohsServer,
                        const std::string &ohsProvider, const std::string &runType,
                        unsigned smk, const boopy::dict &histoIdLBList,
                        const std::string &dataset, const std::string &file,
                        const std::string &archive);

    /**
     *  @brief Update archive name for a given file name.
     *
     *  This method is useful when CoCa registration fails for some file and file is
     *  registered in MDA database with some non-name for archive. Using this method
     *  it is possible to change corresponding archive name at a later time after
     *  registering file in CoCa. In CoCa file name and dataset name must be unique,
     *  so this method takes dataset name and file name and a new archive name. If
     *  for some reason there is more than one matching record then all of them will
     *  be updated.
     *
     *  @param[in] dataset      Dataset name.
     *  @param[in] file         File name.
     *  @param[in] archive      New archive name.
     *
     *  @throw Issues::DBConnectionFailure Thrown when database connection fails.
     *  @throw Issues::CoralException Thrown for other databases-related conditions.
     *  @throw Issues::NoSuchRecord Thrown if file name is not in the database.
     */
    void updateArchive(const std::string &dataset, const std::string &file,
                       const std::string &archive);

private:
    DBArchive m_archive;
};

// constructor
PyDBArchive::PyDBArchive(const std::string &connStr, const std::string &pfx)
    : m_archive(connStr, pfx) {}

// Store histogram lists and return IDs.
boopy::dict PyDBArchive::insertHistoList(const boopy::dict &histoLBList) {
    // Convert python dict to C++ map
    DBArchive::HistoLBList hmap;

    boopy::list keys = histoLBList.keys();
    int nkeys = boopy::len(keys);
    for (int ih = 0; ih != nkeys; ++ih) {

        // get key
        boopy::object key = keys[ih];

        std::string hname = boopy::extract<std::string>(key);
        DBArchive::HistoLBList::mapped_type lbs;

        // value can be None or list of LB numbers
        boopy::object val = histoLBList[key];
        if (val.ptr() != Py_None) {
            boopy::list pylbs = boopy::extract<boopy::list>(val);
            int nlbs = boopy::len(pylbs);
            lbs.reserve(nlbs);
            for (int ilb = 0; ilb != nlbs; ++ilb) {
                unsigned lb = boopy::extract<unsigned>(pylbs[ilb]);
                lbs.push_back(lb);
            }
        }

        hmap.insert(DBArchive::HistoLBList::value_type(hname, lbs));
    }

    // call mda function
    DBArchive::HistoIdLBList hidmap;
    m_archive.insertHistoList(hmap, hidmap);

    boopy::dict result;

    // convert c++ map into Py dict
    for (DBArchive::HistoIdLBList::const_iterator it = hidmap.begin(); it != hidmap.end();
         ++it) {

        // LB list
        typedef DBArchive::HistoIdLBList::mapped_type LBList;
        const LBList &lbs = it->second;

        boopy::object val;
        if (not lbs.empty()) {
            boopy::list pylbs;
            for (LBList::const_iterator lbit = lbs.begin(); lbit != lbs.end(); ++lbit) {
                pylbs.append(*lbit);
            }
            val = pylbs;
        }

        result[boopy::long_(it->first)] = val;
    }

    return result;
}

// Store histograms for a given partition/run/LB/etc.
void PyDBArchive::insertInterval(const std::string &partition, unsigned run, unsigned lb,
                                 const std::string &histoGroup,
                                 const std::string &ohsServer,
                                 const std::string &ohsProvider,
                                 const std::string &runType, unsigned smk,
                                 const boopy::dict &histoIdLBList,
                                 const std::string &dataset, const std::string &file,
                                 const std::string &archive) {
    // Convert python dict to C++ map
    DBArchive::HistoIdLBList hidmap;

    boopy::list keys = histoIdLBList.keys();
    int nkeys = boopy::len(keys);
    for (int ih = 0; ih != nkeys; ++ih) {

        // get key
        boopy::object key = keys[ih];

        uint32_t hid = boopy::extract<uint32_t>(key);
        DBArchive::HistoIdLBList::mapped_type lbs;

        // value can be None or list of LB numbers
        boopy::object val = histoIdLBList[key];
        if (val.ptr() != Py_None) {
            boopy::list pylbs = boopy::extract<boopy::list>(val);
            int nlbs = boopy::len(pylbs);
            lbs.reserve(nlbs);
            for (int ilb = 0; ilb != nlbs; ++ilb) {
                unsigned lb = boopy::extract<unsigned>(pylbs[ilb]);
                lbs.push_back(lb);
            }
        }

        hidmap[hid].swap(lbs);
    }

    // call MDA method
    m_archive.insertInterval(partition, run, lb, histoGroup,
                             OHSProvider(ohsServer, ohsProvider),
                             RunTypeSMK(runType, smk), hidmap, dataset, file, archive);
}

// Update archive name for a given file name.
void PyDBArchive::updateArchive(const std::string &dataset, const std::string &file,
                                const std::string &archive) {
    m_archive.updateArchive(dataset, file, archive);
}

//
// Boost.Python-friendly wrapper for DBStat class.
//
class PyDBStat {
public:
    // constructor
    PyDBStat(const std::string &connStr = DBData::defConnStr(),
             const std::string &pfx = DBData::defPrefix());

    /**
     *  @brief Retrieve the number of saved histograms per run for last several runs.
     *
     *  @param[in]  nRuns    Number of runs to include into statistics, only last nRuns.
     *  @return Dictionary, keys are run numbers, values are StatRun objects.
     */
    boopy::dict nHistoPerRun(int nRuns = 100);

    /**
     *  @brief Retrieve the number of saved histograms per OHS server.
     *
     *  @return Dictionary, keys are server names, values are StatRun objects.
     */
    boopy::dict nHistoPerServer();

    /**
     *  @brief Retrieve the number of saved histograms per OHS provider.
     *
     *  @return Dictionary, keys are provider names, values are StatRun objects.
     */
    boopy::dict nHistoPerProvider();

    /**
     *  @brief Retrieve the number of saved histograms per dataset.
     *
     *  @return Dictionary, keys are dataset names, values are StatRun objects.
     */
    boopy::dict nHistoPerDataset();

private:
    DBStat m_db;
};

PyDBStat::PyDBStat(const std::string &connStr, const std::string &pfx)
    : m_db(connStr, pfx) {}

boopy::dict PyDBStat::nHistoPerRun(int nRuns) {
    // call C++ method
    std::map<unsigned, StatRun> stat;
    m_db.nHistoPerRun(stat, nRuns);

    // convert the result to Python dict
    boopy::dict result;
    for (std::map<unsigned, StatRun>::const_iterator it = stat.begin(); it != stat.end();
         ++it) {
        result[it->first] = it->second;
    }
    return result;
}

// generate all overloads for default arguments
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(nHistoPerRun_overloads, nHistoPerRun, 0, 1)

boopy::dict PyDBStat::nHistoPerServer() {
    // call C++ method
    std::map<std::string, StatRun> stat;
    m_db.nHistoPerServer(stat);

    // convert the result to Python dict
    boopy::dict result;
    for (std::map<std::string, StatRun>::const_iterator it = stat.begin();
         it != stat.end(); ++it) {
        result[it->first] = it->second;
    }
    return result;
}

boopy::dict PyDBStat::nHistoPerProvider() {
    // call C++ method
    std::map<std::string, StatRun> stat;
    m_db.nHistoPerProvider(stat);

    // convert the result to Python dict
    boopy::dict result;
    for (std::map<std::string, StatRun>::const_iterator it = stat.begin();
         it != stat.end(); ++it) {
        result[it->first] = it->second;
    }
    return result;
}

boopy::dict PyDBStat::nHistoPerDataset() {
    // call C++ method
    std::map<std::string, StatRun> stat;
    m_db.nHistoPerDataset(stat);

    // convert the result to Python dict
    boopy::dict result;
    for (std::map<std::string, StatRun>::const_iterator it = stat.begin();
         it != stat.end(); ++it) {
        result[it->first] = it->second;
    }
    return result;
}

//
// Boost.Python-friendly wrapper for DBSchema class.
//
class PyDBSchema {
public:
    // constructor
    PyDBSchema(const std::string &connStr = DBData::defConnStr(),
               const std::string &pfx = DBData::defPrefix(),
               bool update = true)
        : m_db(connStr, pfx, update) {}

    void makeSchema(int version = -1, bool makeMetaTable = true) {
        m_db.makeSchema(version, makeMetaTable);
    }

    void makeMetaTable(int version = -1) { m_db.makeMetaTable(version); }

    boopy::list makeSequncesSQL(int version = -1) {
        return coll2list(m_db.makeSequncesSQL(version));
    }

    std::string metaTableName() const { return m_db.metaTableName(); }

    boopy::list tables(int version = -1) { return coll2list(m_db.tables(version)); }

    boopy::list dbTables() { return coll2list(m_db.dbTables()); }

    bool hasMetaTable() { return m_db.hasMetaTable(); }

    boopy::dict getMeta() { return map2dict(m_db.getMeta()); }

    void setMeta(std::string const& key, std::string const& value) { m_db.setMeta(key, value); }

    int dbSchemaVersion() { return m_db.dbSchemaVersion(); }

    static boopy::list supportedSchemaVersions() {
        return coll2list(DBSchema::supportedSchemaVersions());
    }

private:
    DBSchema m_db;
};

// generate all overloads for default arguments
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(makeSchema_overloads, makeSchema, 0, 2)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(makeMetaTable_overloads, makeMetaTable, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(makeSequncesSQL_overloads, makeSequncesSQL, 0, 1)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(tables_overloads, tables, 0, 1)

} // namespace

BOOST_PYTHON_MODULE(libpymda) {
    // handle exceptions, order is important!
    register_exception_translator<std::exception>(exception_translator);
    register_exception_translator<ers::Issue>(ers_issue_translator);

    scope().attr("LastLB") = ::LastLB;
    scope().attr("defConnStr") = DBData::defConnStr();

    // wrapper for RunInfo class
    class_<RunInfo>("RunInfo", init<unsigned, std::string, unsigned, time_t>())
        .def("run", &RunInfo::run)
        .def("runType", &RunInfo::runType, return_value_policy<copy_const_reference>())
        .def("smk", &RunInfo::smk)
        .def("time", &RunInfo::time)
        .def("__str__", &formatRunInfo);

    // wrapper for RunLB class
    class_<RunLB>("RunLB", init<unsigned, unsigned, time_t>())
        .def("run", &RunLB::run)
        .def("lb", &RunLB::lb)
        .def("time", &RunLB::time)
        .def("__str__", &formatRunLB);

    // wrapper for HistoFile_t class
    class_<HistoFile>(
        "HistoFile",
        init<std::string, unsigned, std::string, std::string, std::string, std::string>())
        .def("histoName", &HistoFile::histoName,
             return_value_policy<copy_const_reference>())
        .def("lb", &HistoFile::lb)
        .def("dataset", &HistoFile::dataset, return_value_policy<copy_const_reference>())
        .def("archive", &HistoFile::archive, return_value_policy<copy_const_reference>())
        .def("file", &HistoFile::file, return_value_policy<copy_const_reference>())
        .def("histoPath", &HistoFile::histoPath,
             return_value_policy<copy_const_reference>());

    // wrapper for DBRead class
    class_<PyDBRead, boost::noncopyable>(
        "DBRead",
        init<optional<std::string, std::string> >((
            arg("connStr") = DBData::defConnStr(),
            arg("pfx") = DBData::defPrefix()
        )))
        .def("partitions", &PyDBRead::partitions,
             "Get the list of partition names in the database")
        .def("runs", &PyDBRead::runs, (arg("partition"), arg("nRuns")),
             "Get the list of runs for given partition name as RunInfo objects")
        .def("runs", &PyDBRead::runs_def, arg("partition"),
             "Get the list of runs for given partition name as RunInfo objects")
        .def("ohsServers", &PyDBRead::ohsServers, (arg("partition"), arg("run")),
             "Returns the list of OH server names for a given partition/run")
        .def("ohsProviders", &PyDBRead::ohsProviders,
             (arg("partition"), arg("run"), arg("ohsServer")),
             "Get the list of provider names for a given partition/run/server")
        .def("histoNames", &PyDBRead::histoNames,
             (arg("partition"), arg("run"), arg("ohsServer"), arg("ohsProvider"),
              arg("folder")),
             "Get the list of histograms names for a given run/server/provider.\n"
             "All histograms below the given folder will be returned (recursive\n"
             "behavior), and the returned names do not include leading folder name.")
        .def("listdir", &PyDBRead::listdir,
             (arg("partition"), arg("run"), arg("ohsServer"), arg("ohsProvider"),
              arg("folder")),
             "Get the list of the histogram names and folders in the  given folder\n"
             "(one level only). Returns 2-element tuple, first element is a list of\n"
             "sub-directories, second is the list of histogram names.")
        .def("histogram", &PyDBRead::histogram,
             (arg("partition"), arg("run"), arg("ohsServer"), arg("ohsProvider"),
              arg("histogram")),
             "Get the list of the histograms versions (lumi blocks) and their\n"
             "corresponding locations files for a given histogram name. Returns\n"
             "a list of HistoFile objects.")
        .def("available", &PyDBRead::available,
             (arg("partition"), arg("since"), arg("until"), arg("ohsServer"),
              arg("ohsProvider"), arg("histogram")),
             "Get all combinations of runs and lumi blocks for a given histogram,\n"
             "returns a list of RunLB objects");

    // wrapper for DBArchive class
    class_<PyDBArchive, boost::noncopyable>(
        "DBArchive",
        init<std::string, optional<std::string> >((
            arg("connStr"),
            arg("pfx") = DBData::defPrefix()
        )))
        .def("insertHistoList", &PyDBArchive::insertHistoList, arg("histoLBList"),
             "Store histogram lists and return IDs. Argument is a dict of histogram "
             "names "
             "and their corresponding LBs, keys must be strings, values can be None or "
             "list of LBs.")
        .def("insertInterval", &PyDBArchive::insertInterval,
             (arg("partition"), arg("run"), arg("lb"), arg("histoGroup"),
              arg("ohsServer"), arg("ohsProvider"), arg("runType"), arg("smk"),
              arg("histoIdLBList"), arg("dataset"), arg("file"), arg("archive")),
             "Store histograms for a given partition/run/LB/etc..")
        .def("updateArchive", &PyDBArchive::updateArchive,
             (arg("dataset"), arg("file"), arg("archive")),
             "Update archive name for a given dataset and file name.");

    // wrapper for StatRun class
    class_<StatRun>("StatRun", init<uint32_t, uint32_t>(args("nHist", "nHistVersions")))
        .def("nHist", &StatRun::nHist)
        .def("nHistVersions", &StatRun::nHistVersions)
        .def(self_ns::str(self))
        .def(self_ns::repr(self));

    // wrapper for DBStat class
    class_<PyDBStat, boost::noncopyable>(
        "DBStat",
        init<optional<std::string, std::string> >((
            arg("connStr") = DBData::defConnStr(),
            arg("pfx") = DBData::defPrefix()
        )))
        .def("nHistoPerRun", &PyDBStat::nHistoPerRun,
             nHistoPerRun_overloads(
                 (arg("nRuns") = 100),
                 "Retrieve the number of saved histograms per run for last several runs. "
                 "Returns dictionary, keys are run numbers, values are StatRun objects."))
        .def("nHistoPerServer", &PyDBStat::nHistoPerServer,
             "Retrieve the number of saved histograms per OHS server. "
             "Returns dictionary, keys are server names, values are StatRun objects.")
        .def("nHistoPerProvider", &PyDBStat::nHistoPerProvider,
             "Retrieve the number of saved histograms per OHS provider. "
             "Returns dictionary, keys are provider names, values are StatRun objects.")
        .def("nHistoPerDataset", &PyDBStat::nHistoPerDataset,
             "Retrieve the number of saved histograms per dataset. "
             "Returns dictionary, keys are dataset names, values are StatRun objects.");

    // wrapper for DBSchema class
    class_<PyDBSchema, boost::noncopyable>(
        "DBSchema",
        init<optional<std::string, std::string, bool> >((
            arg("connStr") = DBData::defConnStr(),
            arg("pfx") = DBData::defPrefix(),
            arg("update") = true
        )))
        .def("makeSchema", &PyDBSchema::makeSchema,
             makeSchema_overloads(
                 (arg("version") = -1, arg("makeMetaTable") = true),
                 "Create completely new schema. Database must be initially empty. If "
                 "version is negative then current default schema version is used. If "
                 "makeMetaTable is True then META table is created too."))
        .def("makeMetaTable", &PyDBSchema::makeMetaTable,
             makeMetaTable_overloads(
                 (arg("version") = -1),
                 "Create schema metadata table and write version number into it. "
                 "If version is negative then current default schema version is used."))
        .def("makeSequncesSQL", &PyDBSchema::makeSequncesSQL,
             makeSequncesSQL_overloads((arg("version") = -1),
                                       "Generate SQL for creating Oracle sequences."))
        .def("metaTableName", &PyDBSchema::metaTableName,
             "Return table name for schema metadata.")
        .def("tables", &PyDBSchema::tables,
             tables_overloads(
                 (arg("version") = -1),
                 "Return table names which are defined in a specified schema version."))
        .def("dbTables", &PyDBSchema::dbTables,
             "Return table names in MDA database which have matching prefix.")
        .def("hasMetaTable", &PyDBSchema::hasMetaTable,
             "Return True is schema metadata table exists.")
        .def("getMeta", &PyDBSchema::getMeta,
             "Return all records from META table as mapping from key to value.")
        .def("setMeta", &PyDBSchema::setMeta,
             "Add/update record in metadata table.")
        .def("dbSchemaVersion", &PyDBSchema::dbSchemaVersion,
             "Return recorded schema version. -1 is returned is schema version table "
             "is missing.")
        .def("supportedSchemaVersions", &PyDBSchema::supportedSchemaVersions,
             "Return list of supported schema versions.")
        .staticmethod("supportedSchemaVersions");
}
