//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class HgroupTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/HgroupTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

namespace {

    /**
     *  @class Issues::NoSuchRecord
     *  @brief Exception generated for the missing record in the database when 
     *  the code expects it to be there.
     */
    ERS_DECLARE_ISSUE_BASE (HgroupTableIssues, NoStatColumns, daq::mda::Issues::Exception,
                "table " << tableName << " does not have stat columns", 
                , ((std::string) tableName))

}

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

HgroupTable::HgroupTable (const std::string& prefix,
                          const std::string& tableName)
    : UniqueIdTable (prefix, tableName, "HGRP_ID")
    , m_hasStatColumns(true)
{
}

uint32_t
HgroupTable::findOrInsert (uint32_t hgroup_id,
                           uint32_t file_id,
                           uint32_t hserver_id,
                           uint32_t provider_id,
                           uint32_t hcount,
                           uint32_t vcount,
                           Transaction& tr)
{

    ERS_DEBUG(3, tableName() << ": with hgroup=" << hgroup_id << " file=" << file_id
            << " hserver=" << hserver_id << " provider=" << provider_id);

    coral::AttributeList data;
    coral_helpers::addAttr(data, "HGRPNAME_ID", hgroup_id);
    coral_helpers::addAttr(data, "FILE_ID", file_id);
    coral_helpers::addAttr(data, "HSRV_ID", hserver_id);
    coral_helpers::addAttr(data, "PROV_ID", provider_id);

    uint32_t id = UniqueIdTable::findOrInsert(data, tr);
    
    // update statistics info
    if (id > 0 and m_hasStatColumns) {
        
        coral::AttributeList bv;
        coral_helpers::addAttr(bv, "ID", id);
        coral_helpers::addAttr(bv, "HCOUNT", hcount);
        coral_helpers::addAttr(bv, "VCOUNT", vcount);
        
        std::string upd ("STAT_HCOUNT = STAT_HCOUNT + :HCOUNT, STAT_VCOUNT = STAT_VCOUNT + :VCOUNT");
        std::string where ("HGRP_ID = :ID");

        try {
            tableHandle(tr).dataEditor().updateRows (upd, where, bv);
        } catch (const coral::Exception& ex) {
            // it may have older schema with those columns missing,
            // issue warning then disable this feature and continue
            ers::warning(HgroupTableIssues::NoStatColumns(ERS_HERE, tableName(), ex));
            m_hasStatColumns = false;
        }

    }
    
    return id;

}

void
HgroupTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("HgroupTable::createTable");
    td.setName(tableName());

    td.insertColumn(idColName(), "int");
    td.setNotNullConstraint(idColName(), true);
    td.setPrimaryKey(idColName());

    auto columns = {"HGRPNAME_ID", "FILE_ID", "HSRV_ID", "PROV_ID"};
    for (auto column: columns) {
        td.insertColumn(column, "int");
        td.setNotNullConstraint(column, true);
    }

    bool Unique = true;
    td.createIndex(TBL("FILE_ID_IDX"), "FILE_ID", not Unique);
    td.createIndex(TBL("HSRV_ID_IDX"), "HSRV_ID", not Unique);
    td.createIndex(TBL("PROV_ID_IDX"), "PROV_ID", not Unique);
    td.createIndex(TBL("UNI"), {"HGRPNAME_ID", "FILE_ID", "HSRV_ID", "PROV_ID"}, Unique);

    td.createForeignKey(TBL("HGRPNAME_ID_FK"), "HGRPNAME_ID", PFX("HGRPNAME"), "HGRPNAME_ID");
    td.createForeignKey(TBL("FILE_ID_FK"), "FILE_ID", PFX("FILE"), "FILE_ID");
    td.createForeignKey(TBL("HSRV_ID_FK"), "HSRV_ID", PFX("HSERVER"), "HSRV_ID");
    td.createForeignKey(TBL("PROV_ID_FK"), "PROV_ID", PFX("PROVIDER"), "PROV_ID");

    tran.session().nominalSchema().createTable(td);
}
