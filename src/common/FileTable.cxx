//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class FileTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/FileTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

/*
 * FileTable
 */

FileTable::FileTable (const std::string& prefix, const std::string& tableName)
    : UniqueIdTable (prefix, tableName, "FILE_ID")
{
}

uint32_t
FileTable::findOrInsert(uint32_t runlb_id,
                        const std::string& file,
                        uint32_t ds_id,
                        uint32_t arch_id,
                        Transaction& tr)
{

    ERS_DEBUG(3, tableName() << ": with runlb_id=" << runlb_id << " file=" << file
            << " ds=" << ds_id << " arch=" << arch_id);

    coral::AttributeList data;
    coral_helpers::addAttr(data, "RUNLB_ID", runlb_id);
    coral_helpers::addAttr(data, "FILE_NAME", file);
    coral_helpers::addAttr(data, "DS_ID", ds_id);
    coral_helpers::addAttr(data, "ARCH_ID", arch_id);

    uint32_t id = UniqueIdTable::findOrInsert(data, tr);
    return id;

}

long
FileTable::updateArchive(const std::string& file,
                         uint32_t ds_id,
                         uint32_t arch_id,
                         Transaction& tr)
{

    ERS_DEBUG(3, tableName() << ": update archive with file=" << file
            << " ds=" << ds_id << " arch=" << arch_id);

    coral::AttributeList data;
    coral_helpers::addAttr(data, "FILE_NAME", file);
    coral_helpers::addAttr(data, "DS_ID", ds_id);
    coral_helpers::addAttr(data, "ARCH_ID", arch_id);

    std::string condition = "FILE_NAME = :FILE_NAME AND DS_ID = :DS_ID";
    std::string setClause = "ARCH_ID = :ARCH_ID";
    long count = tableHandle(tr).dataEditor().updateRows(setClause, condition, data);
    return count;
}

void
FileTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("FileTable::createTable");
    td.setName(tableName());

    td.insertColumn(idColName(), "int");
    td.setNotNullConstraint(idColName(), true);
    td.setPrimaryKey(idColName());

    td.insertColumn("RUNLB_ID", "int");
    td.setNotNullConstraint("RUNLB_ID", true);

    td.insertColumn("FILE_NAME", "string");
    td.setNotNullConstraint("FILE_NAME", true);

    td.insertColumn("DS_ID", "int");
    td.setNotNullConstraint("DS_ID", true);

    td.insertColumn("ARCH_ID", "int");
    td.setNotNullConstraint("ARCH_ID", true);

    bool Unique = true;
    td.createIndex(TBL("RUNLB_ID_IDX"), "RUNLB_ID", not Unique);
    td.createIndex(TBL("DS_ID_IDX"), "DS_ID", not Unique);
    td.createIndex(TBL("ARCH_ID_IDX"), "ARCH_ID", not Unique);

    td.createForeignKey(TBL("RUNLB_ID_FK"), "RUNLB_ID", PFX("RUNLB"), "RUNLB_ID");
    td.createForeignKey(TBL("DS_ID_FK"), "DS_ID", PFX("DATASET"), "DS_ID");
    td.createForeignKey(TBL("ARCH_ID_FK"), "ARCH_ID", PFX("ARCHIVE"), "ARCH_ID");

    tran.session().nominalSchema().createTable(td);
}
