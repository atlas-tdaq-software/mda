//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class RunLBTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/RunLBTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <CoralBase/Exception.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"
#include "mda/common/LB.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

RunLBTable::RunLBTable(const std::string& prefix, const std::string& tableName)
    : UniqueIdTable (prefix, tableName, "RUNLB_ID")
{
}

uint32_t
RunLBTable::findOrInsert (unsigned run,
                          unsigned lb,
                          unsigned smk,
                          uint32_t runtype_id,
                          uint32_t part_id,
                          Transaction& tran)
{

    ERS_DEBUG(3, tableName() << ": with run=" << run << " lb=" << lb << " smk=" << smk
            << " runtype=" << runtype_id << " partition=" << part_id);

    // special condition may include "IS NULL"
    std::string cond = "RUN_NUMBER=:RUN_NUMBER AND SM_KEY=:SM_KEY AND "
        "RUNTYPE_ID=:RUNTYPE_ID AND PART_ID=:PART_ID";

    coral::AttributeList data;
    coral_helpers::addAttr(data, "RUN_NUMBER", run);
    coral_helpers::addAttr(data, "SM_KEY", smk);
    coral_helpers::addAttr(data, "RUNTYPE_ID", runtype_id);
    coral_helpers::addAttr(data, "PART_ID", part_id);

    if ( lb != LB::LastLB ) {
        coral_helpers::addAttr(data, "LB_NUMBER", lb);
        cond += " AND LB_NUMBER=:LB_NUMBER";
    } else {
        cond += " AND LB_NUMBER IS NULL";
    }

    // call base class with all collected data
    uint32_t id = UniqueIdTable::findOrInsert(data, tran, 0, cond);
    return id;

}

void
RunLBTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("RunLBTable::createTable");
    td.setName(tableName());

    td.insertColumn(idColName(), "int");
    td.setNotNullConstraint(idColName(), true);
    td.setPrimaryKey(idColName());

    td.insertColumn("RUN_NUMBER", "int");
    td.setNotNullConstraint("RUN_NUMBER", true);

    td.insertColumn("LB_NUMBER", "int");
    td.setNotNullConstraint("LB_NUMBER", true);

    td.insertColumn("SM_KEY", "int");
    td.setNotNullConstraint("SM_KEY", true);

    td.insertColumn("RUNTYPE_ID", "int");
    td.setNotNullConstraint("RUNTYPE_ID", true);

    td.insertColumn("PART_ID", "int");
    td.setNotNullConstraint("PART_ID", true);

    td.insertColumn("TS_UTC", "time stamp");
    td.setNotNullConstraint("TS_UTC", true);

    bool Unique = true;
    td.createIndex(TBL("RUN_LB_UNI"), {"PART_ID", "RUN_NUMBER", "LB_NUMBER"}, Unique);
    td.createIndex(TBL("PART_ID_IDX"), "PART_ID", not Unique);
    td.createIndex(TBL("TS_UTC_IDX"), "TS_UTC", not Unique);

    td.createForeignKey(TBL("PART_ID_FK"), "PART_ID", PFX("PARTITION"), "PART_ID");
    td.createForeignKey(TBL("RUNTYPE_ID_FK"), "RUNTYPE_ID", PFX("RUNTYPE"), "RUNTYPE_ID");

    tran.session().nominalSchema().createTable(td);
}
