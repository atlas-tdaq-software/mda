
//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/MetaTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------


/*
 * MetaTable
 */

MetaTable::MetaTable(const std::string& prefix, const std::string& tableName)
    : m_tableName(prefix+'_'+tableName),
      m_columnName("VERSION_NUMBER")
{
}

void
MetaTable::createTable(int schemaVersion, Transaction& tran)
{
    auto& schema = tran.session().nominalSchema();

    coral::TableDescription td("MetaTable::createTable");
    td.setName(m_tableName);
    td.insertColumn("METAKEY", "string");
    td.setPrimaryKey("METAKEY");
    td.insertColumn("VALUE", "string");
    schema.createTable(td);

    // add single row
    coral::ITable& table = schema.tableHandle(m_tableName);
    coral::AttributeList data;
    coral_helpers::addAttr(data, "METAKEY", std::string("VERSION"));
    coral_helpers::addAttr(data, "VALUE", std::to_string(schemaVersion));
    table.dataEditor().insertRow(data);
}

bool
MetaTable::tableExists(Transaction& tran)
{
    auto& schema = tran.session().nominalSchema();
    return schema.existsTable(m_tableName);
}

bool
MetaTable::hasKey(const std::string& key, Transaction& tran)
{
    auto& schema = tran.session().nominalSchema();

    if (not schema.existsTable(m_tableName)) {
        return false;
    }

    std::unique_ptr<coral::IQuery> q(schema.newQuery());
    q->addToTableList(m_tableName, "META");
    coral_helpers::defineOutput<int>(*q, "COUNT(*)", "COUNT");

    std::string cond = "META.METAKEY = :MKEY";
    coral::AttributeList data;
    coral_helpers::addAttr(data, "MKEY", key);
    q->setCondition(cond, data);

    // execute
    coral::ICursor& cursor = q->execute() ;
    int count = 0;
    while (cursor.next()) {
        const coral::AttributeList& row = cursor.currentRow();
        count = row[0].data<int>();
    }

    return bool(count);
}

std::string
MetaTable::get(const std::string& key, Transaction& tran,
               const std::string& defVal)
{
    auto& schema = tran.session().nominalSchema();

    if (not schema.existsTable(m_tableName)) {
        return defVal;
    }

    std::unique_ptr<coral::IQuery> q(schema.newQuery());
    q->addToTableList(m_tableName, "META");
    coral_helpers::defineOutput<std::string>(*q, "META.VALUE", "VALUE");

    std::string cond = "META.METAKEY = :MKEY";
    coral::AttributeList data;
    coral_helpers::addAttr(data, "MKEY", key);
    q->setCondition(cond, data);

    // execute
    coral::ICursor& cursor = q->execute() ;
    std::string value = defVal;
    while (cursor.next()) {
        const coral::AttributeList& row = cursor.currentRow();
        value = row[0].data<std::string>();
    }

    return value;
}

std::map<std::string, std::string>
MetaTable::getAll(Transaction& tran)
{
    std::map<std::string, std::string> map;

    auto& schema = tran.session().nominalSchema();

    if (not schema.existsTable(m_tableName)) {
        return map;
    }

    std::unique_ptr<coral::IQuery> q(schema.newQuery());
    q->addToTableList(m_tableName, "META");
    coral_helpers::defineOutput<std::string>(*q, "META.METAKEY", "METAKEY");
    coral_helpers::defineOutput<std::string>(*q, "META.VALUE", "VALUE");

    // execute
    coral::ICursor& cursor = q->execute() ;
    while (cursor.next()) {
        const coral::AttributeList& row = cursor.currentRow();
        map.emplace(row[0].data<std::string>(), row[1].data<std::string>());
    }

    return map;
}

void
MetaTable::put(const std::string& key, const std::string& value, Transaction& tran)
{
    auto& schema = tran.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(m_tableName);

    // first drop existing row if any
    coral::AttributeList condData;
    coral_helpers::addAttr(condData, "MKEY", key);
    std::string cond = "METAKEY = :MKEY";
    table.dataEditor().deleteRows(cond, condData);

    coral::AttributeList data;
    coral_helpers::addAttr(data, "METAKEY", key);
    coral_helpers::addAttr(data, "VALUE", value);
    table.dataEditor().insertRow(data);
}

int
MetaTable::dbSchemaVersion(Transaction& tran)
{
    auto& schema = tran.session().nominalSchema();

    if (not schema.existsTable(m_tableName)) {
        return -1;
    }

    std::unique_ptr<coral::IQuery> q(schema.newQuery());
    q->addToTableList(m_tableName, "META");
    coral_helpers::defineOutput<std::string>(*q, "META.VALUE", "VERSION");

    std::string cond = "META.METAKEY = :VERSION_KEY";
    coral::AttributeList data;
    coral_helpers::addAttr(data, "VERSION_KEY", std::string("VERSION"));
    q->setCondition(cond, data);

    // execute
    coral::ICursor& cursor = q->execute() ;
    int version = -1;
    int count = 0;
    while (cursor.next()) {
        const coral::AttributeList& row = cursor.currentRow();
        auto version_string = row[0].data<std::string>();
        // make sure it is valid number
        try {
            size_t pos = 0;
            version = std::stoi(version_string, &pos);
            if (pos != version_string.size()) {
                // some garbage in string
                throw std::invalid_argument(version_string);
            }
        } catch (std::exception const& exc) {
            throw Issues::SchemaVersionRecordError(ERS_HERE,
                "VERSION record has invalid format: '" + version_string + "'");
        }
        ++ count;
    }

    if (count != 1) {
        throw Issues::SchemaVersionRecordError(ERS_HERE, "META table is missing VERSION record");
    }

    return version;
}
