//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/DBSchema.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ConnectionServiceException.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/Transaction.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;


int DBSchema::s_latestVersion = 1;
std::vector<int> DBSchema::s_supportedVersions = {1};


/*
 * DBSchema
 */

DBSchema::DBSchema(const std::string& connStr, const std::string& prefix, bool update)
    : DBData(connStr, prefix, update, CacheMode::Cache)
    , m_prefix(prefix)
{
}

void
DBSchema::makeSchema(int version, bool makeVersionTable)
try {
    if (version < 0) {
        version = s_latestVersion;
    }

    auto& session = this->session();
    auto& schema =  session.nominalSchema();
    Transaction tran(session, Transaction::Update);

    // check for existing tables
    const unsigned pfxLen = m_prefix.size();
    for (auto& table: schema.listTables()) {
        if (table.size() > pfxLen + 1 and
                table.compare(0, pfxLen, m_prefix) == 0 and
                table[pfxLen] == '_') {
            throw Issues::SchemaTablesExistError(ERS_HERE, m_prefix);
        }
    }

    if (makeVersionTable) {
        // META table did not exist in schema v1, though one could
        // explicitly create it by calling makeMetaTable()
        m_metaTbl.createTable(version, tran);
    }

    m_partitionTbl.createTable(version, tran);
    m_runtypeTbl.createTable(version, tran);
    m_datasetTbl.createTable(version, tran);
    m_archiveTbl.createTable(version, tran);
    m_providerTbl.createTable(version, tran);
    m_hserverTbl.createTable(version, tran);
    m_hgrpnameTbl.createTable(version, tran);
    m_hnameTbl.createTable(version, tran);
    m_runlbTbl.createTable(version, tran);
    m_fileTbl.createTable(version, tran);
    m_folderTbl.createTable(version, tran);
    m_histoTbl.createTable(version, tran);
    m_hgroupTbl.createTable(version, tran);
    m_lbSeqTbl.createTable(version, tran);
    m_lbRangeTbl.createTable(version, tran);
    m_hgroup2histoTbl.createTable(version, tran);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

void
DBSchema::makeMetaTable(int version)
try {
    if (version < 0) {
        version = s_latestVersion;
    }

    auto tableName = metaTableName();

    auto& session = this->session();
    auto& schema =  session.nominalSchema();
    Transaction tran(session, Transaction::Update);

    if (schema.existsTable(tableName)) {
        throw Issues::MetaTableExistError(ERS_HERE, tableName);
    }

    m_metaTbl.createTable(version, tran);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

std::vector<std::string>
DBSchema::makeSequncesSQL(int version)
{
    if (version < 0) {
        version = s_latestVersion;
    }

    std::vector<std::string> sql;

    struct {
        const char* name;
        int cache;
    } seq_defs[] = {
        {"ARCHIVE", 10},
        {"DATASET", -1},
        {"FILE", 100},
        {"FOLDER", 100},
        {"HGROUP", 1000},
        {"HGRPNAME", -1},
        {"HISTO", 1000},
        {"HNAME", 100},
        {"HSERVER", -1},
        {"LBSEQ", 10},
        {"PARTITION", -1},
        {"PROVIDER", 10},
        {"RUNLB", 10},
        {"RUNTYPE", -1},
    };

    for (auto& def: seq_defs) {
        std::string stmt = "CREATE SEQUENCE " + m_prefix + "_SEQ_" + def.name + " START WITH 1";
        if (def.cache > 0) {
            stmt += " CACHE ";
            stmt += std::to_string(def.cache);
        }
        sql.push_back(stmt);
    }

    return sql;
}

std::string
DBSchema::metaTableName() const
{
    return m_metaTbl.tableName();
}

std::vector<std::string>
DBSchema::tables(int version)
{
    if (version < 0) {
        version = s_latestVersion;
    }

    std::vector<std::string> tables({
        m_partitionTbl.tableName(),
        m_runtypeTbl.tableName(),
        m_datasetTbl.tableName(),
        m_archiveTbl.tableName(),
        m_providerTbl.tableName(),
        m_hserverTbl.tableName(),
        m_hgrpnameTbl.tableName(),
        m_hnameTbl.tableName(),
        m_runlbTbl.tableName(),
        m_fileTbl.tableName(),
        m_folderTbl.tableName(),
        m_histoTbl.tableName(),
        m_hgroupTbl.tableName(),
        m_lbSeqTbl.tableName(),
        m_lbRangeTbl.tableName(),
        m_hgroup2histoTbl.tableName(),
    });

    if (version > 1) {
        // VERSION table appeared in v2 (though it could be manually created in v1 too)
        tables.push_back(m_metaTbl.tableName());
    }

    return tables;
}

std::vector<std::string>
DBSchema::dbTables()
try {
    std::vector<std::string> tables;

    auto& session = this->session();
    auto& schema =  session.nominalSchema();

    // start read-only transaction
    Transaction tran(session);

    const unsigned pfxLen = m_prefix.size();
    auto allTables = schema.listTables();
    for (auto& table: allTables) {
        if (table.size() > pfxLen + 1 and
                table.compare(0, pfxLen, m_prefix) == 0 and
                table[pfxLen] == '_') {
            tables.push_back(table);
        }
    }

    return tables;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

bool
DBSchema::hasMetaTable()
try {
    auto& session = this->session();
    Transaction tran(session);
    return m_metaTbl.tableExists(tran);
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

std::map<std::string, std::string>
DBSchema::getMeta()
try {
    auto& session = this->session();
    Transaction tran(session);
    return m_metaTbl.getAll(tran);
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

void
DBSchema::setMeta(std::string const& key, std::string const& value)
try {
    auto& session = this->session();
    Transaction tran(session, Transaction::Update);
    return m_metaTbl.put(key, value, tran);
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

int
DBSchema::dbSchemaVersion()
try {
    auto& session = this->session();
    Transaction tran(session);
    return m_metaTbl.dbSchemaVersion(tran);
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex);
}

std::vector<int>
DBSchema::supportedSchemaVersions()
{
    return s_supportedVersions;
}
