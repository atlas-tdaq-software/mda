//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class LBSeqTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/LBSeqTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <CoralBase/Exception.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/LBRangeTable.h"
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------


/*
 * LBSeqTable
 */

LBSeqTable::LBSeqTable (const std::string& prefix,
                        const std::string& tableName)
    : TableBase(prefix, tableName)
    , m_nextIdGen(prefix, tableName)
{
}

uint32_t
LBSeqTable::findOrInsert (const std::vector<unsigned>& lbList,
                          LBRangeTable& rangeTable,
                          Transaction& tran)
{
    
    ERS_DEBUG(3, tableName() << ": with lbList.size=" << lbList.size());

    // do not care about empty lists
    if ( lbList.empty() ) return 0 ;

    // sort the list first and remove duplicates
    std::vector<unsigned> lbs(lbList) ;
    std::sort(lbs.begin(), lbs.end());
    std::vector<unsigned>::iterator end = std::unique(lbs.begin(), lbs.end());
    lbs.erase(end, lbs.end());

    // First try to find existing matching list
    std::string cond = "LBSQ_SIZE=:LBSQ_SIZE AND LBSQ_MIN_LB=:LBSQ_MIN_LB"
                       " AND LBSQ_MAX_LB=:LBSQ_MAX_LB";

    coral::AttributeList data;
    coral_helpers::addAttr(data, "LBSQ_SIZE", uint32_t(lbs.size()));
    coral_helpers::addAttr(data, "LBSQ_MIN_LB", lbs.front());
    coral_helpers::addAttr(data, "LBSQ_MAX_LB", lbs.back());

    coral::ISchema& schema = tran.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(tableName());
    std::unique_ptr<coral::IQuery> q( table.newQuery() ) ;
    coral_helpers::defineOutput<uint32_t>(*q, "LBSQ_ID");
    q->setCondition ( cond, data ) ;
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {

        // check this ID
        uint32_t seqId = cursor.currentRow()[0].data<uint32_t>() ;
        if ( rangeTable.checkList(seqId, lbs, tran) ) {
            ERS_DEBUG(3, tableName() << ": found id " << seqId);
            return seqId ;
        }

    }

    // none yet, insert a new one
    uint32_t id = m_nextIdGen.next(tran);
    coral_helpers::addAttr(data, "LBSQ_ID", id);
    ERS_DEBUG(3, tableName() << ": inserting id " << id);
    table.dataEditor().insertRow(data);

    rangeTable.addList(id, lbs, tran);

    return id ;

}

void
LBSeqTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("LBSeqTable::createTable");
    td.setName(tableName());

    auto columns = {"LBSQ_ID", "LBSQ_SIZE", "LBSQ_MIN_LB", "LBSQ_MAX_LB"};
    for (auto column: columns) {
        td.insertColumn(column, "int");
        td.setNotNullConstraint(column, true);
    }

    td.setPrimaryKey("LBSQ_ID");

    bool Unique = true;
    td.createIndex(TBL("LBSQ_IDX"), {"LBSQ_SIZE", "LBSQ_MIN_LB", "LBSQ_MAX_LB"}, not Unique);

    tran.session().nominalSchema().createTable(td);
}
