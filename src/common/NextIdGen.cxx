//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class NextIdGen...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/NextIdGen.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/Exception.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/coral-helpers.h"
#include "mda/common/Issues.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------



NextIdGen::NextIdGen(const std::string& prefix, const std::string& name)
    : m_seqName(prefix+"_SEQ_"+name)
    , m_queryStr("CAST("+m_seqName+".NEXTVAL AS NUMBER(10))")
{
}

uint32_t
NextIdGen::next(Transaction& tr)
{

    // TO DO: this is Oracle-specific, future CORAL versions will include
    // technology-independent implementation of sequences.
    std::unique_ptr<coral::IQuery> q( tr.session().nominalSchema().newQuery() ) ;
    q->addToTableList("DUAL");
    coral_helpers::defineOutput<uint32_t>(*q, m_queryStr, "NEXTID");

    coral::ICursor& cursor = q->execute() ;
    if ( cursor.next() ) {

        // there is a record, return
        const coral::AttributeList& row = cursor.currentRow();
        uint32_t id = row[0].data<uint32_t>() ;
        ERS_DEBUG(3, m_seqName << ": next id " << id);
        return id;

    } else {

        // should not happen
        return 0 ;

    }

}
