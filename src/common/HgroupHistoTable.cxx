//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class HgroupHistoTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/HgroupHistoTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------


/*
 * HgroupHistoTable
 */

HgroupHistoTable::HgroupHistoTable (const std::string& prefix,
                                    const std::string& tableName)
    : TableBase(prefix, tableName)
{
}

void
HgroupHistoTable::insert(uint32_t hgroup_id,
                         const HistoIdLBListId& histo_ids,
                         Transaction& tr)
{

    ERS_DEBUG(3, tableName() << ": insert with hgroup_id=" << hgroup_id);
    
    coral::AttributeList data;
    coral_helpers::addAttr(data, "HGRP_ID", hgroup_id);
    coral_helpers::addAttr(data, "HIST_ID", uint32_t(0));
    coral_helpers::addAttr(data, "LBSQ_ID", uint32_t(0));

    // use bulk insertion
    coral::ISchema& schema = tr.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(tableName());
    std::unique_ptr<coral::IBulkOperation> bulkInserter(table.dataEditor().bulkInsert(data, 1000));

    for (HistoIdLBListId::const_iterator it = histo_ids.begin() ; it != histo_ids.end() ; ++ it ) {
        coral_helpers::setAttr(data, "HIST_ID", it->first);
        if ( it->second ) {
            data["LBSQ_ID"].setNull(false);
            coral_helpers::setAttr(data, "LBSQ_ID", it->second);
        } else {
            data["LBSQ_ID"].setNull(true);
        }

        bulkInserter->processNextIteration();
    }

    bulkInserter->flush();

}

void
HgroupHistoTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("HgroupHistoTable::createTable");
    td.setName(tableName());

    td.insertColumn("HGRP_ID", "int");
    td.setNotNullConstraint("HGRP_ID", true);

    td.insertColumn("HIST_ID", "int");
    td.setNotNullConstraint("HIST_ID", true);

    td.insertColumn("LBSQ_ID", "int");
    td.setNotNullConstraint("LBSQ_ID", false);

    td.setPrimaryKey(std::vector<std::string>({"HGRP_ID", "HIST_ID"}));

    bool Unique = true;
    td.createIndex(TBL("HIST_ID_IDX"), "HIST_ID", not Unique);

    td.createForeignKey(TBL("HGRP_ID_FK"), "HGRP_ID", PFX("HGROUP"), "HGRP_ID");
    td.createForeignKey(TBL("HIST_ID_FK"), "HIST_ID", PFX("HISTO"), "HIST_ID");
    td.createForeignKey(TBL("LBSQ_ID_FK"), "LBSQ_ID", PFX("LBSEQ"), "LBSQ_ID");

    tran.session().nominalSchema().createTable(td);
}
