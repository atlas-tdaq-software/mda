//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class HistoTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/HistoTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

HistoTable::HistoTable (const std::string& prefix,
                        const std::string& tableName,
                        CacheMode::Mode cacheMode,
                        int cacheSize)
    : UniqueIdTable(prefix, tableName, "HIST_ID")
    , m_cache()
    , m_cacheMode(cacheMode)
    , m_cacheSize(cacheSize)
{
}

uint32_t
HistoTable::findOrInsert (uint32_t hnam_id,
                          uint32_t folder_id,
                          Transaction& tr,
                          bool* newId)
{

    ERS_DEBUG(3, tableName() << ": with hnam=" << hnam_id << " folder=" << folder_id);

    // populate cache if necessary first time around
    if (m_cache.empty()) initCache(tr);

    if ( newId ) *newId = false ;

    uint64_t cacheKey = ((uint64_t)hnam_id)<<32 | folder_id;

    // check the cache first
    if (m_cacheMode != CacheMode::NoCache) {
        CacheMap::iterator it = m_cache.find(cacheKey);
        if ( it != m_cache.end() ) {
            ERS_DEBUG(3, tableName() << ": cached (" <<  hnam_id << "," << folder_id << ") -> " << it->second);
            return it->second;
        }
    }

    coral::AttributeList data;
    coral_helpers::addAttr(data, "HNAM_ID", hnam_id);
    coral_helpers::addAttr(data, "FLDR_ID", folder_id);

    uint32_t id = UniqueIdTable::findOrInsert(data, tr, newId);

    // update the cache
    if (m_cacheMode != CacheMode::NoCache and id != 0) {
        ERS_DEBUG(3, tableName() << ": caching (" <<  hnam_id << "," << folder_id << ") -> " << id);
        while ( m_cacheSize>0 and m_cache.size() >= unsigned(m_cacheSize) ) {
          m_cache.erase(m_cache.begin());
        }
        m_cache.insert(CacheMap::value_type(cacheKey, id));
    }

    return id;

}


// returns 0 on unsuccessful find
uint32_t
HistoTable::find (uint32_t hnam_id, uint32_t folder_id, Transaction& tr)
{

  ERS_DEBUG(3, tableName() << ": with hnam=" << hnam_id << " folder=" << folder_id);
  
  // populate cache if necessary first time around
  if (m_cache.empty()) initCache(tr);

  uint64_t cacheKey = ((uint64_t)hnam_id)<<32 | folder_id;

  // check the cache first
  if (m_cacheMode != CacheMode::NoCache) {
      CacheMap::iterator it = m_cache.find(cacheKey);
      if ( it != m_cache.end() ) {
          ERS_DEBUG(3, tableName() << ": cached (" <<  hnam_id << "," << folder_id << ") -> " << it->second);
          return it->second;
      }
  }

  coral::AttributeList data;
  coral_helpers::addAttr(data, "HNAM_ID", hnam_id);
  coral_helpers::addAttr(data, "FLDR_ID", folder_id);

  uint32_t id = UniqueIdTable::find(data, tr);
  return id;

}

void
HistoTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("HistoTable::createTable");
    td.setName(tableName());

    td.insertColumn(idColName(), "int");
    td.setNotNullConstraint(idColName(), true);
    td.setPrimaryKey(idColName());

    td.insertColumn("HNAM_ID", "int");
    td.setNotNullConstraint("HNAM_ID", true);

    td.insertColumn("FLDR_ID", "int");
    td.setNotNullConstraint("FLDR_ID", true);

    bool Unique = true;
    td.createIndex(TBL("HNAM_ID_IDX"), "HNAM_ID", not Unique);
    td.createIndex(TBL("FLDR_ID_IDX"), "FLDR_ID", not Unique);

    td.createForeignKey(TBL("FLDR_ID_FK"), "FLDR_ID", PFX("FOLDER"), "FLDR_ID");
    td.createForeignKey(TBL("HNAM_ID_FK"), "HNAM_ID", PFX("HNAME"), "HNAM_ID");

    tran.session().nominalSchema().createTable(td);
}

// if cache mode is is OnStart then fill cache with data from database
void
HistoTable::initCache(Transaction& tr)
{

    // Fill the cache
    if (m_cacheMode == CacheMode::OnStart) {

        std::unique_ptr<coral::IQuery> q( tableHandle(tr).newQuery() ) ;
        coral_helpers::defineOutput<uint32_t>(*q, "HIST_ID");
        coral_helpers::defineOutput<uint32_t>(*q, "HNAM_ID");
        coral_helpers::defineOutput<uint32_t>(*q, "FLDR_ID");

        // limit the cache size, get the N latest entries
        if ( m_cacheSize>0 ) {
            q->limitReturnedRows(m_cacheSize) ;
            q->addToOrderList("HIST_ID DESC");
        }

        // workaround for CORAL bug
        q->setRowCacheSize(100000);
        q->setMemoryCacheSize(1);

        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();
            int32_t id = row[0].data<uint32_t>() ;
            int32_t hnam_id = row[1].data<uint32_t>() ;
            int32_t folder_id = row[2].data<uint32_t>() ;
            uint64_t cacheKey = ((uint64_t)hnam_id)<<32 | folder_id;
            ERS_DEBUG(3, this->tableName() << ": caching (" <<  hnam_id << "," << folder_id << ") -> " << id);
            m_cache.insert(CacheMap::value_type(cacheKey, id));
        }

    }

}
