//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/LB.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>
#include <sstream>
#include <iomanip>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace mda {

/**
 *  Format Lumi Block, will print it as a number with leading
 *  zeros or 'EoR' if LastLB.
 */
void
LB::print(std::ostream& out, uint32_t lb, int width)
{
    if (lb == LastLB) {
        out << "EoR";
    } else {
        char oldfill = out.fill('0');
        out << std::setw(width) << lb;
        out.fill(oldfill);
    }
}

/**
 *  Return the string formatted in same way as in previous method.
 */
std::string
LB::lb2string(uint32_t lb, int width)
{
    std::ostringstream str;
    LB::print(str, lb, width);
    return str.str();
}

} // namespace mda
} // namespace daq
