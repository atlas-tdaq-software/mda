//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class NameIdTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/DBData.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ConnectionService.h>
#include <RelationalAccess/IConnectionServiceConfiguration.h>
#include <CoralBase/Exception.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

const std::string DBData::FileNotRegistered = "_FILE_NOT_REGISTERED_";

/*
 * DBData
 */
DBData::DBData (const std::string& connStr, const std::string& prefix, CacheMode::Mode lgMode)
    : DBData (connStr, prefix, false, lgMode)
{
}

DBData::DBData (const std::string& connStr, const std::string& prefix, bool update, CacheMode::Mode lgMode)
    : m_connStr(connStr)
    , m_update(update)
    , m_session(0)
    , m_partitionTbl(prefix, "PARTITION", "PART_ID", "PART_NAME", CacheMode::OnStart)
    , m_runtypeTbl(prefix, "RUNTYPE", "RUNTYPE_ID", "RUNTYPE_NAME", CacheMode::OnStart)
    , m_datasetTbl(prefix, "DATASET", "DS_ID", "DS_NAME", CacheMode::OnStart)
    , m_archiveTbl(prefix, "ARCHIVE", "ARCH_ID", "ARCH_NAME", CacheMode::Cache, 10000)
    , m_providerTbl(prefix, "PROVIDER", "PROV_ID", "PROV_NAME", CacheMode::Cache, 10000)
    , m_hserverTbl(prefix, "HSERVER", "HSRV_ID", "HSRV_NAME", CacheMode::OnStart)
    , m_hgrpnameTbl(prefix, "HGRPNAME", "HGRPNAME_ID", "HGRPNAME_NAME", CacheMode::OnStart)
    , m_hnameTbl(prefix, "HNAME", "HNAM_ID", "HNAM_NAME", lgMode, lgMode==CacheMode::OnStart?-1:200000)
    , m_runlbTbl(prefix, "RUNLB")
    , m_fileTbl(prefix, "FILE")
    , m_folderTbl(prefix, "FOLDER", "FLDR_ID", "FLDR_NAME", lgMode, lgMode==CacheMode::OnStart?-1:100000)
    , m_histoTbl(prefix, "HISTO", lgMode, lgMode==CacheMode::OnStart?-1:1000000)
    , m_hgroupTbl(prefix, "HGROUP")
    , m_lbSeqTbl(prefix, "LBSEQ")
    , m_lbRangeTbl(prefix, "LBRANGE")
    , m_hgroup2histoTbl(prefix, "HGROUP_HISTO")
    , m_metaTbl(prefix, "META")
{
}

DBData::~DBData ()
    throw ()
{
    delete m_session;
}

// create session object for a given connection string
coral::ISessionProxy& 
DBData::session()
{

    if (not m_session) {
        coral::ConnectionService conn_serv;
    
        if (getenv("MDA_CORAL_DEBUG")) {
            conn_serv.setMessageVerbosityLevel(coral::Debug);
        } else {
            conn_serv.setMessageVerbosityLevel(coral::Warning);
        }
    
        // set connection parameter so that connection failures happen sooner
        // and replicas are not excluded for too long
        coral::IConnectionServiceConfiguration& config = conn_serv.configuration();
        config.setMissingConnectionExclusionTime(0);
        config.setConnectionRetrialPeriod(2);
        config.setConnectionRetrialTimeOut(2);
    
        ERS_DEBUG (0, "database connection: \"" << m_connStr << '"');
        auto accessMode = m_update ? coral::Update : coral::ReadOnly;
        m_session = conn_serv.connect (m_connStr, accessMode);
    }        
    
    return *m_session; 

}

const char* 
DBData::defPrefix() 
{ 
    return "MDA3"; 
}

// Returns default CORAL connection string for MDA database.
std::string 
DBData::defConnStr()
{
  static const std::string connStr("MDA");
  return connStr;
}
