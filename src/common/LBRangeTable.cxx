//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class LBRangeTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/LBRangeTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <CoralBase/Exception.h>
#include <RelationalAccess/IBulkOperation.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

LBRangeTable::LBRangeTable (const std::string& prefix,
                            const std::string& tableName)
    : TableBase(prefix, tableName)
{
}

bool
LBRangeTable::checkList (uint32_t lbSeqId, const std::vector<unsigned>& lbList, Transaction& tr)
{

    ERS_DEBUG(3, tableName() << ": checkList with lbSeqId=" << lbSeqId);
    
    // First try to find existing matching list
    std::string cond = "LBSQ_ID=:LBSQ_ID";

    coral::AttributeList data;
    coral_helpers::addAttr(data, "LBSQ_ID", lbSeqId);

    coral::ISchema& schema = tr.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(tableName());
    std::unique_ptr<coral::IQuery> q( table.newQuery() ) ;
    
    coral_helpers::defineOutput<uint32_t>(*q, "LBRNG_MIN");
    coral_helpers::defineOutput<uint32_t>(*q, "LBRNG_MAX");
    
    q->setCondition ( cond, data ) ;
    
    q->addToOrderList("LBRNG_MIN");

    q->setRowCacheSize(100000);
    q->setMemoryCacheSize(1);

    std::vector<unsigned>::const_iterator lbitr = lbList.begin() ;
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {

        const coral::AttributeList& row = cursor.currentRow();
        uint32_t minLB = row["LBRNG_MIN"].data<uint32_t>() ;
        uint32_t maxLB = row["LBRNG_MAX"].data<uint32_t>() ;
        for ( uint32_t lb = minLB ; lb <= maxLB ; ++ lb, ++ lbitr ) {
            // check that we have not exhausted input data
            if ( lbitr == lbList.end() ) return false;
            // check that they are the same
            if ( *lbitr != lb ) return false;
        }
    }

    // input list may have more entries
    if ( lbitr != lbList.end() ) return false;

    return true ;

}

void
LBRangeTable::addList (uint32_t lbSeqId, const std::vector<unsigned>& lbList, Transaction& tr)
{
    
    ERS_DEBUG(3, tableName() << ": addList with lbSeqId=" << lbSeqId);
    
    coral::AttributeList data;
    coral_helpers::addAttr(data, "LBSQ_ID", lbSeqId);
    coral_helpers::addAttr(data, "LBRNG_MIN", uint32_t(0));
    coral_helpers::addAttr(data, "LBRNG_MAX", uint32_t(0));

    coral::ISchema& schema = tr.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(tableName());
    std::unique_ptr<coral::IBulkOperation> bulkInserter(table.dataEditor().bulkInsert(data, 1000));

    std::vector<unsigned>::const_iterator lbitr = lbList.begin() ;
    while ( lbitr != lbList.end() ) {

        uint32_t lbmin = *lbitr++ ;
        uint32_t lbmax = lbmin ;
        while ( lbitr != lbList.end() and *lbitr == lbmax+1 ) {
            ++lbmax ;
            ++lbitr ;
        }

        coral_helpers::setAttr(data, "LBRNG_MIN", lbmin);
        coral_helpers::setAttr(data, "LBRNG_MAX", lbmax);

        bulkInserter->processNextIteration();

    }

    bulkInserter->flush();

}


void
LBRangeTable::getList (uint32_t lbSeqId, std::vector<unsigned>& lbList, Transaction& tr)
{

    ERS_DEBUG(3, tableName() << ": getList with lbSeqId=" << lbSeqId);

    std::string cond = "LBSQ_ID=:LBSQ_ID";

    coral::AttributeList data;
    coral_helpers::addAttr(data, "LBSQ_ID", lbSeqId);

    coral::ISchema& schema = tr.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(tableName());
    std::unique_ptr<coral::IQuery> q( table.newQuery() ) ;

    coral_helpers::defineOutput<uint32_t>(*q, "LBRNG_MIN");
    coral_helpers::defineOutput<uint32_t>(*q, "LBRNG_MAX");
    
    q->setCondition ( cond, data ) ;
    
    q->addToOrderList("LBRNG_MIN");

    q->setRowCacheSize(100000);
    q->setMemoryCacheSize(1);

    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {

        const coral::AttributeList& row = cursor.currentRow();
        uint32_t minLB = row["LBRNG_MIN"].data<uint32_t>() ;
        uint32_t maxLB = row["LBRNG_MAX"].data<uint32_t>() ;
        for ( uint32_t lb = minLB ; lb <= maxLB ; ++ lb ) {
            lbList.push_back(lb);
        }
    }

}

void
LBRangeTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("LBRangeTable::createTable");
    td.setName(tableName());

    auto columns = {"LBSQ_ID", "LBRNG_MIN", "LBRNG_MAX"};
    for (auto column: columns) {
        td.insertColumn(column, "int");
        td.setNotNullConstraint(column, true);
    }

    td.setPrimaryKey({"LBSQ_ID", "LBRNG_MIN", "LBRNG_MAX"});

    td.createForeignKey(TBL("LBSQ_ID_FK"), "LBSQ_ID", PFX("LBSEQ"), "LBSQ_ID");

    tran.session().nominalSchema().createTable(td);
}
