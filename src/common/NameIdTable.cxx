//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class NameIdTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/NameIdTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

NameIdTable::NameIdTable (const std::string& prefix,
                          const std::string& tableName,
                          const std::string& idColName,
                          const std::string& nameColName,
                          CacheMode::Mode cacheMode,
                          int cacheSize)
    : UniqueIdTable (prefix, tableName, idColName)
    , m_nameCol (nameColName)
    , m_cache()
    , m_cacheMode(cacheMode)
    , m_cacheSize(cacheSize)
{
}

uint32_t
NameIdTable::findOrInsert (const std::string& name, Transaction& tran, bool* newId)
{

    ERS_DEBUG(3, tableName() << ": findOrInsert with name " << name);

    // populate cache if necessary first time around
    if (m_cache.empty()) initCache(tran);

    if ( newId ) *newId = false ;

    // check the cache first
    if (m_cacheMode != CacheMode::NoCache) {
        CacheMap::iterator it = m_cache.find(name);
        if ( it != m_cache.end() ) {
            ERS_DEBUG(3, tableName() << ": cached " << name << " -> " << it->second);
            return it->second;
        }
    }

    // build attribute list
    coral::AttributeList data;
    coral_helpers::addAttr(data, m_nameCol, name);

    uint32_t id = UniqueIdTable::findOrInsert(data, tran, newId);

    // update the cache
    if (m_cacheMode != CacheMode::NoCache and id != 0) {
        ERS_DEBUG(3, tableName() << ": caching " << name << " -> " << id);
        while ( m_cacheSize>0 and m_cache.size() >= unsigned(m_cacheSize) ) {
          m_cache.erase(m_cache.begin());
        }
        m_cache.insert(CacheMap::value_type(name, id));
    }

    return id;

}


uint32_t
NameIdTable::find(const std::string& name, Transaction& tran)
{

    ERS_DEBUG(3, tableName() << ": find with name " << name);

    // populate cache if necessary first time around
    if (m_cache.empty()) initCache(tran);

    // check the cache first
    if (m_cacheMode != CacheMode::NoCache) {
        CacheMap::iterator it = m_cache.find(name);
        if ( it != m_cache.end() ) {
            ERS_DEBUG(3, tableName() << ": cached " << name << " -> " << it->second);
            return it->second;
        }
    }

    // build attribute list
    coral::AttributeList data;
    coral_helpers::addAttr(data, m_nameCol, name);

    uint32_t id = UniqueIdTable::find(data, tran);

    // update the cache
    if (m_cacheMode != CacheMode::NoCache and id != 0) {
        ERS_DEBUG(3, tableName() << ": caching " << name << " -> " << id);
        while ( m_cacheSize>0 and m_cache.size() >= unsigned(m_cacheSize) ) {
          m_cache.erase(m_cache.begin());
        }
        m_cache.insert(CacheMap::value_type(name, id));
    }

    return id;

}

void
NameIdTable::createTable(int schemaVersion, Transaction& tran)
{
    coral::TableDescription td("NameIdTable::createTable");
    td.setName(tableName());

    td.insertColumn(idColName(), "int");
    td.setNotNullConstraint(idColName(), true);
    td.setPrimaryKey(idColName());

    td.insertColumn(m_nameCol, "string");
    td.setNotNullConstraint(m_nameCol, true);
    std::string indexName = TBL(m_nameCol + "_IDX");
    bool Unique = true;
    td.createIndex(indexName, m_nameCol, Unique);

    tran.session().nominalSchema().createTable(td);
}

// if cache mode is is OnStart then fill cache with data from database
void
NameIdTable::initCache(Transaction& tr)
{

    // Fill the cache
    if (m_cacheMode == CacheMode::OnStart) {

        std::unique_ptr<coral::IQuery> q( tableHandle(tr).newQuery() ) ;

        coral_helpers::defineOutput<uint32_t>(*q, idColName());
        coral_helpers::defineOutput<std::string>(*q, m_nameCol);

        // limit the cache size, get the N latest entries
        if ( m_cacheSize>0 ) {
            q->limitReturnedRows(m_cacheSize) ;
            q->addToOrderList( idColName()+" DESC");
        }

        // adjust cache params for performance
        q->setRowCacheSize(100000);
        q->setMemoryCacheSize(1);

        coral::ICursor& cursor = q->execute() ;
        while ( cursor.next() ) {
            const coral::AttributeList& row = cursor.currentRow();
            uint32_t id = row[0].data<uint32_t>() ;
            std::string name = row[1].data<std::string>() ;
            ERS_DEBUG(3, this->tableName() << ": caching " << name << " -> " << id);
            m_cache.insert(CacheMap::value_type(name, id));
        }

    }

}
