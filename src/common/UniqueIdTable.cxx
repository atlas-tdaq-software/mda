//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class UniqueIdTable...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "mda/common/UniqueIdTable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/AttributeSpecification.h>
#include <CoralBase/Exception.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/SchemaException.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/Issues.h"
#include "mda/common/coral-helpers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

UniqueIdTable::UniqueIdTable(const std::string& prefix,
							 const std::string& tableName,
							 const std::string& idColName)
    : TableBase(prefix, tableName)
    , m_idColName(idColName)
    , m_nextIdGen(prefix, tableName)
{
}

uint32_t
UniqueIdTable::findOrInsert (const coral::AttributeList& uniqueData,
							 Transaction& tran,
						     bool* newId,
						     const std::string& acond)
{
    ERS_DEBUG(3, tableName() << ": UniqueIdTable::findOrInsert");

    if ( newId ) *newId = false ;

    // build condition if needed
    std::string cond(acond) ;
    if ( cond.empty() ) {
        for ( size_t i = 0 ; i < uniqueData.size() ; ++ i ) {
            if ( not cond.empty() ) cond += " AND ";
            const std::string& name = uniqueData[i].specification().name();
            cond += name;
            cond += "=:";
            cond += name;
        }
    }

    // build query
    std::unique_ptr<coral::IQuery> q( tableHandle(tran).newQuery() ) ;
    coral_helpers::defineOutput<uint32_t>(*q, m_idColName);
    q->setCondition(cond, uniqueData);
    coral::ICursor& cursor = q->execute() ;
    uint32_t id;
    if ( cursor.next() ) {

        // there is a record, return
        const coral::AttributeList& row = cursor.currentRow();
        id = row[0].data<uint32_t>() ;
        ERS_DEBUG(3, tableName() << ": found id " << id);

    } else {

        // none yet, insert a new one
        coral::AttributeList newdata(uniqueData);
        id = m_nextIdGen.next(tran);
        coral_helpers::addAttr(newdata, m_idColName, id);
        ERS_DEBUG(3, tableName() << ": inserting id " << id);
        try {

            tableHandle(tran).dataEditor().insertRow(newdata);
            if ( newId ) *newId = true ;

        } catch (const coral::DuplicateEntryInUniqueKeyException& ex) {

            // meanwhile somebody inserted a record
            ERS_DEBUG(3, tableName() << ": insert failed, record added");

            // repeat query
            std::unique_ptr<coral::IQuery> q( tableHandle(tran).newQuery() ) ;
            q->addToOutputList(m_idColName);
            q->defineOutputType(m_idColName, coral::AttributeSpecification::typeNameForType<uint32_t>() ) ;
            q->setCondition(cond, uniqueData);
            coral::ICursor& cursor = q->execute() ;

            if ( cursor.next() ) {
                // there is a record, return
                const coral::AttributeList& row = cursor.currentRow();
                id = row[0].data<uint32_t>() ;
                ERS_DEBUG(3, tableName() << ": found id " << id);
            } else {
                // re-raise exception
                throw;
            }

        }

    }

    return id;
}


uint32_t
UniqueIdTable::find(const coral::AttributeList& uniqueData,
					Transaction& tran,
					const std::string& acond)
{
    ERS_DEBUG(3, tableName() << ": UniqueIdTable::find");

    // build condition if needed
    std::string cond(acond) ;
    if ( cond.empty() ) {
        for ( size_t i = 0 ; i < uniqueData.size() ; ++ i ) {
            if ( not cond.empty() ) cond += " AND ";
            const std::string& name = uniqueData[i].specification().name();
            cond += name;
            cond += "=:";
            cond += name;
        }
    }

    std::unique_ptr<coral::IQuery> q( tableHandle(tran).newQuery() ) ;
    coral_helpers::defineOutput<uint32_t>(*q, m_idColName);
    q->setCondition(cond, uniqueData);
    coral::ICursor& cursor = q->execute() ;
    uint32_t id = 0;
    if ( cursor.next() ) {

        // there is a record, return
        const coral::AttributeList& row = cursor.currentRow();
        id = row[0].data<uint32_t>() ;
        ERS_DEBUG(3, tableName() << ": found id " << id);

    }

    return id;

}

coral::ITable& 
UniqueIdTable::tableHandle(Transaction& tran)
{

    coral::ISchema& schema = tran.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(tableName());
    return table;

}
