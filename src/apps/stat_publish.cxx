// $Id$
// $Name$

#include <algorithm>
#include <chrono>
#include <boost/lexical_cast.hpp>
#include <unistd.h>
#include <signal.h>

#include "cmdl/cmdargs.h"
#include "coca/client/DBStat.h"
#include "coca/common/Issues.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "mda/read_back/DBStat.h"
#include "mda/common/Issues.h"
#include "oh/OHRootProvider.h"
#include "TH1D.h"

using namespace daq;
using namespace std::chrono;

namespace {

    ERS_DECLARE_ISSUE(errors, DatabaseError,
     "Database error, will retry in " << minutes << " minutes", ((int) minutes))

    ERS_DECLARE_ISSUE(errors, UncaughtException, "Uncaught exception, terminating.", )

    // Method which publishes coca statistics for one interval type
    void cocaIntervalStat(coca::DBStat& dbStat, coca::DBStat::Interval interval, OHRootProvider& provider);

    // Method which publishes coca statistics for datasets
    void cocaDatasetStat(coca::DBStat& dbStat, bool cached, OHRootProvider& provider);

    // Method which publishes mda statistics for runs
    void mdaRunStat(mda::DBStat& dbStat, int nRuns, OHRootProvider& provider);

    // Method which publishes mda statistics for servers/providers/datasets
    void mdaNameStat(mda::DBStat& dbStat, OHRootProvider& provider, const std::string& what);


    // flag which tells that terminating signal has occurred
    int termSignalFired = 0;
    extern "C"
    void sighandler(int sig) {
        if (sig == SIGTERM or sig == SIGINT or sig == SIGQUIT) {
            termSignalFired = sig;
        }
    }

}

int
main(int argc, char **argv)
try {
    /*
     * command line
     */

    CmdLine cmdl (argv[0]);

    CmdArgUsage usage_arg ('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // database
    CmdArgStr coca_conn_str_arg ('c', "coca-conn", "<connection_string>",
                            "connection string for CoCa database"
                            " (default: ATLAS offline Oracle RAC)");
    coca_conn_str_arg = coca::DBBase::defConnStr().c_str();
    cmdl << coca_conn_str_arg;

    // database
    CmdArgStr mda_conn_str_arg ('m', "mda-conn", "<connection_string>",
                            "connection string for MDA database"
                            " (default: ATLAS offline Oracle RAC)");
    mda_conn_str_arg = mda::DBData::defConnStr().c_str();
    cmdl << mda_conn_str_arg;

    // partition
    CmdArgStr partition_arg ('p', "partition", "<partition>",
                             "partition containing OH server [initial]");
    partition_arg = "initial";
    cmdl << partition_arg;

    // server name
    CmdArgStr server_arg ('s', "server", "<server>", "OH server name [Histogramming]");
    server_arg = "Histogramming";
    cmdl << server_arg;

    // provider name
    CmdArgStr prov_arg ('n', "provider", "<name>", "name of this provider [MDAStat]");
    prov_arg = "MDAStat";
    cmdl << prov_arg;

    // update frequency
    CmdArgInt freq_arg ('u', "update", "<minutes>", "update frequency, minutes [30]");
    freq_arg = 30;
    cmdl << freq_arg;

    // run once only
    CmdArgBool once_arg ('1', "once", "finish after single update");
    cmdl << once_arg;

    CmdArgvIter arg_iter (argc - 1, argv + 1);
    cmdl.description ("Monitoring Data Archiving statistics publishing app");
    cmdl.parse (arg_iter);

    std::string cocaConnStr = (const char *) coca_conn_str_arg;
    std::string mdaConnStr = (const char *) mda_conn_str_arg;
    std::string partition = (const char *) partition_arg;
    std::string server = (const char *) server_arg;
    std::string name = (const char *) prov_arg;
    int freqMinutes = freq_arg;
    bool once = once_arg;

    ERS_DEBUG (0, "partition=" << partition << " server=" << server);

    /*
     * IPC initialization
     */

    IPCCore::init (argc, argv);

    // instantiate provider
    OHRootProvider* provider = 0;
    try {
        provider = new OHRootProvider(partition, server, name);
    } catch( daq::oh::Exception & ex ) {
        ers::fatal( ex );
        return 1;
    }

    // register signal handler for usual set of terminating signals
    signal(SIGTERM, ::sighandler);
    signal(SIGINT, ::sighandler);
    signal(SIGQUIT, ::sighandler);
    signal(SIGUSR1, ::sighandler);
    signal(SIGUSR2, ::sighandler);

    // loop until signalled

    while (::termSignalFired == 0) {

        try {

            // instantiate collector
            coca::DBStat cocaStat(cocaConnStr);
            mda::DBStat mdaStat(mdaConnStr);

            ERS_DEBUG (0, "publishing CoCa hourly statistics");
            cocaIntervalStat(cocaStat, coca::DBStat::Hour, *provider);
            if (::termSignalFired) break;
            ERS_DEBUG (0, "publishing CoCa daily statistics");
            cocaIntervalStat(cocaStat, coca::DBStat::Day, *provider);
            if (::termSignalFired) break;
            ERS_DEBUG (0, "publishing CoCa monthly statistics");
            cocaIntervalStat(cocaStat, coca::DBStat::Month, *provider);
            if (::termSignalFired) break;
            ERS_DEBUG (0, "publishing CoCa yearly statistics");
            cocaIntervalStat(cocaStat, coca::DBStat::Year, *provider);

            ERS_DEBUG (0, "publishing CoCa dataset statistics");
            cocaDatasetStat(cocaStat, false, *provider);
            if (::termSignalFired) break;
            ERS_DEBUG (0, "publishing CoCa cached dataset statistics");
            cocaDatasetStat(cocaStat, true, *provider);
            if (::termSignalFired) break;

            ERS_DEBUG (0, "publishing MDA per-run statistics (20 runs)");
            mdaRunStat(mdaStat, 20, *provider);
            if (::termSignalFired) break;
            ERS_DEBUG (0, "publishing MDA per-run statistics (100 runs)");
            mdaRunStat(mdaStat, 100, *provider);
            if (::termSignalFired) break;

            ERS_DEBUG (0, "publishing MDA per-server statistics");
            mdaNameStat(mdaStat, *provider, "Server");
            if (::termSignalFired) break;
            ERS_DEBUG (0, "publishing MDA per-provider statistics");
            mdaNameStat(mdaStat, *provider, "Provider");
            if (::termSignalFired) break;
            ERS_DEBUG (0, "publishing MDA per-dataset statistics");
            mdaNameStat(mdaStat, *provider, "Dataset");
            if (::termSignalFired) break;

        } catch (const coca::Issues::CoralException& ex) {

            // report to log file only
            ers::log(::errors::DatabaseError(ERS_HERE, freqMinutes, ex));

        } catch (const mda::Issues::CoralException& ex) {

            // report to log file only
            ers::log(::errors::DatabaseError(ERS_HERE, freqMinutes, ex));

        }

        // stop if no looping
        if (once) break;

        // sleep until signalled or timeout expires
        if (::termSignalFired) break;
        ERS_DEBUG (0, "sleeping until next update");
        sleep(freqMinutes*60);
    }

    if (::termSignalFired) {
        ERS_DEBUG (0, "terminating signal was detected");
    }

} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}


namespace {

/**
 *  Calculate bin number from time
 */
int binNum(const system_clock::time_point& time, const system_clock::time_point& begin, coca::DBStat::Interval interval)
{
    if (interval == coca::DBStat::Hour) {
        int32_t deltaSec = duration_cast<seconds>(time - begin).count();
        return deltaSec/3600;
    } else  if (interval == coca::DBStat::Day) {
        int32_t deltaSec = duration_cast<seconds>(time - begin).count();
        return deltaSec/(24*3600);
    } else {
        time_t t0 = system_clock::to_time_t(begin);
        time_t t1 = system_clock::to_time_t(time);
        struct tm tm0, tm1;
        localtime_r(&t0, &tm0);
        localtime_r(&t1, &tm1);
        if (interval == coca::DBStat::Month) {
            return tm1.tm_mon - tm0.tm_mon + (tm1.tm_year - tm0.tm_year) * 12;
        } else {
            return tm1.tm_year - tm0.tm_year;
        }
    }
}

/**
 *  Calculate bin time from bin number
 */
time_t binTime(int bin, const system_clock::time_point& begin, coca::DBStat::Interval interval)
{
    if (interval == coca::DBStat::Hour) {
        return system_clock::to_time_t(begin) + bin*3600;
    } else  if (interval == coca::DBStat::Day) {
        return system_clock::to_time_t(begin) + bin*24*3600;
    } else {
        time_t t0 = system_clock::to_time_t(begin);
        struct tm tm0;
        localtime_r(&t0, &tm0);
        if (interval == coca::DBStat::Month) {
            tm0.tm_mon += bin%12;
            tm0.tm_year += bin/12;
            return mktime(&tm0);
        } else {
            tm0.tm_year += bin;
            return mktime(&tm0);
        }
    }
}

/**
 *  Format time expressed in seconds
 */
std::string
sec2str(time_t t, const char* fmt = "%Y-%m-%d %H:%M:%S%z")
{
    struct tm tm;
    localtime_r(&t, &tm);
    char buf[64];
    strftime(buf, sizeof buf, fmt, &tm);
    return std::string(buf);
}

/**
 *  Produce bin label
 */
std::string binLabel(int bin, const system_clock::time_point& begin, coca::DBStat::Interval interval)
{
    const char* fmt = "";
    switch (interval) {
    case coca::DBStat::Hour:
        fmt = "%b/%d %H:%M";
        break;
    case coca::DBStat::Day:
        fmt = "%b/%d";
        break;
    case coca::DBStat::Month:
        fmt = "%Y/%b";
        break;
    case coca::DBStat::Year:
        fmt = "%Y";
        break;
    }

    return sec2str(binTime(bin, begin, interval), fmt);
}

/**
 *  Produce time interval histograms
 */
void cocaIntervalStat(coca::DBStat& dbStat, coca::DBStat::Interval interval, OHRootProvider& provider)
{
    // current time
    auto now = system_clock::now();

    system_clock::duration duration;
    switch (interval) {
    case coca::DBStat::Hour:
        duration = hours(5*24);  // 5 days back
        break;
    case coca::DBStat::Day:
        duration = hours(90*24);  // 90 days back
        break;
    case coca::DBStat::Month:
        duration = hours(4*365*24);  // ~4 years back
        break;
    case coca::DBStat::Year:
        duration = now.time_since_epoch();  // whole period
        break;
    }
    auto begin = now - duration;

    ERS_DEBUG (0, "now: " << sec2str(system_clock::to_time_t(now)) << "  interval start: " << sec2str(system_clock::to_time_t(begin)));

    // get the statistics
    typedef std::map<system_clock::time_point, coca::StatFiles> StatMap;
    StatMap stat;
    dbStat.intervalStat(stat, begin, system_clock::time_point(), interval);
    if (stat.empty()) {
        ERS_DEBUG (0, "statistics is empty, nothing to publish");
        return;
    }

    // to book histogram we need to know the range of bins
    begin = stat.begin()->first;
    int nbins = binNum(now, begin, interval) + 1;

    // book histogram
    const char* nf_name = "";
    const char* sz_name = "";
    const char* xtitle = "";
    const char* nf_ytitle = "";
    const char* sz_ytitle = "";
    switch (interval) {
    case coca::DBStat::Hour:
        nf_name = "FileNumPerHour";
        nf_ytitle = "Number of files per hour";
        sz_name = "FileSizeMBPerHour";
        sz_ytitle = "Size of files, MB/hour";
        xtitle = "Time";
        break;
    case coca::DBStat::Day:
        nf_name = "FileNumPerDay";
        nf_ytitle = "Number of files per day";
        sz_name = "FileSizeMBPerDay";
        sz_ytitle = "Size of files, MB/day";
        xtitle = "Date";
        break;
    case coca::DBStat::Month:
        nf_name = "FileNumPerMonth";
        nf_ytitle = "Number of files per month";
        sz_name = "FileSizeMBPerMonth";
        sz_ytitle = "Size of files, MB/month";
        xtitle = "Month";
        break;
    case coca::DBStat::Year:
        nf_name = "FileNumPerYear";
        nf_ytitle = "Number of files per year";
        sz_name = "FileSizeMBPerYear";
        sz_ytitle = "Size of files, MB/year";
        xtitle = "Year";
        break;
    }

    TH1D nf_histo(nf_name, nf_name, nbins, -0.5, nbins-0.5);
    nf_histo.SetXTitle(xtitle);
    nf_histo.SetYTitle(nf_ytitle);

    TH1D sz_histo(sz_name, sz_name, nbins, -0.5, nbins-0.5);
    sz_histo.SetXTitle(xtitle);
    sz_histo.SetYTitle(sz_ytitle);

    // fill histogram
    double nf_entries = 0;
    double sz_entries = 0;
    for (auto it = stat.cbegin(); it != stat.cend(); ++ it ) {

        int bin = binNum(it->first, begin, interval);

        ERS_DEBUG (2, "histogram fill, bin=" << bin << " nf=" << it->second.count()
                << " sz=" << it->second.size());

        nf_histo.SetBinContent(bin+1, double(it->second.count()));
        sz_histo.SetBinContent(bin+1, double(it->second.size().megabytes()));

        nf_entries += it->second.count();
        sz_entries += it->second.size().megabytes();
    }

    // update entries
    nf_histo.SetEntries(nf_entries);
    sz_histo.SetEntries(sz_entries);

    // set bin labels
    int bin_delta = std::max(nbins/20, 1);
    for (int i = 0; i < nbins; i += bin_delta) {
        std::string label = binLabel(i, begin, interval);
        nf_histo.GetXaxis()->SetBinLabel(i+1, label.c_str());
        sz_histo.GetXaxis()->SetBinLabel(i+1, label.c_str());
    }

    // publish
    std::string dir("CoCa/");
    provider.publish(nf_histo, dir + nf_name);
    provider.publish(sz_histo, dir + sz_name);
}

/**
 *  Produce dataset histograms
 */
void cocaDatasetStat(coca::DBStat& dbStat, bool cached, OHRootProvider& provider)
{

    // get the statistics
    typedef std::map<std::string, coca::StatFiles> StatMap;
    StatMap stat;
    dbStat.datasetStat(stat, cached);
    if (stat.empty()) {
        ERS_DEBUG (0, "statistics is empty, nothing to publish");
        return;
    }

    // number of datasets
    unsigned nbins = stat.size();

    // book histos
    const char* nf_name = cached ? "CachedFileNumPerDataset" : "FileNumPerDataset";
    TH1D nf_histo(nf_name, nf_name, nbins, -0.5, nbins-0.5);
    nf_histo.SetXTitle("Dataset");
    nf_histo.SetYTitle("Number of files");

    const char* sz_name = cached ? "CachedFileSizeMBPerDataset" : "FileSizeMBPerDataset";
    TH1D sz_histo(sz_name, sz_name, nbins, -0.5, nbins-0.5);
    sz_histo.SetXTitle("Dataset");
    sz_histo.SetYTitle("Size of files, MB");

    // fill histogram
    int bin = 0;
    double nf_entries = 0;
    double sz_entries = 0;
    for (auto it = stat.cbegin(); it != stat.cend(); ++ it, ++ bin ) {

        ERS_DEBUG (2, "histogram fill, bin=" << bin << " dataset=" << it->first
                << " nf=" << it->second.count() << " sz=" << it->second.size());

        nf_histo.SetBinContent(bin+1, double(it->second.count()));
        nf_histo.GetXaxis()->SetBinLabel(bin+1, it->first.c_str());
        sz_histo.SetBinContent(bin+1, double(it->second.size().megabytes()));
        sz_histo.GetXaxis()->SetBinLabel(bin+1, it->first.c_str());

        nf_entries += it->second.count();
        sz_entries += it->second.size().megabytes();
    }

    // update entries
    nf_histo.SetEntries(nf_entries);
    sz_histo.SetEntries(sz_entries);

    // publish
    std::string dir("CoCa/");
    provider.publish(nf_histo, dir + nf_name);
    provider.publish(sz_histo, dir + sz_name);
}

void mdaRunStat(mda::DBStat& dbStat, int nRuns, OHRootProvider& provider)
{

    // get statistics
    typedef std::map<unsigned, mda::StatRun> RunStat;
    RunStat stat;
    dbStat.nHistoPerRun(stat, nRuns);
    if (stat.empty()) {
        ERS_DEBUG (0, "statistics is empty, nothing to publish");
        return;
    }

    // number of bins
    int nbins = stat.size();

    // book histos
    std::string nh_name = "HistosPerRun" + boost::lexical_cast<std::string>(nRuns);
    TH1D nh_histo(nh_name.c_str(), nh_name.c_str(), nbins, -0.5, nbins-0.5);
    nh_histo.SetXTitle("Run Number");
    nh_histo.SetYTitle("Number of histograms");

    std::string nv_name = "HistoVersionsPerRun" + boost::lexical_cast<std::string>(nRuns);
    TH1D nv_histo(nv_name.c_str(), nv_name.c_str(), nbins, -0.5, nbins-0.5);
    nv_histo.SetXTitle("Run Number");
    nv_histo.SetYTitle("Number of histogram versions");

    // fill histogram
    int bin = 0;
    int label_delta = std::max(nbins/20, 1);
    double nh_entries = 0;
    double nv_entries = 0;
    for (auto it = stat.cbegin(); it != stat.cend(); ++ it, ++ bin ) {
        uint32_t nh = it->second.nHist();
        uint32_t nv = it->second.nHistVersions();
        nh_histo.SetBinContent(bin+1, double(nh));
        nv_histo.SetBinContent(bin+1, double(nv));
        if (bin % label_delta == 0) {
            std::string label = boost::lexical_cast<std::string>(it->first);
            nh_histo.GetXaxis()->SetBinLabel(bin+1, label.c_str());
            nv_histo.GetXaxis()->SetBinLabel(bin+1, label.c_str());
        }
        nh_entries += nh;
        nv_entries += nv;
        ERS_DEBUG (2, "histogram fill, bin=" << bin << " run=" << it->first << " nh=" << nh << " nv=" << nv );
    }
    nh_histo.SetEntries(nh_entries);
    nv_histo.SetEntries(nv_entries);

    // publish
    std::string dir("MDA/");
    provider.publish(nh_histo, dir + nh_name);
    provider.publish(nv_histo, dir + nv_name);
}

void mdaNameStat(mda::DBStat& dbStat, OHRootProvider& provider, const std::string& what)
{

    // get statistics
    typedef std::map<std::string, mda::StatRun> RunStat;
    RunStat stat;
    if (what == "Server") {
        dbStat.nHistoPerServer(stat);
    } else if (what == "Provider") {
        dbStat.nHistoPerProvider(stat);
    } else if (what == "Dataset") {
        dbStat.nHistoPerDataset(stat);
    }
    if (stat.empty()) {
        ERS_DEBUG (0, "statistics is empty, nothing to publish");
        return;
    }

    // number of bins
    int nbins = stat.size();

    // book histos
    std::string nh_name = "HistosPer" + what;
    TH1D nh_histo(nh_name.c_str(), nh_name.c_str(), nbins, -0.5, nbins-0.5);
    nh_histo.SetXTitle(what.c_str());
    nh_histo.SetYTitle("Number of histograms");

    std::string nv_name = "HistoVersionsPer" + what;
    TH1D nv_histo(nv_name.c_str(), nv_name.c_str(), nbins, -0.5, nbins-0.5);
    nv_histo.SetXTitle(what.c_str());
    nv_histo.SetYTitle("Number of histogram versions");

    // fill histogram
    int bin = 0;
    double nh_entries = 0;
    double nv_entries = 0;
    for (RunStat::const_iterator it = stat.begin(); it != stat.end(); ++ it, ++ bin ) {
        uint32_t nh = it->second.nHist();
        uint32_t nv = it->second.nHistVersions();
        nh_histo.SetBinContent(bin+1, double(nh));
        nv_histo.SetBinContent(bin+1, double(nv));
        nh_histo.GetXaxis()->SetBinLabel(bin+1, it->first.c_str());
        nv_histo.GetXaxis()->SetBinLabel(bin+1, it->first.c_str());
        nh_entries += nh;
        nv_entries += nv;
        ERS_DEBUG (2, "histogram fill, bin=" << bin << " name=" << it->first << " nh=" << nh << " nv=" << nv );
    }
    nh_histo.SetEntries(nh_entries);
    nv_histo.SetEntries(nv_entries);

    // publish
    std::string dir("MDA/");
    provider.publish(nh_histo, dir + nh_name);
    provider.publish(nv_histo, dir + nv_name);
}

}
