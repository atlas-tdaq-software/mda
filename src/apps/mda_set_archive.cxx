//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITableDataEditor.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "cmdl/cmdargs.h"
#include "coca/client/DBClient.h"
#include "mda/common/coral-helpers.h"
#include "mda/common/DBData.h"
#include "mda/common/Transaction.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq;
using namespace daq::mda;

namespace {

ERS_DECLARE_ISSUE( errors, NoCocaArchive, "Archive name not found in coca", )
ERS_DECLARE_ISSUE( errors, NoMatchingFile, "Cannot find file matching given name", )
ERS_DECLARE_ISSUE( errors, MultipleFiles, "More than one file is matching given name", )
ERS_DECLARE_ISSUE( errors, UncaughtException, "Uncaught exception, terminating.", )

class SetArchive : public DBData {
public:

    SetArchive (const std::string& mdaConnStr, const std::string& prefix, const std::string& cocaConnStr)
        : DBData (mdaConnStr, prefix, true)
        , m_cocaConnStr(cocaConnStr)
    {}

    ~SetArchive () throw () {}

    /// Print the list of files with missing archives
    void printMissing();

    /// Update the name of the archive
    void setArchive(const std::string& fname, const std::string& dataset, const std::string& archive);

private:
    std::string m_cocaConnStr;
};


void
SetArchive::printMissing()
{
    // set a database session
    coral::ISessionProxy& session = DBData::session();

    Transaction tran (session);

    // new query
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    q->addToTableList(m_fileTbl.tableName(), "F");
    q->addToTableList(m_archiveTbl.tableName(), "AR");
    q->addToTableList(m_datasetTbl.tableName(), "DS");

    coral_helpers::defineOutput<std::string>(*q, "F.FILE_NAME");
    coral_helpers::defineOutput<std::string>(*q, "DS.DS_NAME");

    std::string cond = "F.DS_ID=DS.DS_ID AND F.ARCH_ID=AR.ARCH_ID AND AR.ARCH_NAME=:MISSING";
    coral::AttributeList data;
    coral_helpers::addAttr(data, "MISSING", DBData::FileNotRegistered);
    q->setCondition ( cond, data ) ;

    q->addToOrderList("F.FILE_ID");

    // file with dataset
    typedef std::pair<std::string, std::string> FileDs;
    std::list<FileDs> files;

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();
        files.push_back(FileDs(row[0].data<std::string>(), row[1].data<std::string>()));
    }


    if (not files.empty()) {

        daq::coca::DBClient coca(m_cocaConnStr);

        for (std::list<FileDs>::const_iterator it = files.begin(); it != files.end(); ++ it) {

            // get matching files from coca
            std::vector<daq::coca::RemoteFile> rfiles;
            try {
                rfiles = coca.files(it->second, "", it->first);
            } catch (daq::coca::DBClientIssues::FileNotFound& ex) {
            } catch (daq::coca::DBClientIssues::DatasetNotFound& ex) {
            }

            std::cout << it->first << "  dataset=" << it->second;
            if (not rfiles.empty()) std::cout << "  archive=" << rfiles.front().archive();
            std::cout << '\n';

        }

    }
}


/// Update the name of the archive
void
SetArchive::setArchive(const std::string& fname, const std::string& dataset, const std::string& archive)
{
    // set a database session
    coral::ISessionProxy& session = DBData::session();

    Transaction tran (session, Transaction::Update, Transaction::Abort);

    // new query to find a record in file table
    std::unique_ptr<coral::IQuery> q( tran.session().nominalSchema().newQuery() ) ;

    q->addToTableList(m_fileTbl.tableName(), "F");
    q->addToTableList(m_archiveTbl.tableName(), "AR");
    q->addToTableList(m_datasetTbl.tableName(), "DS");

    coral_helpers::defineOutput<uint32_t>(*q, "F.FILE_ID");
    coral_helpers::defineOutput<std::string>(*q, "AR.ARCH_NAME");

    std::string cond = "F.DS_ID=DS.DS_ID AND F.ARCH_ID=AR.ARCH_ID AND F.FILE_NAME=:FNAME AND DS.DS_NAME=:DATASET";
    coral::AttributeList data;
    coral_helpers::addAttr(data, "FNAME", fname);
    coral_helpers::addAttr(data, "DATASET", dataset);
    q->setCondition ( cond, data ) ;

    // file id with archive name
    typedef std::pair<uint32_t, std::string> FileAr;
    std::list<FileAr> files;

    // execute
    coral::ICursor& cursor = q->execute() ;
    while ( cursor.next() ) {
        const coral::AttributeList& row = cursor.currentRow();
        files.push_back(FileAr(row[0].data<uint32_t>(), row[1].data<std::string>()));
    }

    // need exactly one matching file
    if (files.empty()) {
        throw errors::NoMatchingFile(ERS_HERE);
    } else if (files.size() > 1) {
        throw errors::MultipleFiles(ERS_HERE);
    }

    const uint32_t fileId = files.front().first;
    const std::string oldArName = files.front().second;

    // Archive name
    std::string newArName = archive;
    if (newArName.empty()) {

        // get the name of the archive from coca
        daq::coca::DBClient coca(m_cocaConnStr);

        // get matching files from coca
        std::vector<daq::coca::RemoteFile> rfiles;
        try {
            rfiles = coca.files(dataset, "", fname);
        } catch (daq::coca::DBClientIssues::FileNotFound& ex) {
            throw errors::NoCocaArchive(ERS_HERE, ex);
        } catch (daq::coca::DBClientIssues::DatasetNotFound& ex) {
            throw errors::NoCocaArchive(ERS_HERE, ex);
        }

        if (rfiles.empty()) {
            throw errors::NoCocaArchive(ERS_HERE);
        }

        // should not be empty or _not_registered_
        newArName = rfiles.front().archive();
        if (newArName.empty() or newArName == DBData::FileNotRegistered) {
            throw errors::NoCocaArchive(ERS_HERE);
        }
    }

    std::cout << "Old archive name: " << oldArName << '\n';
    std::cout << "New archive name: " << newArName << '\n';

    // find or insert new archive name
    uint32_t arId = m_archiveTbl.findOrInsert(newArName, tran);

    // update file table
    coral::ITable& table =  tran.session().nominalSchema().tableHandle(m_fileTbl.tableName());
    coral::ITableDataEditor& dataEditor = table.dataEditor();
    coral::AttributeList updata;
    coral_helpers::addAttr(updata, "ARCH_ID", arId);
    coral_helpers::addAttr(updata, "FILE_ID", fileId);
    dataEditor.updateRows("ARCH_ID=:ARCH_ID", "FILE_ID=:FILE_ID", updata);

    // finish
    tran.commit();
}

}


int
main(int argc, char **argv)
try {
    /*
     * command line
     */

    CmdLine cmdl (argv[0]);

    CmdArgUsage usage_arg ('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // database
    CmdArgStr mda_conn_str_arg ('m', "mda-conn-string", "conn_string",
                                "connection string for MDA database (default: ATLAS offline Oracle RAC)");
    mda_conn_str_arg = DBData::defConnStr().c_str();
    cmdl << mda_conn_str_arg;

    CmdArgStr coca_conn_str_arg ('c', "coca-conn-string", "conn_string",
                                 "connection string for CoCa database (default: ATLAS offline Oracle RAC)");
    coca_conn_str_arg = coca::DBBase::defConnStr().c_str();
    cmdl << coca_conn_str_arg;

    // table name prefix
    CmdArgStr prefix_arg ('x', "table-prefix", "<string>", "prefix for table names",
                            CmdArg::isOPT | CmdArg::isVALREQ);
    prefix_arg = DBData::defPrefix();
    cmdl << prefix_arg;

    // print
    CmdArgBool print_arg ('p', "print", "print the list of files with missing archives");
    cmdl << print_arg;

    // use archive name from coca
    CmdArgBool from_coca_arg ('f', "from-coca", "use archive name obtained from CoCa for this file name");
    cmdl << from_coca_arg;

    // file name
    CmdArgStr fname_arg ("file", "relative name of MDA file", CmdArg::isPOSVALOPT);
    cmdl << fname_arg;

    // dataset name
    CmdArgStr dataset_arg ("dataset", "name of CoCa dataset", CmdArg::isPOSVALOPT);
    cmdl << dataset_arg;

    // archive name
    CmdArgStr arname_arg ("archive", "archive name", CmdArg::isPOSVALOPT);
    cmdl << arname_arg;

    /* parse the cmd-line parameters */
    CmdArgvIter arg_iter (argc - 1, argv + 1);
    cmdl.description ("MDA application to store version numbers");
    cmdl.parse (arg_iter);

    std::string mda_conn_str = (const char *) mda_conn_str_arg;
    std::string coca_conn_str = (const char *) coca_conn_str_arg;
    std::string prefix = (const char *)prefix_arg ;
    bool print = print_arg ;
    bool from_coca = from_coca_arg ;
    std::string file;
    if (not fname_arg.isNULL()) file = (const char *)fname_arg ;
    std::string dataset;
    if (not dataset_arg.isNULL()) dataset = (const char *)dataset_arg ;
    std::string archive;
    if (not arname_arg.isNULL()) archive = (const char *)arname_arg ;


    // check arguments
    if (print) {
        if (not file.empty()) {
            cmdl.error() << "Option -p cannot be used with arguments\n";
            return 2;
        }
        if (from_coca) {
            cmdl.error() << "Options -p and -f are mutually exclusive\n";
            return 2;
        }
    } else if (from_coca) {
        if (dataset.empty() or not archive.empty()) {
            cmdl.error() << "Option -f requires two positional arguments\n";
            return 2;
        }
    } else {
        if (archive.empty()) {
            cmdl.error() << "Three positional arguments are required\n";
            return 2;
        }
    }

    // instantiate database access object
    SetArchive db(mda_conn_str, prefix, coca_conn_str);

    if (print) {
        db.printMissing();
    } else {
        db.setArchive(file, dataset, archive);
    }

    return 0;

} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
