//----------------------------------------------------------------------
// File and Version Information:
//    Aplication that reads MDA-saved files and send histograms to IS
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>
#include <map>
#include <set>
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#ifdef ERS_NO_DEBUG
#undef ERS_NO_DEBUG
#endif
#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "oh/OHRootProvider.h"
#include "TFile.h"
#include "TH1.h"
#include "TKey.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace po = boost::program_options;

namespace {

ERS_DECLARE_ISSUE(Issues, UncaughtException, "Uncaught exception, terminating.", )
ERS_DECLARE_ISSUE(Issues, ProviderSearchFails,
                  "Cannot determine provider name from ROOT file: " << message,
                  ((std::string) message))
ERS_DECLARE_ISSUE(Issues, CannotOpenFile,
                  "Failed to open ROOT file " << fileName, ((std::string) fileName))

// Struct to keep few strings together
struct DirNames {
    std::string serverName;
    std::string providerName;
    std::string runDirName;
};
std::ostream& operator<<(std::ostream& out, const DirNames& dirNames) {
    return out << "server=" << dirNames.serverName << " provider=" <<
               dirNames.providerName << " run_dir=" << dirNames.runDirName;
}

// Special provider which filters out some publications from
// EoR which were created by MDA but did not exist in IS.
//
// To use this provider one has to publish all per-LB histograms
// first and then publish EoR histograms.
class MyProvider {
public:
    MyProvider(const IPCPartition & p,
               const std::string & server,
               const std::string & name) : m_provider(p, server, name) {}

    void
    publish(const TH1 & histogram,
            const std::string & name,
            int tag = -1,
            const std::vector< std::pair<std::string,std::string>>& ann = oh::util::EmptyAnnotation)
    {
        if (tag < 0) {
            // For WoR publishing, if we saw the name in per-LB histograms then skip it
            if (m_names.find(name) != m_names.end()) {
                ERS_DEBUG(0, "Skipping publication of " << name);
                return;
            }
        } else {
            // Remember all per-LB names
            m_names.insert(name);
        }
        m_provider.publish(histogram, name, tag, ann);
    }
private:
    OHRootProvider m_provider;
    std::set<std::string> m_names;
};


// Guess histogram server name, provider name, and run directory from TFile
DirNames guessDirNames(TFile* tfile);

// playback all histograms from file
int playback(TFile* tfile, MyProvider& prov, const DirNames& dirNames);

}

int
main(int argc, char **argv)
try {

    // init IPC stuff
    IPCCore::init (argc, argv);

    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
        ("help,h", "print usage and exit")
        ("partition,p", po::value<std::string>()->value_name("STRING")->required(),
                "partition with IS server, required")
        ("server,s", po::value<std::string>()->value_name("STRING")->required(),
                "IS server name, required")
        ("provider", po::value<std::string>()->value_name("STRING")->default_value(""),
                "Provider name, if not given then name from ROOT file is used")
        ;

    po::options_description pos_options("Hidden options");
    pos_options.add_options()
            ("root-file", po::value<std::string>()->required(), "ROOT file path (standard ROOT syntax).")
            ;

    po::options_description all_options("All options");
    all_options.add(cmdline_options).add(pos_options);

    po::positional_options_description positional;
    positional.add("root-file", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(all_options).positional(positional).run(), vm);

    if (vm.count("help")) {
        std::cout << "Playback MDA file contents to IS server.\n\n"
              << "Usage: " << argv[0] << " [options] root-file\n\n"
              << cmdline_options << "\n";
        return 0;
    }

    po::notify(vm);

    const std::string partName = vm["partition"].as<std::string>();
    const std::string serverName = vm["server"].as<std::string>();
    std::string provider = vm["provider"].as<std::string>();
    const std::string fileName = vm["root-file"].as<std::string>();

    // open ROOT file
    TFile* tfile = TFile::Open(fileName.c_str());
    if (tfile == nullptr) {
        ers::error(Issues::CannotOpenFile(ERS_HERE, fileName));
        return 1;
    }

    // Extract some info from ROOT file.
    DirNames dirNames;
    try {
        dirNames = ::guessDirNames(tfile);
    } catch (const ers::Issue& exc) {
        ers::error(exc);
        return 1;
    }
    if (provider.empty()) {
        provider = dirNames.providerName;
    }

    try {
        // walk the histogram tree and publish them
        IPCPartition part(partName);
        MyProvider prov(part, serverName, provider);
        int count = playback(tfile, prov, dirNames);
        ERS_LOG("Published " << count << " histograms");
    } catch (const ers::Issue& exc) {
        ers::error(exc);
        return 1;
    }

} catch (const po::error& ex) {
    std::cerr << ex.what() << "\nUse --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(Issues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(Issues::UncaughtException(ERS_HERE));
    return 1;
}

namespace {

// Guess histogram server name, provider name, and run directory from TFile
DirNames guessDirNames(TFile* tfile)
{
    // there should be few top-level directories in a file:
    //  - optional ".meta" for metadata, ignored
    //  - optional "run_XYZ" for per-LB histograms
    //  - required "ServerName" which contains "ProviderName"

    std::string serverDirName;
    std::string runDirName;
    TIter topKeyIter(tfile->GetListOfKeys());
    while (TObject *obj = topKeyIter()) {
       const std::string dirName(obj->GetName());
       if (dirName == ".meta") {
           continue;
       } else if (dirName.size() > 4 && dirName.compare(0, 4, "run_") == 0) {
           runDirName = dirName;
           continue;
       } else {
           if (serverDirName.empty()) {
               serverDirName = dirName;
           } else {
               throw Issues::ProviderSearchFails(ERS_HERE, "More than one candidate directories in a file: " +
                                                 serverDirName + " and " + dirName);
           }
       }
    }

    if (serverDirName.empty()) {
        throw Issues::ProviderSearchFails(ERS_HERE, "No top-level server directory in file");
    }

    // below the server name level there should be just one directory
    // with the name of OH provider - wee want that
    auto srvDir = tfile->GetDirectory(serverDirName.c_str());
    if (srvDir == nullptr) {
        throw Issues::ProviderSearchFails(ERS_HERE, "Failed to retrieve top-level server directory");
    }
    auto keys = srvDir->GetListOfKeys();
    if (keys->GetSize() != 1) {
        throw Issues::ProviderSearchFails(ERS_HERE, "Unexpected number of providers in ROOT file");
    }
    const std::string provName(keys->At(0)->GetName());
    DirNames dirNames{serverDirName, provName, runDirName};
    ERS_LOG("Found directories: " << dirNames);

    return dirNames;
}

// Recursively walk the ROOT folder tree, publish all TH1s
int walk(TDirectory* tdir, MyProvider& prov, int tag, const std::string& path=std::string())
{
    int count = 0;
    TIter keyIter(tdir->GetListOfKeys());
    while (TObject *keyObj = keyIter()) {
        // get object from file
        TKey* key = dynamic_cast<TKey*>(keyObj);
        TObject* tobj = tdir->Get(key->GetName());

        // make its full path name
        std::string objPath = path + std::string(path.empty() ? "" : "/") + key->GetName();

        if (TDirectory* subDir = dynamic_cast<TDirectory*>(tobj)) {
            // if directory then go down recursively
            count += walk(subDir, prov, tag, objPath);
        } else if (TH1* th1 = dynamic_cast<TH1*>(tobj)) {
            // if TH1 subclass then publish
            ERS_DEBUG(0, "Publishing TH1: " << objPath << " (tag=" << tag << ")");
            prov.publish(*th1, objPath, tag);
            ++ count;
        } else {
            // do not care about all other object types (yet)
        }

        // remove from memory
        tdir->Remove(tobj);
    }
    return count;
}

// playback all histograms from file
int playback(TFile* tfile, MyProvider& prov, const DirNames& dirNames)
{
    std::string srvProvDir = dirNames.serverName + '/' + dirNames.providerName;
    int count = 0;

    // look at all lb_NN directories
    if (! dirNames.runDirName.empty()) {
        auto treeTop = tfile->GetDirectory(dirNames.runDirName.c_str());

        // we want to publish in the LB order because IS server returns them ordered by time
        std::map<int, std::string> tag2name;
        TIter topKeyIter(treeTop->GetListOfKeys());
        while (TObject *obj = topKeyIter()) {
            const std::string dirName(obj->GetName());
            if (dirName.size() > 3 && dirName.compare(0, 3, "lb_") == 0) {
                try {
                    int tag = std::stoi(dirName.substr(3));
                    tag2name.insert(std::make_pair(tag, dirName));
                } catch (const std::invalid_argument& exc) {
                    ERS_LOG("Failed to extract LB number from directory name: " << dirName);
                }
            }
        }

        // iterate in LB order
        for (auto&& pair: tag2name) {
           int tag(pair.first);
           std::string dirName(pair.second);

           ERS_LOG("Found LBN directory " << dirName << " (tag=" << tag << ")");
           dirName += '/' + srvProvDir;
           auto lbnTreeTop = treeTop->GetDirectory(dirName.c_str());
           if (lbnTreeTop == nullptr) {
               ERS_LOG("Failed to find directory " << dirNames.runDirName << "/" << dirName);
               continue;
           }

           // publish per-LB histograms
           int tag_count = walk(lbnTreeTop, prov, tag);
           count += tag_count;
           ERS_LOG("Published " << tag_count << " histograms (tag=" << tag << ")");
        }
    }

    // publish EoR histograms
    auto treeTop = tfile->GetDirectory(srvProvDir.c_str());

    int tag = -1;
    ERS_LOG("Publishing EoR histograms (tag=" << tag << ")");
    int tag_count = walk(treeTop, prov, -1);
    count += tag_count;
    ERS_LOG("Published " << tag_count << " histograms (tag=" << tag << ")");

    return count;
}

}
