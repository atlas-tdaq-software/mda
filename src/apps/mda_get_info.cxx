// $Id$
// $Name$

#include <boost/lexical_cast.hpp>

#include "mda/mda.h"
#include "mda/common/LB.h"
#include "ers/ers.h"
#include "cmdl/cmdargs.h"

using namespace daq::mda;

namespace {

    ERS_DECLARE_ISSUE(errors, UncaughtException, "Uncaught exception, terminating.", )

    ERS_DECLARE_ISSUE(errors, MissingOptions,
     "when looking up for a histogram, the following options must be specified:"
     " -p, -s, -u, -S and -P", )
}

int
main (int argc, char **argv)
try {
    /*
     * command line
     */

    CmdLine cmdl (argv[0]);

    CmdArgUsage usage_arg ('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // database
    CmdArgStr conn_str_arg ('c', "connection-string", "<connection_string>",
                            "connection string for database"
                            " (default: ATLAS offline Oracle RAC)",
                            CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << conn_str_arg;

    // table name prefix
    CmdArgStr prefix_arg ('x', "table-prefix", "<string>", "prefix for table names",
                            CmdArg::isOPT | CmdArg::isVALREQ);
    prefix_arg = DBData::defPrefix();
    cmdl << prefix_arg;

    // partition
    CmdArgStr partition_arg ('p', "partition", "<partition>", "partition name",
                             CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << partition_arg;

    // run
    CmdArgStr run_arg ('r', "run", "<run>", "run number",
                       CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << run_arg;

    // n_runs
    CmdArgStr n_runs_arg ('n', "n_runs", "<n_runs>",
                          "number of runs to show (default: 30)",
                          CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << n_runs_arg;

    // OHS server
    CmdArgStr ohs_server_arg ('S', "ohs-server", "<OHS_server>",
                              "OHS server name",
                              CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << ohs_server_arg;

    // OHS provider
    CmdArgStr ohs_provider_arg ('P', "ohs-provider", "<OHS_provider>",
                                "OHS provider name",
                                CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << ohs_provider_arg;

    // folder
    CmdArgStr folder_arg ('f', "folder", "<folder>",
                          "folder name (default: \"/\")",
                          CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << folder_arg;

    // recursive
    CmdArgBool recursive_arg ('R', "recursive",
                              "recursive histogram listing");
    cmdl << recursive_arg;

    // lookup
    CmdArgStr lookup_arg ('L', "lookup", "<histogram>", "histogram name",
                          CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << lookup_arg;

    // since
    CmdArgStr since_arg ('s', "since", "<run_since>", "first run to search",
                         CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << since_arg;

    // until
    CmdArgStr until_arg ('u', "until", "<run_until>", "last run to search",
                         CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << until_arg;

    // timing
    CmdArgBool time_arg ('t', "time", "print real time used");
    cmdl << time_arg;


    /* do parse the cmd-line parameters */
    CmdArgvIter arg_iter (argc - 1, argv + 1);
    cmdl.description ("MDA (Monitoring Data Archive)"
                      " database textual browser");
    cmdl.parse (arg_iter);

    // database
    std::string conn_str = DBData::defConnStr();
    if ((const char *) conn_str_arg) {
        conn_str = (const char *) conn_str_arg;
    }

    // table name prefix
    std::string prefix = (const char *)prefix_arg ;

    // partition
    std::string partition;
    if ((const char *) partition_arg) {
        partition = (const char *) partition_arg;
    }

    // run
    unsigned run = 0;
    bool run_is_set = (const char *) run_arg;
    if (run_is_set) {
        run = boost::lexical_cast<unsigned> ((const char *) run_arg);
    }

    // n_runs
    unsigned int n_runs = 30;
    if ((const char *) n_runs_arg) {
        n_runs = boost::lexical_cast<unsigned int>((const char *) n_runs_arg);
    }

    // OHS server
    std::string ohs_server;
    if ((const char *) ohs_server_arg) {
        ohs_server = (const char *) ohs_server_arg;
    }

    // OHS provider
    std::string ohs_provider;
    if ((const char *) ohs_provider_arg) {
        ohs_provider = (const char *) ohs_provider_arg;
    }

    // folder
    std::string folder = "/";
    if ((const char *) folder_arg) {
        folder = (const char *) folder_arg;
    }

    // recursive
    bool recursive = recursive_arg;

    // lookup
    std::string lookup;
    if ((const char *) lookup_arg) {
        lookup = (const char *) lookup_arg;
    }

    // since
    unsigned since = 0;
    bool since_is_set = (const char *) since_arg;
    if (since_is_set) {
        since = boost::lexical_cast<unsigned> ((const char *) since_arg);
    }

    // until
    unsigned until = 0;
    bool until_is_set = (const char *) until_arg;
    if (until_is_set) {
        until = boost::lexical_cast<unsigned> ((const char *) until_arg);
    }

    bool timing = time_arg ;

    /*
     * fundamental objects ("session" is needed only to create "db")
     */

    DBRead db (conn_str, prefix);


    // this is only used to measure the time it takes to execute the command
    struct timespec ts0 ;
    clock_gettime( CLOCK_REALTIME, &ts0 );

    /*
     * run the proper query
     */

    std::cout << '\n';

    if (! lookup.empty()) {

        // look for histogram
        if (partition.empty()) {
            ers::error(::errors::MissingOptions (ERS_HERE));
            return 1;
        }

        if  ( since_is_set and until_is_set) {
            std::cout << "Lookup for histogram " << lookup << ":"
                      << "\n  partition = " << partition
                      << "\n  OHS server = " << ohs_server
                      << "\n  OHS provider = " << ohs_provider
                      << "\n  since = " << since
                      << "\n  until = " << until
                      << "\n\n";
            std::set<RunLB> runlbs;
            db.available (runlbs, partition, since, until, ohs_server,
                          ohs_provider, lookup);
            for (std::set<RunLB>::const_iterator it = runlbs.begin(); 
                    it != runlbs.end(); ++ it) {
                std::cout << "run: " << it->run()
                          << " - luminosity block: "
                          << LB::lb2string (it->lb()) << '\n';
            }
        } else {
            std::cout << "Lookup for histogram " << lookup << ":"
                      << "\n  partition = " << partition
                      << "\n  run = " << run
                      << "\n  OHS server = " << ohs_server
                      << "\n  OHS provider = " << ohs_provider
                      << "\n\n";
            std::set<HistoFile> histos;
            db.histogram (histos, partition, run, ohs_server, ohs_provider, lookup);
            for (std::set<HistoFile>::const_iterator it = histos.begin();
                 it != histos.end(); ++ it)
            {
                std::cout << "Histogram: " << it->histoName()
                          << "\n  LB     : " << LB::lb2string(it->lb())
                          << "\n  Dataset: " << it->dataset()
                          << "\n  Archive: " << it->archive()
                          << "\n  File   : " << it->file()
                          << "\n  Path   : " << it->histoPath()
                          << '\n';
            }
        }

    } else if (partition.empty()) {

        // list partitions
        std::cout << "Looking for available partitions ...\n\n";
        std::set<std::string> partitions;
        db.partitions (partitions);
        for (std::set<std::string>::const_iterator it = partitions.begin();
             it != partitions.end(); ++ it) {
            std::cout << "partition available: " << *it << '\n';
        }

    } else if (! run_is_set) {

        // runs in a partition
        std::cout << "Getting last " << n_runs << " runs for:"
                  << "\n  partition = " << partition
                  << "\n\n";
        std::set<RunInfo> runs;
        db.runs (runs, partition, n_runs);
        for (std::set<RunInfo>::const_iterator it = runs.begin(); it != runs.end(); ++ it) {
            std::cout << "partition " << partition << " - run " << it->run()
                      << " - run type: '" << it->runType()
                      << "' - super master key = " << it->smk() << '\n';
        }

    } else if (ohs_server.empty()) {

        // OHS servers in a lumi block
        std::set<std::string> ohs_servers;
        std::cout << "Getting available OHS servers for:"
                  << "\n  partition = " << partition
                  << "\n  run = " << run
                  << "\n\n";
        db.ohsServers (ohs_servers, partition, run);
        for (std::set<std::string>::const_iterator it = ohs_servers.begin();
             it != ohs_servers.end(); ++ it) {
            std::cout << "OHS server: " << *it << '\n';
        }

    } else if (ohs_provider.empty()) {

        // OHS provider for an OHS server
        std::set<std::string> ohs_providers;
        std::cout << "Getting available OHS providers for:"
                  << "\n  partition = " << partition
                  << "\n  run = " << run
                  << "\n  OHS server = " << ohs_server
                  << "\n\n";
        db.ohsProviders (ohs_providers, partition, run, ohs_server);
        for (std::set<std::string>::const_iterator it = ohs_providers.begin();
             it != ohs_providers.end(); ++ it) {
            std::cout << "OHS provider: " << *it << '\n';
        }

    } else if (recursive) {

        // histograms for an OHS provider
        std::cout << "Getting available histograms for:"
                  << "\n  partition = " << partition
                  << "\n  run = " << run
                  << "\n  OHS server = " << ohs_server
                  << "\n  OHS provider = " << ohs_provider
                  << "\n  folder = " << folder
                  << "\n\n";
        std::set<std::string> histos;
        db.histoNames (histos, partition, run, ohs_server, ohs_provider, folder);
        for (std::set<std::string>::const_iterator it = histos.begin();
             it != histos.end(); ++ it) {
            std::cout << "Histogram: " << *it << '\n';
        }

    } else {

        // folder listing for an OHS provider
        std::cout << "Listing folder for:"
                  << "\n  partition = " << partition
                  << "\n  run = " << run
                  << "\n  OHS server = " << ohs_server
                  << "\n  OHS provider = " << ohs_provider
                  << "\n  folder = " << folder
                  << "\n\n";
        std::set<std::string> folders;
        std::set<std::string> histos;
        db.listdir (folders, histos, partition, run, ohs_server,
                       ohs_provider, folder);
        for (std::set<std::string>::const_iterator it = folders.begin();
             it != folders.end(); ++ it) {
            std::cout << "Folder: " << *it << '\n';
        }
        std::cout << '\n';
        for (std::set<std::string>::const_iterator it = histos.begin();
             it != histos.end(); ++ it) {
            std::cout << "Histogram: " << *it << '\n';
        }

    }
    std::cout << '\n';

    if ( timing ) {

        // print time statistics for this command
        struct timespec ts1 ;
        clock_gettime( CLOCK_REALTIME, &ts1 );

        double delta_time = (ts1.tv_sec-ts0.tv_sec) + (ts1.tv_nsec-ts0.tv_nsec) / 1e9 ;
        std::cout << "\nreal time: " << delta_time << " sec\n";
    }

    return 0;

} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
