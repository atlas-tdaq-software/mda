// $Id$
// $Name$

#include "mda/archive/MDAControllable.h"

#include <memory>

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "ipc/core.h"
#include "ers/ers.h"

using namespace daq::mda;

namespace {

    ERS_DECLARE_ISSUE( errors, UncaughtException, "Uncaught exception, terminating.", )

}

int
main(int argc, char **argv)
try {

    /*
     * command line
     */

    daq::rc::CmdLineParser cmdline(argc, argv, true);

    IPCCore::init (argc, argv);

    std::shared_ptr<daq::rc::Controllable> ctrl = std::make_shared<MDAControllable>(cmdline.applicationName());
    daq::rc::ItemCtrl myItem (cmdline, ctrl);
    myItem.init();
    myItem.run();

    return 0;

} catch (const daq::rc::CmdLineHelp& ex) {
    std::cout << ex.what() << '\n';
    return 0;
} catch (const daq::rc::CmdLineError& ex) {
    std::cerr << ex.what() << '\n';
    return 1;
} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
