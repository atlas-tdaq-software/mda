// $Id$
// $Name$


#include "mda/mda.h"
#include "mda/common/DBData.h"

#include "cmdl/cmdargs.h"
#include "ers/ers.h"
#include "mda/common/coral-helpers.h"
#include "mda/common/LB.h"

#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>

using namespace daq::mda;

namespace {

    ERS_DECLARE_ISSUE( errors, VersionsExist, "Version list already exists for this histogram", )
    ERS_DECLARE_ISSUE( errors, MultipleHistos, "More than one histogram found", )
    ERS_DECLARE_ISSUE( errors, NoHistogram, "No histogram found", )
    ERS_DECLARE_ISSUE( errors, UncaughtException, "Uncaught exception, terminating.", )

}

class StoreVersions : public DBData {
public:

    StoreVersions (const std::string& connStr, const std::string& prefix)
        : DBData (connStr, prefix, true)
    {}

    ~StoreVersions () throw () {}

    int storeVersions (const std::string& partition,
            unsigned run, unsigned lb,
            const std::string& ohs_server, const std::string& ohs_provider,
            const std::string histo, const std::vector<unsigned>& versions );

};

int
StoreVersions::storeVersions (const std::string& partition,
        unsigned run, unsigned lb,
        const std::string& ohs_server, const std::string& ohs_provider,
        const std::string histo, const std::vector<unsigned>& versions )
{
    // split histo name
    std::string::size_type p = histo.rfind('/');
    assert( p != std::string::npos );
    std::string folder (histo, 0, p);
    std::string hnam (histo, p+1);

    // set a database session
    coral::ISessionProxy& session = DBData::session();

    Transaction tr (session, Transaction::Update, Transaction::Abort);

    // new query
    std::unique_ptr<coral::IQuery> q( tr.session().nominalSchema().newQuery() ) ;

    // what
    q->addToTableList(m_hgroup2histoTbl.tableName(), "HG2HI");
    q->addToTableList(m_partitionTbl.tableName(), "PART");
    q->addToTableList(m_fileTbl.tableName(), "FT");
    q->addToTableList(m_runlbTbl.tableName(), "RUNLB");
    q->addToTableList(m_hgroupTbl.tableName(), "HGRP");
    q->addToTableList(m_hserverTbl.tableName(), "HSRV");
    q->addToTableList(m_providerTbl.tableName(), "PROV");
    q->addToTableList(m_histoTbl.tableName(), "HIST");
    q->addToTableList(m_folderTbl.tableName(), "FLDR");
    q->addToTableList(m_hnameTbl.tableName(), "HNAM");

    coral_helpers::defineOutput<uint32_t>(*q, "HG2HI.HGRP_ID", "HGRP_ID");
    coral_helpers::defineOutput<uint32_t>(*q, "HG2HI.HIST_ID", "HIST_ID");
    coral_helpers::defineOutput<uint32_t>(*q, "HG2HI.LBSEQ_ID", "LBSEQ_ID");

    // where
    std::string cond = 
        "PART.PART_NAME = :PART_NAME"
        " AND RUNLB.RUN_NUMBER = :RUN"
        " AND HSRV.HSRV_NAME = :HSRV_NAME"
        " AND PROV.PROV_NAME = :PROV_NAME"
        " AND HNAM.HNAM_NAME = :HNAM_NAME"
        " AND FLDR.FLDR_NAME = :FOLDER"
        " AND PART.PART_ID = RUNLB.PART_ID"
        " AND RUNLB.RUNLB_ID = FT.RUNLB_ID"
        " AND FT.FILE_ID = HGRP.FILE_ID"
        " AND HGRP.HSRV_ID = HSRV.HSRV_ID"
        " AND HGRP.PROV_ID = PROV.PROV_ID"
        " AND HGRP.HGRP_ID = HG2HI.HGRP_ID"
        " AND HG2HI.HIST_ID = HIST.HIST_ID"        
        " AND HIST.HNAM_ID = HNAM.HNAM_ID"
        " AND HIST.FLDR_ID = FLDR.FLDR_ID" ;
    coral::AttributeList cdata;
    coral_helpers::addAttr(cdata, "PART_NAME", partition);
    coral_helpers::addAttr(cdata, "RUN", run);
    coral_helpers::addAttr(cdata, "HSRV_NAME", ohs_server);
    coral_helpers::addAttr(cdata, "PROV_NAME", ohs_provider);
    coral_helpers::addAttr(cdata, "HNAM_NAME", hnam);
    coral_helpers::addAttr(cdata, "FOLDER", folder);
    if ( lb == LB::LastLB ) {
        cond += " AND RUNLB.LB_NUMBER IS NULL";
    } else {
        cond += " AND RUNLB.LB_NUMBER = :LB";
        coral_helpers::addAttr(cdata, "LB", lb);
    }
    q->setCondition ( cond, cdata ) ;
    
    /*
     * execute
     */

    // execute
    coral::ICursor& cursor = q->execute() ;
    int count = 0 ;
    int32_t hgrpId = 0;
    int32_t histId = 0;
    for ( ; cursor.next() ; ++ count ) {
        const coral::AttributeList& row = cursor.currentRow();

        if ( not row[2].isNull() ) {
            throw errors::VersionsExist(ERS_HERE);
        }
        if ( count > 0 ) {
            throw errors::MultipleHistos(ERS_HERE);
        }
        hgrpId = row[0].data<int32_t>();
        histId = row[1].data<int32_t>();
    }
    if ( count == 0 ) {
        throw errors::NoHistogram(ERS_HERE);
    }
    std::cout << "Found hgrpId=" << hgrpId << " histId=" << histId << std::endl ;

    // Store the list
    uint32_t lbListId = m_lbSeqTbl.findOrInsert( versions, m_lbRangeTbl, tr );

    // Update the table
    coral::AttributeList data;
    coral_helpers::addAttr(data, "HGRP_ID", hgrpId);
    coral_helpers::addAttr(data, "HIST_ID", histId);
    coral_helpers::addAttr(data, "LBSEQ_ID", lbListId);
    ERS_DEBUG(1,"HgroupHistoTable.attributeList = " << data);

    std::string setClause = "LBSEQ_ID = :LBSEQ_ID";
    std::string condition = "HGRP_ID = :HGRP_ID AND HIST_ID = :HIST_ID";
    std::cout << "setClause='" << setClause << "' condition='" << condition << "'" << std::endl ;
    
    coral::ITable& table = tr.session().nominalSchema().tableHandle(m_hgroup2histoTbl.tableName());
    long nrows = table.dataEditor().updateRows(setClause, condition, data);
    std::cout << "Updated " << nrows << " rows" << std::endl ;

    // finish, commit everything
    tr.commit();
    
    return 0;
}

int
main(int argc, char **argv)
try {
    /*
     * command line
     */

    CmdLine cmdl (argv[0]);

    CmdArgUsage usage_arg ('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // database
    CmdArgStr conn_str_arg ('c', "conn-string", "conn_string",
                            "connection string for database"
                            " (default: ATLAS offline Oracle RAC)",
                            CmdArg::isOPTVALREQ);
    conn_str_arg = DBData::defConnStr().c_str();
    cmdl << conn_str_arg;

    // table name prefix
    CmdArgStr prefix_arg ('x', "table-prefix", "<string>", "prefix for table names",
                            CmdArg::isOPT | CmdArg::isVALREQ);
    prefix_arg = DBData::defPrefix();
    cmdl << prefix_arg;

    // timing
    CmdArgBool time_arg ('t', "time", "print real time used");
    cmdl << time_arg;

    // partition
    CmdArgStr partition_arg ("partition", "partition name", CmdArg::isPOSVALREQ);
    cmdl << partition_arg;

    // run
    CmdArgInt run_arg ("run", "run number", CmdArg::isPOSVALREQ);
    cmdl << run_arg;

    // lb
    CmdArgInt lb_arg ("lumi_block", "luminosity block number (-1 indicates EoR)",
                      CmdArg::isPOSVALREQ);
    cmdl << lb_arg;

    // OHS server
    CmdArgStr ohs_server_arg ("OHS_server", "OHS server name", CmdArg::isPOSVALREQ);
    cmdl << ohs_server_arg;

    // OHS provider
    CmdArgStr ohs_provider_arg ("OHS_provider", "OHS provider name", CmdArg::isPOSVALREQ);
    cmdl << ohs_provider_arg;

    // folder
    CmdArgStr histo_arg ("histo", "histogram path name", CmdArg::isOPTVALREQ);
    cmdl << histo_arg;

    // versions
    CmdArgIntList versions_arg ("version ...", "version numbers", CmdArg::isPOSVALREQ);
    cmdl << versions_arg;


    /* do parse the cmd-line parameters */
    CmdArgvIter arg_iter (argc - 1, argv + 1);
    cmdl.description ("MDA application to store version numbers");
    cmdl.parse (arg_iter);

    std::string conn_str = (const char *) conn_str_arg;
    std::string prefix = (const char *)prefix_arg ;
    std::string partition ((const char *) partition_arg);
    unsigned run = int(run_arg);
    unsigned lb = LB::LastLB;
    if ( int(lb_arg) != -1 ) lb = int(lb_arg) ;
    std::string ohs_server ((const char *) ohs_server_arg);
    std::string ohs_provider ((const char *) ohs_provider_arg);
    std::string histo ((const char *) histo_arg);
    std::vector<unsigned> versions;
    for ( unsigned int i = 0 ; i < versions_arg.count() ; ++ i ) {
        versions.push_back(versions_arg[i]) ;
    }
    bool timing = time_arg ;


    struct timespec ts0 ;
    clock_gettime( CLOCK_REALTIME, &ts0 );

    StoreVersions db (conn_str, prefix);

    int res = db.storeVersions (partition, run, lb, ohs_server, ohs_provider, histo, versions);

    if ( timing ) {
        struct timespec ts1 ;
        clock_gettime( CLOCK_REALTIME, &ts1 );

        double delta_time = (ts1.tv_sec-ts0.tv_sec) + (ts1.tv_nsec-ts0.tv_nsec) / 1e9 ;
        std::cout << "\nreal time: " << delta_time << " sec\n";
    }

    return res;

} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
