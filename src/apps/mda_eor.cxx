//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <signal.h>
#include <boost/lexical_cast.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "RunControl/Common/OnlineServices.h"
#include "cmdl/cmdargs.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "mda/archive/MDAConfig.h"
#include "mda/archive/PredicateVar.h"
#include "mda/archive/RunTypeSMK.h"
#include "mda/common/LB.h"
#include "pmg/pmg_initSync.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::mda;

namespace {

    ERS_DECLARE_ISSUE( errors, UncaughtException, "Uncaught exception, terminating.", )

    // signal handler stuff
    int signalFired = 0;
    extern "C" void
    sighandler(int sig)
    {
        signalFired = sig;
    }

    std::string PartitionFromEnvironment()
    {
        static const char* envvar = "TDAQ_PARTITION";
        if ( const char* var = getenv(envvar) ) {
            return std::string(var);
        } else {
            daq::mda::MDAConfigIssues::EnvVarNotDefined err (ERS_HERE, envvar);
            ERS_DEBUG (0, err);
            throw err;
        }
    }
}

//              ---------------------------------
//              -- Function/Method Definitions --
//              ---------------------------------

int
main(int argc, char **argv)
try {
    /*
     * initialise IPC
     */

    try {
        IPCCore::init(argc, argv);
    } catch (const ers::Issue& ex) {
        ers::fatal(ex);
        return 2;
    }

    /*
     * command line
     */

    CmdLine cmdl(argv[0]);

    CmdArgUsage usage_arg('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // name
    CmdArgStr name_arg('n', "name", "name", "application name", CmdArg::isREQ | CmdArg::isVALREQ);
    cmdl << name_arg;

    // run type
    CmdArgStr run_type_arg('t', "run_type", "<run_type>", "run type", CmdArg::isREQ | CmdArg::isVALREQ);
    cmdl << run_type_arg;

    // smk
    CmdArgStr smk_arg('s', "smk", "<smk>", "smk number", CmdArg::isREQ | CmdArg::isVALREQ);
    cmdl << smk_arg;

    // run
    CmdArgStr run_arg('r', "run", "<run>", "run number", CmdArg::isREQ | CmdArg::isVALREQ);
    cmdl << run_arg;

    // LB
    CmdArgStr lb_arg('l', "lb", "<LB>", "LumiBlock number, optional, if not specified then LastLB is assumed",
            CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << lb_arg;

    CmdArgvIter arg_iter(argc - 1, argv + 1);
    cmdl.description("MDA: Monitoring Data Archiving (EoR)");
    cmdl.parse(arg_iter);

    // name
    const std::string name = (const char *) name_arg;

    // run type
    const std::string run_type = (const char *) run_type_arg;

    // smk
    unsigned smk = boost::lexical_cast<unsigned>((const char *) smk_arg);

    // run
    unsigned run = boost::lexical_cast<unsigned>((const char *) run_arg);

    // run
    uint32_t lb = LB::LastLB;
    if ((const char *) lb_arg) {
        lb = boost::lexical_cast < uint32_t > ((const char *) lb_arg);
    }

    ERS_INFO("going to archive histograms for run " << run << " (" << run_type << ") and LB " << lb);

    // Have to explicitly initialize OnlineServices, we do not know segment
    // name and we do not care.
    daq::rc::OnlineServices::initialize(PartitionFromEnvironment(), "NotASegment");

    /*
     * parse configuration
     */
    MDAConfig config(name);
    std::shared_ptr < Archiver > archiver = config.archiver();

    /*
     * Read histograms, LB here must be LAstLB otherwise the logic which filters
     * lumi blocks can filter out this request.
     */
    archiver->readHistos(true, run, LB::LastLB);

    /*
     * signal that histograms were read
     */
    pmg_initSync();

    // register signal handler for usual set of terminating signals
    signal(SIGTERM, ::sighandler);
    signal(SIGINT, ::sighandler);
    signal(SIGQUIT, ::sighandler);

    // make predicate that fires when signals is seen
    PredicateVar<int> async_signal(::signalFired);

    // archive collected histograms, LB argument here defines the nanme of the output file
    // and database information, so it has to be the one specified as an argument.
    RunTypeSMK run_type_smk(run_type, smk);
    archiver->archive(run_type_smk, run, lb, async_signal);

    return 0;

} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
