
//---------------
// C++ Headers --
//---------------
#include <algorithm>
#include <cstdio>
#include <cerrno>
#include <boost/filesystem.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/common/DBSchema.h"
#include "mda/common/Issues.h"

#define BOOST_TEST_MODULE MDA DBSchema Unit Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace daq::mda;
namespace tt = boost::test_tools;
namespace fs = boost::filesystem;

/**
 * Simple test suite for class mda/archive/Predicate and subclasses.
 * See http://www.boost.org/doc/libs/1_36_0/libs/test/doc/html/index.html
 */

// ==============================================================

namespace {

// RAII factory for temporary folders
class DbMgr {
public:

    DbMgr() {
        const char* tmpdir = getenv("TMPDIR");
        if (tmpdir == nullptr) {
            tmpdir = "/tmp";
        }
        std::string templ_str = tmpdir;
        templ_str += "/mda_test_dbschema_XXXXXX";
        char* ptr = mkdtemp(templ_str.data());  // data() is null-terminated since C++11
        if (ptr == nullptr) {
            throw std::runtime_error(strerror(errno));
        }
        m_folder = ptr;
    }

    ~DbMgr() {
        // delete folder with all stuff in it, ignore errors
        boost::system::error_code ec;
        fs::remove_all(fs::path(m_folder), ec);
    }

    std::string make_connstr() {
        std::string uri = "sqlite_file:";
        uri += m_folder;
        uri += "/mda_test_db";
        uri += std::to_string(++m_counter);
        uri += ".db";
        return uri;
    }

private:

  std::string m_folder;  // directory to remove
  int m_counter = 0;
};


// names of all tables defined for V1
std::vector<std::string> tables_v1 = {
    "MDA_ARCHIVE",
    "MDA_DATASET",
    "MDA_FILE",
    "MDA_FOLDER",
    "MDA_HGROUP",
    "MDA_HGROUP_HISTO",
    "MDA_HGRPNAME",
    "MDA_HISTO",
    "MDA_HNAME",
    "MDA_HSERVER",
    "MDA_LBRANGE",
    "MDA_LBSEQ",
    "MDA_PARTITION",
    "MDA_PROVIDER",
    "MDA_RUNLB",
    "MDA_RUNTYPE",
};

// names of all tables defined for V2
std::vector<std::string> tables_v2 = {
    "MDA_ARCHIVE",
    "MDA_DATASET",
    "MDA_FILE",
    "MDA_FOLDER",
    "MDA_HGROUP",
    "MDA_HGROUP_HISTO",
    "MDA_HGRPNAME",
    "MDA_HISTO",
    "MDA_HNAME",
    "MDA_HSERVER",
    "MDA_LBRANGE",
    "MDA_LBSEQ",
    "MDA_META",
    "MDA_PARTITION",
    "MDA_PROVIDER",
    "MDA_RUNLB",
    "MDA_RUNTYPE",
};

}


BOOST_AUTO_TEST_CASE(test_supportedSchemaVersions)
{
    auto versions = DBSchema::supportedSchemaVersions();

    std::vector<int> expected = {1};
    BOOST_TEST(versions == expected, tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_versionTableName)
{
    DbMgr dbmgr;
    DBSchema dbSchema(dbmgr.make_connstr(), "MDAX");
    auto name = dbSchema.metaTableName();
    BOOST_TEST(name == "MDAX_META");
}

BOOST_AUTO_TEST_CASE(test_empty)
{
    DbMgr dbmgr;
    DBSchema dbSchema(dbmgr.make_connstr(), "MDA");
    BOOST_TEST(not dbSchema.hasMetaTable());

    auto tables = dbSchema.dbTables();
    BOOST_TEST(tables.empty());

    auto dbSchemaVersion = dbSchema.dbSchemaVersion();
    BOOST_TEST(dbSchemaVersion == -1);
}

BOOST_AUTO_TEST_CASE(test_makeVersionTable)
{
    DbMgr dbmgr;

    DBSchema dbSchema(dbmgr.make_connstr(), "MDA");
    BOOST_TEST(not dbSchema.hasMetaTable());

    dbSchema.makeMetaTable(10);
    BOOST_TEST(dbSchema.hasMetaTable());
    BOOST_TEST(dbSchema.dbSchemaVersion() == 10);

    // second attempt will fail
    BOOST_CHECK_THROW(dbSchema.makeMetaTable(10), Issues::MetaTableExistError);

    // TEst with current version
    DBSchema dbSchema2(dbmgr.make_connstr(), "MDA");
    BOOST_TEST(not dbSchema2.hasMetaTable());

    dbSchema2.makeMetaTable();
    BOOST_TEST(dbSchema2.hasMetaTable());

    auto versions = DBSchema::supportedSchemaVersions();
    auto max_it = std::max_element(versions.begin(), versions.end());
    BOOST_TEST(dbSchema2.dbSchemaVersion() == *max_it);
}

BOOST_AUTO_TEST_CASE(test_makeSchema_v1)
{
    DbMgr dbmgr;

    DBSchema dbSchema(dbmgr.make_connstr(), "MDA");
    BOOST_TEST(dbSchema.dbTables().empty());

    // v1 did not have VERSION table
    dbSchema.makeSchema(1, false);
    auto tables = dbSchema.dbTables();
    std::sort(tables.begin(), tables.end());
    BOOST_TEST(tables == ::tables_v1, tt::per_element());

    // second attempt will fail
    BOOST_CHECK_THROW(dbSchema.makeSchema(1), Issues::SchemaTablesExistError);
}

BOOST_AUTO_TEST_CASE(test_makeSchema_v2)
{
    DbMgr dbmgr;

    DBSchema dbSchema(dbmgr.make_connstr(), "MDA");
    BOOST_TEST(dbSchema.dbTables().empty());

    dbSchema.makeSchema(2);
    auto tables = dbSchema.dbTables();
    std::sort(tables.begin(), tables.end());
    BOOST_TEST(tables == ::tables_v2, tt::per_element());

    // second attempt will fail
    BOOST_CHECK_THROW(dbSchema.makeSchema(2), Issues::SchemaTablesExistError);
}

BOOST_AUTO_TEST_CASE(test_makeSequncesSQL_v1)
{
    DbMgr dbmgr;
    DBSchema dbSchema(dbmgr.make_connstr(), "MDA");
    BOOST_TEST(not dbSchema.hasMetaTable());

    auto seq_sql = dbSchema.makeSequncesSQL(1);
    BOOST_TEST(seq_sql.size() == 14);
}

BOOST_AUTO_TEST_CASE(test_tables_v1)
{
    DbMgr dbmgr;
    DBSchema dbSchema(dbmgr.make_connstr(), "MDA");

    auto tables = dbSchema.tables(1);
    std::sort(tables.begin(), tables.end());
    BOOST_TEST(tables == ::tables_v1, tt::per_element());
}

BOOST_AUTO_TEST_CASE(test_tables_v2)
{
    DbMgr dbmgr;
    DBSchema dbSchema(dbmgr.make_connstr(), "MDA");

    auto tables = dbSchema.tables(2);
    std::sort(tables.begin(), tables.end());
    BOOST_TEST(tables == ::tables_v2, tt::per_element());
}
