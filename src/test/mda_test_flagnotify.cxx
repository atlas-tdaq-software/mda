
//---------------
// C++ Headers --
//---------------
#include <ctime>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/archive/ArchiveThread.h"

#define BOOST_TEST_MODULE MDAFlagNotifyUnitTest
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace daq::mda;
using daq::mda::detail::FlagNotify;

/**
 * Simple test suite for class mda/archive/Predicate and subclasses.
 * See http://www.boost.org/doc/libs/1_36_0/libs/test/doc/html/index.html
 */

// ==============================================================


BOOST_AUTO_TEST_CASE(test_trivial)
{
    FlagNotify flag1;
    BOOST_CHECK(not flag1);
    bool oldval = flag1.set(true);
    BOOST_CHECK(not oldval);
    BOOST_CHECK(flag1);

    FlagNotify flag2(true);
    BOOST_CHECK(flag2);
    oldval = flag2.set(false);
    BOOST_CHECK(oldval);
    BOOST_CHECK(not flag2);
}

BOOST_AUTO_TEST_CASE(test_immediate)
{
    FlagNotify flag;
    BOOST_CHECK(not flag);

    std::time_t t0 = std::time(nullptr);
    bool val = flag.wait_for(false, 5000);
    std::time_t t1 = std::time(nullptr);
    BOOST_CHECK(not val);
    BOOST_CHECK(std::difftime(t1, t0) < 2);

    flag.set(true);
    t0 = std::time(nullptr);
    val = flag.wait_for(true, 5000);
    t1 = std::time(nullptr);
    BOOST_CHECK(val);
    BOOST_CHECK(std::difftime(t1, t0) < 2);
}

BOOST_AUTO_TEST_CASE(test_timeout)
{
    FlagNotify flag;
    BOOST_CHECK(not flag);
    bool val = flag.wait_for(true, 1);
    BOOST_CHECK(not val);

    flag.set(true);
    val = flag.wait_for(false, 1);
    BOOST_CHECK(val);
}

BOOST_AUTO_TEST_CASE(test_wait)
{
    FlagNotify flag;
    BOOST_CHECK(not flag);

    boost::thread set_flag([&flag]() {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
        flag.set(true);
    });

    std::time_t t0 = std::time(nullptr);
    bool val = flag.wait_for(true, 5000);
    std::time_t t1 = std::time(nullptr);
    BOOST_CHECK(val);
    BOOST_CHECK(std::difftime(t1, t0) < 2);

    set_flag.join();

    flag.set(true);
    boost::thread reset_flag([&flag]() {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
        flag.set(false);
    });

    t0 = std::time(nullptr);
    val = flag.wait_for(false, 5000);
    t1 = std::time(nullptr);
    BOOST_CHECK(not val);
    BOOST_CHECK(std::difftime(t1, t0) < 2);

    reset_flag.join();
}
