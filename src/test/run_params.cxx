// $Id$
// $Name$

#include "mda/archive/RunParameters.h"

#include "cmdl/cmdargs.h"
#include "ers/ers.h"
#include "ipc/core.h"

using namespace daq::mda;

int
main(int argc, char **argv)
{
    /*
     * IPC
     */
    try {
        IPCCore::init (argc, argv);
    } catch (const ers::Issue& ex) {
        ers::fatal(ex);
        return 2;
    }

    /*
     * command line
     */

    CmdLine cmdl (argv[0]);

    CmdArgUsage usage_arg ('h', "help", "print usage and exit");
    cmdl << usage_arg;

    CmdArgStr run_type_arg ('t', "run_type", "run_type", "run type",
                            CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << run_type_arg;

    CmdArgInt run_number_arg ('r', "run_number", "run_number", "run number",
                              CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << run_number_arg;

    CmdArgBool increase_run_number_arg ('R', "increase_run_number",
                                        "increase run number");
    cmdl << increase_run_number_arg;

    CmdArgInt LB_number_arg ('l', "LB_number", "LB_number", "LB number",
                             CmdArg::isOPT | CmdArg::isVALREQ);
    cmdl << LB_number_arg;

    CmdArgBool increase_LB_number_arg ('L', "increase_LB_number",
                                       "increase LB number");
    cmdl << increase_LB_number_arg;

    CmdArgvIter arg_iter (argc - 1, argv + 1);
    cmdl.description ("CoCa: Collection and Cache service");
    cmdl.parse (arg_iter);

    std::string run_type;
    if ((const char *) run_type_arg) {
        run_type = run_type_arg;
    }

    unsigned run_number = run_number_arg;
    bool increase_run_number = increase_run_number_arg;

    unsigned LB_number = LB_number_arg;
    bool increase_LB_number = increase_LB_number_arg;

    /*
     * read and write
     */

    RunParameters rp;
    
    if (! run_type.empty()) {
        rp.setRunType (run_type);
    }

    if (run_number > 0) {
        rp.setRunNumber (run_number);
    } else if (increase_run_number)  {
        unsigned tmp = 0;
        try {
            tmp =  rp.getRunNumber();
        } catch (const RunParametersIssues::Exception &ex) {
            ERS_INFO (ex);
        }
        ++ tmp;
        rp.setRunNumber (tmp);
    }

    if (LB_number > 0) {
        rp.setLBNumber (LB_number);
    } else if (increase_LB_number)  {
        unsigned tmp(0);
        try {
            tmp =  rp.getLBNumber();
        } catch (const RunParametersIssues::Exception &ex) {
            ERS_INFO (ex);
        }
        ++ tmp;
        rp.setLBNumber (tmp);
    }

    /*
     * show
     */

    ERS_INFO ("\n run number: " << rp.getRunNumber()
             << "\n run type: " << rp.getRunType()
             << "\n LB number: " << rp.getLBNumber());

    return 0;
}
