// $Id$
// $Name$

//---------------
// C++ Headers --
//---------------
#include <chrono>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "mda/archive/PredicateFun.h"
#include "mda/archive/PredicateVar.h"

#define BOOST_TEST_MODULE MDAPredicateUnitTest
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace std::chrono;
using namespace daq::mda;

/**
 * Simple test suite for class mda/archive/Predicate and subclasses.
 * See http://www.boost.org/doc/libs/1_36_0/libs/test/doc/html/index.html
 */

namespace {

	// simple functions returning true or false
	bool true_fun() { return true; }
	bool false_fun() { return false; }

}


// ==============================================================


BOOST_AUTO_TEST_CASE( test_var )
{
    bool varFalse = false;
    bool varTrue = true;

    BOOST_CHECK_NO_THROW( PredicateVar<bool> pred(varFalse) ) ;
    BOOST_CHECK_NO_THROW( PredicateVar<bool> pred(varTrue) ) ;

    PredicateVar<bool> pred_false(varFalse);
    BOOST_CHECK(not pred_false());

    PredicateVar<bool> pred_true(varTrue);
    BOOST_CHECK(pred_true());
    BOOST_CHECK_NO_THROW(pred_true.wait());
    BOOST_CHECK(pred_true.timedWait(seconds(1)));
}

BOOST_AUTO_TEST_CASE( test_timeout )
{
    bool varFalse = false;
    PredicateVar<bool> pred_false(varFalse);
    BOOST_CHECK(not pred_false());

    // wait 0.1 seconds polling every 0.005 seconds
    auto t0 = system_clock::now();
    BOOST_CHECK(not pred_false.timedWait(milliseconds(100), milliseconds(5)));
    auto t1 = system_clock::now();
    int delta = duration_cast<milliseconds>(t1-t0).count();
    BOOST_CHECK( delta > 99 );
    BOOST_CHECK( delta < 150 );

    // wait 2 seconds polling every 0.1 seconds
    t0 = system_clock::now();
    BOOST_CHECK(not pred_false.timedWait(seconds(2), milliseconds(100)));
    t1 = system_clock::now();
    delta = duration_cast<milliseconds>(t1-t0).count();
    BOOST_CHECK( delta > 1990 );
    BOOST_CHECK( delta < 2200 );

}

BOOST_AUTO_TEST_CASE( test_function )
{
    BOOST_CHECK_NO_THROW( PredicateFun<bool> pred_fun(::true_fun) ) ;
    BOOST_CHECK_NO_THROW( PredicateFun<bool> pred_fun(::false_fun) ) ;

    PredicateFun<bool> pred_false(::false_fun);
    BOOST_CHECK(not pred_false());

    PredicateFun<bool> pred_true(::true_fun);
    BOOST_CHECK(pred_true());
    BOOST_CHECK_NO_THROW(pred_true.wait());
    BOOST_CHECK(pred_true.timedWait(seconds(1)));
}

