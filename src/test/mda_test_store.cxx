// $Id$
// $Name$

#include <sstream>
#include <iomanip>

#include "mda/mda.h"
#include "mda/common/DBData.h"
#include "mda/common/LB.h"
#include "mda/archive/DBArchive.h"
#include "mda/archive/OHSProvider.h"
#include "mda/archive/RunTypeSMK.h"

#include "cmdl/cmdargs.h"
#include "ers/ers.h"

using namespace daq::mda;

namespace {

    const char* strings[] = { "LIST", "MDA", "MON", "ATLAS", "TEST", "HISTO",
            "RANDOM", "ANY", "ALL", "TODAY", "SHIFT", "RUN", "EXPERT", "WINDOW",
            "SEQ", "Toast", "Lumi", "BlackHole" };
    const unsigned nstrings = sizeof strings / sizeof strings[0];

    std::string genPath(int ncomp)
    {
        std::string path ;
        for ( int i = 0 ; i < ncomp ; ++ i ) {
            path += '/';
            int c = rand() % nstrings ;
            path += strings[c];
        }
        return path;
    }


    void printTimeDiff(const char* msg, struct timespec ts0, struct timespec ts1) {
        double delta_time = (ts1.tv_sec-ts0.tv_sec) + (ts1.tv_nsec-ts0.tv_nsec) / 1e9 ;
        std::cout << msg << delta_time << " sec\n";
    }

}

int
main(int argc, char **argv)
{
    /*
     * command line
     */

    CmdLine cmdl (argv[0]);

    CmdArgUsage usage_arg ('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // database
    CmdArgStr conn_str_arg ('c', "conn-string", "conn_string",
                            "connection string for database (default: oracle://INTR/ATLAS_MDA)");
    conn_str_arg = "oracle://INTR/ATLAS_MDA";
    cmdl << conn_str_arg;

    // table name prefix
    CmdArgStr prefix_arg ('x', "table-prefix", "<string>", "prefix for table names",
                            CmdArg::isOPT | CmdArg::isVALREQ);
    prefix_arg = DBData::defPrefix();
    cmdl << prefix_arg;

    // seed
    CmdArgInt seed_arg ('s', "seed", "seed_number",
                            "random number seed (default is based on current time)");
    seed_arg = int(time(0));
    cmdl << seed_arg;

    // timing
    CmdArgBool time_arg ('t', "time", "print real time used");
    cmdl << time_arg;

    // partition
    CmdArgStr partition_arg ('p', "partition", "partition_name","partition name, default: MDA_TEST");
    partition_arg = "MDA_TEST";
    cmdl << partition_arg;

    // run
    CmdArgInt run_arg ('r', "run", "run_number", "run number, default = 1000");
    run_arg = 1000;
    cmdl << run_arg;

    // lb
    CmdArgInt lb_arg ('l', "lumi_block", "LB_number", "luminosity block number (-1 indicates EoR)");
    lb_arg = -1;
    cmdl << lb_arg;

    // OHS server
    CmdArgStr ohs_server_arg ('S', "ohs-server", "server_name", "OHS server name, default: MDA-TEST-Server");
    ohs_server_arg = "MDA-TEST-Server";
    cmdl << ohs_server_arg;

    // OHS provider
    CmdArgStr ohs_provider_arg ('P', "ohs-provider", "provider_name", "OHS provider name, default: MDA-TEST-Provider");
    ohs_provider_arg = "MDA-TEST-Provider";
    cmdl << ohs_provider_arg;

    // Histo group
    CmdArgStr ohs_hgroup_arg ('G', "histo-group", "histo_group", "Histo group name, default: MDA-TEST-Hgroup");
    ohs_hgroup_arg = "MDA-TEST-Hgroup";
    cmdl << ohs_hgroup_arg;

    // versions
    CmdArgIntList versions_arg ("version ...", "version_numbers", CmdArg::isPOSVALOPT|CmdArg::isLIST);
    cmdl << versions_arg;

    /* do parse the cmd-line parameters */
    CmdArgvIter arg_iter (argc - 1, argv + 1);
    cmdl.description ("MDA application to test histogram store");
    cmdl.parse (arg_iter);

    // get all parameters

    std::string conn_str = (const char *) conn_str_arg;

    std::string prefix = (const char *) prefix_arg ;

    unsigned seed = seed_arg;

    std::string partition ((const char *) partition_arg);

    unsigned run = int(run_arg);

    unsigned lb = LB::LastLB;
    if ( int(lb_arg) != -1 ) lb = int(lb_arg) ;

    std::string ohs_server ((const char *) ohs_server_arg);

    std::string ohs_provider ((const char *) ohs_provider_arg);

    std::string ohs_hgroup ((const char *) ohs_hgroup_arg);

    std::string run_type("MDATest");
    std::string dataset("MDADataset");

    std::vector<unsigned> versions;
    for ( unsigned int i = 0 ; i < versions_arg.count() ; ++ i ) {
        versions.push_back(versions_arg[i]) ;
    }

    bool timing = time_arg ;


    struct timespec ts0 ;
    clock_gettime( CLOCK_REALTIME, &ts0 );

    DBArchive dbArchive(conn_str, prefix);

    srand(seed);

    // number of histograms between 100 and 10000
    unsigned nhisto = 100 + rand() % 9900;
    std::cout << "Will generate " << nhisto << " histograms" << std::endl ;

    // generate file name
    std::ostringstream fileName ;
    fileName << 'r' << std::setw(10) << std::setfill('0') << run
             << "_l" << LB::lb2string(lb, 4) << '_' << ohs_server
             << '_' << ohs_provider << '_' << dataset << ".root" ;

    // archive name
    std::ostringstream archive;
    archive << dataset << '_' << (rand() % 1000) << ".zip" ;

    struct timespec ts1 ;
    clock_gettime( CLOCK_REALTIME, &ts1 );
    if ( timing ) printTimeDiff("Session setup took ", ts0, ts1);

#if MDA_TEST_OLD_CODE

    // make histo list
    typedef std::set<std::string> StrList;
    StrList histoNames;
    while ( histoNames.size() < nhisto ) {
        // number of components in path between 2 and 10
        int ncomp = 2 + rand() % 9;
        const std::string& path = genPath(ncomp);
        if ( histoNames.count(path) == 0 ) {
            histoNames.insert(StrList::value_type(path));
        }
    }

    HistoList histoList ;
    for ( StrList::const_iterator i = histoNames.begin(); i != histoNames.end(); ++i ) {
        Histo4DB h4db(dbArchive.GetStr2Id(), *i);
        histoList.insert(h4db);
    }

    struct timespec ts2 ;
    clock_gettime( CLOCK_REALTIME, &ts2 );
    if ( timing ) printTimeDiff("Histo generation took ", ts1, ts2);

    // store histograms
    s2i_t hl_id = dbArchive.InsertHistoList (histoList);

    struct timespec ts3 ;
    clock_gettime( CLOCK_REALTIME, &ts3 );
    if ( timing ) printTimeDiff("Histo storage took ", ts2, ts3);

    // store intervals
    DBData::HistoListIDs hlIds(hl_id, 0, 0);
    OHSProvider provider(ohs_server, ohs_provider);
    dbArchive.InsertInterval (partition, run, lb, ohs_hgroup, provider, run_type,
            hlIds, dataset, fileName.str(), archive.str());

#else

    // make histo list
    DBArchive::HistoLBList histoList ;
    while ( histoList.size() < nhisto ) {

        // number of components in path between 2 and 10
        int ncomp = 2 + rand() % 9;
        const std::string& path = genPath(ncomp);
        if ( histoList.count(path) == 0 ) {
            histoList.insert(DBArchive::HistoLBList::value_type(path, versions));
        }
    }

    struct timespec ts2 ;
    clock_gettime( CLOCK_REALTIME, &ts2 );
    if ( timing ) printTimeDiff("Histo generation took ", ts1, ts2);

    // store histograms
    DBArchive::HistoIdLBList histoIdList;
    dbArchive.insertHistoList (histoList, histoIdList);

    struct timespec ts3 ;
    clock_gettime( CLOCK_REALTIME, &ts3 );
    if ( timing ) printTimeDiff("Histo storage took ", ts2, ts3);

    // store intervals
    OHSProvider provider(ohs_server, ohs_provider);
    dbArchive.insertInterval (partition, run, lb, ohs_hgroup, provider, RunTypeSMK(run_type),
                histoIdList, dataset, fileName.str(), archive.str());

#endif

    struct timespec ts4 ;
    clock_gettime( CLOCK_REALTIME, &ts4 );
    if ( timing ) printTimeDiff("Interval storage took ", ts3, ts4);
    if ( timing ) printTimeDiff("Storage took total ", ts2, ts4);

    return 0;
}
