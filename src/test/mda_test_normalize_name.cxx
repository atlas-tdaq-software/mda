
#include "mda/archive/ArchiveHistoGroup.h"

#define BOOST_TEST_MODULE normalize_histo_name
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace daq::mda;

/**
 * Simple test suite for class mda/archive/Predicate and subclasses.
 * See http://www.boost.org/doc/libs/1_36_0/libs/test/doc/html/index.html
 */

// ==============================================================

BOOST_AUTO_TEST_CASE(test_simple)
{
    std::string name;
    name = ArchiveHistoGroup::normalize_histo_name("");
    BOOST_TEST(name.empty());

    name = ArchiveHistoGroup::normalize_histo_name("x");
    BOOST_TEST(name == "/x");

    name = ArchiveHistoGroup::normalize_histo_name("/x");
    BOOST_TEST(name == "/x");

    name = ArchiveHistoGroup::normalize_histo_name("//x");
    BOOST_TEST(name == "/x");

    name = ArchiveHistoGroup::normalize_histo_name("/xxxx//y");
    BOOST_TEST(name == "/xxxx/y");

    name = ArchiveHistoGroup::normalize_histo_name("/xy///abc////");
    BOOST_TEST(name == "/xy/abc/");

    name = ArchiveHistoGroup::normalize_histo_name("/xy/abc/def");
    BOOST_TEST(name == "/xy/abc/def");

    name = ArchiveHistoGroup::normalize_histo_name("xy/abc/def");
    BOOST_TEST(name == "/xy/abc/def");
}
