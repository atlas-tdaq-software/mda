// $Id$
// $Name$

#include "mda/read_back/FileRead.h"

#include "cmdl/cmdargs.h"
#include "ers/ers.h"

using namespace daq::mda;

int
main(int argc, char **argv)
{
    /*
     * command line
     */

    CmdLine cmdl (argv[0]);

    CmdArgUsage usage_arg ('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // file name is a required argument
    CmdArgStr file_arg ("file", "file_name", CmdArg::isPOSVALREQ);
    cmdl << file_arg;

    // parse
    CmdArgvIter arg_iter (argc - 1, argv + 1);
    cmdl.description ("test for mda::fileread");
    cmdl.parse (arg_iter);

    // instantiated reader
    FileRead reader;

    // try to open file
    const char* fname = file_arg;
    TFile* f = reader.openFile(fname, "");

    if (f and not f->IsZombie()) {
        ERS_INFO("File " << fname << " opened successfully");
        ERS_INFO("File name: " << f->GetName());
        return 0;
    } else {
        ERS_INFO("Failed to open file " << fname);
        return 1;
    }
}
